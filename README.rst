The HASPOC hypervisor for ARMv8
===============================


This is the HASPOC thin hypervisor for ARMv8, developed by the Security Lab at
SICS.

HASPOC (High Assurance Security Products On COTS platforms) is a project in
Vinnova's Challenge Driven Innovation program.
See https://haspoc.sics.se for more information.


IMPORTANT NOTES
===============

* This is a research project and the code is provided without any guarantees.
  In particular, we do not make any functionality or security guarantees outside
  what has been proven by formal analysis.
* You may notice that some parts are implemented in an unusual or inefficient way.
  The reason for this is often to ease formal verification and related tasks.
* Some services, drivers, guests and configurations found in this repository
  are not secure. These have been added for use in a specific demo or experiment
  and should not be used in production. When in doubt, contact the authors.

For development information, see doc/


LICENSE
-------

::

   Copyright 2016 SICS

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

Files in this repository are distributed under the Apache 2.0 license,
see *LICENSE.txt*.


Note that foreign files in the project retain their old license. In particular,
Linux is GPL. In addition, our kernel drivers and any changes to the Linux kernel
are also GPL licensed, as required by the GPL license.
