# -*- Makefile -*-

include $(BASE)/config
include $(BASE)/common/make/filenames.inc

SRC_LD ?= $(BASE)/common/ld/guest.ld.S

ifeq (, $(NAME))
$(error "No guest NAME was given")
endif

OUT = $(BUILD)/$(NAME)

#
all: $(BUILD) $(OUT).elf $(OUT).dis $(OUT).bin $(OUT).sym
	ls -l $(OUT).*


$(OUT).elf: $(SRC) $(SRC_H) Makefile $(OUT).ld
	$(GCC) $(CFLAGS) $(SRC) -T $(OUT).ld -o $(OUT).elf

$(OUT).dis: $(OUT).elf
	$(OBJDUMP) $(OUT).elf > $(OUT).dis

$(OUT).bin: $(OUT).elf
	$(OBJCOPY) $(OUT).elf $(OUT).bin

$(OUT).sym: $(OUT).elf
	$(NM) $(OUT).elf > $(OUT).sym

$(OUT).ld: $(BUILD) $(SRC) # update on source change since it may depend on .h files

$(OUT).ld: $(SRC_LD)
	$(LDPROC) $(CFLAGS) $< -o $@
#

show: $(OUT).dis
	less $(OUT).dis

#

$(BUILD):
	mkdir -p $(BUILD)

clean:
	rm -f $(OUT)*
	rm -f cscope.*

##

include $(BASE)/common/make/cscope.inc
