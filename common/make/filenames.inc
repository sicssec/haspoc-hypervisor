# -*- Makefile -*-

include $(BASE)/external/external.inc

BUILD = $(BASE)/build/$(GUEST)_$(TZ)_$(TARGET)

##
NAME_HV = $(BUILD)/hv
NAME_TZ = $(BUILD)/tz
NAME_PAYLOAD = $(BUILD)/payload
NAME_JOINED = $(BUILD)/joined


# some sanity checks to minimise the impact of Murphys law:
ifneq (, $(findstring ;, $(BUILD)))
$(error "Your configuration contains a ;")
endif

ifneq (, $(findstring >, $(BUILD)))
$(error "Your configuration contains a >")
endif

ifneq (, $(findstring <, $(BUILD)))
$(error "Your configuration contains a <")
endif

ifneq (, $(findstring |, $(BUILD)))
$(error "Your configuration contains a |")
endif

# findstring has space problems
ifneq (1,$(words [$(BUILD)]))
$(error "Your configuration contains a space")
endif
