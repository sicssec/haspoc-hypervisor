# -*- Makefile -*-

# sanity
ifeq ($(BASE),)
$(error BASE was not defined)
endif

# input
SRC_DIR ?= src
INC_DIR ?= src
EXE ?= unknown.exe

# add modules:
MODULES += .
SRC_DIR += $(foreach d,$(MODULES), $(BASE)/common/src/$(d) )
INC_DIR += $(foreach d,$(MODULES), $(BASE)/common/src/$(d) )
SRC_FILES = $(foreach d,$(SRC_DIR), $(wildcard $(d)/*.c ) )
INC_FILES = $(foreach d,$(SRC_DIR), $(wildcard $(d)/*.h ) )


CFLAGS += -O2
CFLAGS += $(foreach d, $(INC_DIR), -I$(d))
CFLAGS += -DTARGET_DESKTOP=1

##

$(EXE): $(SRC_FILES) $(INC_FILES) Makefile $(DEPS)
	gcc $(CFLAGS) $(SRC_FILES) -o $(EXE)

clean:
	rm -f $(EXE)
	rm -f cscope.*
