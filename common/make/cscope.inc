
# -*- Makefile -*-

cscope:
	echo $(SRC) $(SRC_H) | tr  ' ' '\n' > cscope.files
	cscope -q -R -b -i cscope.files
