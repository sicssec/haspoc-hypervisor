# -*- Makefile -*-

include $(BASE)/common/make/filenames.inc

NAME ?= unnamed
OUT = $(NAME_TZ)

SRC_LD ?= $(BASE)/common/ld/tz.ld.S
DST_LD = $(NAME_TZ).ld


all: compile
	ls $(OUT)*

compile: $(BUILD) $(OUT).elf $(OUT).dis $(OUT).bin $(OUT).sym

$(OUT).elf: $(SRC) $(SRC_H) $(DST_LD)
	$(GCC) $(CFLAGS) $(SRC) -T $(DST_LD) -o $(OUT).elf

$(OUT).dis: $(OUT).elf
	$(OBJDUMP) $(OUT).elf > $(OUT).dis

$(OUT).bin: $(OUT).elf
	$(OBJCOPY) $(OUT).elf $(OUT).bin

$(OUT).sym: $(OUT).elf
	$(NM) $(OUT).elf > $(OUT).sym

$(DST_LD): $(SRC_LD) $(TARGETDIR)/*.*
	$(LDPROC) $(CFLAGS) $< -o $@

#

show: $(OUT).dis
	less $(OUT).dis

$(BUILD):
	mkdir -p $(BUILD)

clean:
	rm -rf $(NAME_TZ)*
	rm -f cscope.*

###

include $(BASE)/common/make/cscope.inc
