# -*- Makefile -*-

# make all the first rule even if the included files have another at top
all:

# sanity
ifeq ($(BASE),)
$(error BASE was not defined)
endif

ifeq ($(TARGET),)
$(error TARGET was not defined)
endif

# debug level
DEBUG ?= 2

# repo version
VERSION_STRING = $(shell git describe --always  --dirty)

# Tools
CROSS_COMPILE ?= aarch64-linux-gnu-

# see http://askubuntu.com/questions/282666/make-gcc-coloring-the-default
GCC_COLORS ?= "error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01"

GCC = $(CROSS_COMPILE)gcc
CFLAGS += -std=c99 -mcpu=cortex-a53 -march=armv8-a
CFLAGS += -nostartfiles -nostdlib -ffreestanding -Os -fno-common -static -fno-plt -fno-pic
CFLAGS += -mgeneral-regs-only -mstrict-align
CFLAGS += -DTARGET_EMBEDDED=1
CFLAGS += -DDEBUG=$(DEBUG)
CFLAGS += -DVERSION_STRING="$(VERSION_STRING)"
OBJDUMP = $(CROSS_COMPILE)objdump -D -x -t -r
OBJCOPY = $(CROSS_COMPILE)objcopy -O binary
NM = $(CROSS_COMPILE)nm
DTC = dtc

# pre-processor:
LDPROC = $(CROSS_COMPILE)gcc -P -E -c -Ulinux -Uunix


# input
SRC_DIR ?= src
INC_DIR ?= .

# platform
TARGETDIR = $(BASE)/common/target/$(TARGET)
include $(TARGETDIR)/arch_build.mk

MODULES += $(ARCH_MODULES)
CFLAGS += $(foreach o,$(ARCH_DEFS), -D$(o))
CFLAGS += $(ARCH_FLAGS)


# add modules:
MODULES += .

SRC_DIR += $(foreach d,$(MODULES), $(BASE)/common/src/$(d) )

# more directories

INC_DIR += $(TARGETDIR)
INC_DIR += $(SRC_DIR)


# extract files
SRC_C = $(foreach d,$(SRC_DIR),$(wildcard $(d)/*.c))
SRC_S = $(foreach d,$(SRC_DIR),$(wildcard $(d)/*.S))
SRC_H = $(foreach d,$(INC_DIR),$(wildcard $(d)/*.h))

SRC = $(SRC_C) $(SRC_S)

CFLAGS += $(foreach d,$(INC_DIR), -I$(d))


# options
OPT=GUEST=$(GUEST) TZ="$(TZ)" TARGET=$(TARGET)

CFLAGS += $(foreach o,$(OPT), -D$(o))
CFLAGS += $(foreach o,$(OPT), -D$(subst =,_,$(o))=1)
