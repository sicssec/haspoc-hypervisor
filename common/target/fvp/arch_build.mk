# -*- Makefile -*-

# arch components
ARCH_MODULES += gicv2 uart/pl011

ARCH_DEFS =
ARCH_FLAGS =

# Running:
FVP = Foundation_Platform
FVP_OPTIONS += --cores=$(CORES)
FVP_OPTIONS += --no-gicv3
FVP_OPTIONS += --no-secure-memory

ifneq ($(TRACE),)
	FVP_OPTIONS += --trace build/$(TRACE).trace
endif

# XXX: if guest is mini, we will need the EE service
ifeq ($(GUEST),mini)
	SERVICES += ee
endif


ifeq ($(TZ),atf)
sim: fip
	@echo "Press Ctrl-C to interrupt the simulation"
	$(FVP) $(FVP_OPTIONS) \
		--data=$(BASE)/build/atf_bl1_$(GUEST)_$(TARGET).bin@0 \
		--data=$(BASE)/build/atf_fip_$(GUEST)_$(TARGET).bin@0x08000000
else
sim:
	@echo "Press Ctrl-C to the interrupt simulation"
	$(FVP) $(FVP_OPTIONS) --image=$(NAME_TZ).elf \
		--nsdata=$(NAME_HV).bin@0x88000000 \
		--nsdata=$(NAME_PAYLOAD).bin@0x80000000
endif
	# this will probably not be called because we will ^C the simulator
	make split

split:
	# split the trace into core files for easier inspection
ifneq ($(TRACE),)
	grep cpu0 build/$(TRACE).trace > build/$(TRACE).cpu0.trace
	grep cpu1 build/$(TRACE).trace > build/$(TRACE).cpu1.trace
	grep cpu2 build/$(TRACE).trace > build/$(TRACE).cpu2.trace
	grep cpu3 build/$(TRACE).trace > build/$(TRACE).cpu3.trace
endif

debug:
ifeq ($(TRACE),)
	@echo no TRACE set
else
	less build/$(TRACE).trace
endif
