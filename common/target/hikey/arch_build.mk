# -*- Makefile -*-


# arch components
ARCH_MODULES += gicv2 uart/pl011 smmu/hisi


# XXX: if guest is mini or irqprio, we will need the EE service
ifeq ($(GUEST),mini)
	SERVICES += ee
endif

ifeq ($(GUEST),irqprio)
	SERVICES += ee
endif


# running

sim:
	echo "Cannot simulate this platform..."

debug:
	echo "Cannot debug this platform, for now..."
