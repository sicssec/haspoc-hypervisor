#ifndef _ARCH_DEFS_H_
#define _ARCH_DEFS_H_

/*
 * Memory definitions
 */

#define INTERNAL_RAM_START 0x2e000000
#define INTERNAL_RAM_END   0x2e008000

/* DRAM */
#define EXTERNAL_RAM_START 0x080000000
#define EXTERNAL_RAM_END   0x100000000


/* TRUSTZONE */
#define TZ_ROM_START 0x04000000
#define TZ_ROM_SIZE (16 * 1024)
#define TZ_ROM_END  ((TZ_ROM_START) + (TZ_ROM_SIZE))

#define TZ_RAM_START 0x04100000
#define TZ_RAM_SIZE  (16 * 1024)
#define TZ_RAM_END   ((TZ_RAM_START) + (TZ_RAM_SIZE))


/* HYPERVISOR */
#define HYP_ROM_SIZE  (64 * 1024)
#define HYP_ROM_START 0xE0000000
#define HYP_ROM_END   ((HYP_ROM_START) + (HYP_ROM_SIZE))

#define HYP_RAM_SIZE  (64 * 1024)
#define HYP_RAM_START (HYP_ROM_END)
#define HYP_RAM_END   ((HYP_RAM_START) + (HYP_RAM_SIZE))

/* PAYLOAD: is at the start of the the RAM for now */
#define PAYLOAD_START 0x0A7C0000
#define PAYLOAD_SIZE (20 * 1024 * 1024)


/*
 * Board configuration
 */

#define ARCH_CNT_FREQ 24000000

/*
 * CPU definitions
 */


#define ARCH_MIDR_VALUE 0x410fd030
#define ARCH_MIDR_MASK  0xF

#define ARCH_CORES 6
#define ARCH_CLUSTER_COUNT 2
#define ARCH_CLUSTER0_COUNT  2
#define ARCH_CLUSTER1_COUNT  4
#define ARCH_CLUSTER2_COUNT  0



/*
 * Peripherals
 */

/* GICv2 */
#define ARCH_HAS_GICv2 1
#define GICD_BASE 0x2c010000
#define GICC_BASE 0x2C02F000
#define GICH_BASE 0x2C04F000
#define GICV_BASE 0x2C06F000


/* UART */
#define ARCH_HAS_PL011
#define ARCH_UART0_BASE 0x7FF80000ULL
#define ARCH_UART1_BASE 0x7FF70000ULL

#endif /* _ARCH_DEFS_H_ */
