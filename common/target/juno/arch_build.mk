# -*- Makefile -*-


# arch components
ARCH_MODULES += gicv2 uart/pl011 smmu/MMU401 dma/DMA330


# running

sim:
	echo "Cannot simulate this platform..."

debug:
	echo "Cannot debug this platform, for now..."
