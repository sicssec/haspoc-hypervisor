# -*- Makefile -*-


# arch components
ARCH_MODULES += gicv2 uart/16550


# running

sim:
	echo "Cannot simulate this platform..."

debug:
	echo "Cannot debug this platform, for now..."
