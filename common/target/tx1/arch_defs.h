#ifndef _ARCH_DEFS_H_
#define _ARCH_DEFS_H_

/*
 * Memory definitions, note that some defs are "qualified guesses"
 */

/* XG2RAM0 */
#define INTERNAL_RAM_START 0x7c010000
#define INTERNAL_RAM_END   0x7c020000

/* DRAM */
#define EXTERNAL_RAM_START 0x80000000
#define EXTERNAL_RAM_END   0x280000000

/* SRAM */
//#define EXTERNAL_RAM2_START 0xFFF80000
//#define EXTERNAL_RAM2_END   0xFFF92000


/* TRUSTZONE: dummy values, not rreally used in hikey */
#define TZ_ROM_START 0x00100000
#define TZ_ROM_SIZE (96 * 1024)
#define TZ_ROM_END  ((TZ_ROM_START) + (TZ_ROM_SIZE))

/* XG2RAM0, ATF will execute here */
#define TZ_RAM_START 0x7c010000
#define TZ_RAM_SIZE  0x00010000
#define TZ_RAM_END   ((TZ_RAM_START) + (TZ_RAM_SIZE))


/* HYPERVISOR */
#define HYP_ROM_SIZE  (64 * 1024)
#define HYP_ROM_START 0x90000000
#define HYP_ROM_END   ((HYP_ROM_START) + (HYP_ROM_SIZE))

#define HYP_RAM_SIZE  (64 * 1024)
#define HYP_RAM_START (HYP_ROM_END)
#define HYP_RAM_END   ((HYP_RAM_START) + (HYP_RAM_SIZE))

/* PAYLOAD image is at platform dependent DRAM area */
#define PAYLOAD_START 0x90020000
#define PAYLOAD_SIZE (30 * 1024 * 1024)




/*
 * Board configuration
 */

#define ARCH_CNT_FREQ 24000000 // ???

/*
 * CPU definitions
 */

#define ARCH_CORES 4
#define ARCH_CLUSTER_COUNT 1
#define ARCH_CLUSTER0_COUNT 4
#define ARCH_CLUSTER1_COUNT 0
#define ARCH_CLUSTER2_COUNT 0


#define ARCH_MIDR_VALUE 0x411FD071
#define ARCH_MIDR_MASK  0xF


/*
 * Peripherals
 */

//#define DEVICE_BASE	0xF4000000ULL
//#define DEVICE_SIZE	0x05800000ULL

/* GICv2 */
#define ARCH_HAS_GICv2 1
#define GICD_BASE 0x50041000
#define GICC_BASE 0x50042000
#define GICH_BASE 0x50044000
#define GICV_BASE 0x50046000

/* UART */
//#define ARCH_HAS_PL011
#define ARCH_UART0_BASE 0x70006000ULL
#define ARCH_UART1_BASE 0x70006040ULL
#define ARCH_UART2_BASE 0x70006080ULL
#define ARCH_UART3_BASE 0x700060C0ULL


/* CCI-400 related constants */
//#define CCI400_BASE			0xF6E90000ULL
//#define CCI400_SL_IFACE3_CLUSTER_IX	0
//#define CCI400_SL_IFACE4_CLUSTER_IX	1


#endif /* _ARCH_DEFS_H_ */
