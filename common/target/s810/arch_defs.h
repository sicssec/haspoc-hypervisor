#ifndef _ARCH_DEFS_H_
#define _ARCH_DEFS_H_

/*
 * This is a generic target file for Snapdragon 810, mostly based
 * on android kernels published by LG.
 *
 * Note that it doesn't necessarily work on any actual board
 * without minor modifications
 */

// Find a more suitable to put this
#define NO_PSCI

/*
 * Memory definitions: these numbers are all just made up
 */

#define INTERNAL_RAM_START 0x00000000
#define INTERNAL_RAM_END   0x00100000
#define EXTERNAL_RAM_START 0x04000000
#define EXTERNAL_RAM_END   0x40000000


/* TRUSTZONE ROM: since flash is partition based this is N/A */
#define TZ_ROM_START 0x00000000
#define TZ_ROM_SIZE (16 * 1024)
#define TZ_ROM_END  ((TZ_ROM_START) + (TZ_ROM_SIZE))

// TZ area
#define TZ_RAM_START 0x06D00000
#define TZ_RAM_SIZE  (1408 * 1024)
#define TZ_RAM_END   ((TZ_RAM_START) + (TZ_RAM_SIZE))

/* HYPERVISOR: 1MB available RAM */
#define HYP_ROM_SIZE  (128 * 1024)
#define HYP_ROM_START 0x06C00000
#define HYP_ROM_END   ((HYP_ROM_START) + (HYP_ROM_SIZE))

#define HYP_RAM_SIZE  (80 * 1024)
#define HYP_RAM_START (HYP_ROM_END)
#define HYP_RAM_END   ((HYP_RAM_START) + (HYP_RAM_SIZE))

/* PAYLOAD */
#define PAYLOAD_START 0x06C90000
#define PAYLOAD_SIZE (30 * 1024 * 1024)


/*
 * Board configuration
 */

#define ARCH_CNT_FREQ 24000000 // N/A for s810

/*
 * CPU definitions
 */

#define ARCH_MIDR_VALUE 0x410FD030
#define ARCH_MIDR_MASK  0xF

/* Just use the A53 cluster for now */
#define ARCH_CORES 8
#define ARCH_CLUSTER_COUNT 2
#define ARCH_CLUSTER0_COUNT  4
#define ARCH_CLUSTER1_COUNT  4
#define ARCH_CLUSTER2_COUNT  0



/*
 * Peripherals
 */

/* QGICv2 */
#define ARCH_HAS_GICv2 1
#define GICD_BASE 0xf9000000
#define GICC_BASE 0xf9002000
#define GICH_BASE 0xf9001000
#define GICV_BASE 0xf9004000
//#define GICM0_BASE 0xf9006040 // PCI0 QC 0x180
//#define GICM1_BASE 0xf9007040 // PCI1 QC 0x1a0

/* UART */
#define ARCH_UART0_BASE 0xF991E000 /* blsp1_uart2 */
#define ARCH_UART1_BASE 0xF991F000 /* blsp1_uart3 */
#define ARCH_UART2_BASE 0xF995E000 /* blsp2_uart2 */

#endif /* _ARCH_DEFS_H_ */
