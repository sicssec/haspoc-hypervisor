# -*- Makefile -*-

# arch components
ARCH_MODULES += gicv2 uart/uartdm


DRIVERS += hvc-s810 smc_forward

# running
sim:
	echo "Cannot simulate this platform..."

debug:
	echo "Cannot debug this platform, for now..."
