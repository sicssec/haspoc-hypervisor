
#ifndef _DEVICETREE_H_
#define _DEVICETREE_H_

struct dt_block {
    uint32_t token;
    uint32_t start;
    uint32_t end;
    uint32_t data_len;
    uint32_t level;
    char *name;
    union {
        void *ptr;
        uint32_t *num;
        char *str;
    } data;
};

struct dt_context {
    uint32_t *start;
    uint32_t size;
    uint32_t off_stc;
    uint32_t off_str;
    uint32_t size_stc;
    uint32_t size_str;
    struct dt_block root;
};

struct dt_foreach {
    struct dt_context *context;
    uint32_t req_type;
    uint32_t req_level;
    uint32_t curr_level;
    uint32_t curr_offset;
    struct dt_block storage;
};


extern int dt_init(struct dt_context *ctx, void *start, size_t size);
extern int dt_block_find(struct dt_context *ctx, struct dt_block *block,
                         struct dt_block *result, int asnode,
                         char *name, int len);
extern void dt_foreach_init(struct dt_context *ctx,
                            struct dt_block *parent,
                            struct dt_foreach *fe,
                            int asnode);
extern struct dt_block *dt_foreach_next(struct dt_foreach *fe);
extern struct dt_block *dt_foreach_next_of(struct dt_foreach *fe, char *prefix);

#endif /* _DEVICETREE_H_ */
