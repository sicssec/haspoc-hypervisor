
#include "base.h"

#define PL011_DR   ( 0x00 / 4)
#define PL011_FR   ( 0x18 / 4)
#define PL011_IBRD ( 0x24 / 4)
#define PL011_FBRD ( 0x28 / 4)
#define PL011_CR   ( 0x30 / 4)


// #define PL011_DR   0x00
// #define PL011_FR   0x06
// #define PL011_IBRD 0x09
// #define PL011_FBRD 0x0A
// #define PL011_CR   0x0C

#define PL011_FR_TXFF 5


void uart_init(adr_t adr, uint32_t clkdiv_16_16)
{
    volatile uint32_t *base = (uint32_t *) adr;

    if(clkdiv_16_16) {
        base[PL011_IBRD] = clkdiv_16_16 >> 16;
        base[PL011_FBRD] = (clkdiv_16_16 >> 10) & 63;
    }
    base[PL011_CR] = 0x301;
}

void uart_write(adr_t adr, int c)
{
    volatile uint32_t *base = (uint32_t *) adr;

    for(;;) {
        while( (base[PL011_FR] & (1UL << PL011_FR_TXFF)) )
            ; /* wait for queue space */

        base[PL011_DR] = c;

        if(c != '\n')
            break;
        c = '\r';
    }
}
