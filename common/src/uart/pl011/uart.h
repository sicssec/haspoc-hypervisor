
#ifndef __UART_H__
#define __UART_H__

static inline uint32_t uart_calc_clkdiv(uint32_t clkfreq, uint32_t bitrate)
{
    uint64_t tmp = (uint64_t) clkfreq;
    tmp <<= 16;
    tmp /= bitrate * 16;
    return (uint32_t ) tmp;
}

extern void uart_init(adr_t base, uint32_t clkdiv_16_16);
extern void uart_write(adr_t base, int c);

#endif /* __UART_H__ */
