

#include "base.h"


#define UART_LCR_MODE_OP 0
#define UART_LCR_MODE_A 0x80
#define UART_LCR_MODE_B 0xBF

#define UART_LCR_DIV_EN (1UL << 7)

#define UART_LCR_LENGTH_8 ( 3UL << 0)
#define UART_LCR_STOP_1  (0UL << 2)

#define UART_LSR_TX_FIFO_E (1UL << 5)
#define UART_LSR_RX_FIFO_E (1UL << 0)

#define UART_OP_FLAGS  (UART_LCR_MODE_OP | UART_LCR_STOP_1 | UART_LCR_LENGTH_8)


union uart_regs {
    struct {
        uint32_t dll;
        uint32_t dlh;
        uint32_t iir;
        uint32_t lcr;
        uint32_t mcr;
        uint32_t lsr;
        uint32_t msr;
        uint32_t tlr; /* and SPR */
        uint32_t mdr1;
        uint32_t mdr2;
        uint32_t unuse1[4];
        uint32_t usar;
        uint32_t unused2;
        uint32_t scr;
        uint32_t ssr;
        uint32_t unused3[2];
        uint32_t mvr;
        uint32_t sysc;
        uint32_t syss;
        uint32_t wer;
    } a;

    struct {
        uint32_t dll;
        uint32_t dlh;
        uint32_t efr;
        uint32_t lcr;
        uint32_t xon1_addr1;
        uint32_t xon2_addr2;
        uint32_t xoff1;
        uint32_t tlr; /* and xoff2 */
        uint32_t mdr1;
        uint32_t mdr2;
        uint32_t unuse1[4];
        uint32_t usar;
        uint32_t unused2;
        uint32_t scr;
        uint32_t ssr;
        uint32_t unused3[2];
        uint32_t mvr;
        uint32_t sysc;
        uint32_t syss;
        uint32_t wer;
    } b;

    struct {
        uint32_t rhr;
        uint32_t ier;
        uint32_t iir;
        uint32_t lcr;
        uint32_t mcr;
        uint32_t lsr;
        uint32_t msr;
        uint32_t tlr; /* and TCR */
        uint32_t mdr1;
        uint32_t mdr2;
        uint32_t unuse1[6];
        uint32_t scr;
        uint32_t ssr;
        uint32_t unused3[2];
        uint32_t mvr;
        uint32_t sysc;
        uint32_t syss;
        uint32_t wer;
    } op;
};

/* ------------------------------------------------ */

void uart_init(adr_t base, uint32_t param)
{

}


void uart_write(adr_t base, int c)
{
	volatile union uart_regs * uart = (union uart_regs*) base;


	/* switch to OP mode if needed */
	if(uart->op.lcr & UART_LCR_DIV_EN)
        	uart->op.lcr = UART_OP_FLAGS;

	 while( !(uart->op.lsr & UART_LSR_TX_FIFO_E))
		;
    uart->op.rhr = c; /* = THR */

    if(c == '\n')
	uart_write(base, '\r');

}
