
#ifndef __UART_H__
#define __UART_H__


extern void uart_init(adr_t base, uint32_t param);
extern void uart_write(adr_t base, int c);

#endif /* __UART_H__ */
