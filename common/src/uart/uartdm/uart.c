#include "base.h"

#define UARTDM_TXFS (0x04C / 4)
#define DM_NBR_OF_CHARS_OFFSET      (0x40/4)
#define DM_TXRDY_IRQ_OFFSET         (0xA8/4)
#define DM_DATA_OFFSET              (0x100/4)

void uart_init(adr_t adr, uint32_t param)
{

}


void uart_write(adr_t adr, int c)
{
    volatile uint32_t *uart_base = (uint32_t *) adr;

    while( uart_base[UARTDM_TXFS]);

    uart_base[DM_NBR_OF_CHARS_OFFSET] = 1;
    uart_base[DM_TXRDY_IRQ_OFFSET] = 0x300;
    uart_base[DM_DATA_OFFSET] = c;

    if( c == '\n' )
        uart_write(adr, '\r');
}
