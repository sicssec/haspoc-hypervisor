
#include "base.h"

#ifdef TARGET_EMBEDDED
# include "minilib.h"
#endif

#include "bundle.h"


struct bundle_data *bundle_find(void *bundle, const char *id)
{
    struct bundle_hdr *blob = (struct bundle_hdr *) bundle;
    int i;

    if(blob->magic == BUNDLE_MAGIC) {
        for(i = 0; i < blob->count; i++) {
            if(!strcmp(blob->data[i].id, id)) {
                return & blob->data[i];
            }
        }
    }

    /* failed */
    return 0;
}
