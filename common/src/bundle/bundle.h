#ifndef _BUNDLE_H_
#define _BUNDLE_H_

#define BUNDLE_MAGIC 0xAA6400F2
#define BUNDLE_ID_LENGTH 31

struct bundle_data {
    uint32_t size;
    uint32_t offset;
    uint32_t flags;
    uint32_t time;
    char id[BUNDLE_ID_LENGTH + 1];
};

struct bundle_hdr {
    uint32_t magic;
    uint32_t size;
    uint32_t count;
    uint32_t flags;
    uint32_t time;
    struct bundle_data data[];
};

struct bundle_ftr {
    uint32_t reserved[32];
    uint32_t magic;
};

/* bundle */
extern struct bundle_data *bundle_find(void *bundle, const char *id);


#endif /* ! _BUNDLE_H_ */
