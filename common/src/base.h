
#ifndef _BASE_H_
#define _BASE_H_


/*
 * SHARED
 */
#define KB (1024)
#define MB (KB * KB)
#define GB (MB * KB)

/* 4KB pages */
#define PAGE_BITS 12
#define PAGE_SIZE 4096 /* 1 << PAGE_BITS */

#ifndef NULL
    #define NULL (0)
#endif

/* macro stringify */
#define QUOTE(name) #name
#define STR(macro) QUOTE(macro)

#ifdef __ASSEMBLER__

.macro CHECK_DISTANCE from to max
.if ( \to - \from) > \max
      .error "Distance check failed"
.endif
.endm

#else /* __ASSEMBLER__ */

#include <stdint.h>
#include <stdbool.h>

/* arch types */
typedef uint64_t adr_t;
typedef uint64_t size_t;

/*
 * shorthands for common attributes
 */

#define __packed __attribute__((packed))
#define __naked __attribute__((naked))
#define __weak __attribute__((weak))
#define __align(n) __attribute__ ((aligned (n)))
#define __used __attribute__((used))
#define __section(s) __attribute__((section (s)))



/*
 * misc macros
 */

#define DECLARE_BASE(_type, _name, _adr) \
 volatile _type  * const _name = (_type *) _adr

#define _BV(x) (1ULL << (x))

#define ALIGN_DOWN(x, size)  (((uint64_t) (x)) & (~((size) -1)))
#define ALIGN_UP(x, size) ALIGN_DOWN( (x) + (size) - 1, size)

/* member offset in a structure */
#define GET_OFFSET(type, member) (&((type *)0)->member)

/* get structure parent, similar to container_of in Linux */
#define GET_CONTAINER(child, type, name) \
 ((type *) ((uint8_t *)child - (uint8_t *)(&((type *)0)->name)))


 /* byte-order:
  * 1. little-endian, which is our internal (see hvend)
  * 2. big-endina, used by devicetrees
  */
 #if (__BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__)
 #  define bigend(x) __builtin_bswap32(x)
 #  define litend(x) (x)
 #elif (__BYTE_ORDER__ == __ORDER_BIG_ENDIAN__)
 #  define bigend(x) (x)
 #  define litend(x) __builtin_bswap32(x)
 #else
 #  error "Unknown endianness"
 #endif

#endif /* ! __ASSEMBLER__ */


#endif /* _BASE_H_ */
