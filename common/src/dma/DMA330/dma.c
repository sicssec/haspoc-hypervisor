
#include "dma.h"

#define __raw_readb(a) (*(volatile uint8_t *)(a))
#define __raw_writeb(a,v) (*(volatile uint8_t *)(a) = (v))
#define __raw_readl(a) (*(volatile uint32_t *)(a))
#define __raw_writel(a,v) (*(volatile uint32_t *)(a) = (v))


struct DMAC_regs*	dmac_regs = (struct DMAC_regs*)DMA_BASE;


// This function sets up code for channel thread and runs DMAGO in manager thread via debug interface.
// Channel code loads SAR and DAR registers, then loads data from SAR and stores it in DAR
// The idea is for DMA to excercise SMMU when running DMALD and DMAST instructions
// Since DMAC works with 32-bit addresses, all code must be located in the low 4GB of address-map.
void dma_copy(uint8_t channel, uint32_t sourceaddr, uint32_t destaddr)
{

	uint16_t sourcelow =  (sourceaddr & 0xFFFF);
	uint16_t sourcehigh = ((sourceaddr >> 16) & 0xFFFF);
	uint16_t destlow = (destaddr & 0xFFFF);
	uint16_t desthigh= ((destaddr >> 16) & 0xFFFF);

	// 1. Create DMA channel program
	uint16_t dma_prog_ch[8]={0x00BC, sourcelow, sourcehigh,	/* DMAMOV SAR LowWord,HighWord */
				0x02BC, destlow, desthigh,	/* DMAMOV DAR LowWord,HighWord */
				0x0804, 			/* DMALD + DMAST */
				0x0000 };			/* DMA END (x2) */

	// 2. Poll DBG status
	printf("[DMA] DBGSTATUS: %x\n",dmac_regs->DBGSTATUS); // DBGSTATUS must be 0 to continue
	// 3. Write DGBINST0
	dmac_regs->DBGINST0 = (0x00A20000 | (channel << 24)); // Mask DMAGO with channelnumber
	// 4. Write Adress to INST1
	dmac_regs->DBGINST1 = (uint32_t)&dma_prog_ch; // Address of first channel thread instruction
	// 5. Start manager thread
	dmac_regs->DBGCMD = 0; // Manager thread will start the channel thread and then stop

}


void dma_transaction()
{

	printf("[DMA] JUNO DMA TESTCODE\n");
	printf("[DMA] Source data prior to transaction: %x\n", __raw_readb(0x80000020));
	printf("[DMA] Destination data prior to transaction: %x\n", __raw_readb(0x80000040));

	dma_copy(0, 0x80000020, 0x80000040); // Channel(StreamID) 0, DMA copy 1byte 0x80000020 to 0x80000040

	/* Delay to complete DMA transfer */
	uint32_t i;
	for(i=0; i<10; i++)
		__asm__ volatile ("nop");

	__asm__ volatile ("dsb sy"); // Make sure d-cache is flushed
	printf("[DMA] Destination data post transaction: %x\n", __raw_readb(0x80000040));
	//printf("[DMA] DMA CPC0: %x\n",dmac_regs->SRPC[0].CPC);// print thread PC
	printf("[DMA] DMA SAR0: %x\n",dmac_regs->SARDAR[0].SAR);
	printf("[DMA] DMA DAR0: %x\n",dmac_regs->SARDAR[0].DAR);
	if (dmac_regs->DMA_FTR[0] > 0) printf("[DMA] DMA FTR0: %x\n",dmac_regs->DMA_FTR[0]); // Error if > 0
	if (__raw_readl(0x7FB08068) > 0) printf("[DMA] SMMU CB0-FSYNR0: %x\n", __raw_readl(0x7FB08068));
	if (__raw_readl(0x7FB08058) > 0) printf("[DMA] SMMU CB0-FSR: %x\n", __raw_readl(0x7FB08058));
	if (__raw_readl(0x7FB08060) > 0) printf("[DMA] SMMU CB0-FAR0: %x\n", __raw_readl(0x7FB08060));
	if (__raw_readl(0x7FB00048) > 0) printf("[DMA] SMMU GFSR: %x\n", __raw_readl(0x7FB00048));
	if (__raw_readl(0x7FB00050) > 0) printf("[DMA] SMMU GFSYNR0: %x\n", __raw_readl(0x7FB00050));
	if (__raw_readl(0x7FB00054) > 0) printf("[DMA] SMMU GFSYNR1: %x\n", __raw_readl(0x7FB00054));


}

