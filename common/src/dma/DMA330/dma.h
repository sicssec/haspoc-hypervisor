
#ifndef __DMA_H__
#define __DMA_H__

#include "defs.h"

#define DMA_BASE	0x7FF00000	// DMA base address
#define DMAGO		0x00A20000	// DMAGO instruction, [31:24]=Inst1 ([26:24]=Channel0), [23:16]=Inst0(NS=1)


struct csr_cpc_s
{
        volatile uint32_t CSR;              // Channel status
        volatile uint32_t CPC;              // Channel PC
};

struct sar_dar_s
{
        volatile uint32_t SAR;              // Source register
        volatile uint32_t DAR;              // Destination register
  const volatile uint32_t padding[6];		// We dont need access to these regs
};

struct DMAC_regs
{
 const  volatile uint32_t padding0[16];		// We dont need to access general regs
        volatile uint32_t DMA_FTR[8];		// Fault types channel 0-7
 const  volatile uint32_t padding1[40];		// Reserved
	struct csr_cpc_s SRPC[8];
 const  volatile uint32_t padding2[176];	// RESERVED
	struct sar_dar_s SARDAR[8];
 const  volatile uint32_t padding3[512];	// RESERVED

	volatile uint32_t DBGSTATUS;
	volatile uint32_t DBGCMD;
	volatile uint32_t DBGINST0;
	volatile uint32_t DBGINST1;
};



extern void dma_transaction();

#endif /* !__DMA_H__ */

