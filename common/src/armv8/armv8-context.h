
#ifndef _ARMV8_CONTEXT_H_
#define _ARMV8_CONTEXT_H_

#ifdef __ASSEMBLER__

    .macro HANDLER_TOP label
        .align 7
        \label:
    .endm

    .macro SAVE_REGISTERS level
        sub sp, sp, #8 * 4
        PUSH2 x28, x29
        PUSH2 x26, x27
        PUSH2 x24, x25
        PUSH2 x22, x23
        PUSH2 x20, x21
        PUSH2 x18, x19
        PUSH2 x16, x17
        PUSH2 x14, x15
        PUSH2 x12, x13
        PUSH2 x10, x11
        PUSH2 x8, x9
        PUSH2 x6, x7
        PUSH2 x4, x5
        PUSH2 x2, x3
        PUSH2 x0, x1

        stp x30, xzr, [sp, #30 * 8]      /* LR, - */

        mov x0, sp
        mrs x1, spsr_el\level
        mrs x2, elr_el\level
        stp x2, x1, [sp, #32 * 8]       /* PC, PSR */
    .endm

    .macro RESTORE_REGISTERS level
        ldp x10, x11, [sp, #32 * 8]     /* PC, PSR */
        ldr x30, [sp, #30 * 8]          /* LR */
        msr elr_el\level, x10
        msr spsr_el\level, x11

        POP2 x0, x1
        POP2 x2, x3
        POP2 x4, x5
        POP2 x6, x7
        POP2 x8, x9
        POP2 x10, x11
        POP2 x12, x13
        POP2 x14, x15
        POP2 x16, x17
        POP2 x18, x19
        POP2 x20, x21
        POP2 x22, x23
        POP2 x24, x25
        POP2 x26, x27
        POP2 x28, x29
        add sp, sp, #(8 * 4)
    .endm

    // id added to be able to share the same handler
    .macro HANDLE_EXCEPTION level label_handler label_return get_esr=0 id=0
        .align 7
        enter_\label_handler\id:
        SAVE_REGISTERS \level
        .if \get_esr == 1
            mrs x3, esr_el\level
        .endif
        adr x30, \label_return
        b \label_handler
        CHECK_DISTANCE enter_\label_handler\id . 0x80
    .endm
#else /* __ASSEMBLER__ */


struct registers {
    uint64_t x[30];
    uint64_t lr;
    uint64_t unused;
    uint64_t pc, cpsr;
};

#endif /* !__ASSEMBLER__ */


#define REGISTERS_SIZE (8 * (32 + 2))

#define REGISTERS_OFFSET_X(n) (8 * (n))

#define REGISTERS_OFFSET_X28 (8 * 28)
#define REGISTERS_OFFSET_LR  (8 * 30)
#define REGISTERS_OFFSET_SP  (8 * 31)
#define REGISTERS_OFFSET_PC  (8 * 32)
#define REGISTERS_OFFSET_SPR (8 * 33)

#endif /* _ARMV8_CONTEXT_H_ */
