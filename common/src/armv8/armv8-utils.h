
#ifndef _ARMV8_UTILS_H_
#define _ARMV8_UTILS_H_


/*
 * ASSEMBLER
 */

#ifdef __ASSEMBLER__

/* assembler push and pop (pair) macros*/
.macro PUSH2, r1, r2
stp \r1, \r2, [sp, #-16]!
.endm

.macro POP2, r1, r2
ldp \r1, \r2, [sp], #16
.endm


#else /* ! __ASSEMBLER__ */

#define hvend(x) litend(x)
#define dtend(x) bigend(x)

#define MRS64(reg)\
({ uint64_t x; __asm__ volatile ("mrs %0, " reg : "=r"(x)); x; })

#define MSR64(reg, val) \
do {  __asm__ volatile ("msr " reg ", %0" :: "r"(val)); } while(0)

#define isb() \
do {  __asm__ volatile ("isb"); } while(0)


/*
 * CPU identification
 */

 #define CPU_INVALID_CPUID 0xFF

typedef uint32_t cpuid_t;

#ifdef ARCH_CLUSTER0_COUNT

static inline int mpidr_get_core(uint64_t mpidr)
{
    return mpidr & 0xFF;
}

static inline int mpidr_get_cluster(uint64_t mpidr)
{
    return (mpidr >> 8) & 0xFF;
}

static inline int mpidr_is_valid(uint64_t mpidr)
{
    int core, cluster;
    cluster = mpidr_get_cluster(mpidr);
    core = mpidr_get_core(mpidr);

    switch(cluster) {
        case 0: return core < ARCH_CLUSTER0_COUNT;
#if ARCH_CLUSTER1_COUNT > 0
        case 1: return core < ARCH_CLUSTER1_COUNT;
#endif
#if ARCH_CLUSTER2_COUNT > 0
        case 2: return core < ARCH_CLUSTER2_COUNT;
#endif
        default: return 0;
    }
}


/* same as mpidr_to_cpuid() but can be called from assembler without an stack */
extern cpuid_t __mpidr_to_cpuid(uint64_t mpidr);

static inline cpuid_t mpidr_to_cpuid(uint64_t mpidr)
{
    int core, cluster;
    cluster = mpidr_get_cluster(mpidr);
    core = mpidr_get_core(mpidr);

    switch(cluster) {
        case 0: return core;

#if ARCH_CLUSTER1_COUNT > 0
        case 1: return core + ARCH_CLUSTER0_COUNT;
#endif
#if ARCH_CLUSTER2_COUNT > 0
        case 2: return core + ARCH_CLUSTER0_COUNT + ARCH_CLUSTER1_COUNT;
#endif
        default: return CPU_INVALID_CPUID;
    }
}

static inline uint64_t cpuid_to_mpidr(cpuid_t cpuid)
{
#if ARCH_CLUSTER2_COUNT > 0
    /* in cluster 2 ? */
    if(cpuid >= ARCH_CLUSTER0_COUNT + ARCH_CLUSTER1_COUNT)
        return 0x0200 + (cpuid - ARCH_CLUSTER0_COUNT - ARCH_CLUSTER1_COUNT);
#endif

#if ARCH_CLUSTER1_COUNT > 0
    /* in cluster 1 ? */
    if(cpuid >= ARCH_CLUSTER0_COUNT)
        return 0x0100 + (cpuid - ARCH_CLUSTER0_COUNT);
#endif
    /* in cluster 0 ? */
    return cpuid;
}

#endif /* ARCH_CLUSTER0_COUNT */

/*
 * other ARMv8 utility functions
 */

static inline int pc_is_valid(adr_t pc)
{
    return (pc & 7) == 0;
}



/*
 * Multicore capable spinlock implementation
 */
typedef volatile uint32_t spinlock_t;

extern void spinlock_init(spinlock_t *, int);
extern void spinlock_lock(spinlock_t *);
extern void spinlock_unlock(spinlock_t *);
extern int spinlock_read(spinlock_t *);

#endif /* ! __ASSEMBLER__ */




#endif /* _ARMV8_UTILS_H_ */
