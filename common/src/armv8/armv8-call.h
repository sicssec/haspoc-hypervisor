
#ifndef _ARMV8_CALL_H_
#define _ARMV8_CALL_H_


/*
 * SMC and HVC call definitions
   interface to ARM Trusted Firmware and related components
 */


/*
 * monitor interface
 */

/* SMC return codes */
#define SMC_RET_SUCCESS ((uint32_t)0)
#define SMC_RET_UNKNOWN ((uint32_t)-1)

#endif /* !_ARMV8_CALL_H_ */
