
#ifndef _ARMV8_REGS_H_
#define _ARMV8_REGS_H_

#define ESR_EC_UNKNOWN 0x00
#define ESR_EC_WFx     0x01
#define ESR_EC_PABORT_LOWER 0x20
#define ESR_EC_PABORT_SAME 0x21
#define ESR_EC_DABORT_LOWER 0x24
#define ESR_EC_DABORT_SAME 0x25
#define ESR_EC_HVC64     0x16
#define ESR_EC_HVC32     0x12
#define ESR_EC_SMC64     0x17
#define ESR_EC_SMC32     0x13

#if !defined( __ASSEMBLER__)

/* important CPU registers */
union reg_esr {
    uint32_t all;
    struct {
        /* ISS */

#define DFSC_TF_L0 0b000100 /* Translation fault */
#define DFSC_TF_L1 0b000101
#define DFSC_TF_L2 0b000110
#define DFSC_TF_L3 0b000111
        uint32_t dfsc : 6; /* Data Fault Status Code */

        uint32_t wnr : 1; /* 0: read, 1: write */
        uint32_t s1ptw : 1;
        uint32_t cm : 1;
        uint32_t ea : 1;
        uint32_t res0 : 4;
        uint32_t sf : 1;
        uint32_t ar : 1;
        uint32_t srt : 5; /* register */
        uint32_t sse : 1;
        uint32_t sas : 2; /* access size */
        uint32_t isv : 1;

        uint32_t il : 1; /* ISS section above is valid */
        uint32_t ec: 6;
    };
};


union reg_hcr {
    uint64_t all;
    struct {
        uint32_t vm : 1;    /* Virtualization MMU enable for Non-secure EL1&0 stage 2 address translation */
        uint32_t swio : 1;  /* Set/Way Invalidation Override */
        uint32_t ptw : 1;   /* Protected Table Walk. Stage 1/2 mem attribute combination handling */
        uint32_t fmo : 1;   /* Physical FIQ EL routing */
        uint32_t imo : 1;   /* Physical IRQ EL routing */
        uint32_t amo : 1;   /* Asynchronous External Abort and SError Interrupt routing */
        uint32_t vf : 1;    /* Virtual FIQ Interrupt pending */
        uint32_t vi : 1;    /* Virtual IRQ Interrupt pending */
        uint32_t vse : 1;   /* Virtual System Error/Asynchronous abort pending */
        uint32_t fb : 1;    /* Force instruction broadcast from non-sec EL1 */
        uint32_t bsu : 2;   /* Min shareability domain for barrier from non-sec EL0,1 */
        uint32_t dc : 1;    /* Default cacheable */
        uint32_t twi : 1;   /* Trap non-secure EL0 and EL1 execution of WFI instructions to EL2 */
        uint32_t twe : 1;   /* Trap non-secure EL0 and EL1 execution of WFE instructions to EL2 */
        uint32_t tid0 : 1;  /* Trap ID group 0 register accesses to EL2 */
        uint32_t tid1 : 1;  /* Trap ID group 1 non-secure EL1 register reads to EL2 */
        uint32_t tid2 : 1;  /* Trap ID group 2 register accesses to EL2 */
        uint32_t tid3 : 1;  /* Trap ID group 3 non-secure EL1 register reads to EL2 */
        uint32_t tsc : 1;   /* Trap non-secure EL1 execution of SMC to EL2 */
        uint32_t tidcp : 1; /* Trap implementation dependent functionality from non-secure EL1 to EL2 */
        uint32_t tacr : 1;  /* Trap auxiliary control register access from non-secure EL1 to EL2 */
        uint32_t tsw : 1;   /* Trap data or unified cache maintenance operations by Set/Way from non-secure EL1 to EL2 */
        uint32_t tpc : 1;   /* Trap data or unified cache maintenance operations to point of coherency from non-secure EL0/1 to EL2 */
        uint32_t tpu : 1;   /* Trap data or unified cache maintenance operations to point of unification from non-secure EL0/1 to EL2 */
        uint32_t ttlb : 1;  /* Trap TLB maintenance instructions from non-secure EL1 to EL2 */
        uint32_t tvm : 1;   /* Trap EL1 non-secure virtual memory control register writes to EL2 */
        uint32_t tge : 1;   /* Trap general exceptions */
        uint32_t tdz : 1;   /* Trap non-secure EL0/1 DC ZVA instructions to EL2 */
        uint32_t hcd : 1;   /* Disable non-secure state execution of HVC instruction */
        uint32_t trvm : 1;  /* Trap non-secure EL1 reads of the virtual memory control registers to EL2 */
        uint32_t rw : 1;    /* Execution state (AArch32/64) control for lower exception levels */
        uint32_t cd : 1;    /* Stage 2 data cache disable from EL0/1 */
        uint32_t id : 1;    /* Stage 2 instruction cache disable from EL0/1 */
        uint32_t res: 4;    /* Reserved, RES0 */
        uint32_t miocnce: 1;/* Mismatched inner/outer cacheable non-coherency enable for the non-secure EL0/1 translation regime */
    };
};


#endif /* ! __ASSEMBLER__ */


#endif /* !  _ARMV8_REGS_H_ */
