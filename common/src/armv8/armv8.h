/*
 * some ARMv8 specific registers and such are gathered here for better clarity
 */

#ifndef _ARMV8_H_
#define _ARMV8_H_

/* CPU interrupts */
#define IRQ_PPI_CNTPS 13    /* secure timer */
#define IRQ_PPI_CNTHP 10    /* hypervisor timer */

/* exception handlers */
#define EXP_SIZE 128

#define EXP_SRC_SAME_SP0 0
#define EXP_SRC_SAME 4
#define EXP_SRC_LOWER_64 8
#define EXP_SRC_LOWER_32 12

#define EXP_TYPE_SYNC 0
#define EXP_TYPE_IRQ 1
#define EXP_TYPE_FIQ 2
#define EXP_TYPE_ERROR 3


#include "armv8-regs.h"
#include "armv8-context.h"
#include "armv8-utils.h"
#include "armv8-call.h"

#endif /* ! _ARMV8_H_ */
