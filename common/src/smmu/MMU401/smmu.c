
#include "smmu.h"


struct MMU401_global0_s*	smmu_global0;
struct MMU401_global1_s*	smmu_global1;
struct MMU401_cb_s*		smmu_cb_base;
struct MMU401_ssd_s*		smmu_ssd_table; // Secure access only

#if 0 // Hypervisor can never use ssd
smmu_ssd_table = (struct MMU401_ssd_s*)((uint64_t)SMMU_BASE + (4 * SMMU_PAGESIZE)); // 4 is from IHI0062B
#endif

static uint64_t l1_xlation_table[8]
	__attribute__((aligned(8 * sizeof(uint64_t))))
/* Long-descriptor spec in DDI0487A_f: G4-4092, Upper- Lower-attributes: G4-4097 */
/* Lowest 8 GB described with 8 entries 1-1 mapping, normal memory - cachable (0x3D5 for non-cachable) */
={
	[0] = 0x0000003DD,
	[1] = 0x0400003DD,
	[2] = 0x0800003DD,
	[3] = 0x0C00003DD,
	[4] = 0x1000003DD,
	[5] = 0x1400003DD,
	[6] = 0x1800003DD,
	[7] = 0x1C00003DD
};

void smmu_global_init()
{
	smmu_global0   = (struct MMU401_global0_s*)SMMU_BASE;
	smmu_global1   = (struct MMU401_global1_s*)((uint64_t)SMMU_BASE + (1 * SMMU_PAGESIZE));
	smmu_cb_base   = (struct MMU401_cb_s*)((uint64_t)SMMU_BASE + (SMMU_NUMPAGE * SMMU_PAGESIZE));

	smmu_global0->SMMU_sGFSRRESTORE = 0; // Reset Global Fault reg
	smmu_global0->SMMU_sGFSRNR[0] = 0; // Reset Global syndrome reg
	smmu_global0->SMMU_sGFSRNR[1] = 0; // Reset Global syndrome reg
	printf("[SMMU] IDR0: %x\n", smmu_global0->SMMU_IDR[0]);
}

void smmu_global_bypass()
{
	smmu_global0->SMMU_sCR0 = 1;	// Bypass mode
	printf("[SMMU] Bypass sCR0: %x\n", smmu_global0->SMMU_sCR0);
}

void smmu_global_enable()
{
	smmu_global0->SMMU_sCR0 = 0;	// Enable translation
	printf("[SMMU] Enable sCR0: %x\n", smmu_global0->SMMU_sCR0);
}

void smmu_assign_id(uint16_t group, uint32_t streamid)
{
	smmu_global0->SMMU_SMR[group] = (0x80000000 | streamid); // Stream Match [group] valid
	printf("[SMMU] Setup SMR[%d](%x): %x\n", group,&smmu_global0->SMMU_SMR[group], smmu_global0->SMMU_SMR[group]);
}

void smmu_assign_context(uint16_t group, uint16_t context)
{
	smmu_global0->SMMU_S2CR[group] = context;
	printf("[SMMU] Setup S2CR[%d](%x): %x\n", group,&smmu_global0->SMMU_S2CR[group], smmu_global0->SMMU_S2CR[group]);
}

void smmu_context_init(uint16_t context)
{
	struct MMU401_cb_s* context_n = (struct MMU401_cb_s*)(smmu_cb_base + (SMMU_PAGESIZE * context));
	smmu_global1->SMMU_CBAR[context] = 0; // Stage2 context, TODO investigate interrupts
	smmu_global1->SMMU_CBA2R[context] = 0; // 0=V7S/V7L, 1=V8L  (p.3-7 in DDI0521B, IHI0067A)
	context_n->SMMU_CBn_FSYNR0 = 0; // Reset Context fault syndrome reg
	context_n->SMMU_CBn_FAR[0] = 0; // Reset fault address reg
	context_n->SMMU_CBn_FAR[1] = 0; // Reset fault address reg
}

void smmu_context_enable(uint16_t context)
{
	struct MMU401_cb_s* context_n = (struct MMU401_cb_s*)(smmu_cb_base + (SMMU_PAGESIZE * context));
	context_n->SMMU_CBn_SCTLR = (1 | 8); // Enable context 0, Access flag faults are disabled
	printf("[SMMU] SMMU_CBn_SCTLR[%d]: %x\n",context,context_n->SMMU_CBn_SCTLR);
}

void smmu_context_disable(uint16_t context)
{
	struct MMU401_cb_s* context_n = (struct MMU401_cb_s*)(smmu_cb_base + (SMMU_PAGESIZE * context));
	context_n->SMMU_CBn_SCTLR = 0xC00000; // Disable context 0
}

void smmu_context_table_setup(uint16_t context)
{
	struct MMU401_cb_s* context_n = (struct MMU401_cb_s*)(smmu_cb_base + (SMMU_PAGESIZE * context));

	context_n->SMMU_CBn_TTBCR = 0x80023540;
	printf("[SMMU] SMMU_CBn_TTBCR[%d]: %x\n",context,context_n->SMMU_CBn_TTBCR);

	context_n->SMMU_CBn_TTBR0 = (uint64_t)l1_xlation_table;
	printf("[SMMU] SMMU_CBn_TTBR0[%d]: %x = %x\n",context,context_n->SMMU_CBn_TTBR0, *l1_xlation_table);
}

void smmu_setup()
{
	smmu_global_init();
	smmu_global_bypass();
	smmu_context_disable(0);
	smmu_context_init(0); // Initialize Context 0
	smmu_assign_id(0,0); // Assign StreamID:0 to StreamGroup:0
	smmu_assign_context(0,0); // Assign Context:0 to StreamGroup:0
	smmu_context_table_setup(0);
	smmu_context_enable(0);
	smmu_global_enable();


#if 0
// Fault prints
printf("[D-SMMU] CB-FSR(%x): %x\n", &context0->SMMU_CBn_FSR, context0->SMMU_CBn_FSR);
printf("[D-SMMU] CB-FSYNR0(%x): %x\n", &context0->SMMU_CBn_FSYNR0, context0->SMMU_CBn_FSYNR0);
printf("[D-SMMU] sGFSR(%x): %x\n", &smmu_global0->SMMU_sGFSR, smmu_global0->SMMU_sGFSR);
printf("[D-SMMU] sGFSRNR[0](%x): %x\n", &smmu_global0->SMMU_sGFSRNR[0], smmu_global0->SMMU_sGFSRNR[0]);
printf("[D-SMMU] sGFSRNR[1]: %x\n", smmu_global0->SMMU_sGFSRNR[1]);
#endif

}

