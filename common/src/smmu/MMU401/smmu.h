#ifndef __SMMU_H__
#define __SMMU_H__

#include "defs.h"

#define SMMU_BASE	0x7FB00000
#define SMMU_PAGESIZE	0x1000	// 4k page
#define SMMU_NUMPAGE	8	// 8 pages
#define SSD_IDX		15	// Specified bit width in SMMU_IDR1.NUMSSDNDXB (bit width=4 -> SSD_IDX=15)

struct MMU401_global0_s
{
        volatile uint32_t SMMU_sCR0;              // +0x0000 - RW - Configuration Register 0 - Banked
        volatile uint32_t SMMU_SCR1;              // +0x0004 - RW - Configuration Register 1 - Secure only
        volatile uint32_t SMMU_sCR2;              // +0x0008 - RW - Configuration Register 2 - Banked

  const volatile uint32_t padding0;               // +0x000C - RESERVED

        volatile uint32_t SMMU_sACR0;             // +0x0010 - RW - Aux Configuration Register - Banked

  const volatile uint32_t padding1[3];            // +0x0014-0x001C - RESERVED

        volatile uint32_t SMMU_IDR[8];            // +0x0020 - RW - Identification registers
        volatile uint32_t SMMU_sGFAR[2];          // +0x0040 - RW - Global Fault Address Register - Banked
        volatile uint32_t SMMU_sGFSR;             // +0x0048 - RW - Global Fault Status Register - Banked
        volatile uint32_t SMMU_sGFSRRESTORE;      // +0x004C - WO - Global Fault Status Restore Register - Banked
        volatile uint32_t SMMU_sGFSRNR[3];        // +0x0050 - RW - Global Fault Syndrome Registers - Banked

  const volatile uint32_t padding2;               // +0x005C - RESERVED

        volatile uint32_t SMMU_STLBIALL;          // +0x0060 - WO - TLB Invalidate All - Secure only
        volatile uint32_t SMMU_TLBIVMID;          // +0x0064 - WO - TLB Invalidate by VMID
        volatile uint32_t SMMU_TLBIALLNSNH;       // +0x0068 - WO - TLB Invalidate All, Non-Secure Non-Hyp
        volatile uint32_t SMMU_TLBIALLH;          // +0x006C - WO - TLB Invalidate All Hyp
        volatile uint32_t SMMU_sTLBGSYNC;         // +0x0070 - WO - Global Synchronize TLB Invalidate- Banked
        volatile uint32_t SMMU_sTLBGSTATUS;       // +0x0074 - WO - Global TLB Status - Banked
  const volatile uint32_t padding3;	          // +0x0078 - RESERVED

        volatile uint32_t DBGRPTR;	          // +0x0080 - RW - Debug registers
        volatile uint32_t DBGRDATA;	          // +0x0084 - RO - Debug registers

  const volatile uint32_t padding4[222];          // +0x0088-0x03FC - RESERVED

        volatile uint32_t SMMU_SNSCR[3];          // +0x0400 - RW - Secure alias for Non-secure copy of SMMU_sCRn
  const volatile uint32_t padding5;               // +0x040C - RESERVED
        volatile uint32_t SMMU_NSACR;             // +0x0410 - RW - Secure alias for Non-secure copy of SMMU_sACR
  const volatile uint32_t padding6[60];           // +0x0414-0x04FC - RESERVED

  const volatile uint32_t padding7[192];         // +0x0500-0x07FC - RESERVED

        volatile uint32_t SMMU_SMR[128];          // +0x0800 - RW - Stream Match Registers

  const volatile uint32_t padding8[128];         // +0x0A00-0x0BFC - RESERVED

        volatile uint32_t SMMU_S2CR[128];         // +0x0C00 - RW - Stream Match Registers
};

struct MMU401_global1_s
{
        volatile uint32_t SMMU_CBAR[128];         // +0x0000 - RW - Context Bank Attribute Registers TODO 8

  const volatile uint32_t padding0[128];          // +0x0200-0x03FC - RESERVED

        volatile uint32_t SMMU_CBFRSYNRA[128];    // +0x0400 - RW - Context Bank Attribute Registers TODO 8

  const volatile uint32_t padding1[128];          // +0x0600-0x07FC - RESERVED

        volatile uint32_t SMMU_CBA2R[8];          // +0x0800 - RW - Context Bank Attribute 2 Registers
};

/* Context Bank - Includes registers from stage 2 formats */
struct MMU401_cb_s
{
        volatile uint32_t SMMU_CBn_SCTLR;         // +0x0000 - RW - System Control Register
  const volatile uint32_t padding0[7];            // +0x0004 - 0x001C - RESERVED
        volatile uint64_t SMMU_CBn_TTBR0;	  // +0x0020 - RW - Transaction Table Base Register 0
  const volatile uint32_t padding1[2];	          // +0x0028 - 0x002C - RESERVED
        volatile uint32_t SMMU_CBn_TTBCR;         // +0x0030 - RW - Transaction Table Base Control Register
  const volatile uint32_t padding2[9];	          // +0x0034 - 0x0054 - RESERVED
        volatile uint32_t SMMU_CBn_FSR;           // +0x0058 - RW - Fault Status Register
        volatile uint32_t SMMU_CBn_FSRRESTORE;    // +0x005C - R0 - FSR Restore register
        volatile uint32_t SMMU_CBn_FAR[2];        // +0x0060 - RW - 64b Fault Address Register
        volatile uint32_t SMMU_CBn_FSYNR0;        // +0x0068 - RW - Fault syndrome register 0
};

struct MMU401_ssd_s /* Secure access only */
{
        volatile uint32_t SMMU_SSD[SSD_IDX];
};


extern void smmu_setup();

#endif /* !__SMMU_H__ */

