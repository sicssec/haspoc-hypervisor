#ifndef __SMMU_H__
#define __SMMU_H__

#define PERIPH_BASE	0xF7030000
#define MEDIA_BASE	0xF4410000
#define SMMU_BASE	0xF4210000
#define MEDIA_CLKEN12	(PERIPH_BASE + 0x270)
#define MEDIA_CLKEN	(MEDIA_BASE + 0x520)
#define MEDIA_RSTDIS (MEDIA_BASE + 0x530)
#define MEDIA_CTRL5 (MEDIA_BASE + 0x51C)

#include "defs.h"

struct SMMU_regs_s
{
        volatile uint32_t SMMU_CTRL;	//(0x0000)
        volatile uint32_t SMMU_ENABLE;	// (0x0004)
        volatile uint32_t SMMU_PTBR;	// (0x0008)
        volatile uint32_t SMMU_START;	// (0x000C)
        volatile uint32_t SMMU_END;	// (0x0010)
        volatile uint32_t SMMU_INTMASK;	// (0x0014)
        volatile uint32_t SMMU_RINTSTS;	// (0x0018)
        volatile uint32_t SMMU_MINTSTS;	// (0x001C)
        volatile uint32_t SMMU_INTCLR;	// (0x0020)
        volatile uint32_t SMMU_STATUS;	// (0x0024)
        volatile uint32_t SMMU_AXIID;	// (0x0028)
        volatile uint32_t SMMU_CNTCTRL;	// (0x002C)
        volatile uint32_t SMMU_TRANSCNT;	// (0x0030)
        volatile uint32_t SMMU_L0TLBHITCNT;	// (0x0034)
        volatile uint32_t SMMU_L1TLBHITCNT;	// (0x0038)
        volatile uint32_t SMMU_WRAPCNT;	// (0x003C)
        volatile uint32_t SMMU_SEC_START;	// (0x0040)
        volatile uint32_t SMMU_SEC_END;	// (0x0044)
        volatile uint32_t SMMU_VERSION;	// (0x0048)
        volatile uint32_t SMMU_IPTSRC;	// (0x004C)
        volatile uint32_t SMMU_IPTPA;	// (0x0050)
        volatile uint32_t SMMU_TRBA;	// (0x0054)
        volatile uint32_t SMMU_BYS_START;	// (0x0058)
        volatile uint32_t SMMU_BYS_END;	// (0x005C)
};

extern int smmu_setup();

#endif /* !__SMMU_H__ */

