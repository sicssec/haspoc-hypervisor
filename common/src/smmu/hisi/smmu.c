
#include "smmu.h"

struct SMMU_regs_s* smmu_regs;

static inline void writel(uint32_t value, adr_t addr)
{
        *(volatile uint32_t *)addr = value;
}

static inline uint32_t readl(adr_t addr)
{
        return *(const volatile uint32_t *)addr;
}

void smmu_global_init()
{
	smmu_regs = (struct SMMU_regs_s*)SMMU_BASE;
	printf("[SMMU] Global init done\n");
}

int smmu_enable()
{
	uint32_t regread;

	// Enable media subsystem clk
	regread = readl(MEDIA_CLKEN12);
	regread |= 0x400;
	writel(regread,MEDIA_CLKEN12);

	// Disable media smmu bypass
	regread = readl(MEDIA_CTRL5);
	regread &= (~(1<<2));
	writel(regread,MEDIA_CTRL5);

	// Disable media smmu reset
	regread = readl(MEDIA_RSTDIS);
	regread |= 0x40;
	writel(regread,MEDIA_RSTDIS);

	// Enable media smmu clk
	regread = readl(MEDIA_CLKEN);
	regread |= 0x100;
	writel(regread,MEDIA_CLKEN);

	// Set axi id
	smmu_regs->SMMU_AXIID = 0xC70000FF;
	// Code from hisilicon: if (smmu_level_zero_cache_enable) smmu_regs->SMMU_AXIID = 0x870000ff
	// iommu page size 4K
	smmu_regs->SMMU_CTRL = 0x000001A6;
	// Clear interrupt pa address
	smmu_regs->SMMU_IPTPA = 0x00000000;
	// Clear interrupt
	smmu_regs->SMMU_INTCLR = 0xFF;
	// Set page table phy_addr for ptable and preload
	smmu_regs->SMMU_PTBR= 0; // FIXME
	smmu_regs->SMMU_START= 0; // FIXME
	// Set the page table memory size
	smmu_regs->SMMU_END= 0x200000; // FIXME
	smmu_regs->SMMU_ENABLE= 0x3;

	printf("[SMMU] SMMU_CTRL = %x\n",smmu_regs->SMMU_CTRL);
	printf("[SMMU] SMMU_ENABLE = %x\n",smmu_regs->SMMU_ENABLE);
	printf("[SMMU] SMMU_PTBR = %x\n",smmu_regs->SMMU_PTBR);
	printf("[SMMU] SMMU_START = %x\n",smmu_regs->SMMU_START);
	printf("[SMMU] SMMU_END = %x\n",smmu_regs->SMMU_END);
	printf("[SMMU] SMMU_VERSION = %x\n",smmu_regs->SMMU_VERSION);

	// Copy from hisilicon patch:
	/*set page table phy_addr for ptable and preload*/
	//__smmu_writel(smmu_dev, smmu_dev->pgtable_phy, SMMU_PTBR_OFFSET);
	//SMMU_regs->SMMU_PTBR= smmu_dev->pgtable_phy;
	//__smmu_writel(smmu_dev, smmu_dev->pgtable_phy, SMMU_START_OFFSET);
	//SMMU_regs->SMMU_START= smmu_dev->pgtable_phy;
	/*set the page table memory size*/
	//__smmu_writel(smmu_dev, smmu_dev->pgtable_size, SMMU_END_OFFSET);
	//SMMU_regs->SMMU_END= smmu_dev->pgtable_size;

	return 0;
}

int smmu_setup()
{
	smmu_global_init();
	smmu_enable();
	return 0;
}
