
#ifndef _GICV2_H_
#define _GICV2_H_

#define _gicd ((volatile struct gicv2_gicd *) GICD_BASE)
#define _gicc ((volatile struct gicv2_gicc *) GICC_BASE)
#define _gich ((volatile struct gicv2_gich *) GICH_BASE)
#define _gicv ((volatile struct gicv2_gicc *) GICV_BASE)


#define GIC_IRQ_TYPE_SGI 0
#define GIC_IRQ_TYPE_PPI 1
#define GIC_IRQ_TYPE_SPI 2

#define GICV2_ICFG_1N  2
#define GICV2_ICFG_NN  0
#define GICV2_ICFG_EDGE  1
#define GICV2_ICFG_LEVEL 0

struct gicv2_gicd {
    uint32_t ctlr;
    uint32_t typer;
    uint32_t iidr;
    uint32_t unused0[29];
    uint32_t igrouprn[32];
    uint32_t isenablern[32];
    uint32_t icenablern[32];
    uint32_t ispendrn[32];
    uint32_t icpendrn[32];
    uint32_t isactivern[32];
    uint32_t icactivern[32];
    uint32_t ipriorityrn[255];
    uint32_t unused2;
    uint32_t itargetsrn[8];
    uint32_t unused3[248];
    uint32_t icfgrn[64];
    uint32_t unused4[64];
    uint32_t nsacrn[64];
    uint32_t sgir;
    uint32_t unused5[3];
    uint32_t cpendsgirn[4];
    uint32_t spendsgirn[4];
};

struct gicv2_gicc {
    uint32_t ctlr;
    uint32_t pmr;
    uint32_t bpr;
    uint32_t iar;
    uint32_t eoir;
    uint32_t rpr;
    uint32_t hppir;
    uint32_t abpr;
    uint32_t aiar;
    uint32_t aeoir;
    uint32_t ahppir;
    uint32_t unused0[41];
    uint32_t aprn[4];
    uint32_t nsaprn[4];
    uint32_t unused1[3];
    uint32_t iidr;
    uint32_t unused2[960];
    uint32_t dir;
};

struct gicv2_gich {
    uint32_t hcr;
    uint32_t vtr;
    uint32_t vmcr;
    uint32_t unused0;
    uint32_t misr;
    uint32_t unused1[3];
    uint32_t eisr[2];
    uint32_t unused2[2];
    uint32_t elsr[2];
    uint32_t unused3[46];
    uint32_t apr;
    uint32_t unused4[3];
    uint32_t lr[64];
};

union gicd_sgir_reg {
    uint32_t all;
    struct {
        uint32_t id : 4;
        uint32_t reserved0 : 11;
        uint32_t ns : 1;

        uint32_t targetlist : 8;
        uint32_t filter : 2;
        uint32_t reserved1 : 6;
    };
};


static inline int gicv2_irqid_get_type(int irqid)
{
    if(irqid < 16)
        return GIC_IRQ_TYPE_SGI;
    else if(irqid < 32)
        return GIC_IRQ_TYPE_PPI;
    else
        return GIC_IRQ_TYPE_SPI;
}

static inline int gicv2_get_irqid(int type, int n)
{
    if(type == GIC_IRQ_TYPE_SGI)
        return n;
    else if(type == GIC_IRQ_TYPE_PPI)
        return n + 16;
    else
        return n + 32;
}

/* getters */
static inline int gicv2_dist_get_lines(volatile struct gicv2_gicd *gicd)
{
    return (gicd->typer & 0x1F) + 1;
}

static inline int gicv2_dist_get_max(volatile struct gicv2_gicd *gicd)
{
    return gicv2_dist_get_lines(gicd) * 32;
}

static inline int gicv2_hyp_get_regs(volatile struct gicv2_gich *gich)
{
    return (gich->vtr & 0x3F) + 1;
}

static inline int gicv2_dist_get_enable(volatile struct gicv2_gicd *gicd, int n)
{
    return 1 & (gicd->isenablern[n >> 5] >> (n & 31));
}

static inline int gicv2_dist_get_active(volatile struct gicv2_gicd *gicd, int n)
{
    return 1 & (gicd->isactivern[n >> 5] >> (n & 31));
}
static inline int gicv2_dist_get_pending(volatile struct gicv2_gicd *gicd, int n)
{
    return 1 & (gicd->ispendrn[n >> 5] >> (n & 31));
}

static inline int gicv2_dist_get_config(volatile struct gicv2_gicd *gicd, int n)
{
    return 3 & (gicd->icfgrn[n >> 4] >> (2 * (n & 15)));
}


static inline int gicv2_dist_get_priority(volatile struct gicv2_gicd *gicd, int n)
{
    volatile uint8_t *pb = (uint8_t *) &(gicd->ipriorityrn[0]);
    return pb[n];
}


static inline int gicv2_dist_get_group(volatile struct gicv2_gicd *gicd, int n)
{
    return 1 & (gicd->igrouprn[n >> 5] >> (n & 31));
}

static inline uint8_t gicv2_dist_get_target(volatile struct gicv2_gicd *gicd, int n)
{
    volatile uint8_t *pb = (uint8_t *) &(gicd->itargetsrn[0]);
    return pb[n];
}



/*  setters */
static inline void gicv2_dist_set_enable(volatile struct gicv2_gicd *gicd,
                                  int intnum, int enable)
{
    int index = intnum & 31;
    intnum >>= 5;

    if(enable) {
        gicd->isenablern[intnum] = 1UL << index;
    } else {
        gicd->icenablern[intnum] = 1UL << index;
    }
}

static inline void gicv2_dist_set_active(volatile struct gicv2_gicd *gicd,
                                         int intnum, int active)
{
    int index = intnum & 31;
    intnum >>= 5;

    if(active) {
        gicd->isactivern[intnum] = 1UL << index;
    } else {
        gicd->icactivern[intnum] = 1UL << index;
    }
}

static inline void gicv2_dist_set_pending(volatile struct gicv2_gicd *gicd,
                                         int intnum, int pending)
{
    int index = intnum & 31;
    intnum >>= 5;

    if(pending) {
        gicd->ispendrn[intnum] = 1UL << index;
    } else {
        gicd->icpendrn[intnum] = 1UL << index;
    }
}

static inline void gicv2_dist_set_config(volatile struct gicv2_gicd *gicd, int
                                 intnum, uint32_t conf)
{
    int index = 2 * (intnum & 15);
    intnum >>= 4;

    gicd->icfgrn[intnum] = ((conf & 3) << index) |
          (gicd->icfgrn[intnum]  & ~ (3UL << index))
          ;
}

static inline void gicv2_dist_set_priority(volatile struct gicv2_gicd *gicd,
                                    int intnum, uint32_t prio)
{
    volatile uint8_t *pb = (uint8_t *) &(gicd->ipriorityrn[0]);

    pb[intnum] = (uint8_t) prio;
}


static inline void gicv2_dist_set_group(volatile struct gicv2_gicd *gicd,
                                    int intnum, int grp1)
{
    int index = intnum & 31;
    intnum >>= 5;


    if(grp1)
        gicd->igrouprn[intnum] |= 1UL << index;
    else
        gicd->igrouprn[intnum] &= ~(1UL << index);
}

static inline void gicv2_dist_set_target(volatile struct gicv2_gicd *gicd,
                                    int intnum, uint8_t cpu)
{
    volatile uint8_t *pb = (uint8_t *) &(gicd->itargetsrn[0]);
    pb[intnum] = cpu;
}


#endif /* _GICV2_H_ */
