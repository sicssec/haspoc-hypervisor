#ifndef _MINILIB_H_
#define _MINILIB_H_

#include <stdint.h>
#include <stdarg.h>

/* printf */
extern void vprintf(const char *fmt, va_list args);
extern void printf(const char *, ...);
extern void printf_putchar(int c); /* added by your implementation! */

/* string */
extern int strlen(const char *s);
extern void strcpy(char *dst, const char *src);
extern char *strchr(char *s, char c);
extern int strcmp(const char *s1, const char *s2);
extern int strncmp(const char *s1, const char *s2, int n);

extern void memcpy(void *dst, const void *src, int n);
extern void memset(void *dst, int c, int n);


#endif /* _MINILIB_H_ */
