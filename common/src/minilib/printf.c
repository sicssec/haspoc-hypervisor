
#include <stdint.h>
#include <stdarg.h>

#include "minilib.h"


// ---------------------------------------

static void printf_string(char *str)
{
    if(!str)
        str = "(null)";

    while(*str)
        printf_putchar(*str++);
}



static void printf_int(int i)
{
    int f = 0, neg = 0;
    char buffer[28];

    if(i < 0) {
        neg ++;
        i = - i;
    }
    do {
        buffer[f++] = '0' + (i % 10);
        i /= 10;
    } while(i);

    if(neg) buffer[f++] = '-';

    while(f) {
        printf_putchar( buffer[--f]);
    }
}

static void printf_hex(uint64_t n, int size)
{
    uint64_t h;
    int i;

    for(i = size * 8; i; ) {
        i -= 4;
        h = (n >> i) & 15;

        if(h < 10) h += '0';
        else h += 'A' - 10;

        printf_putchar(h);
    }
}

static void printf_bin(uint64_t n)
{
    int i;
    for(i = 64; i != 0; i--) {
        if( (i != 64) && !(i & 3)) printf_putchar('_');
        printf_putchar( (n >> 63) ? '1' : '0');
        n <<= 1;
    }
}

// -----------------------------------

void vprintf(const char *fmt, va_list args)
{
    int c;

    for(;;) {
        c = *fmt;
        if(c == '\0') return;

        fmt ++;
        if(c == '%') {
            c = *fmt;
            fmt++;

            // sanity check?
            if(c == '\0') {
                printf_putchar(c);
                return;
            }

            switch(c) {

            case 'c':
                printf_putchar(va_arg(args, int));
                break;

            case 's':
                printf_string(va_arg(args, char *));
                break;
            case 'i':
            case 'd':
                printf_int(va_arg(args, int));
                break;
            case 'x':
                printf_hex(va_arg(args, uint32_t), 4);
                break;
            case 'X':
                printf_hex(va_arg(args, uint64_t), 8);
                break;
#if 0
            case '2':
                printf_hex(va_arg(args, uint64_t), 2);
                break;
#endif
            case 'b':
                printf_bin(va_arg(args, uint64_t));
                break;

            case '%':
                printf_putchar(c);
                break;
            default:
                printf_putchar('%');
                printf_putchar(c);
            }

        } else printf_putchar(c);
    }
}


void printf(const char *fmt, ...)
{
    int c;
    va_list args;
    va_start(args, fmt);
    vprintf(fmt, args);
    va_end(args);
}
