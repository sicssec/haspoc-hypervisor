
This folder is here only to make sure that a joined.bin file is created when
processing the payload directory, after bundle.bin has been created by
payload/bundle.

Note that joined.bin creation uses the utility in tools/joiner. Note
also that creation of joined.bin may fail if the gap between hv.bin and
payload.bin is too large.
