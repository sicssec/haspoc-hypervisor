// -*- dts  -*-
/dts-v1/;

#define GIC_SPI 0
#define GIC_PPI 1

#define HI6220_UART0_PCLK      36
#define HI6220_STUB_ACPU0      0

/ {
    idtag = "guest3";
	model = "HiKey Development Board";
	compatible = "hisilicon,hikey", "hisilicon,hi6220";
	hisi,boardid = <0 0 4 3>;
	interrupt-parent = <0x1>;
	#address-cells = <0x2>;
	#size-cells = <0x2>;

	aliases {
		serial0 = &uart0;
	};

	psci {
		compatible = "arm,psci-0.2";
		method = "smc";
	};

	chosen {
		bootargs = "earlycon console=ttyAMA0 earlyprintk debug user_debug=1 loglevel=31 cma=16M";
		stdout-path = &uart0;
	};


	cpus {
		#address-cells = <0x2>;
		#size-cells = <0x0>;

		cpu0: cpu@0 {
			device_type = "cpu";
			compatible = "arm,cortex-a53","arm,armv8";
			reg = <0x0 0x0>;
			enable-method = "psci";
			clocks = <&clock_acpu HI6220_STUB_ACPU0>;
			clock-names = "acpu0";
			clock-latency = <0>;
			operating-points = <
				/* kHz */
				1200000  0
				960000   0
				729000   0
				432000   0
				208000   0
			>;
			#cooling-cells = <2>; /* min followed by max */
			cpu-idle-states = <&CPU_SLEEP_0_0 &CLUSTER_SLEEP_0>;
		};

		cpu1: cpu@1 {
			compatible = "arm,cortex-a53", "arm,armv8";
			device_type = "cpu";
			reg = <0x0 0x1>;
			enable-method = "psci";
			cpu-idle-states = <&CPU_SLEEP_0_0 &CLUSTER_SLEEP_0>;
		};

/*		cpu2: cpu@2 {
			compatible = "arm,cortex-a53", "arm,armv8";
			device_type = "cpu";
			reg = <0x0 0x2>;
			enable-method = "psci";
			cpu-idle-states = <&CPU_SLEEP_0_0 &CLUSTER_SLEEP_0>;
		};
		cpu3: cpu@3 {
			compatible = "arm,cortex-a53", "arm,armv8";
			device_type = "cpu";
			reg = <0x0 0x3>;
			enable-method = "psci";
			cpu-idle-states = <&CPU_SLEEP_0_0 &CLUSTER_SLEEP_0>;
		};
		cpu4: cpu@4 {
			compatible = "arm,cortex-a53", "arm,armv8";
			device_type = "cpu";
			reg = <0x0 0x100>;
			enable-method = "psci";
			clock-latency = <0>;
			cpu-idle-states = <&CPU_SLEEP_0_0 &CLUSTER_SLEEP_0>;
		};
		cpu5: cpu@5 {
			compatible = "arm,cortex-a53", "arm,armv8";
			device_type = "cpu";
			reg = <0x0 0x101>;
			enable-method = "psci";
			cpu-idle-states = <&CPU_SLEEP_0_0 &CLUSTER_SLEEP_0>;
		};
		cpu6: cpu@6 {
			compatible = "arm,cortex-a53", "arm,armv8";
			device_type = "cpu";
			reg = <0x0 0x102>;
			enable-method = "psci";
			cpu-idle-states = <&CPU_SLEEP_0_0 &CLUSTER_SLEEP_0>;
		};
		cpu7: cpu@7 {
			compatible = "arm,cortex-a53", "arm,armv8";
			device_type = "cpu";
			reg = <0x0 0x103>;
			enable-method = "psci";
			cpu-idle-states = <&CPU_SLEEP_0_0 &CLUSTER_SLEEP_0>;
		};

		cpu-map {
			cluster0 {
				core0 {
					cpu = <&cpu0>;
				};
				core1 {
					cpu = <&cpu1>;
				};
				core2 {
					cpu = <&cpu2>;
				};
				core3 {
					cpu = <&cpu3>;
				};
			};
			cluster1 {
				core0 {
					cpu = <&cpu4>;
				};
				core1 {
					cpu = <&cpu5>;
				};
				core2 {
					cpu = <&cpu6>;
				};
				core3 {
					cpu = <&cpu7>;
				};
			};
		};
*/

		idle-states {
			entry-method = "arm,psci";

			CPU_SLEEP_0_0: cpu-sleep-0-0 {
				compatible = "arm,idle-state";
				local-timer-stop;
				arm,psci-suspend-param = <0x0010000>;
				entry-latency-us = <250>;
				exit-latency-us = <500>;
				min-residency-us = <950>;
			};

			CLUSTER_SLEEP_0: cluster-sleep-0 {
				compatible = "arm,idle-state";
				local-timer-stop;
				arm,psci-suspend-param = <0x1010000>;
				entry-latency-us = <600>;
				exit-latency-us = <1100>;
				min-residency-us = <2700>;
				wakeup-latency-us = <1500>;
			};
		};
	};

	memory@00000000 {
		device_type = "memory";
		reg = <0x0 0x00000000 0x0 0x10000000>;
	};

	pmu {
		compatible = "arm,armv8-pmuv3";
//		interrupts = <0x0 0x3c 0x4 0x0 0x3d 0x4 0x0 0x3e 0x4 0x0 0x3f 0x4>;

		interrupts = <GIC_SPI 18 4>,
			     <GIC_SPI 22 4>,
			     <GIC_SPI 26 4>,
			     <GIC_SPI 30 4>,
			     <GIC_SPI 02 4>,
			     <GIC_SPI 06 4>;
	};


	clock_acpu: clock_acpu {
		compatible = "hisilicon,hi6220-clock-stub";
		#address-cells = <2>;
		#size-cells = <2>;
		reg = <0x0 0xFFF81AF4 0x0 0x000C>,
		      <0x0 0xF7510000 0x0 0x1000>;

		#clock-cells = <1>;
	};

	ao_ctrl: ao_ctrl {
		compatible = "hisilicon,hi6220-aoctrl", "syscon";
		#address-cells = <1>;
		#size-cells = <1>;
		reg = <0x0 0xC0100000 0x0 0x2000>;

                #clock-cells = <1>;
	};

	timer {
		compatible = "arm,armv8-timer";
		interrupts = <GIC_PPI 13 0xff08>,
			     <GIC_PPI 14 0xff08>,
			     <GIC_PPI 11 0xff08>,
			     <GIC_PPI 10 0xff08>;
		clock-frequency = <1200000>;
	};



	/* hypervisor mapping of peripherals... */

	smb {
		compatible = "simple-bus";
		#address-cells = <0x2>;
		#size-cells = <0x1>;
		ranges = <0x0 0x0 0x0 0xC0000000 0x00400000>;
		/* temp fix
			<0x0 0x0 0x0 0xC0000000 0x00200000>,
			<0x1 0x0 0x0 0xC0200000 0x00200000>;
		*/


		/* Directly mapped */
		mapped@1,00000000 {
			compatible = "simple-bus";
			#address-cells = <0x1>;
			#size-cells = <0x1>;
			ranges = <0x0 0x0 0x00000000 0x00400000>;

			/* ARM GICv2 */
			gic: interrupt-controller@000000 {
				compatible = "arm,cortex-a15-gic";
				#interrupt-cells = <0x3>;
				#address-cells = <0x0>;
				interrupt-controller;
				reg = <0x00000 0x1000 0x01000 0x1000>;
				interrupts = <GIC_PPI 0x9 0xf04>;
				linux,phandle = <0x1>;
				phandle = <0x1>;
			};

			uart0: uart@0x4000 {
				compatible = "arm,pl011", "arm,primecell";
				reg = <0x4000 0x1000>;
				interrupts = <0 0 4>;	/* 0, First SPI:0, 4 */
				clocks = <&ao_ctrl HI6220_UART0_PCLK>, <&ao_ctrl HI6220_UART0_PCLK>;
				clock-names = "uartclk", "apb_pclk";
			};

			igc-channels@0x200000 {
				/* Change compatible to "sth,igc" if you want to use the
				igc char device driver instead */
				compatible = "sth,eth";
				reg = <0x200000 0xC1000>; /* total size for all channels */
				nr_of_channels = <1>;

				channel0 {
					ctrl  = <0x0 0x1000>;
					write = <0x1000 0x60000>;
					read  = <0x61000 0x60000>;
					interrupts = <0 17 0>; /* 47 = 32 + 17 */
					interrupt-parent = <&gic>;
					other-end = "Enc"; /* max 20 chars */
				};
			};
		};
	};
};
