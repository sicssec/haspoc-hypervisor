// -*- dts  -*-
/dts-v1/;

#define GIC_SPI 0
#define GIC_PPI 1

#define HI6220_UART0_PCLK      36
#define HI6220_STUB_ACPU0      0
#define HI6220_EDMAC_ACLK      11
#define HI6220_USBOTG_HCLK     7
#define HI6220_CLK_TCXO        2
#define HI6220_MMC2_CIUCLK     6
#define HI6220_MMC2_CLK		   5

#define MUX_M0                0
#define MUX_M1                1
#define PULL_DIS              (0)
#define PULL_UP               (1 << 0)
#define PULL_DOWN             (1 << 1)
#define DRIVE_MASK            (7 << 4)
#define DRIVE1_02MA           (0 << 4)
#define DRIVE1_04MA           (1 << 4)
#define DRIVE1_08MA           (2 << 4)

/ {
	idtag = "guest1";
	model = "HiKey Development Board";
	compatible = "hisilicon,hikey", "hisilicon,hi6220";
	hisi,boardid = <0 0 4 3>;
	interrupt-parent = <0x1>;
	#address-cells = <0x2>;
	#size-cells = <0x2>;

	aliases {
		serial0 = &uart0;
	};

	psci {
		compatible = "arm,psci-0.2";
		method = "smc";
	};

	chosen {
		bootargs = "earlycon console=ttyAMA0 earlyprintk debug user_debug=1 loglevel=31 cma=16M";
		stdout-path = &uart0;
	};


	cpus {
                #address-cells = <2>;
                #size-cells = <0>;
/*
                cpu-map {
                        cluster0 {
                                core0 {
                                        cpu = <&cpu0>;
                                };
                                core1 {
                                        cpu = <&cpu1>;
                                };
                                core2 {
                                        cpu = <&cpu2>;
                                };
                                core3 {
                                        cpu = <&cpu3>;
                                };
                        };
                        cluster1 {
                                core0 {
                                        cpu = <&cpu4>;
                                };
                                core1 {
                                        cpu = <&cpu5>;
                                };
                                core2 {
                                        cpu = <&cpu6>;
                                };
                                core3 {
                                        cpu = <&cpu7>;
                                };
                        };
                };
*/
                idle-states {
                        entry-method = "psci";

                        CPU_SLEEP: cpu-sleep {
                                compatible = "arm,idle-state";
                                local-timer-stop;
                                arm,psci-suspend-param = <0x0010000>;
                                entry-latency-us = <700>;
                                exit-latency-us = <250>;
                                min-residency-us = <1000>;
                        };

                        CLUSTER_SLEEP: cluster-sleep {
                                compatible = "arm,idle-state";
                                local-timer-stop;
                                arm,psci-suspend-param = <0x1010000>;
                                entry-latency-us = <1000>;
                                exit-latency-us = <700>;
                                min-residency-us = <2700>;
                                wakeup-latency-us = <1500>;
                        };
                };

                cpu0: cpu@0 {
                        compatible = "arm,cortex-a53", "arm,armv8";
                        device_type = "cpu";
                        reg = <0x0 0x0>;
                        enable-method = "psci";
                        clocks = <&stub_clock 0>;
                        clock-latency = <100000>;
                        operating-points = <
                                /* kHz */
                                1200000  0
                                960000   0
                                729000   0
                                432000   0
                                208000   0
                        >;
                        #cooling-cells = <2>; /* min followed by max */
                        cpu-idle-states = <&CPU_SLEEP &CLUSTER_SLEEP>;
                };

                cpu1: cpu@1 {
                        compatible = "arm,cortex-a53", "arm,armv8";
                        device_type = "cpu";
                        reg = <0x0 0x1>;
                        enable-method = "psci";
                        cpu-idle-states = <&CPU_SLEEP &CLUSTER_SLEEP>;
                };

                cpu2: cpu@2 {
                        compatible = "arm,cortex-a53", "arm,armv8";
                        device_type = "cpu";
                        reg = <0x0 0x2>;
                        enable-method = "psci";
                        cpu-idle-states = <&CPU_SLEEP &CLUSTER_SLEEP>;
                };
/*
                cpu3: cpu@3 {
                        compatible = "arm,cortex-a53", "arm,armv8";
                        device_type = "cpu";
                        reg = <0x0 0x3>;
                        enable-method = "psci";
                        cpu-idle-states = <&CPU_SLEEP &CLUSTER_SLEEP>;
                };

                cpu4: cpu@100 {
                        compatible = "arm,cortex-a53", "arm,armv8";
                        device_type = "cpu";
                        reg = <0x0 0x100>;
                        enable-method = "psci";
                        cpu-idle-states = <&CPU_SLEEP &CLUSTER_SLEEP>;
                };

                cpu5: cpu@101 {
                        compatible = "arm,cortex-a53", "arm,armv8";
                        device_type = "cpu";
                        reg = <0x0 0x101>;
                        enable-method = "psci";
                        cpu-idle-states = <&CPU_SLEEP &CLUSTER_SLEEP>;
                };

                cpu6: cpu@102 {
                        compatible = "arm,cortex-a53", "arm,armv8";
                        device_type = "cpu";
                        reg = <0x0 0x102>;
                        enable-method = "psci";
                        cpu-idle-states = <&CPU_SLEEP &CLUSTER_SLEEP>;
                };

                cpu7: cpu@103 {
                        compatible = "arm,cortex-a53", "arm,armv8";
                        device_type = "cpu";
                        reg = <0x0 0x103>;
                        enable-method = "psci";
                        cpu-idle-states = <&CPU_SLEEP &CLUSTER_SLEEP>;
                };
*/
        };

	memory@00000000 {
		device_type = "memory";
		reg = <0x0 0x08000000 0x0 0x10000000>;
	};

	pmu {
		compatible = "arm,armv8-pmuv3";
//		interrupts = <0x0 0x3c 0x4 0x0 0x3d 0x4 0x0 0x3e 0x4 0x0 0x3f 0x4>;

		interrupts = <GIC_SPI 18 4>,
			     <GIC_SPI 22 4>,
			     <GIC_SPI 26 4>,
			     <GIC_SPI 30 4>,
			     <GIC_SPI 02 4>,
			     <GIC_SPI 06 4>;
	};

	timer {
		compatible = "arm,armv8-timer";
		interrupts = <GIC_PPI 13 0xff08>,
			     <GIC_PPI 14 0xff08>,
			     <GIC_PPI 11 0xff08>,
			     <GIC_PPI 10 0xff08>;
		clock-frequency = <1200000>;
	};



	/* hypervisor mapping of peripherals... */
	smb {
		compatible = "simple-bus";
		#address-cells = <0x2>;
		#size-cells = <0x1>;
		ranges = <0x0 0x0 0x0 0xC0000000 0x01000000>;
		/* temp fix
			<0x0 0x0 0x0 0xC0000000 0x00200000>,
			<0x1 0x0 0x0 0xC0200000 0x00200000>;
		*/

		/* Directly mapped */
		mapped@1,00000000 {
			compatible = "simple-bus";
			#address-cells = <0x1>;
			#size-cells = <0x1>;
			ranges = <0x0 0x0 0x00000000 0x00400000>;

			/* ARM GICv2 */
			gic: interrupt-controller@000000 {
				compatible = "arm,cortex-a15-gic";
				#interrupt-cells = <0x3>;
				#address-cells = <0x0>;
				interrupt-controller;
				reg = <0x00000 0x1000 0x01000 0x1000>;
				interrupts = <GIC_PPI 0x9 0xf04>;
				linux,phandle = <0x1>;
				phandle = <0x1>;
			};

			uart0: uart@0x4000 {
				compatible = "arm,pl011", "arm,primecell";
				reg = <0x4000 0x1000>;
				interrupts = <0 0 4>;	/* 0, First SPI:0, 4 */
				clocks = <&ao_ctrl HI6220_UART0_PCLK>,
                                         <&ao_ctrl HI6220_UART0_PCLK>;
				clock-names = "uartclk", "apb_pclk";
			};

			/* igc-channels@0x200000 { */
			/* 	/1* Change compatible to "sth,eth" if you want to use the */
			/* 	virtual ethernet driver instead *1/ */
			/* 	compatible = "sth,igc"; */
			/* 	reg = <0x200000 0x21000>; /1* total size for all channels *1/ */
			/* 	nr_of_channels = <1>; */

			/* 	channel0 { */
			/* 	ctrl  = <0x0 0x1000>; */
			/* 	write = <0x1000 0x10000>; */
			/* 	read  = <0x11000 0x10000>; */
			/* 	interrupts = <0 16 0>; /1* 48 = 32 + 16 *1/ */
			/* 	interrupt-parent = <&gic>; */
			/* 	other-end = "Encryptor"; /1* max 20 chars *1/ */
			/* 	}; */
			/* }; */
                };
        };

        soc {
                compatible = "simple-bus";
                #address-cells = <2>;
                #size-cells = <2>;
                ranges;

                ao_ctrl: ao_ctrl@0xC0100000 {
                    compatible = "hisilicon,hi6220-aoctrl", "syscon";
                    reg = <0x0 0xC0100000 0x0 0x2000>;
                    #clock-cells = <1>;
                };

                sys_ctrl: sys_ctrl@0xC0104000 {
                    compatible = "hisilicon,hi6220-sysctrl", "syscon";
                    reg = <0x0 0xC0104000 0x0 0x2000>;
                    #clock-cells = <1>;
                    #reset-cells = <1>;
                };

                pm_ctrl: pm_ctrl@0xC0106000 {
                    compatible = "hisilicon,hi6220-pmctrl", "syscon";
                    reg = <0x0 0xC0106000 0x0 0x1000>;
                    #clock-cells = <1>;
                };

                stub_clock: stub_clock {
                    compatible = "hisilicon,hi6220-stub-clk";
                    #clock-cells = <1>;
                };

                pmx0: pinmux@0xC0107000 {
                    compatible = "pinctrl-single";
                    reg = <0x0 0xC0107000  0x0 0x27c>;
                    #address-cells = <1>;
                    #size-cells = <1>;
                    /* #gpio-range-cells = <3>; */
                    pinctrl-single,register-width = <32>;
                    pinctrl-single,function-mask = <7>;
                    range: gpio-range {
                        #pinctrl-single,gpio-range-cells = <3>;
                    };

                    sdio_pmx_func: sdio_pmx_func {
                            pinctrl-single,pins = <
                                    0x128  MUX_M0   /* SDIO_CLK     (IOMG074) */
                                    0x12c  MUX_M0   /* SDIO_CMD     (IOMG075) */
                                    0x130  MUX_M0   /* SDIO_DATA0   (IOMG076) */
                                    0x134  MUX_M0   /* SDIO_DATA1   (IOMG077) */
                                    0x138  MUX_M0   /* SDIO_DATA2   (IOMG078) */
                                    0x13c  MUX_M0   /* SDIO_DATA3   (IOMG079) */
                            >;
                    };
                    sdio_pmx_idle: sdio_pmx_idle {
                            pinctrl-single,pins = <
                                    0x128  MUX_M1   /* SDIO_CLK     (IOMG074) */
                                    0x12c  MUX_M1   /* SDIO_CMD     (IOMG075) */
                                    0x130  MUX_M1   /* SDIO_DATA0   (IOMG076) */
                                    0x134  MUX_M1   /* SDIO_DATA1   (IOMG077) */
                                    0x138  MUX_M1   /* SDIO_DATA2   (IOMG078) */
                                    0x13c  MUX_M1   /* SDIO_DATA3   (IOMG079) */
                            >;
                    };
                };

                pmx1: pinmux@0xC0107800 {
                    compatible = "pinconf-single";
                    reg = <0x0 0xC0107800 0x0 0x28c>;
                    #address-cells = <1>;
                    #size-cells = <1>;
                    pinctrl-single,register-width = <32>;

                    sdio_clk_cfg_func: sdio_clk_cfg_func {
                            pinctrl-single,pins = <
                                    0x134  0x0      /* SDIO_CLK     (IOCFG077) */
                            >;
                            pinctrl-single,bias-pulldown  = <PULL_DIS  PULL_DOWN PULL_DIS  PULL_DOWN>;
                            pinctrl-single,bias-pullup    = <PULL_DIS  PULL_UP   PULL_DIS  PULL_UP>;
                            pinctrl-single,drive-strength = <DRIVE1_08MA DRIVE_MASK>;
                    };
                    sdio_clk_cfg_idle: sdio_clk_cfg_idle {
                            pinctrl-single,pins = <
                                    0x134  0x0      /* SDIO_CLK     (IOCFG077) */
                            >;
                            pinctrl-single,bias-pulldown  = <PULL_DOWN PULL_DOWN PULL_DIS  PULL_DOWN>;
                            pinctrl-single,bias-pullup    = <PULL_DIS  PULL_UP   PULL_DIS  PULL_UP>;
                            pinctrl-single,drive-strength = <DRIVE1_02MA DRIVE_MASK>;
                    };

                    sdio_cfg_func: sdio_cfg_func {
                            pinctrl-single,pins = <
                                    0x138  0x0      /* SDIO_CMD     (IOCFG078) */
                                    0x13c  0x0      /* SDIO_DATA0   (IOCFG079) */
                                    0x140  0x0      /* SDIO_DATA1   (IOCFG080) */
                                    0x144  0x0      /* SDIO_DATA2   (IOCFG081) */
                                    0x148  0x0      /* SDIO_DATA3   (IOCFG082) */
                            >;
                            pinctrl-single,bias-pulldown  = <PULL_DIS  PULL_DOWN PULL_DIS  PULL_DOWN>;
                            pinctrl-single,bias-pullup    = <PULL_UP   PULL_UP   PULL_DIS  PULL_UP>;
                            pinctrl-single,drive-strength = <DRIVE1_04MA DRIVE_MASK>;
                    };
                    sdio_cfg_idle: sdio_cfg_idle {
                            pinctrl-single,pins = <
                                    0x138  0x0      /* SDIO_CMD     (IOCFG078) */
                                    0x13c  0x0      /* SDIO_DATA0   (IOCFG079) */
                                    0x140  0x0      /* SDIO_DATA1   (IOCFG080) */
                                    0x144  0x0      /* SDIO_DATA2   (IOCFG081) */
                                    0x148  0x0      /* SDIO_DATA3   (IOCFG082) */
                            >;
                            pinctrl-single,bias-pulldown  = <PULL_DIS  PULL_DOWN PULL_DIS  PULL_DOWN>;
                            pinctrl-single,bias-pullup    = <PULL_UP   PULL_UP   PULL_DIS  PULL_UP>;
                            pinctrl-single,drive-strength = <DRIVE1_02MA DRIVE_MASK>;
                    };
                };

                pmic: pmic@0xC0108000 {
                    compatible = "hisilicon,hi655x-pmic";
                    reg = <0x0 0xC0108000 0x0 0x1000>;
                    #interrupt-cells = <2>;
                    interrupt-controller;
                    pmic-gpios = <&gpio1 2 0>;
                    status = "okay";
                };

                dwmmc_2: dwmmc2@0xC0109000 {
                    compatible = "hisilicon,hi6220-dw-mshc";
                    status = "okay";
                    num-slots = <0x1>;
                    reg = <0x0 0xC0109000 0x0 0x1000>;
                    interrupts = <0x0 8 0x4>;
                    clocks = <&sys_ctrl HI6220_MMC2_CIUCLK>, <&sys_ctrl HI6220_MMC2_CLK>;
                    clock-names = "ciu", "biu";
                    bus-width = <0x4>;
                    broken-cd;
                    pinctrl-names = "default", "idle";
                    pinctrl-0 = <&sdio_pmx_func &sdio_clk_cfg_func &sdio_cfg_func>;
                    pinctrl-1 = <&sdio_pmx_idle &sdio_clk_cfg_idle &sdio_cfg_idle>;

                    ti,non-removable;
                    non-removable;
                    /* WL_EN */
                    vmmc-supply = <&wlan_en_reg>;

                    #address-cells = <0x1>;
                    #size-cells = <0x0>;
                    wlcore: wlcore@2 {
                        compatible = "ti,wl1835";
                        reg = <2>;      /* sdio func num */
                        /* WL_IRQ, WL_HOST_WAKE_GPIO1_3 */
                        interrupt-parent = <&gpio1>;
                        interrupts = <3 1> /* IRQ_TYPE_EDGE_RISING */;
                    };
                };

                wlan_en_reg: fixedregulator@1 {
                    compatible = "regulator-fixed";
                    regulator-name = "wlan-en-regulator";
                    regulator-min-microvolt = <1800000>;
                    regulator-max-microvolt = <1800000>;
                    /* WLAN_EN GPIO */
                    gpio = <&gpio0 5 0>;
                    /* WLAN card specific delay */
                    startup-delay-us = <70000>;
                    enable-active-high;
                };

                fixed_5v_hub: regulator@0 {
                        compatible = "regulator-fixed";
                        regulator-name = "fixed_5v_hub";
                        regulator-min-microvolt = <5000000>;
                        regulator-max-microvolt = <5000000>;
                        regulator-boot-on;
                        gpio = <&gpio0 7 0>;
                        regulator-always-on;
                };

                usb_phy: usbphy {
                        compatible = "hisilicon,hi6220-usb-phy";
                        #phy-cells = <0>;
                        phy-supply = <&fixed_5v_hub>;
                        hisilicon,peripheral-syscon = <&sys_ctrl>;
                };

                usb: usb@0xC0120000 {
                        compatible = "hisilicon,hi6220-usb";
                        reg = <0x0 0xC0120000 0x0 0x40000>;
                        phys = <&usb_phy>;
                        phy-names = "usb2-phy";
                        clocks = <&sys_ctrl HI6220_USBOTG_HCLK>;
                        clock-names = "otg";
                        dr_mode = "otg";
                        g-use-dma;
                        g-rx-fifo-size = <512>;
                        g-np-tx-fifo-size = <128>;
                        g-tx-fifo-size = <128 128 128 128 128 128>;
                        interrupts = <0 11 0x4>;
                };

                gpio0: gpio@0xC010A000 {
                        compatible = "arm,pl061", "arm,primecell";
                        reg = <0x0 0xC010A000 0x0 0x1000>;
                        interrupts = <0 1 0x4>;
                        gpio-controller;
                        #gpio-cells = <2>;
                        interrupt-controller;
                        #interrupt-cells = <2>;
                        clocks = <&ao_ctrl 2>;
                        clock-names = "apb_pclk";
                        status = "ok";
                };

                gpio1: gpio@0xC010B000 {
                        compatible = "arm,pl061", "arm,primecell";
                        reg = <0x0 0xC010B000 0x0 0x1000>;
                        interrupts = <0 2 0x4>;
                        gpio-controller;
                        #gpio-cells = <2>;
                        interrupt-controller;
                        #interrupt-cells = <2>;
                        clocks = <&ao_ctrl 2>;
                        clock-names = "apb_pclk";
                        status = "ok";
                };

                gpio2: gpio@0xC010C000 {
                    compatible = "arm,pl061", "arm,primecell";
                    reg = <0x0 0xC010C000 0x0 0x1000>;
                    interrupts = <0 3 0x4>;
                    gpio-controller;
                    #gpio-cells = <2>;
                    interrupt-controller;
                    #interrupt-cells = <2>;
                    clocks = <&ao_ctrl 2>;
                    clock-names = "apb_pclk";
                    status = "ok";
                };
	};

        /* LCB: PMU_IRQ_N_GPIO1_2 */
        gpio_pmu_irq_n:gpio_pmu_irq_n {
            gpios = <&gpio1 2 0>;
        };
        gpio_usb_hub_reset_n:gpio_usb_hub_reset_n {
            gpios = <&gpio0 2 0>;
        };
        /* LCB: USB_SEL_GPIO0_3 */
        gpio_usb_sel:gpio_usb_sel {
            gpios = <&gpio0 3 0>;
        };
        gpio_usb_dev_det:gpio_usb_dev_det {
            gpios = <&gpio0 7 0>;
        };
        /* LCB: USB_ID_DET_GPIO2_5 */
        gpio_usb_id_det:gpio_usb_id_det {
            gpios = <&gpio2 5 0>;
        };
        /* LCB: USB_VBUS_DET_GPIO2_6 */
        gpio_vbus_det:gpio_vbus_det {
            gpios = <&gpio2 6 0>;
        };

    eme: eme_gpio {
        compatible = "eme-gpio";
        gpio = <&gpio2 0 0>;
    };
};
