// -*- dts  -*-
/dts-v1/;

#include "arch_defs.h"

// this is the hypervisor configuration devicetree.
// it is not forwarded to the guests

/ {
    id = "2 x linux";
    version = <0x00010000>;

    /* CPU mapping: <flags, mpidr> */
    topology =
        <0 0x00000000>,
        <0 0x00000001>,
        <0 0x00000002>,
        <0 0x00000003>
        ;

    regions =
        /* type, start-hi, start-lo, end-hi, end-lo */
        <0 0x00000000 0x00000000 0x00000000 0x00004000>, /* trustzone */
        <0 0x00000000 0x80000000 0x00000000 0x84000000>, /* payload */
        <0 0x00000000 0x88000000 0x00000000 0x88018000>, /* hypervisor static ROM + RAM */
        <2 0x00000000 0x88020000 0x00000000 0x88190000>,
        <3 0x00000008 0x80000000 0x00000008 0x80200000>,
        <4 0x00000000 0x88190000 0x00000001 0x00000000>,
        <4 0x00000008 0x80200000 0x00000009 0x00000000>
        ;


    guest@0 {
        id = "linux 1";
        cpumask = <5>; // using cpu 0 and 2
        flags = <0x00000001>;
        entry = <0x00080000 0x00010000 0>;

        /* type flags vadr64 size64 */
        ram = <4 0 0x00000000 0x00000000 0x00000000 0x08000000 >;

        driver@0 {
            type = "vgic";
            vadr = <0xC0000000 0xC0002000>;
            irq-map =
                <0 0 0>, /* SGI-0 = IPI0: Rescheduling interrupts */
                <0 1 1>, /* SGI-1 = IPI1: Function call interrupts */
                <0 2 2>, /* SGI-2 = IPI2: CPU stop interrupts */
                <0 3 3>, /* SGI-3 = IPI3: Timer broadcast interrupts */
                <0 4 4>, /* SGI-4 = IPI4: IRQ work interrupts */
                <0 27 27>,
                <0 30 30>,
                <0xC0000000 32 38>,  /* UART SPI-00*/
                <0xA0000001 48 0>   /* IGC */
                ;
        };

        firmware@0 {
            filename = "guest1.dtb";
            adr = <0x00010000>;
        };

        firmware@1 {
            filename = "linux.bin";
            adr = <0x00080000>;
        };

        device@0 {
            vadr = <0xC0004000>;
            padr = <ARCH_UART1_BASE>;
            pages = <1>;
        };
    };

    guest@1 {
        id = "linux 2";
        cpumask = <2>; // using cpu 1
        flags = <0x00000001>;
        entry = <0x00080000 0x00010000 0>;

        /* type flags vadr64 size64 */
        ram = <4 0 0x00000000 0x00000000 0x00000000 0x08000000>;

        driver@1 {
            type = "vgic";
            vadr = <0xC0000000 0xC0002000>;
            irq-map =
                <0 0 0>, /* SGI-0 = IPI0: Rescheduling interrupts */
                <0 1 1>, /* SGI-1 = IPI1: Function call interrupts */
                <0 2 2>, /* SGI-2 = IPI2: CPU stop interrupts */
                <0 3 3>, /* SGI-3 = IPI3: Timer broadcast interrupts */
                <0 4 4>, /* SGI-4 = IPI4: IRQ work interrupts */
                <0 27 27>,
                <0 30 30>,
                <0 32 39>,
                <1 48 0>,   /* IGC: channel0 */
                <1 49 0>   /* IGC: channel1 */
                ;
        };


        firmware@0 {
            filename = "guest2.dtb";
            adr = <0x00010000>;
        };

        firmware@1 {
            filename = "linux.bin";
            adr = <0x00080000>;
        };

        device@0 {
            vadr = <0xC0004000>;
            padr = <ARCH_UART2_BASE>;
            pages = <1>;
        };
    };

#if 0
/* to get output from guest 3 you have to enable fourth terminal (uncomment UART3) in:
 *      hypervisor/src/target/fvp/init.c
 */
    guest@2 {
        id = "linux 3";
        cpumask = <8>; // using cpu 3
        flags = <0x00000001>;
        memory = <0x00000000 0x08000000>;
        entry = <0x00080000 0x00010000 0>;

        driver@0 {
            type = "vgic";
            vadr = <0xC0000000 0xC0002000>;
            irq-map =
                <0 0 0>, /* SGI-0 = IPI0: Rescheduling interrupts */
                <0 1 1>, /* SGI-1 = IPI1: Function call interrupts */
                <0 2 2>, /* SGI-2 = IPI2: CPU stop interrupts */
                <0 3 3>, /* SGI-3 = IPI3: Timer broadcast interrupts */
                <0 4 4>, /* SGI-4 = IPI4: IRQ work interrupts */
                <0 27 27>,
                <0 30 30>,
                <0 32 40>,
                <1 48 0>   /* IGC */
                ;
        };


        firmware@0 {
            filename = "guest3.dtb";
            adr = <0x00010000>;
        };

        firmware@1 {
            filename = "linux.bin";
            adr = <0x00080000>;
        };

        device@0 {
            vadr = <0xC0004000>;
            padr = <ARCH_UART3_BASE>;
            pages = <1>;
        };
    };
#endif

    igc {
        channel@0 {
            id = "test channel 1";

            endpoint1 {
                guest = "linux 1";
                pages = <4>;
                irq = <48>;
            };

            endpoint2 {
                guest = "linux 2";
                pages = <1>;
                irq = <48>;
            };
        };
#if 0
	channel@1 {
            id = "test channel 2";

            endpoint1 {
                guest = "linux 2";
                pages = <3>;
                irq = <49>;
            };

            endpoint2 {
                guest = "linux 3";
                pages = <2>;
                irq = <48>;
            };
        };
#endif
    };


};
