// -*- dts  -*-
/dts-v1/;

/ {
	model = "dummy";
	compatible = "arm";
	interrupt-parent = <0x1>;
	#address-cells = <0x2>;
	#size-cells = <0x2>;

	aliases {
		serial0 = &uart0;
	};

	chosen {
		bootargs = "earlycon console=ttyAMA0 earlyprintk debug user_debug=1 loglevel=31";
		stdout-path = &uart0;
	};


	cpus {
		#address-cells = <0x2>;
		#size-cells = <0x0>;

		cpu@0 {
			device_type = "cpu";
			compatible = "arm,armv8";
			reg = <0x0 0x0>;
			enable-method = "psci";
		};
		/*
		cpu@1 {
			device_type = "cpu";
			compatible = "arm,armv8";
			reg = <0x0 0x1>;
			enable-method = "psci";
		};

		cpu@2 {
			device_type = "cpu";
			compatible = "arm,armv8";
			reg = <0x0 0x2>;
			enable-method = "psci";
		};

		cpu@3 {
			device_type = "cpu";
			compatible = "arm,armv8";
			reg = <0x0 0x3>;
			enable-method = "psci";
		};
		*/
	};

	memory@00000000 {
		device_type = "memory";
		reg = <0x0 0x00000000 0x0 0x08000000>;
	};

	pmu {
		compatible = "arm,armv8-pmuv3";
		interrupts = <0x0 0x3c 0x4 0x0 0x3d 0x4 0x0 0x3e 0x4 0x0 0x3f 0x4>;
	};

	/* ARM GICv2 ? */
	gic: interrupt-controller@C0000000 {
        compatible = "arm,cortex-a15-gic";
        #interrupt-cells = <0x3>;
        #address-cells = <0x0>;
        interrupt-controller;
        reg = <0x0 0xC0000000 0 0x1000>,
              <0x0 0xC0001000 0 0x1000>;
        interrupts = <0x1 0x9 0xf04>;
	};

	/* FVP clocks and timers */
	timer {
		compatible = "arm,armv8-timer";
		interrupts =
		<0x1 0xd 0xff01>,   // secure
		<0x1 0xe 0xff01>,   // non-secure
		<0x1 0xb 0xff01>,   // virtual
		<0x1 0xa 0xff01>    // hypervisor
		;
		clock-frequency = <0x5f5e100>;
	};

	v2m_clk24mhz: clk24mhz {
		compatible = "fixed-clock";
		#clock-cells = <0>;
		clock-frequency = <24000000>;
		clock-output-names = "v2m:clk24mhz";
	};

	v2m_refclk1mhz: refclk1mhz {
		compatible = "fixed-clock";
		#clock-cells = <0>;
		clock-frequency = <1000000>;
		clock-output-names = "v2m:refclk1mhz";
	};

	v2m_refclk32khz: refclk32khz {
		compatible = "fixed-clock";
		#clock-cells = <0>;
		clock-frequency = <32768>;
		clock-output-names = "v2m:refclk32khz";
	};


	/* hypervisor mapping of peripherals... */

	smb {
		compatible = "simple-bus";
		#address-cells = <0x2>;
		#size-cells = <0x1>;
		ranges = <0x0 0x0 0x0 0xC0000000 0x00400000>;
		/* temp fix
			<0x0 0x0 0x0 0xC0000000 0x00200000>,
			<0x1 0x0 0x0 0xC0200000 0x00200000>;
		*/

        #interrupt-cells = <1>;
		interrupt-map-mask = <0 0 127>;
		interrupt-map =
				<0 0  0 &gic 0  0 4>,
				<0 0  1 &gic 0  1 4>,
				<0 0  2 &gic 0  2 4>,
				<0 0  3 &gic 0  3 4>,
				<0 0  4 &gic 0  4 4>,
				<0 0  5 &gic 0  5 4>,
				<0 0  6 &gic 0  6 4>,
				<0 0  7 &gic 0  7 4>,
				<0 0  8 &gic 0  8 4>,
				<0 0  9 &gic 0  9 4>,
				<0 0 10 &gic 0 10 4>,
				<0 0 11 &gic 0 11 4>,
				<0 0 12 &gic 0 12 4>,
				<0 0 13 &gic 0 13 4>,
				<0 0 14 &gic 0 14 4>,
				<0 0 15 &gic 0 15 4>,
				<0 0 16 &gic 0 16 4>,
				<0 0 17 &gic 0 17 4>,
				<0 0 18 &gic 0 18 4>,
				<0 0 19 &gic 0 19 4>,
				<0 0 20 &gic 0 20 4>,
				<0 0 21 &gic 0 21 4>,
				<0 0 22 &gic 0 22 4>,
				<0 0 23 &gic 0 23 4>,
				<0 0 24 &gic 0 24 4>,
				<0 0 25 &gic 0 25 4>,
				<0 0 26 &gic 0 26 4>,
				<0 0 27 &gic 0 27 4>,
				<0 0 28 &gic 0 28 4>,
				<0 0 29 &gic 0 29 4>,
				<0 0 30 &gic 0 30 4>,
				<0 0 31 &gic 0 31 4>,
				<0 0 32 &gic 0 32 4>,
				<0 0 33 &gic 0 33 4>,
				<0 0 34 &gic 0 34 4>,
				<0 0 35 &gic 0 35 4>,
				<0 0 36 &gic 0 36 4>,
				<0 0 37 &gic 0 37 4>,
				<0 0 38 &gic 0 38 4>,
				<0 0 39 &gic 0 39 4>,
				<0 0 40 &gic 0 40 4>,
				<0 0 41 &gic 0 41 4>,
				<0 0 42 &gic 0 42 4>,
				<0 0 61 &gic 0 61 4>,
				<0 0 62 &gic 0 62 4>,
				<0 0 63 &gic 0 63 4>;


		/* Directly mapped */
		mapped@0,00000000 {
			compatible = "arm,amba-bus", "simple-bus";
			#address-cells = <0x1>;
			#size-cells = <0x1>;
			ranges = <0x0 0x0 0x00000000 0x00400000>;

			/* uart */
			uart0: uart@0x4000 {
				compatible = "arm,primecell", "arm,pl011";
				reg = <0x4000 0x1000>;
				interrupts = <0x0>;
				clocks = <&v2m_clk24mhz>, <&v2m_clk24mhz>;
				clock-names = "uartclk", "apb_pclk";
				phandle = <0x22>;
			};

			igc-channels@0x200000 {
			    compatible = "sth,igc";
			    reg = <0x200000 0xC000>; /* total size for all channels */
			    nr_of_channels = <2>;
			    
			    channel0 {
			      ctrl  = <0x0 0x1000>;
			      write = <0x1000 0x1000>;
			      read  = <0x2000 0x4000>;
			      interrupts = <0 16 0>; /* 48 = 32 + 16 */
			      interrupt-parent = <&gic>;
			      other-end = "User"; /* max 20 chars */
			    };
#if 0
			    channel1 { 
			      ctrl  = <0x6000 0x1000>;
			      write = <0x7000 0x3000>;
			      read  = <0xA000 0x2000>;
			      interrupts = <0 17 0>; /* 49 = 32 + 17 */
			      interrupt-parent = <&gic>;
			      other-end = "Com"; /* max 20 chars */
			    };
#endif
			};
		};
	};
};
