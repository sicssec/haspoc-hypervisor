// -*- dts  -*-
/dts-v1/;

#define GIC_SPI 0
#define GIC_PPI 1

/ {
	model = "Juno";
	compatible = "arm,juno", "arm,vexpress";
	interrupt-parent = <0x1>;
	#address-cells = <0x2>;
	#size-cells = <0x2>;

	aliases {
		serial0 = &uart0;
	};

	chosen {
		bootargs = "earlycon console=ttyAMA0 earlyprintk debug user_debug=1 loglevel=31";
		stdout-path = &uart0;
	};

	psci {
		compatible = "arm,psci";
		method = "smc";
		cpu_suspend = <0xc4000001>;
		cpu_off = <0x84000002>;
		cpu_on = <0xc4000003>;
	};

	cpus {
		#address-cells = <0x2>;
		#size-cells = <0x0>;

		cpu@0 {
			device_type = "cpu";
			compatible = "arm,cortex-a53","arm,armv8";
			reg = <0x0 0x0>;
			enable-method = "psci";
			clocks = <&scpi_dvfs 1>;
			clock-names = "vlittle";
		};
		
		cpu@1 {
			device_type = "cpu";
			compatible = "arm,armv8";
			reg = <0x0 0x1>;
			enable-method = "psci";
		};
		/*
		cpu@2 {
			device_type = "cpu";
			compatible = "arm,armv8";
			reg = <0x0 0x2>;
			enable-method = "psci";
		};

		cpu@3 {
			device_type = "cpu";
			compatible = "arm,armv8";
			reg = <0x0 0x3>;
			enable-method = "psci";
		};
		*/
	};

	memory@00000000 {
		device_type = "memory";
		reg = <0x0 0x00000000 0x0 0x08000000>;
	};

	pmu {
		compatible = "arm,armv8-pmuv3";
//		interrupts = <0x0 0x3c 0x4 0x0 0x3d 0x4 0x0 0x3e 0x4 0x0 0x3f 0x4>;

		interrupts = <GIC_SPI 18 4>,
			     <GIC_SPI 22 4>,
			     <GIC_SPI 26 4>,
			     <GIC_SPI 30 4>,
			     <GIC_SPI 02 4>,
			     <GIC_SPI 06 4>;
	};


	clocks {
		compatible = "arm,scpi-clks";

		scpi_dvfs: scpi_clocks@0 {
			compatible = "arm,scpi-clk-indexed";
			#clock-cells = <1>;
			clock-indices = <0>, <1>, <2>;
			clock-output-names = "vbig", "vlittle", "vgpu";
		};

		scpi_clk: scpi_clocks@3 {
			compatible = "arm,scpi-clk-range";
			#clock-cells = <1>;
			clock-indices = <3>, <4>;
			frequency-range = <23000000 210000000>;
			clock-output-names = "pxlclk0", "pxlclk1";
		};

	};

	timer {
		compatible = "arm,armv8-timer";
		interrupts =
	        <GIC_PPI 13 0xff01>, // secure
	        <GIC_PPI 14 0xff01>, // non-secure
	        <GIC_PPI 11 0xff01>, // virtual
	        <GIC_PPI 10 0xff01>; // hypervisor
//		clock-frequency = <0x5f5e100>;
	};


	soc_uartclk: refclk72738khz {
		compatible = "fixed-clock";
		#clock-cells = <0>;
		clock-frequency = <7273800>;
		clock-output-names = "juno:uartclk";
	};

	soc_refclk100mhz: refclk100mhz {
		compatible = "fixed-clock";
		#clock-cells = <0>;
		clock-frequency = <100000000>;
		clock-output-names = "apb_pclk";
	};


	/* hypervisor mapping of peripherals... */

	smb {
		compatible = "simple-bus";
		#address-cells = <0x2>;
		#size-cells = <0x1>;
		ranges =
			<0x0 0x0 0x0 0xC0000000 0x00200000>,
			<0x1 0x0 0x0 0xC0200000 0x00200000>;


		/* Directly mapped */
		mapped@1,00000000 {
			compatible = "arm,amba-bus", "simple-bus";
			#address-cells = <0x1>;
			#size-cells = <0x1>;
			ranges = <0x0 0x0 0x00000000 0x00200000>;

			/* ARM GICv2 ? */
			interrupt-controller@000000 {
				compatible = "arm,cortex-a15-gic";
				#interrupt-cells = <0x3>;
				#address-cells = <0x0>;
				interrupt-controller;
				reg = <0x00000 0x1000 0x01000 0x1000>;
				interrupts = <GIC_PPI 0x9 0xf04>;
				linux,phandle = <0x1>;
				phandle = <0x1>;
			};

			uart0: uart@0x4000 {
				compatible = "arm,primecell", "arm,pl011";
				reg = <0x4000 0x1000>;
				interrupts = <0 0 4>;
				clocks = <&soc_uartclk>, <&soc_refclk100mhz>;
				clock-names = "uartclk", "apb_pclk";
			};
		};
	};
};
