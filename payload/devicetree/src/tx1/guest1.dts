/dts-v1/;

/*
 * Interrupt specifier cell 2.
 * The flags in irq.h are valid, plus those below.
 */
#define GIC_CPU_MASK_RAW(x) ((x) << 8)
#define GIC_CPU_MASK_SIMPLE(num) GIC_CPU_MASK_RAW((1 << (num)) - 1)
#define GIC_PPI 1
#define GIC_SPI 0
#define IRQ_TYPE_LEVEL_HIGH	4
#define IRQ_TYPE_LEVEL_LOW	8

#define TEGRA210_CLK_TIMER 5
#define TEGRA210_CLK_UARTA 6
#define TEGRA210_CLK_APBDMA 34
#define TEGRA210_CLK_PCLK 293

/ {
	model = "NVIDIA Jetson TX1 Developer Kit";
	compatible = "nvidia,p2371-2180", "nvidia,tegra210";
        interrupt-parent = <&gic>;
        #address-cells = <2>;
        #size-cells = <2>;

	aliases {
                serial0 = &uart0;
        };

	chosen {
                bootargs = "console=ttyS0,115200n8 rootwait";
		stdout-path = &uart0;
        };

	psci {
                compatible = "arm,psci-0.2";
                method = "smc";
        };

	cpus {
                #address-cells = <1>;
                #size-cells = <0>;

                cpu@0 {
                        device_type = "cpu";
                        compatible = "arm,cortex-a57";
                        reg = <0>;
                };
/*
                cpu@1 {
                        device_type = "cpu";
                        compatible = "arm,cortex-a57";
                        reg = <1>;
                };

                cpu@2 {
                        device_type = "cpu";
                        compatible = "arm,cortex-a57";
                        reg = <2>;
                };

                cpu@3 {
                        device_type = "cpu";
                        compatible = "arm,cortex-a57";
                        reg = <3>;
                };
*/
        };

        memory@0x91400000 {
                device_type = "memory";
                reg = <0x0 0x91400000 0x0 0x10000000>;
        };

        pmc: pmc@0,C0010000 {
                compatible = "nvidia,tegra210-pmc";
                reg = <0x0 0xC0010000 0x0 0x400>;
                clocks = <&tegra_car TEGRA210_CLK_PCLK>, <&clk32k_in>;
                clock-names = "pclk", "clk32k_in";

                #power-domain-cells = <1>;

                nvidia,invert-interrupt;
        };

        clocks {
                compatible = "simple-bus";
                #address-cells = <1>;
                #size-cells = <0>;

                clk32k_in: clock@0 {
                        compatible = "fixed-clock";
                        reg = <0>;
                        #clock-cells = <0>;
                        clock-frequency = <32768>;
                };
        };

        timer {
                compatible = "arm,armv8-timer";
                interrupts = <GIC_PPI 13
                                (GIC_CPU_MASK_SIMPLE(4) | IRQ_TYPE_LEVEL_LOW)>,
                             <GIC_PPI 14
                                (GIC_CPU_MASK_SIMPLE(4) | IRQ_TYPE_LEVEL_LOW)>,
                             <GIC_PPI 11
                                (GIC_CPU_MASK_SIMPLE(4) | IRQ_TYPE_LEVEL_LOW)>,
                             <GIC_PPI 10
                                (GIC_CPU_MASK_SIMPLE(4) | IRQ_TYPE_LEVEL_LOW)>;
                interrupt-parent = <&gic>;
        };

        timer@0,C0012000 {
                compatible = "nvidia,tegra210-timer", "nvidia,tegra20-timer";
                reg = <0x0 0xC0012000 0x0 0x400>;
                interrupts = <GIC_SPI 0 IRQ_TYPE_LEVEL_HIGH>,
                             <GIC_SPI 1 IRQ_TYPE_LEVEL_HIGH>,
                             <GIC_SPI 41 IRQ_TYPE_LEVEL_HIGH>,
                             <GIC_SPI 42 IRQ_TYPE_LEVEL_HIGH>,
                             <GIC_SPI 121 IRQ_TYPE_LEVEL_HIGH>,
                             <GIC_SPI 122 IRQ_TYPE_LEVEL_HIGH>;
                clocks = <&tegra_car TEGRA210_CLK_TIMER>;
                clock-names = "timer";
        };

        tegra_car: clock@0,C0013000 {
                compatible = "nvidia,tegra210-car";
                reg = <0x0 0xC0013000 0x0 0x1000>;
                #clock-cells = <1>;
                #reset-cells = <1>;
        };

	apbdma: dma@0,C0015000 {
                compatible = "nvidia,tegra210-apbdma", "nvidia,tegra148-apbdma";
                reg = <0x0 0xC0015000 0x0 0x1400>;
                clocks = <&tegra_car TEGRA210_CLK_APBDMA>;
                clock-names = "dma";
                resets = <&tegra_car 34>;
                reset-names = "dma";
                #dma-cells = <1>;
        };

        apbmisc@0,C0017000 {
                compatible = "nvidia,tegra210-apbmisc", "nvidia,tegra20-apbmisc";
                reg = <0x0 0xC0017800 0x0 0x64>,   /* Chip revision */
                      <0x0 0xC0010864 0x0 0x04>;   /* Strapping options */
        };

	uart0: serial@0,C0004000 {
                compatible = "nvidia,tegra210-uart", "nvidia,tegra20-uart";
                reg = <0x0 0xC0004000 0x0 0x40>;
                reg-shift = <2>;
                interrupts = <GIC_SPI 36 IRQ_TYPE_LEVEL_HIGH>;
                clocks = <&tegra_car TEGRA210_CLK_UARTA>;
                clock-names = "serial";
                resets = <&tegra_car 6>;
                reset-names = "serial";
                dmas = <&apbdma 8>, <&apbdma 8>;
                dma-names = "rx", "tx";
                status = "okay";
        };

	/* hypervisor mapping of peripherals... */

        smb@0xC0000000 {
                compatible = "simple-bus";
                #address-cells = <0x2>;
                #size-cells = <0x1>;
                ranges = <0x0 0x0 0x0 0xC0000000 0x00200000>,
                         <0x1 0x0 0x0 0xC0200000 0x00200000>;


                /* Directly mapped */
                mapped@1,00000000 {
                        compatible = "arm,amba-bus", "simple-bus";
                        #address-cells = <0x1>;
                        #size-cells = <0x1>;
                        ranges = <0x0 0x0 0x00000000 0x00200000>;

                        /* ARM GICv2 ? */
                        gic: interrupt-controller@000000 {
                                compatible = "arm,cortex-a15-gic";
                                #interrupt-cells = <0x3>;
                                #address-cells = <0x0>;
                                interrupt-controller;
                                reg = <0x00000 0x1000 0x01000 0x1000>;
                                interrupts = <GIC_PPI 0x9 0xf04>;
                                linux,phandle = <0x1>;
                                phandle = <0x1>;
                        };
                };
        };
};

/* These are the unmodified nodes

gic: interrupt-controller@0,50041000 {
                compatible = "arm,gic-400";
                #interrupt-cells = <3>;
                interrupt-controller;
                reg = <0x0 0x50041000 0x0 0x1000>,
                      <0x0 0x50042000 0x0 0x2000>,
                      <0x0 0x50044000 0x0 0x2000>,
                      <0x0 0x50046000 0x0 0x2000>;
                interrupts = <GIC_PPI 9
                        (GIC_CPU_MASK_SIMPLE(4) | IRQ_TYPE_LEVEL_HIGH)>;
                interrupt-parent = <&gic>;
        };

uarta: serial@0,70006000 {
                compatible = "nvidia,tegra210-uart", "nvidia,tegra20-uart";
                reg = <0x0 0x70006000 0x0 0x40>;
                reg-shift = <2>;
                interrupts = <GIC_SPI 36 IRQ_TYPE_LEVEL_HIGH>;
                clocks = <&tegra_car TEGRA210_CLK_UARTA>;
                clock-names = "serial";
                resets = <&tegra_car 6>;
                reset-names = "serial";
                dmas = <&apbdma 8>, <&apbdma 8>;
                dma-names = "rx", "tx";
                status = "okay";
        };
*/
