/*
 * interrupt and exception management and handlers
 */

#include "defs.h"
#include <gicv2.h>

static volatile struct gicv2_gicd * const gicd = (struct gicv2_gicd *) GICD_VBASE;
static volatile struct gicv2_gicc * const gicc = (struct gicv2_gicc *) GICC_VBASE;

static intr_callback handlers[IRQ_COUNT];


void intr_dump()
{
    int i;
    printf("GICC CTLR=%x PMR=%x BPR=%x IAR=%x\n",
           gicc->ctlr, gicc->pmr, gicc->bpr, gicc->iar);

    for(i = 0; i < 2; i++) {
        printf("GICD_%d GRP=%x EN=%x PEND=%x ACT=%x\n",
               i,
               gicd->igrouprn[i],
               gicd->isenablern[i],
               gicd->ispendrn[i],
               gicd->isactivern[i]
               );
    }
}

/* check if this guest can use this interrupt */
int intr_can_use(int id)
{
    int old, new;
    old = gicv2_dist_get_enable(gicd, id);
    gicv2_dist_set_enable(gicd, id, !old);
    new = gicv2_dist_get_enable(gicd, id);
    gicv2_dist_set_enable(gicd, id, old);
    return old != new;
}

void intr_config(int id, int cfg, int prio, int enable, intr_callback c)
{
    if(id < 0 || id >= IRQ_COUNT) {
        printf("Invalid irq ID: %d\n", id);
        return;
    }

    gicv2_dist_set_config(gicd, id, cfg);
    gicv2_dist_set_priority(gicd, id, prio);
    gicv2_dist_set_enable(gicd, id, enable);
    handlers[id] = c;
}

void intr_init()
{
    int i;

    for(i = 0; i < IRQ_COUNT; i++)
        handlers[i] = 0;


    /* map gicc & gicd  */
    mmu_map(GICD_VBASE, GICD_VBASE - DEVICE_VBASE + DEVICE_PBASE,
            PAGE_SIZE, PT_ATTR_L3_DEV);
    mmu_map(GICC_VBASE, GICC_VBASE - DEVICE_VBASE + DEVICE_PBASE,
            PAGE_SIZE, PT_ATTR_L3_DEV);

    gicc->ctlr = 0;
    gicc->pmr = 0xFF;
    gicc->bpr = 7;
    gicc->ctlr = 0x1;

    gicd->ctlr = 0x1;
    __asm__ ("msr DAIFClr, #15");
}


/* ------------------------------------------------------------------ */

void el1h_irq_handler(struct registers *r, uint64_t psr, uint64_t elr)
{
    intr_callback cb = 0;
    uint32_t iar, id;
    int i;

    iar = gicc->iar;
    id = (iar & 1023);

    if(id < IRQ_COUNT)
        cb = handlers[id];

    if(cb) {
        cb();
    } else {
        printf("CPU%d EL1 IRQ: IAR=%x\n", mc_whoami(), iar);
        __die();
    }

    // ack it!
    gicc->eoir = iar;
}

void el1h_fiq_handler(struct registers *r, uint64_t psr, uint64_t elr)
{
    printf("CPU%d EL1 FIQ IAR=%x, PSR=%x PC=%x\n",
           mc_whoami(), gicc->iar,  psr, r->pc);
    for(;;);
}


void el1h_sync_handler(struct registers *r, uint64_t psr, uint64_t elr, uint64_t esr)
{
    printf("Sync ESR=%x PSR=%x at PC=%x FAR_EL1=%x\n", esr, psr, r->pc,MRS64("FAR_EL1"));
	printf("X0=%X, X1=%X, X2=%X, X3=%X\n",r->x[0],r->x[1],r->x[2],r->x[3]);
    r->pc += 4;
     for(;;);
}


void el1h_serror_handler(struct registers *r, uint64_t psr, uint64_t elr, uint64_t esr)
{
    printf("serror ESR=%x PSR=%x at PC=%x\n", esr, psr, r->pc);
     for(;;);
}

// ------------------------------------------------------------------------

void el1_bad_handler(uint64_t pc, uint64_t cpsr, uint64_t esr, adr_t adr)
{
    printf("BAD EXCEPTION (%x): PC=%X PSR=%x ESR=%x\n",
           adr, pc, cpsr, esr);
    for(;;);
}
