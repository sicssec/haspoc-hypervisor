/*
 * memory operatins including MMU, cache and TLB
 */


#include "defs.h"

/*
 * TLB
 */

void tlb_flush_adr(adr_t adr, size_t size)
{
    int i;
    adr_t end;

    /* calc as pages */
    size += adr & (PAGE_SIZE - 1);
    adr = adr / PAGE_SIZE;
    end = adr + (size + PAGE_SIZE - 1) / PAGE_SIZE;


    __asm__ volatile("dsb ish\n" ::: "memory");

    for( ; adr < end; adr++)
        __asm__ volatile("tlbi vae1, %0" ::"r"(adr));


    __asm__ volatile("dsb ish\n"
                     "isb"
                     ::: "memory");
}

void tlb_flush()
{
    __asm__ volatile(
                     "dsb ishst\n"
                     "tlbi vmalle1is\n"
                     "dsb ishst\n"
                     "isb"
                     ::: "memory");
}

/*
 * caches
 */
void cache_inst_clear()
{
    __asm__ volatile("isb\nic IALLUIS\nisb" ::: "memory");
}

void cache_data_cleaninv(adr_t start, size_t len)
{
    uint64_t ctr_el0, dline;
    adr_t end;

    /* get cache line size in bytes */
    ctr_el0 = MRS64("CTR_EL0");
    dline = 4ULL << ((ctr_el0 >> 16) & 0xF);

    /* align start to dline and calc end */
    len += start & (dline-1);
    start &= ~(dline-1);
    end = start + len;

    /* now clean all lines in that region */
    for(; start < end; start += dline)
        __asm__ volatile("DC CIVAC, %0" :: "r"(start) : "memory");
}



/*
 * MMU
 */

static uint64_t * mmu_get_pt(int index)
{
    return (uint64_t *) (__get_pt_start() + index * PT_SIZE);
}

void mmu_map(adr_t vadr, adr_t padr, size_t size, uint64_t attr)
{
    int cnt, idx;
    uint64_t *pt;
    adr_t mask = ~(PT_L1_SIZE - 1);

    if( (vadr | padr) & (PAGE_SIZE - 1)) {
        printf("Bad mapping alignment\n");
        return;
    }

    /* find what L2 this is */
    if((vadr & mask) == DEVICE_VBASE)
        pt = mmu_get_pt(3);
    else if((vadr & mask) == IGC_VBASE)
        pt = mmu_get_pt(4);
    else {
        printf("Cannot map to vadr=%x\n", vadr);
        return;
    }

    /* get range in pages */
    cnt = size / PAGE_SIZE;
    idx = (vadr % PT_L1_SIZE) / PT_L2_SIZE;

    if(idx + cnt >= PT_ENTRIES) {
        printf("range overflows L2 table\n");
        return;
    }

    cache_data_cleaninv(& pt[idx], sizeof(uint64_t) * cnt);

    for( ; cnt; cnt--) {
        pt[idx] = padr | attr;
        idx ++;
        padr += PAGE_SIZE;
    }

    tlb_flush_adr(vadr, size);
}
