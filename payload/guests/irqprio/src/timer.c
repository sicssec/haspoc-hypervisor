/*
 * CPU timer
 */
#include "defs.h"

#include <gicv2.h>

#define TICK_CLOCKS 0x300000

static void timer_setup(void)
{
    MSR64("CNTP_CTL_EL0", 0);
    MSR64("CNTP_CVAL_EL0", 0x00000);
    // MSR64("CNTP_TVAL_EL0", TICK_CLOCKS * timer_delay() );
    MSR64("CNTP_TVAL_EL0", 1 );
}

int timer_delay()
{
    return 1 << (2 * guest_id + 1);
}

void timer_next()
{
    timer_setup();
    MSR64("CNTP_CTL_EL0", 1);
}

void timer_start()
{
    MSR64("CNTP_CTL_EL0", 1);
}

void timer_init()
{
    timer_setup();
    // intr_config(IRQ_TIMER, GICV2_ICFG_EDGE | GICV2_ICFG_1N, 0xE0, 1, timer_lockup);
    intr_config(IRQ_TIMER, GICV2_ICFG_EDGE | GICV2_ICFG_1N, 0xE0, 1, timer_syscall);
}
