/*
 * exception and interrupt entry points
 */
#include "defs.h"

    .text
    .global __vectors

    .macro DUMMY_HANDLER label
    HANDLER_TOP \label
        mrs x0, elr_el1
        mrs x1, spsr_el1
        mrs x2, esr_el1
        ldr x4, \label

        b el1_bad_handler
        b .
    .endm


    .align 11
__vectors:
    /* FROM EL1 using SP_EL0 */
    DUMMY_HANDLER	el1t_sync
    DUMMY_HANDLER	el1t_irq
    DUMMY_HANDLER	el1t_fiq
    DUMMY_HANDLER	el1t_error

    /* FROM EL1 using SP_EL1 */
    HANDLE_EXCEPTION 1 el1h_sync_handler el1_restore 1
    HANDLE_EXCEPTION 1 el1h_irq_handler el1_restore 0
    HANDLE_EXCEPTION 1 el1h_fiq_handler el1_restore 0
    HANDLE_EXCEPTION 1 el1h_serror_handler el1_restore 1

    /* FROM EL0 in aarch64 */
    DUMMY_HANDLER	el0_sync
    DUMMY_HANDLER	el0_irq
    DUMMY_HANDLER	el0_fiq
    DUMMY_HANDLER	el0_error

   /* FROM EL0 in aarch32 */
    DUMMY_HANDLER	el0_32_sync
    DUMMY_HANDLER	el0_32_irq
    DUMMY_HANDLER	el0_32_fiq
    DUMMY_HANDLER	el0_32_error



/* handler bottom */
el1_restore:
    RESTORE_REGISTERS 1
    eret
