
#include "defs.h"

#include <gicv2.h>

/***********************************************
 * timer
 ***********************************************/

static int timer_cnt = 0;

void timer_tick()
{
    int i;
    uint32_t data;

    /* update tick counter */
    timer_cnt++;
    printf("TICK %d\n", timer_cnt);

    /* emergency erase test code */
    if(timer_cnt == 5) {
        printf("GUEST requesting emergency erase...\n");
        __emergency_erase();
    }

    /* send some data to the other end  */
    chan0_data_send();

    /* and schedule next tick */
    timer_next();
}

void timer_lockup()
{
    printf("Looping forever..\n");
    for (;;) ;

    timer_next();
}

void timer_syscall()
{
    printf("Calling eternal syscall\n");
    __eternal_syscall();

    timer_next();
}

/***********************************************
 * IGC channel 0
 ***********************************************/

struct chan0_data
{
    uint32_t cnt;
    uint32_t sender;
    uint32_t data;
};

void chan0_data_send()
{
    struct chan0_data *data = (struct chan0_data *) & chan0.write[0];
    int i;

    /* make up some semi random data */
    data->cnt++;
    data->sender = guest_id;
    data->data =  timer_cnt | (guest_id << 16);

    /* clear it from the cache */
    cache_data_cleaninv((adr_t) data, sizeof(struct chan0_data));

    /* notify other end */
    igc_notify(&chan0);
}

 void chan0_data_recv()
{
    static int old_version = -1;

    struct chan0_data *data = (struct chan0_data *) & chan0.read[0];
    int version = igc_get_version(&chan0);

    /* just dump whatever we can see */
    printf("RECV v%d (%d) cnt=%d from %d\tData=%x\n",
           version, old_version, data->cnt, data->sender, data->data);

    old_version = version;
}

static void chan0_init()
{
    struct chan0_data *data = (struct chan0_data *) & chan0.write[0];
    memset(data, 0, sizeof(struct chan0_data));
    cache_data_cleaninv((adr_t) data, sizeof(struct chan0_data));
}


/***********************************************
 * main
 ***********************************************/

void main()
{
    int i;
    debug_init();
    intr_init();
    timer_init();
    timer_start();
    /* igc_init(); */

    printf("kernel_main: CPU %d. guest-ID=%d, delay=%d\n",
           mc_whoami(), guest_id, timer_delay() );

    /* set up channel data */
    /* chan0_init(); */

    /* start the initial timer */
    /* timer_start(); */

    /* END: return and wait for interrupts */
}
