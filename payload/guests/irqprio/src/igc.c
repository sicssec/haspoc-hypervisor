/*
 * inter-guest communication code
 */
#include "defs.h"

#define IGC_MAGIC_VALUE 0xAA640CCA

/* the two guests use different vIRQs, we don't know which so we test both */
#define IRQ_IGC_CHAN0_A 32
#define IRQ_IGC_CHAN0_B 33


struct igc {
    uint32_t magic;
    uint32_t id;
    uint32_t version;
    uint32_t notify;
    uint32_t write_adr;
    uint32_t write_size;
    uint32_t read_adr;
    uint32_t read_size;
};


struct igc_chan chan0;


uint32_t igc_get_version(struct igc_chan *ic)
{
    volatile struct igc *igc = (struct igc *) ic->igc;
    return igc->version;
}

void igc_notify(struct igc_chan *ic)
{
    volatile struct igc *igc = (struct igc *) ic->igc;

    __asm__ volatile("dsb sy" ::: "memory"); /* just in case ... */

    igc->notify = 0x01234567;
}


void igc_init()
{
    int i;
    volatile struct igc *igc = (struct igc *) IGC_VBASE;
    adr_t diff;

    diff = IGC_PBASE - IGC_VBASE;

    /* map IGC control */
    mmu_map(IGC_VBASE, IGC_VBASE + diff, PAGE_SIZE, PT_ATTR_L3_IGC);

    /* check it! */
    if(igc->magic != IGC_MAGIC_VALUE) {
        printf("NO IGC FOUND\n");
        return;
    }
    printf("IGC magic=%x version=%x\n", igc->magic, igc->version);

    /* setup chan0:  */
    chan0.igc = igc;
    chan0.read = (uint32_t *) igc->read_adr;
    chan0.write = (uint32_t *) igc->write_adr;
    chan0.read_size = igc->read_size;
    chan0.write_size = igc->write_size;
    /* XXX: this test takes whichever IRQ we can use as the IGC irq */
    chan0.irq = intr_can_use(IRQ_IGC_CHAN0_A) ? IRQ_IGC_CHAN0_A : IRQ_IGC_CHAN0_B;
    intr_config(chan0.irq, 0, 0xF0, 1, chan0_data_recv);

    /* map it */
    mmu_map((adr_t) chan0.read, diff + (adr_t) chan0.read,
            chan0.read_size, PT_ATTR_L3_IGC);
    mmu_map((adr_t) chan0.write, diff + (adr_t) chan0.write,
            chan0.write_size, PT_ATTR_L3_IGC);


    /* debug IGC config */
    printf("IGC chan0 irq: %d\n", chan0.irq);
    printf("IGC chan0 write: %x:%x\n", chan0.write, chan0.write_size);
    printf("IGC chan0 read: %x:%x\n", chan0.read, chan0.read_size);
}
