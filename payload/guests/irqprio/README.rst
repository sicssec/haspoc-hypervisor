This is basically a clone of mini with some slight changes to the timer
interrupt. The devicetree (config_irqprio.dts) includes this guest as well as a
Linux guest. Manually copy (or symlink) linux.bin from a linux guest build
directory to irqprio's build directory.
