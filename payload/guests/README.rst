
This folder contains different guests and scripts to prepare them for the
payload. In particular::

  * mini is a minimal OS used for testing
  * perf is a variation of mini used for performance measurement
  * linux is vanilla Linux with buildroot. Debian and Ubuntu are full systems
  * LK is Little Kernel, often used as Android boot loader
  * ... and more


The linux build in particular is pretty complex since it includes the initial
filesystem and userspace tools plus drivers for the IGC and more. But please
note that in a real system all those would reside on flash storage and
be loaded using a boot loader such as LK (which would also run inside the VM).
