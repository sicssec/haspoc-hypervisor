This folder contain the logic to build the Linux kernel and the initial
filesystem used to boot a Debian Jessie 8.2 OS based on Linux 3.18 kernel.

Note that the foreign repositories are stored in external/ and that we assume
you are using the Lemaker-version of HiKey with 8GB eMMC.

We do currently not supply the needed rootfile system for debian, it is up to
the user to follow these instructions.

As everything is configured now, Debian expects the rootfs to be found on the
first partition on a micro SDcard. If one would rather boot of the eMMC then
change part of the chosen/bootargs node from

    root=/dev/mmcblk1p1    --->    root=/dev/disk/by-partlabel/system

The rootfs can be downloaded from

    http://builds.96boards.org/releases/hikey/linaro/debian/15.11/hikey-jessie_alip_20151130-387.img.gz

Related downloads can be found at
    http://builds.96boards.org/releases/hikey/linaro/debian/15.11/

Because of hardcoded addresses in the Mali driver, this driver should be
recompiled with the virtual addresses used by default with the hypervisor (more
details in patch found at external/patches/linux_debian/). To do this you need
two tools (by Android?): 'simg2img' and 'ext2simg'. Do the following:

$ gunzip hikey-jessie_alip_20151130-387.img.gz
$ simg2img hikey-jessie_alip_20151130-387.img hikey-jessie.raw.img
$ mkdir local_mount
$ mount -o loop hikey-jessie.raw.img local_mount
$ make -C <sth-folder>/external/linux_HIKEY_debian/ ARCH=arm64 -j32 modules
$ cp <sth-folder>/drivers/gpu/arm/utgard/mali.ko \
    local_mount/lib/modules/3.18.0-linaro-hikey/kernel/drivers/gpu/arm/utgard/mali.ko
$ umount local_mount
$ ext2simg hikey-jessie.raw.img updated-hikey-jessie.img

You can now flash the updated-hikey-jessie.img to the system partition of your
hikey board (use the ptable-linux.img and not ptable-aosp.img).

$ fastboot flash system updated-hikey-jessie.img

If you want to use an SD card, do the same steps up to the mounted raw image.
Then copy the contents of local_mount/ to your EXT4 partitoned SD card with
'cp -rp', p should preserve permissions etc.

If you want to make changes to virtual addresses, know that some are as earlier
stated hardcoded in the Mali driver. See patch for details.
