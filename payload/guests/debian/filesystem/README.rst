
This folder contains the initial linux filesystem. The directory structure is

 * prebuilt           pre-built kernel filesystem with system files
 * userdata           place holder for additional user files
 * buildroot          source for the pre-built filesystem
 * build              the filesystem will be written here

Note that the filesystem ontains two parts: the system files (taken from
buildroot if available, otherwise a prebuilt one) and user files which
are the files found in the userdata/ folder.

The make script will join the two into a single compressed file which will
later be embedded into the Linux kernel.

--

NOTE: since building the system part of the filesystem is very time and storage
consuming, we have provided a pre-built image for you, and have excluded buildroot
from the normal build process. If you want to build your own filesystem, you have
to enter buildroot manually. After a sucessfull build, you must then rebuilt the
linux guest including the filesystem image.
