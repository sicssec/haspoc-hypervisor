#ifndef _DEFS_H_
#define _DEFS_H_

#include "base.h"
#include "arch_defs.h"
#include <armv8.h>



#define RAM_SIZE (MB * 4)
#define RAM_START 0


#define DEVICE_BASE 0xC0000000

#define GICD_BASE ((DEVICE_BASE) + 0x00000)
#define GICC_BASE ((DEVICE_BASE) + 0x01000)
#define UART_BASE ((DEVICE_BASE) + 0x04000)

#define IRQ_COUNT 96
#define IRQ_TIMER 0x1E

#ifndef __ASSEMBLER__

#include <stdint.h>
#include <minilib.h>

/* misc */
extern void fatal(char *msg);
extern void __die();

extern uint32_t __pmcr();
extern uint32_t __smc(uint32_t func, uint64_t x1, uint64_t x2, uint64_t x3);
extern uint32_t __hvc(uint32_t func, uint64_t x1, uint64_t x2, uint64_t x3);


#endif /* ! __ASSEMBLER__ */

#endif /* _DEFS_H_ */
