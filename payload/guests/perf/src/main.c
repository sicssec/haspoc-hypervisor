
#include "defs.h"
#include <gicv2.h>

/* debug */
void printf_putchar(int c)
{
    uart_write(UART_BASE, c);
}

void fatal(char *msg)
{
    printf("*FATAL: %s\n", msg);
    __die();
}

/* reporting, to make it machine readable */
void report_title(char *title)
{
    printf("#%s\n", title);
}

void report_message(char *msg)
{
    printf("*%s\n", msg);
}

void report_value1(char *name, int value)
{
    printf("  $%s=%d\n", name, value);
}

void report_value2(char *name, int time, int count)
{
    printf("  $%s=%d\n", name, time / count);
    printf("  $%s-time=%d\n", name, time);
    printf("  $%s-count=%d\n", name, count);
}

/* gic */
void intr_config(int id, int cfg, int prio, int enable)
{
    volatile struct gicv2_gicd * const gicd = (struct gicv2_gicd *) GICD_BASE;
    gicv2_dist_set_config(gicd, id, cfg);
    gicv2_dist_set_priority(gicd, id, prio);
    gicv2_dist_set_enable(gicd, id, enable);
}

void gic_init()
{
    volatile struct gicv2_gicd * const gicd = (struct gicv2_gicd *) GICD_BASE;
    volatile struct gicv2_gicc * const gicc = (struct gicv2_gicc *) GICC_BASE;
    gicc->ctlr = 0;
    gicc->pmr = 0xFF;
    gicc->bpr = 7;
    gicc->ctlr = 0x1;
    gicd->ctlr = 0x1;
}

/* timer */
volatile uint64_t cnt_timer_start, cnt_timer_stop;

void timer_start(uint32_t counter)
{
    MSR64("CNTP_CTL_EL0", 0);
    MSR64("CNTP_CVAL_EL0", 0x00000);
    MSR64("CNTP_TVAL_EL0", counter );
    intr_config(IRQ_TIMER, GICV2_ICFG_EDGE | GICV2_ICFG_1N, 0x00, 1);

    cnt_timer_start = __pmcr();
    MSR64("CNTP_CTL_EL0", 1);
}

void timer_stop()
{
    MSR64("CNTP_CTL_EL0", 0);
    cnt_timer_stop == __pmcr();
}


/* handlers */
volatile int irq_cnt = 0;
volatile int irq_last = -1;

void el1h_irq_handler(struct registers *r, uint64_t psr, uint64_t elr)
{
    volatile struct gicv2_gicd * const gicd = (struct gicv2_gicd *) GICD_BASE;
    volatile struct gicv2_gicc * const gicc = (struct gicv2_gicc *) GICC_BASE;
    uint32_t iar, id;

    for(;;) {
        iar = gicc->iar;
        id = (iar & 1023);

        if(id >= 1022)
            break;

        irq_cnt++;
        irq_last= id;

        switch(id) {
        case IRQ_TIMER:
            break;
        default:
            fatal("Unknown irq");
        }

        gicc->eoir = iar;
    }
}

void el1h_sync_handler(struct registers *r, uint64_t psr, uint64_t elr, uint64_t esr)
{
    printf("Sync ESR=%x PSR=%x at PC=%x FAR_EL1=%x\n",
           esr, psr, r->pc, MRS64("FAR_EL1"));
	printf("X0=%X, X1=%X, X2=%X, X3=%X\n",r->x[0],r->x[1],r->x[2],r->x[3]);
    fatal("Terminated");
}

void el1_bad_handler(uint64_t pc, uint64_t cpsr, uint64_t esr, adr_t adr)
{
    printf("BAD EXCEPTION (%x): PC=%X PSR=%x ESR=%x\n",
           adr, pc, cpsr, esr);
    fatal("Terminated");
}

/* tests */

void perf_base()
{
    uint32_t c0, c1, c2, c3;
    int i;

    /* warm up :) */
    for(i = 0; i < 5; i++) {
        __pmcr();
    }

#define N1  __asm__ volatile("nop");
#define N2 N1 N1
#define N4 N2 N2
#define N8 N4 N4
#define N16 N8 N8
#define N32 N16 N16
#define N64 N32 N32
#define N128 N64 N64


    c0 = __pmcr();
    c1 = __pmcr();
    N1;
    c2 = __pmcr();
    N128;
    c3 = __pmcr();

    report_title("PMCR precision");
    report_value1("precision-t0", c1 - c0);
    report_value1("precision-t1", c2 - c1);
    report_value1("precision-t128", c3 - c2);
}

void perf_upcall()
{
    uint32_t c0, c1;
    int i;

    /* NULL HVC */
    c0 = __pmcr();
    for(i = 0; i < 1000; i++) {
        __hvc(0, 0, 0, 0);
        __hvc(0, 0, 0, 0);
        __hvc(0, 0, 0, 0);
        __hvc(0, 0, 0, 0);
    }
    c1 = __pmcr();
    report_title("HVC NULL");
    report_value2("hvc-null", c1 - c0, 4 * i);

    c0 = __pmcr();
    for(i = 0; i < 1000; i++) {
        __smc(0, 0, 0, 0);
        __smc(0, 0, 0, 0);
        __smc(0, 0, 0, 0);
        __smc(0, 0, 0, 0);
    }
    c1 = __pmcr();
    report_title("SMC NULL");
    report_value2("smc-null", c1 - c0, 4 * i);
}

void perf_irq()
{
    uint32_t c0, c1;
    int i, old_count;

    c0 = __pmcr();
    for(i = 0; i < 500; i++) {
        old_count = irq_cnt;
        timer_start(1);
        while(old_count == irq_cnt)
            printf("%d %d   \r", old_count, irq_cnt);
            ;

    }
    c1 = __pmcr();

    report_title("irq latency");
    report_value2("irq-timer", c1 - c0, i);
}

void perf_driver()
{
    volatile struct gicv2_gicd * const gicd = (struct gicv2_gicd *) GICD_BASE;
    uint32_t c0, c1, dummy;
    int i;

    c0 = __pmcr();
    for(i = 0; i < 1000; i++) {
        dummy += gicd->typer;
    }
    c1 = __pmcr();

    report_title("driver latency");
    report_value2("driver-gicd", c1 - c0, i);
    report_value1("driver-gicd-dummy", dummy);
}

/* main & init */
void init()
{
    gic_init();
    __asm__ ("msr DAIFClr, #15");
}


void main()
{
    int i;

    report_message("PERF " __DATE__ " " __TIME__ );

#if DEBUG > 0
    fatal("DEBUG > 0, will affect timing");
#endif


    perf_base();
    perf_upcall();
    perf_driver();
    perf_irq();

    report_message("DONE");
}
