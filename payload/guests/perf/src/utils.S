/*
 * low-level utilities
 */

#include "defs.h"

    .text
    .global __die
    .global __zero64
    .global __smc
    .global __hvc
    .global __pmcr

__die:
    msr DAIFSet, #15

    ldr x0, =0xC3000999 /* =UPCALL_DIE */
    hvc #0

    /* in case it didn't work */
1:
    wfe
    b 1b


/* __zero64(adr_t adr, size_t size):
 * zero memory in chunks of 64 byte
 */
__zero64:
    cbz x1, 2f      // dont allow zero size
    add x1, x1, x0  // x1 = end
1:  stp xzr, xzr, [x0], #16
    stp xzr, xzr, [x0], #16
    stp xzr, xzr, [x0], #16
    stp xzr, xzr, [x0], #16
    cmp x0, x1
    blt 1b
2:  ret



__smc:
    smc #0
    ret

__hvc:
    hvc #0
    ret

__pmcr:
    mrs x0, PMCCNTR_EL0
    ret
