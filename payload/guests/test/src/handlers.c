/*
 * handlers
 */

#include "defs.h"
#include <gicv2.h>

/*
 * handlers
 */
static handler_callback irq_global_handlers[IRQ_COUNT];
static handler_callback irq_local_handlers[32 * ARCH_CORES];

static handler_callback sync_handlers[SYNC_COUNT];

handler_callback handler_irq_get(int n)
{
    if(n< 0 || n >= IRQ_COUNT)
        return NULL;

    if(n >= 32) {
        return irq_global_handlers[n];
    } else {
        int cpu = get_cpuid();
        return irq_local_handlers[ n + cpu * 32];
    }
}

void handler_irq_set(int n, handler_callback h)
{
    if(n >= 0 && n < IRQ_COUNT) {
        if(n >= 32) {
            irq_global_handlers[n] = h;
        } else {
            int cpu = get_cpuid();
            irq_local_handlers[ n + cpu * 32] = h;
        }
    }
}

handler_callback handler_sync_get(int code)
{
    if(code >= 0 && code < SYNC_COUNT)
        return sync_handlers[code];
}

void handler_sync_set(int code, handler_callback h)
{
    if(code >= 0 && code < SYNC_COUNT)
        sync_handlers[code] = h;
}



/* ------------------------------------------------------------------ */

void el1h_irq_handler(struct registers *r, uint64_t psr, uint64_t elr)
{
    handler_callback cb;
    uint32_t iar, id;
    int i;

    for(;;) {
        iar = intc_get_active();
        id = (iar & 1023);

        if(iar >= 1022)
            break;

        cb = handler_irq_get(id);
        if(cb) {
            cb(iar, psr, r);
        } else {
            printf("CPU%d EL1 unknown IRQ, IAR=%x\n", get_cpuid(), iar);
            __die();
        }

        intc_ack(iar);
    }
}

void el1h_fiq_handler(struct registers *r, uint64_t psr, uint64_t elr)
{
    printf("CPU%d EL1 FIQ IAR=%x, PSR=%x PC=%x\n",
           get_cpuid(), intc_get_active(),  psr, r->pc);
    for(;;);
}


void el1h_sync_handler(struct registers *r, uint64_t psr, uint64_t elr, uint64_t esr)
{
    handler_callback cb;

    cb = handler_sync_get( (esr >> 26) & (SYNC_COUNT -1));
    if(cb) {
        cb(esr, psr, r);
        r->pc += 4;
    } else {
        /* TEMP */
        printf("Unknown sync operation LR=%X PC=%X ESR=%X\n", r->lr, r->pc, esr);
        for(;;);

        fatal("Unknown sync operation");
    }
}


void el1h_serror_handler(struct registers *r, uint64_t psr, uint64_t elr, uint64_t esr)
{
    printf("serror ESR=%x PSR=%x at PC=%x\n", esr, psr, r->pc);
     for(;;);
}

// ------------------------------------------------------------------------

void el1_bad_handler(uint64_t pc, uint64_t cpsr, uint64_t esr, adr_t adr)
{
    printf("BAD EXCEPTION (%x): PC=%X PSR=%x ESR=%x\n",
           adr, pc, cpsr, esr);
    for(;;);
}
