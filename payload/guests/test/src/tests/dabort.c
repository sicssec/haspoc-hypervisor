#include "defs.h"


static volatile uint32_t dabort_cnt, dabort_esr;
static volatile uint64_t dabort_far, dabort_pc;

static void dabort_callback(uint64_t esr, uint64_t psr, void *regs_)
{
    struct registers *regs = regs_;
    dabort_cnt++;

    dabort_esr = esr;
    dabort_far = MRS64("FAR_EL1");
    dabort_pc = regs->pc;
}
static void check_fail_read(char *msg, adr_t adr, void (*rfunc)(adr_t))
{
    dabort_cnt = 0;
    rfunc(adr);

    test_eq(msg, dabort_cnt, 1);
    test_eq(msg, dabort_esr >> 26, 0x25); /* EC = dabort from same */
    test_eq(msg, dabort_pc, (uint64_t) rfunc);
    test_eq(msg, dabort_far, adr);
}


static void check_fail_write(char *msg, adr_t adr, void (*wfunc)(adr_t, uint64_t))
{
    dabort_cnt = 0;
    wfunc(adr, 0);

    test_eq(msg, dabort_cnt, 1);
    test_eq(msg, dabort_esr >> 26, 0x25); /* EC = dabort from same */
    test_eq(msg, dabort_pc, (uint64_t) wfunc);
    test_eq(msg, dabort_far, adr);
}


static void test_dabort()
{
    uint32_t tmp;
    adr_t ram_start = RAM_BASE, ram_end = RAM_BASE + RAM_SIZE;


    /* set up */
    dabort_cnt = 0;
    handler_sync_set(SYNC_DABORT, dabort_callback);

    /* change valid memory */
    tmp = __read32(ram_start);
    __write32(ram_start, tmp);

    tmp = __read32(ram_end - 4);
    __write32(ram_end - 4, tmp);
    test_eq("No dabort for valid memory access", dabort_cnt, 0); /* check pass */

    /* invalid memory before RAM */
    check_fail_read("invalid read before RAM", ram_start - 4, __read32);
    check_fail_write("invalid write before RAM", ram_start - 4, __write32);


    /* invalid memory after RAM */
    check_fail_read("invalid read after RAM", ram_end, __read32);
    check_fail_write("invalid write after RAM", ram_end, __write32);

    /* invalid memory that overlaps regions. note that these are
     * caught by our own alignment error regardless of SCTLR_EL1.A */
    check_fail_read("invalid overlapping read64 after RAM", ram_end - 4, __read64);
    check_fail_read("invalid overlapping read32 after RAM", ram_end - 2, __read32);
    check_fail_read("invalid overlapping read16 after RAM", ram_end - 1, __read16);

    check_fail_write("invalid overlapping write64 after RAM", ram_end - 4, __write64);
    check_fail_write("invalid overlapping write32 after RAM", ram_end - 2, __write32);
    check_fail_write("invalid overlapping write16 after RAM", ram_end - 1, __write16);


    /* clean up */
    handler_sync_set(SYNC_DABORT, NULL);
}


DEFINE_TEST("dabort", 1, test_dabort);
