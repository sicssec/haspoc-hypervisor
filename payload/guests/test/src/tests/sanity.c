
#include "defs.h"


void test_sanity()
{
    volatile uint64_t tmp1, tmp2, tmp3;
    adr_t adr1;

    /* a number of simple sanity checks */
    test_eq("This is CPU0", get_cpuid(), 0);
    test_eq("CPU count is 2", cpu_count, 2);

    __asm__ volatile("mov %0, x30" : "=r"(tmp1)); /* get LR */
    tmp1 &= ~0xFFFF;
    test_eq("load address is around 0x10000", tmp1, 0x10000);


    __asm__ volatile("mov %0, sp" : "=r"(tmp1)); /* get SP */
    tmp2 = ((adr_t) &__heap_start__) + STACK_SIZE - 1024 * 8 * get_cpuid();
    test_range("stack pointer is in the correct range", tmp1, tmp2 - 1024, tmp2);

    /* memory read function checks */
    tmp3 = 0x0123456789ABCDEFLL;
    adr1 = (adr_t) & tmp3;
    test_eq("read64", __read64(adr1),  tmp3);
    test_eq("read32", __read32(adr1),  (uint32_t) tmp3);
    test_eq("read16", __read16(adr1),  (uint16_t) tmp3);
    test_eq("read8", __read8(adr1),  (uint8_t) tmp3);

    /* memory write function checks */
    tmp1 = 0x08192A3B4C5D6E7FLL;
    adr1 = (adr_t) & tmp3;

    tmp3 = 0;
    __write8(adr1, (uint8_t) tmp1);
    test_eq("write8", (uint8_t) tmp3,  (uint8_t) tmp1);

    tmp3 = 0;
    __write16(adr1, (uint16_t) tmp1);
    test_eq("write16", (uint16_t) tmp3,  (uint16_t) tmp1);

    tmp3 = 0;
    __write32(adr1, (uint32_t) tmp1);
    test_eq("write32", tmp3,  (uint32_t) tmp1);

    tmp3 = 0;
    __write64(adr1, (uint64_t) tmp1);
    test_eq("write64", tmp3,  (uint64_t) tmp1);
}

DEFINE_TEST("sanity checks", 0, test_sanity);
