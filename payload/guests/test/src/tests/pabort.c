
#include "defs.h"

static volatile uint32_t pabort_cnt, pabort_esr;
static volatile uint64_t pabort_far, pabort_pc;


static void pabort_callback(uint64_t esr, uint64_t psr, void *regs_)
{
    struct registers *regs = regs_;
    pabort_cnt++;

    pabort_esr = esr;
    pabort_far = MRS64("FAR_EL1");
    pabort_pc = regs->pc;

    /* no point continueing this path */
    regs->pc = regs->lr - 4; /* minus four since handler with add 4 later */

    /* and no point in a chain of aborts */
    if(regs->pc < RAM_BASE || regs->pc >= RAM_BASE + RAM_SIZE - sizeof(uint32_t))
        fatal("Cannot recover from pabort...");
}



void test_run_fail(char *msg, adr_t fadr)
{
    void (*func)();
    func = (void *) fadr;

    pabort_cnt = 0;

    MSR64("FAR_EL1", 0x123456);
    /* compiler, hands off please */
    __asm__ volatile("nop");
    func();
    __asm__ volatile("nop");

    test_eq(msg, pabort_cnt, 1);
    test_eq(msg, pabort_esr >> 26, 0x21); /* pabort from current level */
    test_eq(msg, pabort_far, fadr); /* did we get the correct adr registered ?*/
}


static void test_pabort()
{
    uint32_t tmp;
    adr_t ram_start = RAM_BASE, ram_end = RAM_BASE + RAM_SIZE;

    /* set up */

    handler_sync_set(SYNC_PABORT, pabort_callback);

    test_run_fail("pabort before RAM", ram_start - 4);
    test_run_fail("pabort after RAM", ram_end);

    /* clean up */
    handler_sync_set(SYNC_PABORT, NULL);
}

DEFINE_TEST("pabort", 2, test_pabort);
