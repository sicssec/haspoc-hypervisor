#ifndef _DEFS_H_
#define _DEFS_H_

#include "base.h"
#include "arch_defs.h"
#include <armv8.h>

#define RAM_SIZE (MB * 4)
#define RAM_BASE 0
#define ENTRY_OFFSET (16 * 1024)
#define STACK_SIZE   (32 * KB) /* 8KB per core! */
#define EL1_SIZE_PT      (PT_SIZE * 3)

#define DEVICE_BASE 0xC0000000
#define IGC_BASE    0xC0200000


#ifdef GICD_BASE
#undef GICD_BASE
#undef GICC_BASE
#endif

#define GICD_BASE ((DEVICE_BASE) + 0x00000)
#define GICC_BASE ((DEVICE_BASE) + 0x01000)
#define UART_BASE ((DEVICE_BASE) + 0x04000)

#define IRQ_COUNT 96
#define IRQ_TIMER 0x1E

#ifndef __ASSEMBLER__

#include <stdint.h>
#include <minilib.h>


static inline int get_cpuid()
{
    return MRS64("mpidr_el1") & 0x7;
}

extern uint64_t __boot_params[4 * 2];
extern uint32_t guest_id, cpu_count;

/* memory */

/* handlers */
typedef void (*handler_callback)(uint64_t d0, uint64_t d1, void *d2);

extern handler_callback handler_irq_get(int n);
extern void handler_irq_set(int n, handler_callback h);

#define SYNC_COUNT 64
#define SYNC_PABORT 0x21
#define SYNC_PC_ALIGN  0x22
#define SYNC_DABORT 0x25

extern handler_callback handler_sync_get(int code);
extern void handler_sync_set(int code, handler_callback h);

/* interrupts */
extern int intc_can_use(int id);
extern void intc_config(int id, int cfg, int prio, int enable, handler_callback c);
extern void intc_init(int global);
extern uint32_t intc_get_active();
extern void intc_ack(uint32_t iar);


/* debug */
extern void debug_init(int global);

/* timer */
extern void timer_tick();
extern void timer_next(uint32_t delay);
extern void timer_start(handler_callback callback, uint32_t delay);
extern void timer_init(int global);
extern uint32_t timer_delay();

/* igc */
struct igc_chan {
    void *igc;
    volatile uint32_t *read;
    volatile uint32_t *write;
    uint32_t read_size;
    uint32_t write_size;
    int irq;
};

extern struct igc_chan chan0;
extern uint32_t igc_get_version(struct igc_chan *ic);
extern void igc_notify(struct igc_chan *ic);
extern void igc_init();

/* psci */
extern void psci_cpuon(int cpu);

/* misc */
extern void fatal(char *msg);
extern void __die();

extern uint32_t __smc(uint32_t func, uint64_t x1, uint64_t x2, uint64_t x3);
extern uint32_t __hvc(uint32_t func, uint64_t x1, uint64_t x2, uint64_t x3);

extern uint64_t __read64(adr_t adr);
extern uint32_t __read32(adr_t adr);
extern uint16_t __read16(adr_t adr);
extern uint8_t __read8(adr_t adr);
extern void __write64(adr_t adr, uint64_t val);
extern void __write32(adr_t adr, uint32_t val);
extern void __write16(adr_t adr, uint16_t val);
extern void __write8(adr_t adr, uint8_t val);



/* link script variables */
extern adr_t  __heap_start__;


/* testing */
#define test_record(msg,succ) _test_record(__LINE__, __FILE__, msg,succ)
#define test_fail(msg) test_record(msg, false)
#define test_eq(msg, a, b) test_record(msg, (a) == (b))
#define test_neq(msg, a, b) test_record(msg, (a) != (b))
#define test_lt(msg, a, b) test_record(msg, (a) < (b))
#define test_gt(msg, a, b) test_record(msg, (a) > (b))
#define test_range(msg, a, min, max) test_record(msg, (a) >= (min) && (a) < (max))

extern void test_run();
extern void _test_record(int line, char *file, char *msg, bool succ);

struct testdata {
    char *name;
    uint32_t prio;
    void (*func)();
    uint32_t marker;
};

extern struct testdata __testdata_start, __testdata_end;

/* macros to generate different names in the same file ... */
#define M0_(a,b)  a##b
#define M1_(a) M0_(testdata_, a)
#define M2_ M1_(__COUNTER__)


#define DEFINE_TEST(name_, prio_, func_) \
    static const struct testdata M2_ \
    __attribute__((section (".testdata"))) __attribute__((used)) =      \
    { .name = name_, .prio = prio_, .func = func_, .marker = 0x666666 }


#endif /* ! __ASSEMBLER__ */

#endif /* _DEFS_H_ */
