/*
 * system (device) functions including low-level hardware stuff
 */

#include "defs.h"
#include <gicv2.h>

static volatile struct gicv2_gicd * const gicd = (struct gicv2_gicd *) GICD_BASE;
static volatile struct gicv2_gicc * const gicc = (struct gicv2_gicc *) GICC_BASE;


uint64_t __boot_params[4 * 2];
uint32_t guest_id, cpu_count;

/*
 * interrupt controller
 */

/*
 * debug
 */
void intc_dump()
{
    int i;
    printf("GICC CTLR=%x PMR=%x BPR=%x IAR=%x\n",
           gicc->ctlr, gicc->pmr, gicc->bpr, gicc->iar);

    for(i = 0; i < 2; i++) {
        printf("GICD_%d GRP=%x EN=%x PEND=%x ACT=%x\n",
               i,
               gicd->igrouprn[i],
               gicd->isenablern[i],
               gicd->ispendrn[i],
               gicd->isactivern[i]
               );
    }
}


/* check if this guest can use this interrupt */
int intc_can_use(int id)
{
    int old, new;
    old = gicv2_dist_get_enable(gicd, id);
    gicv2_dist_set_enable(gicd, id, !old);
    new = gicv2_dist_get_enable(gicd, id);
    gicv2_dist_set_enable(gicd, id, old);
    return old != new;
}

void intc_config(int id, int cfg, int prio, int enable, handler_callback c)
{
    if(id < 0 || id >= IRQ_COUNT) {
        printf("Invalid irq ID: %d\n", id);
        return;
    }

    gicv2_dist_set_config(gicd, id, cfg);
    gicv2_dist_set_priority(gicd, id, prio);
    gicv2_dist_set_enable(gicd, id, enable);
    handler_irq_set(id, c);
}

uint32_t intc_get_active()
{
    return gicc->iar;
}

void intc_ack(uint32_t iar)
{
    gicc->eoir = iar;
}

void intc_init(int global)
{
    int i;

    if(global) {
        for(i = 0; i < IRQ_COUNT; i++)
            handler_irq_set(i, NULL);
    }

    for(i = 0; i < 32; i++)
        handler_irq_set(i, NULL);

    gicc->ctlr = 0;
    gicc->pmr = 0xFF;
    gicc->bpr = 7;
    gicc->ctlr = 0x1;

    if(global) {
        gicd->ctlr = 0x1;
    }

    __asm__ ("msr DAIFClr, #15");
}



/*
 * inter-guest communication code
 */

#define IGC_MAGIC_VALUE 0xAA640CCA

/* the two guests use different vIRQs, we don't know which so we test both */
#define IRQ_IGC_CHAN0_A 32
#define IRQ_IGC_CHAN0_B 33


struct igc {
    uint32_t magic;
    uint32_t id;
    uint32_t version;
    uint32_t notify;
    uint32_t write_adr;
    uint32_t write_size;
    uint32_t read_adr;
    uint32_t read_size;
};


struct igc_chan chan0;


uint32_t igc_get_version(struct igc_chan *ic)
{
    volatile struct igc *igc = (struct igc *) ic->igc;
    return igc->version;
}

void igc_notify(struct igc_chan *ic)
{
    volatile struct igc *igc = (struct igc *) ic->igc;

    __asm__ volatile("dsb sy" ::: "memory"); /* just in case ... */

    igc->notify = 0x01234567;
}


void igc_init(int global)
{
    int i;
    volatile struct igc *igc = (struct igc *) IGC_BASE;

    if(!global) {
        return;
    }

    /* check it! */
    if(igc->magic != IGC_MAGIC_VALUE) {
        printf("NO IGC FOUND\n");
        return;
    }
    printf("IGC magic=%x version=%x\n", igc->magic, igc->version);

    /* setup chan0:  */
    chan0.igc = igc;
    chan0.read = (uint32_t *) igc->read_adr;
    chan0.write = (uint32_t *) igc->write_adr;
    chan0.read_size = igc->read_size;
    chan0.write_size = igc->write_size;
    /* XXX: this test takes whichever IRQ we can use as the IGC irq */
    chan0.irq = intc_can_use(IRQ_IGC_CHAN0_A) ? IRQ_IGC_CHAN0_A : IRQ_IGC_CHAN0_B;
#if 0
    intc_config(chan0.irq, 0, 0xF0, 1, chan0_data_recv);
#endif
    /* debug IGC config */
    printf("IGC chan0 irq: %d\n", chan0.irq);
    printf("IGC chan0 write: %x:%x\n", chan0.write, chan0.write_size);
    printf("IGC chan0 read: %x:%x\n", chan0.read, chan0.read_size);
}


/*
 * CPU timer
 */

uint32_t timer_delay()
{
    return 0x80000 << (4 * get_cpuid() + 2 * guest_id + 1);
}

void timer_next(uint32_t delay)
{
    MSR64("CNTP_CTL_EL0", 0);
    MSR64("CNTP_CVAL_EL0", 0x00000);
    MSR64("CNTP_TVAL_EL0", delay );
    MSR64("CNTP_CTL_EL0", 1);
}

void timer_start(handler_callback callback, uint32_t delay)
{
    intc_config(IRQ_TIMER, GICV2_ICFG_EDGE | GICV2_ICFG_1N, 0xE0, 1, callback);
    timer_next(delay);
    printf("timer irq = %d\n", IRQ_TIMER);
}

void timer_init(int global)
{
    MSR64("CNTP_CTL_EL0", 0);

}


/*
 * caches
 */
void cache_inst_clear()
{
    __asm__ volatile("isb\nic IALLUIS\nisb" ::: "memory");
}

void cache_data_cleaninv(adr_t start, size_t len)
{
    uint64_t ctr_el0, dline;
    adr_t end;

    /* get cache line size in bytes */
    ctr_el0 = MRS64("CTR_EL0");
    dline = 4ULL << ((ctr_el0 >> 16) & 0xF);

    /* align start to dline and calc end */
    len += start & (dline-1);
    start &= ~(dline-1);
    end = start + len;

    /* now clean all lines in that region */
    for(; start < end; start += dline)
        __asm__ volatile("DC CIVAC, %0" :: "r"(start) : "memory");
}


/*
 * PSCI
 */

#define PSCI_CMD_CPU_ON 0xC4000003

extern void __warm_boot();

void psci_cpuon(int cpu)
{
    uint32_t ret;
    adr_t entry;

    printf("Attempting to wake up cpu %d. Entry at %x...\n", cpu, entry);
    ret = __hvc(PSCI_CMD_CPU_ON, cpu, entry, 0);

    printf("PSCI CPU_ON returned %x\n", ret);
}

/*
 * debug
 */

void printf_putchar(int c)
{
    uart_write(UART_BASE, c);
}

void debug_init(int global)
{
}

/*
 *
 */

void system_init(int cpu)
{
    int i;

    if(cpu == 0) {
        guest_id = __boot_params[0];
        cpu_count = __boot_params[1];
    }

    debug_init(cpu == 0);
    intc_init(cpu == 0);
    igc_init(cpu == 0);
    timer_init(cpu == 0);

#if 0
    if(cpu == 0) {
        for(i = 1; i < cpu_count; i++) {
            psci_cpuon(i);
        }
    }
#endif
}
