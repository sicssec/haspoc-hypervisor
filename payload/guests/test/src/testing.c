/*
 * minimal test framework
 */

#include "defs.h"


static char *test_group_name;
static int test_count, test_fail, test_group = 0;
static int test_count_total = 0, test_fail_total = 0;

static void test_group_start(char *name)
{
    test_group_name = name;
    test_count = test_fail = 0;
    test_group ++;

    printf("\t TEST GROUP %d '%s': ",
           test_group, test_group_name);
}

static void test_group_end()
{
    if(test_fail) {
        printf("\n\t FAILED %d of %d tests\n",
               test_fail, test_count);
    } else {
        printf("PASSED %d tests\n", test_count);
    }

    test_group_name = NULL;
    test_fail_total += test_fail;
    test_count_total += test_count;
}




void _test_record(int line, char *file, char *msg, bool succ)
{
    test_count++;
    if(!succ) {

        if(test_fail == 0) /* first fail may need an extra \n */
            printf("\n");

        test_fail++;
        printf("\t\tFAILURE %d/%d in %s:%d\n"
               "\t\t        %s\n",
               test_fail, test_count, file, line,
               msg);
    }
}


/*
 * this function will run all testa referenced in the .testdata section
 * in the order of their priority (starting with 0)
 */
void test_run()
{
    struct testdata *t;
    uint32_t prio, remain;
    int test_levels;

    /* reset test stats */
    test_group_name = 0;
    test_count = test_fail = test_group = 0;
    test_count_total = test_fail_total = 0;
    test_levels = 0;

    /* print start message */
    printf("\n"
           "STARTING TESTS\n");

    /* for all prio levels ... */
    for(prio = 0; ; prio++) {
        printf("\n* TEST LEVEL %d...\n", prio);

        /* process all tests at this prio level */
        for(remain = 0, t = &__testdata_start; t != &__testdata_end; t++) {
            if(t->prio == prio) {
                test_group_start(t->name);
                t->func();
                test_group_end();
            } else if(t->prio > prio) {
                remain ++;
            }
        }

        /* nothing left in higher prio levels */
        if(!remain)
            break;
        else
            test_levels ++;
    }


    /* all done, report test summary */
    printf("\n"
           "TEST SUMMARY: %d levels, %d groups, %d tests",
           test_levels, test_group, test_count_total);

    if(test_fail_total) {
        printf(", %d FAILED!\n", test_fail_total);
    } else {
        printf(", ALL PASSED\n");

    }
}
