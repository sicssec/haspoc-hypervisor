
#include "defs.h"

#include <gicv2.h>


void fatal(char *msg)
{
    printf("FATAL: %s\n", msg);
    __die();
}


/***********************************************
 * timer
 ***********************************************/


void cpu0_timer_tick()
{
    static int timer_cnt = 0;

    /* update tick counter */
    timer_cnt++;
    printf("cpu0 TICK %d\n", timer_cnt);


    /* and schedule next tick */
    timer_next( timer_delay() );
}

void cpu1_timer_tick()
{
    int cpu;

    cpu = get_cpuid();
    printf("cpu%d TICK...\n", cpu);
    timer_next(timer_delay());
}

/***********************************************
 * main
 ***********************************************/

void main_cpu0()
{
    printf("main_cpu0: guest-ID=%d, cpu-count=%x, delay=%X\n",
           guest_id, cpu_count, timer_delay() );

    test_run();
}

void main_cpu1()
{
    printf("main_cpu1: guest-ID=%d, cpu-count=%x, delay=%X\n",
           guest_id, cpu_count, timer_delay() );

    timer_start(cpu1_timer_tick, timer_delay());
}


void main(uint64_t a, uint64_t b)
{
    int i, cpu;

    cpu = get_cpuid();
    switch(cpu) {
    case 0:
        main_cpu0();
        break;
    case 1:
        main_cpu1();
        break;
    default:
        fatal("Unknown cpu activated...");
    }

    /* END: return and wait for interrupts */
}
