/*
 * CPU timer
 */
#include "defs.h"

#include <gicv2.h>


uint32_t timer_delay()
{
    return 0x80000 << (4 * get_cpuid() + 2 * guest_id + 1);
}

void timer_next(uint32_t delay)
{
    MSR64("CNTP_CTL_EL0", 0);
    MSR64("CNTP_CVAL_EL0", 0x00000);
    MSR64("CNTP_TVAL_EL0", delay );
    MSR64("CNTP_CTL_EL0", 1);
}

void timer_start(intr_callback callback, uint32_t delay)
{
    intr_config(IRQ_TIMER, GICV2_ICFG_EDGE | GICV2_ICFG_1N, 0xE0, 1, callback);
    timer_next(delay);
}

void timer_init(int global)
{
    MSR64("CNTP_CTL_EL0", 0);
}
