/*
 * mini boots here
 */

#include "defs.h"

    .text
    .section .boot, "ax"
    .align 3

    .extern __vectors
    .global __warm_boot

    /*
     * cold boot: this is where the primary cpu starts
    */
__cold_boot:
__head:
    msr DAIFSet, #15

    /* save boot parameters in temp registers for now */
    mov x25, x0
    mov x26, x1

    /*
     * boot sanity checks
     */
    /* must be in EL1 */
    mrs x0, currentel
    ubfx x0, x0, #2, #2
    cmp w0, #1
    bne __die

    /* check memory config */
    bl __get_poffset
    mov x28, x0        /* X28 = virtual to physical */
    cmp x1, #0         /* physical base error */
    bne __die

    /* save boot parameters to variables */
    ldr x0, =__boot_param
    add x0, x0, x28
    stp w25, w26, [x0]

    /* zero BSS */
    ldr x0, =__bss_start__
    ldr x1, =__bss_end__
    sub x1, x1, x0      /* size */
    add x0, x0, x28     /* start in padr */
    bl __zero64

    bl __setup_mmu
    bl __setup_interupts
    b __head_core


    /*
     * this is the initialization code for all cores,
     * when this is called global initialization has already been performed
     */
__warm_boot:
    mov x26, x0
    mov x27, x1

__head_core:
    /* get poffset */
    bl __get_poffset
    mov x28, x0        /* X28 = virtual to physical */

    /* get initial stack  */
    bl __get_stack_top
    add x0, x0, x28     /* make it padr */
    mov sp, x0

    /* dont trap on FP or SIMD */
    mrs x0, cpacr_el1
    orr x0, x0, #(3 << 20)
    msr cpacr_el1, x0

    bl __activate_mmu
    bl __activate_interrupts

    /* run init & main  */
    mrs x0, mpidr_el1
    and x0, x0, #7
    bl init

    bl main

    /* we are done */
1:
    wfi
    b 1b


/*
 * interrupts
 */
__setup_interupts:
    ret

__activate_interrupts:
    /* Exception vectors */
    ldr x0, =__vectors
    msr vbar_el1, x0

    msr daifclr, #15
    ret

/*
 * MMU: runs in physical memory and without a stack
 */
__setup_mmu:

    mov x25, x30 /* save return address */

    /* extract X20=l0, X21=l1_ram, X22=l1_misc, l2_dev=X23, l2_igc=X24 */
    bl __get_pt_start
    add x20, x0, x28   /* make it padr */
    add x21, x20, #PT_SIZE
    add x22, x21, #PT_SIZE
    add x23, x22, #PT_SIZE
    add x24, x23, #PT_SIZE


    /* zero all tables */
    mov x0, x20
    mov x1, #EL1_SIZE_PT
    bl __zero64


    /* l0: setup l1_ram mapping */
    add x0, x21, #PT_ATTR_PAGE  /* PT_ATTR_PAGE | (uint64_t) l1_ram */
    str x0, [x20, #8 * RAM_VBASE / PT_L0_SIZE]

#if (RAM_VBASE / PT_L0_SIZE) != (RAM_PBASE / PT_L0_SIZE)
    /* temp 1-1 mapping for the jump to virtual memory */
    str x0, [x20, #8 * RAM_PBASE / PT_L0_SIZE]
#endif

    /* l0: setup l1_misc mapping */
    add x0, x22, #PT_ATTR_PAGE  /* PT_ATTR_PAGE | (uint64_t) l1_misc */
    str x0, [x20, #8 * DEVICE_VBASE / PT_L0_SIZE]

    /* l1_ram: set RAM mapping */
    add x0, x21, #0
    add x1, x21, #RAM_SIZE / PT_L1_SIZE
    ldr x2, =RAM_PBASE + PT_ATTR_L12_MEM
1:  str x2, [x0], #8
    add x2, x2, #PT_L1_SIZE
    cmp x0, x1
    ble 1b

    /* l1_misc set l2_dev and l2_igc tables */
    add x0, x23, #PT_ATTR_PAGE
    add x1, x24, #PT_ATTR_PAGE
    str x0, [x22, #0]
    str x1, [x22, #8]

    br x25


    /*
     * MMU tables are already setup, just activate them
     */
__activate_mmu:
    mov x25, x30

    /* set page table root */
    bl __get_pt_start
    add x20, x0, x28   /* make it padr */
    msr ttbr0_el1, x20
    msr ttbr1_el1, x20
    isb

    ldr x0, =MAIR_EL1
    ldr x1, =0x031b5193519
    ldr x2, =SCTLR_EL1

#if defined(TARGET_s810)
    /* turned D-cache off, temp s810 fix */
    bic x2, x2, #4
#endif

    msr mair_el1, x0
    msr tcr_el1, x1
    msr sctlr_el1, x2
    isb

    /* jump to virtual address */
    ldr x0, =1f
    isb
    br x0
1:
    /* clear caches & TLB */
    dsb sy
    ic IALLUIS
    dc CIVAC, xzr       /* TODO: fix this */
    tlbi VMALLE1IS
    isb

    /* update stack and LR */
    mov x0, sp
    sub x0, x0, x28
    mov sp, x0
    sub x25, x25, x28

    /* and we are done! */
    br x25

    .data
    .global guest_id
    .global cpucount
__boot_param:
guest_id:    .long 0
cpucount:     .long 0
