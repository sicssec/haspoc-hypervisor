#ifndef _DEFS_H_
#define _DEFS_H_

#include "base.h"
#include "arch_defs.h"
#include <armv8.h>

/* MMU related defs */
#define PT_SIZE (PAGE_SIZE)
#define PT_ENTRIES 512
#define PT_L0_SIZE (1 * GB)
#define PT_L1_SIZE (2 * MB)
#define PT_L2_SIZE (4 * KB)

/* TODO: correct these: */
#define PT_ATTR_PAGE 0x003

/* level 1,2 Page descriptors
 *
 * Bits not shown are reseved or ignored (e.g. used in Secure state)
 *--------------------------------------------------
 * 54   UXN, unpriviliged execute-never bit (for EL1 & EL0, otherwise XN)
 * 53   PXN, priviliged execute-never bit, executable in EL1
 * 52   CONT, hint indicating one of a contigous set of entries (see D4-1713
 *      ARM DDI 0487A.i)
 * 47:n Output address with
 *          n = 30  4KB granule size, level 1 descriptor
 *          n = 29  64KB granule size, level 2 descriptor
 *          n = 25  16KB granule size, level 2 descriptor
 *          n = 21  4KB granule size, level 2 descriptor
 * 11   nG, not global bit. Does TLB entry apply to all ASID values or current?
 * 10   AF, access flag. Default value 1.
 * 9:8  SH, shareability field.
 *          00 Non-shareable
 *          01 UNPREDICTABLE
 *          10 Outer Shareable
 *          11 Inner Shareable
 * 7:6  Access Permissions,
 *              Access from EL1     Access from EL0
 *          00    Read/write           None
 *          01    Read/write           Read/write
 *          10    Read-only            None
 *          11    Read-only            Read-only
 * 4:2  AttrIndx, stage 1 memory attributes index field for MAIR_EL1. The
 *      MAIR_EL1 field holds information regarding cacheability etc. See this
 *      field below for more information.
 * 1:0  Set to 01 for block or page
 */

/* [54]=0, [53]=0, [52]=0, [11]=1, [10]=1, [9:8]=10, [7:6]=01,
    [4:2]=001 (i.e. Attr1), [1]=0, [0]=1 */
#define PT_ATTR_L12_MEM  0x725
/* [54]=0, [53]=0, [52]=0, [11]=1, [10]=1, [9:8]=00, [7:6]=01,
    [4:2]=000 (i.e. Attr0), [1]=0, [0]=1 */
#define PT_ATTR_L12_DEV  0x621
/* [54]=0, [53]=0, [52]=0, [11]=1, [10]=1, [9:8]=10, [7:6]=00,
    [4:2]=001 (i.e. Attr1), [1]=0, [0]=1 */
#define PT_ATTR_L12_IGC  0x705

/* level 3
 *--------------------------------------------------
 * 1:0  Set to 11 for page
 */
#define PT_ATTR_L3_MEM  ((PT_ATTR_L12_MEM) | 3)
#define PT_ATTR_L3_DEV  ((PT_ATTR_L12_DEV) | 3)
#define PT_ATTR_L3_IGC  ((PT_ATTR_L12_IGC) | 3)

/* MAIR_EL1
 * Provides the memory attribute encodings corresponding to the possible
 * AttrIndx values in a Long-descriptor format translation table entry for
 * stage 1 translations at EL1
 *
 *--------------------------------------------------
 * 63:56    Attr7
 * 55:48    Attr6
 * 47:40    Attr5
 * 39:32    Attr4
 * 31:24    Attr3
 * 23:16    Attr2
 * 15:8     Attr1
 * 7:0      Attr0
 *
 * For Attr<n>:
 * 7:6  Type of memory / outer cacheability
 *      00  (RW == 00) Device memory, see below for deatils
 *      00  (RW != 00) Normal memory (NM), Outer Write-through transient
 *      01  (RW == 00) NM, Outer Non-cacheable
 *      10  (RW != 00) NM, Outer Write-back transient
 *      10             NM, Outer Write-through non-transient
 *      11             NM, Outer Write-back non-transient
 * 5    R, Outer Read Allocate Policy
 * 4    W, Outer Write Allocate Policy
 * 3:2  Type of memory / inner cacheability
 *      if bit[7:4] == 0000
 *          00  (RW == 00)  Device-nGnRnE memory
 *          00  (RW != 00)  Unpredictable
 *          01  (RW == 00)  Device-nGnRE memory
 *          01  (RW != 00)  Unpredictable
 *          10  (RW == 00)  Device-nGRnE memory
 *          10  (RW != 00)  Unpredictable
 *          11  (RW == 00)  Device-GRE memory
 *          11  (RW != 00)  Unpredictable
 *      else
 *          00  (RW == 00)  Unpredictable
 *          00  (RW != 00)  NM, Inner Write-through transient
 *          01  (RW == 00)  NM, Inner Non-cacheable
 *          01  (RW != 00)  NM, Inner Write-back transient
 *          10              NM, Inner Write-through non-transient
 *          11              NM, Inner Write-back non-transient
 * 1    R, Inner Read Allocate Policy
 * 0    W, Inner Write Allocate Policy
 */

 /* Attr1: [7:6]=11, [5]=1, [4]=1, [3:2]=11, [1]=1, [0]=1 (Outer cacheable)
    Attr0: [7:6]=00, [5]=0, [4]=0, [3:2]=00, [1]=0, [0]=0 (Device memory)
  */
#define MAIR_EL1 0xFF00

/* SCTLR_EL1
 * Provides top level control of the system, including its memory
 * system, at EL1 and EL0.
 *
 * Bits not shown are reseved (RES0/RES1)
 *--------------------------------------------------
 * 26 UCI: Traps EL0 execution of cache maintenance instructions to EL1
 * 25 EE: Endianness of data accesses at EL1, and stage 1 translation table
 *        walks in the EL1&0 translation regime.
 * 24 E0E: Endianness of data accesses at EL0.
 * 19 WXN: Write permission implies XN (Execute-never).
 * 18 nTWE: Traps EL0 execution of WFE instructions to EL1.
 * 16 nTWI: Traps EL0 execution of WFI instructions to EL1.
 * 15 UCT: Traps EL0 accesses to the CTR_EL0 to EL1
 * 14 DZE: Traps EL0 execution of DC ZVA instructions to EL1
 * 12 I: Instruction access Cacheability control, for accesses at
 *       EL0 and EL1.
 * 9 UMA: User Mask Access. Traps EL0 execution of MSR and MRS instructions
 *        that access the PSTATE.{D, A, I, F} masks to EL1.
 * 8 SED: SETEND instruction disable. Disables SETEND instructions at EL0
 *        using AArch32.
 * 7 ITD: IT Disable. Disables some uses of IT instructions at EL0 using
 *        AArch32.
 * 5 CP15BEN: CP15 memory barrier enable. Enables accesses to the CP15 DMB,
 *            DSB, and ISB barrier operations from EL0 using AArch32.
 * 4 SA0: Stack Alignment Check Enable for EL0. When set, use of the stack
 *        pointer as the base address in a load/store instruction at EL0
 *        must be aligned to a 16-byte boundary.
 * 3 SA: Stack Alignment Check Enable. When set, use of the stack pointer as
 *       the base address in a load/store instruction at EL1 must be aligned
 *       to a 16-byte boundary.
 * 2 C: Cacheability control, for data caching.
 * 1 A: Alignment check enable.
 * 0 M: MMU enable for EL1 and EL0 stage 1 address translation
 *--------------------------------------------------
 * [26]=1, [25]=0, [24]=0, [19]=0, [18]=1, [16]=1, [15]=1, [14]=1, [12]=1,
 * [9]=1, [8]=1, [7]=0, [5]=0, [4]=1, [3]=1, [2]=1, [1]=0, [0]=1
 * Other bits are set to their RES0/RES1 values
 */
#define SCTLR_EL1 0x34d5db1d

/* physical memory layout
 *
 *
 *  +--------------------+ 0xC0400000
 *  |        igc         |
 *  +--------------------+ 0xC0200000 = IGC_PBASE
 *  |      devices       |
 *  +--------------------+ 0xC0000000 = DEVICE_PBASE
 *  |     ....           |
 *  +--------------------+ END OF RAM = RAM_PBASE + RAM_SIZE
 *  |     ....           |
 *  +--------------------+
 *  |   page tables      | size = SIZE_PT
 *  +--------------------+
 *  |                    |
 *  |    stack           | size = SIZE_STACK
 *  |                    |
 *  +--------------------+ heap start
 *  |  pad to page size  |
 *  +--------------------+ __bss_end__
 *  |    .bss            |
 *  |    .data           |
 *  |    .text           |
 *  +--------------------+ offset ? ENTRY_OFFSET
 *  |                    |
 *  +--------------------+ RAM_PBASE
 */

#define RAM_SIZE (MB * 128)
#define RAM_PBASE 0
#define ENTRY_OFFSET 0x80000
#define STACK_SIZE   (32 * KB) /* 8KB per core! */
#define EL1_SIZE_PT      (PT_SIZE * 3)

#define DEVICE_PBASE 0xC0000000
#define IGC_PBASE    0xC0200000



/* virtual memory layout
 *
 *  +--------------------+ 0xC0400000
 *  |        igc         |
 *  +--------------------+ 0xC0200000 = IGC_VBASE
 *  |      devices       |
 *  +--------------------+ 0xC0000000 = DEVICE_VBASE
 *  |         ...        |
 *  +--------------------+
 *  | use memory, same   |
 *  | before including   | size = MEMORY_SIZE
 *  | .text .data heap ..|
 *  +--------------------+ RAM_VBASE
 */
#define RAM_VBASE 0x80000000
#define DEVICE_VBASE 0xC0000000
#define IGC_VBASE    0xC0200000

#define GICD_VBASE ((DEVICE_VBASE) + 0x00000)
#define GICC_VBASE ((DEVICE_VBASE) + 0x01000)
#define UART_VBASE ((DEVICE_VBASE) + 0x04000)

#define IRQ_COUNT 96
#define IRQ_TIMER 0x1E

#ifndef __ASSEMBLER__

#include <stdint.h>
#include <minilib.h>


static inline int get_cpuid()
{
    return MRS64("mpidr_el1") & 0x7;
}

extern uint32_t guest_id;
extern uint32_t cpucount;

/* memory */
extern adr_t __get_pt_start();
extern adr_t __get_stack_top();

extern int mmu_vadr_to_padr(adr_t vadr, adr_t *padr);
extern void mmu_map(adr_t vadr, adr_t padr, size_t size, uint64_t attr);
extern void tlb_flush();
extern void tlb_flush_adr(adr_t adr, size_t size);
extern void cache_inst_clear();
extern void cache_data_cleaninv(adr_t start, size_t len);


/* interrupts */
typedef void (*intr_callback)();
extern int intr_can_use(int id);
extern void intr_config(int id, int cfg, int prio, int enable, intr_callback c);
extern void intr_init(int global);

/* debug */
extern void debug_init(int global);

// timer
extern void timer_tick();
extern void timer_next(uint32_t delay);
extern void timer_start(intr_callback callback, uint32_t delay);
extern void timer_init(int global);
extern uint32_t timer_delay();

// igc
struct igc_chan {
    void *igc;
    volatile uint32_t *read;
    volatile uint32_t *write;
    uint32_t read_size;
    uint32_t write_size;
    int irq;
};

extern struct igc_chan chan0;
extern void chan0_data_recv();
extern void chan0_data_send();

extern uint32_t igc_get_version(struct igc_chan *ic);
extern void igc_notify(struct igc_chan *ic);
extern void igc_init();

/* psci */
extern void psci_cpuon(int cpu);

/* misc */
extern void fatal(char *msg);
extern void __die();
extern void __emergency_erase();

extern uint32_t __smc(uint32_t func, uint64_t x1, uint64_t x2, uint64_t x3);
extern uint32_t __hvc(uint32_t func, uint64_t x1, uint64_t x2, uint64_t x3);


#endif /* ! __ASSEMBLER__ */

#endif /* _DEFS_H_ */
