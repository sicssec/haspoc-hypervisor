/*
 * debug code, e.g. printing
 */

#include "defs.h"


void printf_putchar(int c)
{
    uart_write(UART_VBASE, c);
}

void debug_init(int global)
{
    if(global) {
        /* map uart */
        mmu_map(UART_VBASE, UART_VBASE - DEVICE_VBASE + DEVICE_PBASE,
                PAGE_SIZE, PT_ATTR_L3_DEV);
    }
}
