
#include "defs.h"

#define PSCI_CMD_CPU_ON 0xC4000003

extern void __warm_boot();

void psci_cpuon(int cpu)
{
    uint32_t ret;
    adr_t entry;


    if(!mmu_vadr_to_padr((adr_t) __warm_boot, &entry))
        fatal("Internal error: cannot translate PSCI entry");

    printf("Attempting to wake up cpu %d. Entry at %x...\n", cpu, entry);
    ret = __hvc(PSCI_CMD_CPU_ON, cpu, entry, 0);

    printf("PSCI CPU_ON returned %x\n", ret);
}
