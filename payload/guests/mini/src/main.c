
#include "defs.h"

#include <gicv2.h>

void fatal(char *msg)
{
    printf("FATAL: %s\n", msg);
    __die();
}

/***********************************************
 * timer
 ***********************************************/


void cpu0_timer_tick()
{
    static int timer_cnt = 0;

    /* update tick counter */
    timer_cnt++;
    printf("cpu0 TICK %d\n", timer_cnt);

    /* emergency erase test code */
    if(timer_cnt == 25) {
        printf("GUEST requesting emergency erase...\n");
        __emergency_erase();
    }

    /* send some data to the other end  */
    chan0_data_send(timer_cnt);


    /* and schedule next tick */
    timer_next( timer_delay() );
}

void cpu1_timer_tick()
{
    int cpu;

    cpu = get_cpuid();
    printf("cpu%d TICK...\n", cpu);
    timer_next(timer_delay());
}


/***********************************************
 * IGC channel 0
 ***********************************************/

struct chan0_data
{
    uint32_t cnt;
    uint32_t sender;
    uint32_t data;
};

void chan0_data_send(int tick)
{
    struct chan0_data *data = (struct chan0_data *) & chan0.write[0];
    int i;

    /* make up some semi random data */
    data->cnt++;
    data->sender = guest_id;
    data->data =  tick | (guest_id << 16);

    /* clear it from the cache */
    cache_data_cleaninv((adr_t) data, sizeof(struct chan0_data));

    /* notify other end */
    igc_notify(&chan0);
}

 void chan0_data_recv()
{
    static int old_version = -1;

    struct chan0_data *data = (struct chan0_data *) & chan0.read[0];
    int version = igc_get_version(&chan0);

    /* just dump whatever we can see */
    printf("RECV v%d (%d) cnt=%d from %d\tData=%x\n",
           version, old_version, data->cnt, data->sender, data->data);

    old_version = version;
}

static void chan0_init()
{
    struct chan0_data *data = (struct chan0_data *) & chan0.write[0];
    memset(data, 0, sizeof(struct chan0_data));
    cache_data_cleaninv((adr_t) data, sizeof(struct chan0_data));
}

void init(int cpu)
{
    int i;

    debug_init(cpu == 0);
    intr_init(cpu == 0);
    igc_init(cpu == 0);
    timer_init(cpu == 0);

    if(cpu == 0) {
        for(i = 1; i < cpucount; i++) {
            psci_cpuon(i);
        }
    }
}

/***********************************************
 * main
 ***********************************************/

#ifdef TARGET_s810

void main()
{
    printf("kernel_main: CPU %d. guest-ID=%d, delay=%d\n",
           get_cpuid(), guest_id, timer_delay() );

#if 0
    /* set up channel data */
    chan0_init();

    /* start the initial timer */
    timer_start(cpu0_timer_tick, timer_delay());
#endif

	volatile uint64_t j = 1000000;
    while(--j);

	__hvc(0xC3000888, 0, 0, 0);

    /* END: return and wait for interrupts */
}
#else

void main_cpu0()
{
    printf("main_cpu0: guest-ID=%d, cpu-count=%x, delay=%X\n",
           guest_id, cpucount, timer_delay() );

    chan0_init();
    timer_start(cpu0_timer_tick, timer_delay());
}

void main_cpu1()
{
    printf("main_cpu1: guest-ID=%d, cpu-count=%x, delay=%X\n",
           guest_id, cpucount, timer_delay() );

    timer_start(cpu1_timer_tick, timer_delay());
}

void main()
{
    int i, cpu;

    cpu = get_cpuid();
    switch(cpu) {
    case 0:
        main_cpu0();
        break;
    case 1:
        main_cpu1();
        break;
    default:
        fatal("Unknown cpu activated...");
    }

    /* END: return and wait for interrupts */
}

#endif
