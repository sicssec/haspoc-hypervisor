This folder contain the logic to build the Linux kernel to boot an Ubuntu
server OS based on a special version of Linux 4.1.6 kernel by Open-Estuary.

Note that the foreign repositories are stored in external/ and that we assume
you are using the Lemaker-version of HiKey with 8GB eMMC.

The ubuntu rootfs can downloaded from:

http://download.open-estuary.org/?dir=AllDownloads/DownloadsEstuary/releases/2.1/linux/Ubuntu/Common

Other files that may come in handy can be found at

http://download.open-estuary.org/?dir=AllDownloads/DownloadsEstuary/releases/2.1/linux/Common/HiKey

Either untar the contents to an SD card or follow the instructions found in the
Documentation folder at the above given address to get the content on the eMMC.

The current config expects the rootfs to be found on the second partition on a
SD card, to change this replace the following part of the chosen/bootargs node
in the guest devicetree to e.g.:

	root=/dev/mmcblk1p2    --->    root=/dev/mmcblk0p9 (emmc system partition)

Once you have the ubuntu server up and running, you should update the kernel
with the haspoc version of it. To do so make the debian packages of the kernel
by invoking

	$ make -C external/linux_HIKEY_ubuntu_server/ ARCH=arm64 -j32 deb-pkg

Note that this takes a long time, and that you should do it as root. After copy
somehow the smaller linux-image[...].deb and linux-headers[...].deb to the
ubuntu rootfs and from inside ubuntu issue

	$ dpkg -i linux-image[...].deb
	$ dpkg -i linux-headers[...].deb

Restart the hikey board and you should now be using the new kernel. Verify by
issuing 'uname -r'.

Also note that there are quite a few packages from Open Estuary which are
related to cloud services and which may delay the boot process considerbly,
these can be removed or disabled without problems.