This folder contain the logic to build the Linux kernel and the initial
filesystem used to boot Android Open Source Project (aosp) v6.0.1.

Note that the foreign repositories are stored in external/ and that we assume
you are using the Lemaker-version of HiKey with 8GB eMMC.

Building this guest will only produce the kernel and the initfs, you will still
need to flash the imgs to the hikey board using these commands:

    $ fastboot flash ptable ptable-aosp-8g.img
    $ fastboot flash cache cache.img
    $ fastboot flash userdata userdata.img
    $ fastboot flash system system.img

These files are downloaded automatically when building the Android guest and the
img-files are afterwards available in

   ./build/android_sb_hikey/

or ./build/android_atf_hikey/ if Android Trusted Firmware is used instead of
Secure Boot

See the 96boards wiki for more information on how to flash the different images
to the HiKey platform.

https://github.com/96boards/documentation/wiki/HiKeyGettingStarted

If you would like to make changes to the virtual address mappings, know that a
few addresses are hard coded in the Mali driver. See the files in

	external/linux_HIKEY_aosp_4.1/drivers/gpu/arm/utgard/platform/hikey

Also note that if you make changes to the address of the eMMC, SDcard reader or
the USB OTG block. You need to update rootfs.cpio used by the kernel in

	payload/guests/android/filesystem/prebuilt/rootfs.cpio

More specifically you need to make changes to fstab.hikey, init.hikey.rc and
init.hikey.usb.rc.

-------------------------------------------------------------------------

Note that adb is available on OTG usb port, from which you can install apk,
send shell commands, etc. Be sure to run 'sudo adb start-server' or no
devices will be found, check with 'adb devices' which should list a device
with id 01234678 or similar.
