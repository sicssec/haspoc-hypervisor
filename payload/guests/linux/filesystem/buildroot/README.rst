
This will build the initial filesystem for you using the buildroot project.
The generated filesystem will then be embedded into the Linux kernel as a
memory filesystem.

Note however that buildroot is quite large and a simple build will require
a lot of storage and has a relatively long build time!

--

By default, the configuration "sth_defconfig" is used. If you want to use
a different configuration, save it to "user_defconfig", which will automatically
be used instead of "sth_defconfig" if available.
