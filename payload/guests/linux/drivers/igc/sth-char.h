/* Copyright (c) 2016 Tutus AB.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

#ifndef STH_CHAR_H
#define STH_CHAR_H

typedef struct _igc_ {
  uint16_t read_ver;
  uint16_t write_ver;
  uint32_t size;
  uint8_t data[]; /* arbitrary data type */

} __attribute__((packed)) igc_t;

struct igc_channel {
  uint8_t is_available;
  struct cdev igc_cdev;
  unsigned int irq;
  int *notify; // notify address
  igc_t *this; // write area address
  size_t write_size;
  igc_t *other; // read area address
  size_t read_size;
  char *other_end;
  struct mutex igc_mutex;
};

struct igc_device {
  unsigned int major;
  phys_addr_t phys_base;
  __iomem void *virt_base;
  size_t total_size;
  u32 nr_of_channels;
  struct class *class;
  struct igc_channel *channels;
};

#endif /* STH_CHAR_H */
