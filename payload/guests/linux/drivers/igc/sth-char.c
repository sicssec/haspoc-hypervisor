/* Copyright (c) 2016 Tutus AB.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * ------------------------------------------------------------------------- *
 *
 * WARNING: This driver was written mostly as a proof-of-concpet and basic
 * debugging. It could be used for those purposes and possibly for simple
 * messaging between two guests. If you want performance we advise you to use
 * the ethernet driver instead (sth-eth.c).
 *
 * Driver for SICS Thin Hypervisor shared memory inter guest communication
 *
 * Layout below for how the shared memory is set up and how control records
 * area are laid out within these areas.
 *
 * Version variables are 16-bit and size 32-bit. Version actually only needs
 * 1-bit so this might change in the future.
 *
 *    Layout of shared memory area:
 *
 *                               | +----------------+ |
 *                        16bit  | | this read ver  | |
 *                               | +----------------+ |
 *                        16bit  | | this write ver | |
 *                               | +----------------+ |
 *                        32bit  | | size of write  | |
 *             This side         | +----------------+ |        Other side
 *             write area  ----->| |                | |------> read area
 *                               | |                | |
 *                               | |     this       | |
 *                               | |     data       | |
 *                               | |     area       | |
 *                               | |                | |
 *                               | |                | |
 *                               | |                | |
 *                               | |                | |
 *                                 +================+
 *                        16bit  | | other read ver | |
 *                               | +----------------+ |
 *                        16bit  | | other write ver| |
 *                               | +----------------+ |
 *                        32bit  | | size of write  | |
 *                               | +----------------+ |
 *                               | |                | |
 *                               | |                | |
 *             This side         | |     other      | |        Other side
 *             read area   <-----| |     data       | |<------ write area
 *                               | |     area       | |
 *                               | |                | |
 *                               | |                | |
 *                               | |                | |
 *                               | |                | |
 *                               | |                | |
 *                               | |                | |
 *                               | +----------------+ +
 *
 * To determine if we can write we compare this write version
 *      with other side's read  version
 * To determine if we can read  we compare this read  version
 *      with other side's write version
 */

#include <linux/module.h>
#include <linux/cdev.h>
#include <linux/slab.h>
#include <linux/of_irq.h>
#include <linux/of_address.h>
#include <linux/of_platform.h>
#include <linux/fs.h>
#include <linux/interrupt.h>
#include <asm/uaccess.h>
#include <asm/barrier.h>

#include "sth-char.h"

#define IGC_MAGIC 0xAA640CCA

static DECLARE_WAIT_QUEUE_HEAD(igc_wait_read);
static DECLARE_WAIT_QUEUE_HEAD(igc_wait_write);
static DEFINE_MUTEX(igc_mutex);

static const char igc_name[] = "igc";
struct igc_device igc_dev;

static irqreturn_t igc_interrupt_handler(int irq, void *dev_id)
{
    struct igc_channel *chan = (struct igc_channel *) dev_id;

    if ( chan->this->write_ver == chan->other->read_ver ) {
        wake_up_interruptible(&igc_wait_write);
    }

    if ( chan->this->read_ver != chan->other->write_ver ) {
        wake_up_interruptible(&igc_wait_read);
    }

    return IRQ_HANDLED;
}

static ssize_t igc_read(struct file *file, char __user *buf,
    size_t count, loff_t *ppos)
{
    struct igc_channel *chan = file->private_data;
    int retval;

    DECLARE_WAITQUEUE(wait, current);

    add_wait_queue(&igc_wait_read, &wait);

    do {
        __set_current_state(TASK_INTERRUPTIBLE);

        if ( chan->this->read_ver != chan->other->write_ver ) {
            break;
        }

        if (file->f_flags & O_NONBLOCK) {
            retval = -EAGAIN;
            goto out;
        }

        if (signal_pending(current)) {
            retval = -ERESTARTSYS;
            goto out;
        }

        schedule();
    } while (1);

    if( copy_to_user(buf, chan->other->data, chan->other->size) ) {
        // we drop the data, this "if" avoids the compile warning
    }
    chan->this->read_ver++;
    retval = chan->other->size;
    *chan->notify = 1; // notify other end, value is arbitrary

    mb(); // make sure everything is written to cache

out:
    __set_current_state(TASK_RUNNING);
    remove_wait_queue(&igc_wait_read, &wait);
    return retval;
}

static ssize_t igc_write(struct file *file, const char __user *buf,
    size_t count, loff_t *ppos)
{
    struct igc_channel *chan = file->private_data;
    int retval;

    DECLARE_WAITQUEUE(wait, current);

    add_wait_queue(&igc_wait_write, &wait);

    do {
        __set_current_state(TASK_INTERRUPTIBLE);

        if ( chan->this->write_ver == chan->other->read_ver ) {
            break;
        }


        if (file->f_flags & O_NONBLOCK) {
            retval = -EAGAIN;
            goto out;
        }

        if (signal_pending(current)) {
            retval = -ERESTARTSYS;
            goto out;
        }

        schedule();
    } while (1);

    if( copy_from_user(chan->this->data, buf, count) ) {
        // we drop the data, this avoids the compile warning
    }
    chan->this->size = count;
    chan->this->write_ver++;
    *chan->notify = 1; // notify other end, value is arbitrary

    mb(); // make sure everything is written to cache

out:
    __set_current_state(TASK_RUNNING);
    remove_wait_queue(&igc_wait_write, &wait);
    return count;
}

/* allows only one open instance per channel! */
int igc_open(struct inode *inode, struct file *filp)
{
    struct igc_channel *channel; /* device information */

    channel = container_of(inode->i_cdev, struct igc_channel, igc_cdev);
    if(!channel) {
        return -ENODEV;
    }
    mutex_lock(&channel->igc_mutex);
    if(!channel->is_available) {
        return -EBUSY;
    } else { // lock the channel
        channel->is_available = 0; /* false */
    }
    mutex_unlock(&channel->igc_mutex);
    filp->private_data = channel; /* for other methods */

    return 0; /* success */
}

int igc_release(struct inode *inode, struct file *filp)
{
    struct igc_channel *channel; /* device information */

    channel = container_of(inode->i_cdev, struct igc_channel, igc_cdev);
    if(!channel) {
        return -ENODEV;
    }
    mutex_lock(&channel->igc_mutex);
    channel->is_available = 1; /* true */
    mutex_unlock(&channel->igc_mutex);

    return 0; /* success */
}

/* not implemented! */
static long igc_unlocked_ioctl(struct file *file, unsigned int cmd, unsigned long arg)
{
    int ret = 0;

//    mutex_lock(&igc_mutex);
// 	ret = igc_ioigc(file, cmd, arg);
    pr_info("IGC ioctl\n");
//    mutex_unlock(&igc_mutex);

    return ret;
}

const struct file_operations igc_fops = {
    .owner			= THIS_MODULE,
    .read			= igc_read,
    .write			= igc_write,
    .unlocked_ioctl	= igc_unlocked_ioctl,
    .open 			= igc_open,
    .release 		= igc_release,
};

/* Match table for of_platform binding */
static const struct of_device_id igc_of_match[] = {
        { .compatible = "sth,igc", }, /* sth as in SICS Thin Hypervisor */
    {}
};

MODULE_DEVICE_TABLE(of, igc_of_match);

static void igc_cleanup_module(int devices_to_destroy)
{
    int i;

    /* Get rid of channels, i.e. character devices (if any exist) */
    if (igc_dev.channels) {
        for (i = 0; i < devices_to_destroy; ++i) {
            free_irq(igc_dev.channels[i].irq, &igc_dev.channels[i]);
            device_destroy(igc_dev.class, MKDEV(igc_dev.major, i));
            cdev_del(&igc_dev.channels[i].igc_cdev);
            mutex_destroy(&igc_dev.channels[i].igc_mutex);
        }
        if(igc_dev.channels != NULL)
            kfree(igc_dev.channels);
    }

    if (igc_dev.class)
        class_destroy(igc_dev.class);
    iounmap(igc_dev.virt_base);
    release_mem_region(igc_dev.phys_base, igc_dev.total_size);
    return;
}

static int igc_create_device(struct device_node *child, int minor)
{
    struct igc_channel *channel;
    __iomem void *test_ptr;
    int err;
    dev_t devno;
    struct device *temp_device;
    u32 prop_vals[2];

    channel = &igc_dev.channels[minor];
    channel->is_available = 1;

    // retrieves values as in: ctrl = <address-offset size> and stores it in the array prop_vals
    err = of_property_read_u32_array(child, "ctrl", prop_vals, 2);
    if(err) {
        pr_warning("channel setup, ctrl: could not get values\n");
        return err;
    }
    test_ptr = igc_dev.virt_base + prop_vals[0]; /* prop_vals[0] = offset from base */
    if(ioread32(test_ptr) != IGC_MAGIC) {
        pr_warning("channel setup, ctrl: magic value did not match in channel%d, expected 0x%X found 0x%X\n", minor, IGC_MAGIC, ioread32(test_ptr));
        return err;
    }
    channel->notify = test_ptr + 12;
    // write address and size
    err = of_property_read_u32_array(child, "write", prop_vals, 2);
    if(err) {
        pr_warning("channel setup, write: could not get values\n");
        return err;
    }
    channel->this = igc_dev.virt_base + prop_vals[0]; /* prop_vals[0] = offset from base */
    channel->write_size = prop_vals[1]; /* prop_vals[1] = size of area */

    // read address and size
    err = of_property_read_u32_array(child, "read", prop_vals, 2);
    if(err) {
        pr_warning("channel setup, read: could not get values\n");
        return err;
    }
    channel->other = igc_dev.virt_base + prop_vals[0]; /* prop_vals[0] = offset from base */
    channel->read_size = prop_vals[1]; /* prop_vals[1] = size of area */

    /* Gets the name of the guest on the other end of the channel */
    channel->other_end = (char *)kzalloc(21 * sizeof(char), GFP_KERNEL);
    if (channel->other_end == NULL) {
        pr_warning("channel setup: kzalloc failed\n");
        return -ENOMEM;
    }
    err = of_property_read_string(child, "other-end", (const char **) &channel->other_end);
    if(err) {
        pr_warning("channel setup: could not get value for other-end\n");
        kfree(&channel->other_end);
        return err;
    }

    /* create and setup device */
    devno = MKDEV(igc_dev.major, minor);
    cdev_init(&channel->igc_cdev, &igc_fops);
    channel->igc_cdev.owner = THIS_MODULE;

    err = cdev_add(&channel->igc_cdev, devno, 1);
    if (err) {
        pr_warning("[target] Error %d while trying to add %s%d",
            err, igc_name, minor);
        goto fail;
    }

    temp_device = device_create(igc_dev.class, NULL, /* no parent device */
                        devno, NULL, /* no additional data */
                        "%s%d", igc_name, minor);

    if (IS_ERR(temp_device)) {
        err = PTR_ERR(temp_device);
        pr_warning("[target] Error %d while trying to create %s%d",
            err, igc_name, minor);
        goto fail;
    }

    /* setup the irq */
    channel->irq = irq_of_parse_and_map(child, 0); // includes mapping irq to virq
    err = request_irq(channel->irq, igc_interrupt_handler, 0, igc_name, channel);
    if(err) {
        pr_warning("Request of irq = %d in config failed!\n", channel->irq);
        err = -EIO;
        goto fail;
    }

    mutex_init(&channel->igc_mutex);

    pr_info("sth-igc: channel%d setup with \"%s\" on the other end\n", minor, channel->other_end);

    return 0; /* success */

fail:
    kfree(&channel->other_end);
    device_destroy(igc_dev.class, MKDEV(igc_dev.major, minor));
    cdev_del(&channel->igc_cdev);
    return err;
}

static int igc_probe(struct platform_device *op)
{

    struct device_node *master_node;
    struct device_node *child;
    const struct of_device_id *match;
    int err, minor;
    dev_t dev;
    struct resource res;

    pr_info("SICS Thin Hypervisor: Inter Guest Communication driver\n");
    pr_warning("sth-char: This driver was written for debugging purposes, use at own risk\n");

    master_node = op->dev.of_node;
    /* sanity check: this device matches the compatible string */
    match = of_match_device(igc_of_match, &op->dev);
    if (!match)
        return -EINVAL;

    // The following is basically an unwrapped of_io_request_and_map except we
    // want to allocate IGC as cachable memory
    err = of_address_to_resource(master_node, 0, &res);
    if(err) {
        pr_warning("of_address_to_resource failed\n");
        return err;
    }

    if (!request_mem_region(res.start, resource_size(&res), res.name))
	return -EBUSY;

    // make it cachable!
    igc_dev.virt_base = ioremap_cache(res.start, resource_size(&res));
    if (!igc_dev.virt_base) {
	release_mem_region(res.start, resource_size(&res));
	return -ENOMEM;
    }

    igc_dev.phys_base = res.start;
    igc_dev.total_size = resource_size(&res);
    if(igc_dev.total_size == 0) {
        pr_warning("size of igc area found to be 0 - configuration mistake?\n");
        return 0; // no igc so we are done
    }

    /* init so if we goto fail, no channels will be removed unless instantiated */
    minor = 0;
    if(ioread32(igc_dev.virt_base) != IGC_MAGIC) {
        pr_warning("Magic value did not match, expected 0x%X found 0x%X\n", IGC_MAGIC, ioread32(igc_dev.virt_base));
        err = -EINVAL;
        goto fail;
    }

    err = of_property_read_u32_index(master_node, "nr_of_channels", 0, &igc_dev.nr_of_channels);
    if(err) {
        pr_warning("of_property_read_u32_index failed\n");
        goto fail;
    }

    if(igc_dev.nr_of_channels == 0) {
        pr_warning("nr of channels found to be 0 - configuration mistake?\n");
        return 0; // no channels so we are done
    }

    /* Get a range of minor numbers (starting with 0) to work with */
    err = alloc_chrdev_region(&dev, 0, igc_dev.nr_of_channels, igc_name);
    if (err < 0) {
        pr_warning("[target] alloc_chrdev_region() failed\n");
        goto fail;
    }
    igc_dev.major = MAJOR(dev);

    /* Create device class (before allocation of the array of channels) */
    igc_dev.class = class_create(THIS_MODULE, igc_name);
    if (IS_ERR(igc_dev.class)) {
        err = PTR_ERR(igc_dev.class);
        pr_warning("class_create failed\n");
        goto fail;
    }

    /* Allocate the array of devices */
    igc_dev.channels = (struct igc_channel *)kzalloc(igc_dev.nr_of_channels * sizeof(struct igc_channel), GFP_KERNEL);
    if (igc_dev.channels == NULL) {
        pr_warning("kzalloc failed\n");
        err = -ENOMEM;
        goto fail;
    }

    /* Construct character device for each channel*/
    for_each_child_of_node(master_node, child) {
        err = igc_create_device(child, minor);
        if(err) {
            // cleanup for failed device already done
            goto fail;
        }
        minor++;
    }

    return 0; /* success */

fail:
    igc_cleanup_module(minor); // nr of devices to destroy
    return err;
}

static int igc_remove(struct platform_device *op)
{
    pr_info("Cleaning up sth-igc...\n");
    igc_cleanup_module(igc_dev.nr_of_channels);
    return 0;
}

static struct platform_driver igc_platform_driver = {
    .probe = igc_probe,
    .remove = igc_remove,
    .driver = {
        .name = igc_name,
        .of_match_table = igc_of_match,
    },
};

module_platform_driver(igc_platform_driver);

MODULE_DESCRIPTION("Inter Guest Communication driver for the HASPOC Thin Hypervisor");
MODULE_AUTHOR("Erik Norell, Tutus AB");
MODULE_VERSION("1.00");
MODULE_LICENSE("GPL v2");
