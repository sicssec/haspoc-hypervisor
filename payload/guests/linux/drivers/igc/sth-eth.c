/* Copyright (c) 2016 Tutus AB.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * ------------------------------------------------------------------------- *
 *
 * Virtual network driver for SICS Thin Hypervisor over shared memory, i.e.
 * Inter Guest Communication (IGC)
 *
 * Creates an eth-interface which is using buffers in the igc-area, internal
 * description of how it works is available below. The other guest connected to
 * the IGC area is expected to have a corresponding eth-interface on it's side.
 *
 * Layout below for how the shared memory is laid out and a description of how
 * lists of full or available buffers are used respectively.
 *
 *    Layout of shared memory area:
 *
 *          THIS GUEST                                       OTHER GUEST
 *
 *                                 +----------------+
 *                               - |                | -
 *                               | |                | |
 *       igc_write_hdr     ------->|                |--------> igc_read_hdr
 *                               | |                | |
 *                               | +----------------+ |
 *     read/write area     ----->| |                | |------> read only area
 *                               | |                | |
 *                               | |                | |
 *                               | |   Writeable    | |
 *                               | |    buffers     | |
 *                               | |                | |
 *                               | |                | |
 *                               | |                | |
 *           Boundry             - |                | -
 *        between rw/ro area ----->+================+
 *                               - |                | -
 *                               | |                | |
 *        igc_read_hdr     ------->|                |--------> igc_write_hdr
 *                               | |                | |
 *                               | +----------------+ |
 *                               | |                | |
 *      read only area     <-----| |                | |<------ read/write area
 *                               | |                | |
 *                               | |                | |
 *                               | |   Readable     | |
 *                               | |    buffers     | |
 *                               | |                | |
 *                               | |                | |
 *                               | |                | |
 *                               - +----------------+ -
 *
 *
 * This gets a bit more complicated than it actually has to be because of the
 * restrictions on having read/write (rw) access to one area but only read
 * access (ro) to the other area. These areas are mirrored by each guest as
 * explained by the layout above.
 *
 * To be able to keep track of which buffers contain network packets and which
 * are free to fill with new packets, there are two lists containing index
 * numbers to point out which is the next full buffer and free buffer
 * respectively. A producer needs to add indexes of new full written buffers and
 * indexes to released buffers which have been read, both these lists need to be
 * located in the rw area when a guest acts as a producer to  these lists.
 * Conversly, the next full buffer to read can be found in a list in the ro area
 * as well as the list pointing to the next free buffer to fill with new data.
 *
 * The lists are implemented as circular lists, with a tail updated by the
 * consumer of the list and a head updated by the producer of the list. Because
 * of the rw/ro restrictions these are spread out between the rw and ro area
 * depending on how the list is used (as a producer or consumer). There is only
 * one role in one of the lists where multiple CPUs can be a problem and that is
 * adding a new full buffer to the list, in this case two head values are used:
 * 1) To indicate that the index is reserved, 2) To indicate that the index has
 * been written to and contain correct information. In other cases it is
 * simply enough to atomically aqcuire an index in the list, since the size of
 * the lists is equal to the number of buffers there is no 'competition'.
 *
 * When there is (enough) data to be read the other guest is notified through
 * the hypervisor by writing to a special register. See documentation for the
 * for specific details.
 *
 * See the data structures for igc_write_hdr and igc_read_hdr for further
 * details regarding the layout in memory and how it is used.
 */

#include <linux/init.h>
#include <linux/interrupt.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/delay.h>
#include <linux/netdevice.h>
#include <linux/platform_device.h>
#include <linux/etherdevice.h>
#include <linux/skbuff.h>
#include <linux/moduleparam.h>
#include <linux/rtnetlink.h>
#include <net/rtnetlink.h>
#include <linux/u64_stats_sync.h>
#include <linux/of_irq.h>
#include <linux/of_address.h>
#include <linux/of_platform.h>
#include <asm/barrier.h>
#include <asm/io.h>
#include <linux/kthread.h>

#include "sth-eth.h"

int             		num_sth_eths;
uint32_t        		pkgs_in_queue[MAX_NR_OF_IFS];
phys_addr_t     		phys_base;
__iomem void    		*virt_base;
size_t          		total_size;
struct sk_buff  		*list_of_skbs[NR_OF_SKBS];
uint8_t					skb_head = 0, skb_tail = 0;
struct tasklet_struct 	rx_tasklet[MAX_NR_OF_IFS];

struct net_device     *interfaces[MAX_NR_OF_IFS];

#define PERIOD 1 // sleep for PERIOD ms
static struct task_struct *thread = NULL;
/*
  worker_thread has two purposes:
 * 1. If there are packages in queue, notify other end. Packages in queue
 *      meaning there are fewer packages than batch threshold, i.e.
 *      TX_THRESHOLD - e.g. when pinging only 1-3 packages per second.
 * 2. Pre allocate skbs for sth_eth_receive
 */
static int worker_thread(void *ptr)
{
	struct net_device *dev = (struct net_device *) ptr;
	uint8_t unused_skbs = 0;
	uint32_t i;

	 while (!kthread_should_stop()) {
		// 1. If there are packages in queue, notify other end
		for(i = 0; i < MAX_NR_OF_IFS; i++) {
			if(pkgs_in_queue[i]) {
				struct sth_eth_priv *priv = netdev_priv(interfaces[i]);
				pkgs_in_queue[i] = 0;
				*(priv->notify) = 1; // notify other end, value is arbitrary
				// No need to write to cache, notify is virtual interrupt
			}
		}
		// 2. Pre allocate skbs for sth_eth_receive
		unused_skbs = skb_head - skb_tail;
		if(unused_skbs < 0 || unused_skbs > NR_OF_SKBS)
			pr_warning("nr of unused skbs = %d, should not be negative or > %d!\n", unused_skbs, NR_OF_SKBS);
		if(unused_skbs < NR_OF_SKBS/4) {
			while(unused_skbs != NR_OF_SKBS) {
				list_of_skbs[skb_head & SKB_MASK] = netdev_alloc_skb(dev, BUF_LEN+2);
				if (unlikely(list_of_skbs[skb_head & SKB_MASK] == NULL)) {
					pr_warning("could not pre allocate skbs\n");
					break;
				}
				skb_head++;
				mb();
				unused_skbs++;
			}
		}
		msleep(PERIOD);
	 }
	return 0;
}

static void data_copy(void *dst, void *src, int len)
{
	int i;
	for(i=0; i<len; i++)
	{
		*((char*)dst+i) = *((char*)src+i);
	}
}

/* fake multicast ability */
static void set_multicast_list(struct net_device *dev)
{
}

static netdev_tx_t sth_eth_xmit(struct sk_buff *skb, struct net_device *dev)
{
	struct sth_eth_priv *priv = netdev_priv(dev);
	struct igc_write_hdr *rw = priv->rw;
	struct igc_read_hdr *ro = priv->ro;
	uint8_t local_free_tail, prod_next, buf_index;
	buffer_t *buf;

	// Aquire a free buffer atomically
	do {
		local_free_tail = rw->p_free_tail;
		// if all buffers are occupied we drop the package
		if(unlikely((local_free_tail & BUF_MASK) == (ro->p_free_head.p_tail & BUF_MASK))) {
			dev->stats.tx_dropped++;
			goto free_skb;
		}
	}   while(!__sync_bool_compare_and_swap(&rw->p_free_tail, local_free_tail, (local_free_tail + 1)));
	// Get the index of the free buffer and then the buffer
	buf_index = ro->free_p_bufs[local_free_tail & BUF_MASK];
	buf = &rw->p_bufs[buf_index];
	buf->len = skb->len;
	data_copy(buf->data, skb->data, skb->len);
	// Add our buf index to list of full buffers and increment head. There is
	// always room in the list because the list size is the number of buffers.
	// We have multiple producers to this list, so need to make the changes
	// atomically using p_head and p_tail.
	prod_next = __sync_fetch_and_add(&rw->p_full_head.p_head, 1);
	rw->full_p_bufs[prod_next & BUF_MASK] = buf_index;
	// the only time we can increment tail is when prod_next == p_tail, this
	// may not be the case if someone else is updating and got p_head before
	// us but haven't yet updated p_tail - it will eventually be updated though
	while(!__sync_bool_compare_and_swap(&rw->p_full_head.p_tail, prod_next, (prod_next + 1)));

	// Notify other end when more than TX_THRESHOLD unread
	// buffers in producer ring buffer
	if(++pkgs_in_queue[priv->if_index] == TX_THRESHOLD) {
		pkgs_in_queue[priv->if_index] = 0;
		*priv->notify = 1; // value is arbitrary, virtual interrupt
	}
	dev->stats.tx_packets++;
	dev->stats.tx_bytes += skb->len;
free_skb:
	// always free the skb
	dev_kfree_skb(skb);
	return NETDEV_TX_OK;
}

void sth_eth_receive(unsigned long ptr)
{
	struct net_device *dev = (struct net_device *)ptr;
	struct sth_eth_priv *priv = netdev_priv(dev);
	struct igc_write_hdr *rw = priv->rw;
	struct igc_read_hdr *ro = priv->ro;
	struct sk_buff *skb = NULL;
	uint8_t buf_index, local_c_tail, local_c_head, prod_next, local_skb_tail;
	buffer_t *buf;
	unsigned int len;
	unsigned char *data;

	/* get the next full buffer from other side */
	// we use a snapshot, giving the kernel some breathing room
	local_c_head = ro->c_full_head.p_tail;
	while(1) { // if there are no packages, the function will return below
		// get the next buffer, if there is one available
		do {
			local_c_tail = rw->c_full_tail;
			// if there are no full buffers left, we are finished and return
			if(local_c_tail == local_c_head)
				return;
		} while(!__sync_bool_compare_and_swap(&rw->c_full_tail, local_c_tail, (local_c_tail+1)));
		// since the list size is equal to number of bufs there is no other thread
		// waiting for incremention of local_c_tail, i.e. no need for lock or atomicity
		buf_index = ro->full_c_bufs[local_c_tail & BUF_MASK];
		buf = &ro->c_bufs[buf_index];
		len = buf->len;

		// get an skb to put the data in
		do {
			local_skb_tail = skb_tail;
			if(unlikely(local_skb_tail == skb_head)) {
				// nothing pre allocated, get a new instead
				skb = netdev_alloc_skb(dev, BUF_LEN+2);
				if (unlikely(skb == NULL)) { // failed
					// if this happens we drop the package all together
					// however we still need to return the empty buffer
					prod_next = __sync_fetch_and_add(&rw->c_free_head.p_head, 1);
					rw->free_c_bufs[prod_next & BUF_MASK] = buf_index;
					while(!__sync_bool_compare_and_swap(&rw->c_free_head.p_tail, prod_next, (prod_next + 1)));
					dev->stats.rx_dropped++;
					return;
				}
			} else {
				skb = list_of_skbs[local_skb_tail & SKB_MASK];
				// did someone get it before us?
				if(!__sync_bool_compare_and_swap(&skb_tail, local_skb_tail, (local_skb_tail + 1)))
					skb = NULL; // try again
			}
		} while(!skb);
		data = skb_put(skb, len);
		data_copy(data, buf->data, len);
		// Return the buffer to the list of free buffers, and increment head.
		// We have multiple producers to this list, so need to make the changes
		// atomically. No need to check tail, there is always room in the list!
		prod_next = __sync_fetch_and_add(&rw->c_free_head.p_head, 1);
		rw->free_c_bufs[prod_next & BUF_MASK] = buf_index;
		// the only time we can increment tail is when prod_next == (tail + 1)
		// this is only the case if someone else is updating and got p_head before
		// us but haven't yet updated p_tail - it will eventually be updated though
		while(!__sync_bool_compare_and_swap(&rw->c_free_head.p_tail, prod_next, (prod_next + 1)));

		skb->protocol = eth_type_trans(skb, dev);
		skb->dev = dev;
		netif_rx(skb);
		dev->stats.rx_packets++;
		dev->stats.rx_bytes += len;
	}
}

static irqreturn_t
sth_eth_interrupt(int irq, void *dev_id)
{
	struct net_device *dev = dev_id;
	struct sth_eth_priv *priv = netdev_priv(dev);

	tasklet_schedule(&(rx_tasklet[priv->if_index]));

	return IRQ_HANDLED;
}

static int sth_eth_dev_init(struct net_device *dev)
{
	if (request_irq
		(dev->irq, sth_eth_interrupt, 0, dev->name, dev))
		return -EAGAIN;

	netif_start_queue(dev);
	return 0;
}

static void sth_eth_dev_uninit(struct net_device *dev)
{
	netif_stop_queue(dev);
	free_irq(dev->irq, dev);
}

static int sth_eth_change_carrier(struct net_device *dev, bool new_carrier)
{
	if (new_carrier)
		netif_carrier_on(dev);
	else
		netif_carrier_off(dev);
	return 0;
}

static const struct net_device_ops sth_eth_netdev_ops = {
	.ndo_init       = sth_eth_dev_init,
	.ndo_uninit     = sth_eth_dev_uninit,
	.ndo_start_xmit     = sth_eth_xmit,
	.ndo_validate_addr  = eth_validate_addr,
	.ndo_set_rx_mode    = set_multicast_list,
	.ndo_set_mac_address    = eth_mac_addr,
	.ndo_change_carrier = sth_eth_change_carrier,
};

static void sth_eth_get_drvinfo(struct net_device *dev,
				  struct ethtool_drvinfo *info)
{
	strlcpy(info->driver, DRV_NAME, sizeof(info->driver));
	strlcpy(info->version, DRV_VERSION, sizeof(info->version));
}

static const struct ethtool_ops sth_ethtool_ops = {
	.get_drvinfo            = sth_eth_get_drvinfo,
};

static void sth_eth_setup(struct net_device *dev)
{
	ether_setup(dev);

	/* Initialize the device structure. */
	dev->netdev_ops = &sth_eth_netdev_ops;
	dev->watchdog_timeo = msecs_to_jiffies(5000);
	dev->ethtool_ops = &sth_ethtool_ops;
	dev->destructor = free_netdev;

	dev->flags &= ~IFF_MULTICAST; /* fake */
	dev->priv_flags |= IFF_LIVE_ADDR_CHANGE;
	dev->features   |= NETIF_F_HIGHDMA;
	dev->hw_features |= dev->features;
	dev->hw_enc_features |= dev->features;
	eth_hw_addr_random(dev);
}

static int sth_eth_validate(struct nlattr *tb[], struct nlattr *data[])
{
	if (tb[IFLA_ADDRESS]) {
		if (nla_len(tb[IFLA_ADDRESS]) != ETH_ALEN)
			return -EINVAL;
		if (!is_valid_ether_addr(nla_data(tb[IFLA_ADDRESS])))
			return -EADDRNOTAVAIL;
	}
	return 0;
}

static struct rtnl_link_ops sth_eth_link_ops __read_mostly = {
	.kind       = DRV_NAME,
	.setup      = sth_eth_setup,
	.validate   = sth_eth_validate,
};

/* Number of sth devices to be set up by this module. */
module_param(num_sth_eths, int, 0);
MODULE_PARM_DESC(num_sth_eths, "Number of sth pseudo devices");

int run_once = 0;
static int __init sth_eth_init_one(struct device_node *node, int index)
{
	struct net_device *dev_sth;
	struct sth_eth_priv *priv;
	struct igc_hdr *test_ptr;
	int err, i;

	if(index != 0) { // need to calculate where next igc channel is
		// Channel #index is laid out directly after Channel #(index-1)
		struct sth_eth_priv *prev = netdev_priv(interfaces[index-1]);
		test_ptr = (struct igc_hdr *) ((uint8_t *)prev->ro + prev->base_ptr->read_size);
	} else {
		test_ptr = virt_base;
	}

	if(test_ptr->magic != IGC_MAGIC) {
		pr_warning("interface setup, ctrl: magic value did not match in sth-eth%d, expected 0x%X found 0x%X\n", index, IGC_MAGIC, test_ptr->magic);
		return -ENODEV;
	}

	dev_sth = alloc_netdev(sizeof(struct sth_eth_priv), "eth%d", NET_NAME_UNKNOWN, sth_eth_setup);
	if (!dev_sth) {
		pr_warning("could not allocate netdev\n");
		return -ENOMEM;
	}

	/* igc stuff */
	priv = netdev_priv(dev_sth);

	priv->if_index = index;  // I am eth#index
	priv->base_ptr = test_ptr;
	priv->rw = (struct igc_write_hdr *) ((uint8_t *) priv->base_ptr + PAGE_SIZE);
	priv->ro = (struct igc_read_hdr *) ((uint8_t *) priv->rw + priv->base_ptr->write_size);
	priv->notify = &priv->base_ptr->notify;

	// init writable variables
	priv->rw->p_full_head.p_head = 0;
	priv->rw->p_full_head.p_tail = 0;
	priv->rw->p_free_tail = 0;
	// initially all buffers are empty and available, i.e. list of free buffers is full
	priv->rw->c_free_head.p_head = 0;
	priv->rw->c_free_head.p_tail = 0;
	priv->rw->c_full_tail = 0;
	// fill the list with indexes of free buffers
	for(i = 0; i < (NR_OF_BUFS - 1); i++) {
		// add buffer i to list of free buffers
		priv->rw->free_c_bufs[priv->rw->c_free_head.p_head] = i;
		priv->rw->c_free_head.p_head++;
	}
	// p_tail and p_head should be same!
	priv->rw->c_free_head.p_tail = priv->rw->c_free_head.p_head;
	mb(); // just to make sure

	// init package counter
	pkgs_in_queue[index] = 0;

	dev_sth->irq = irq_of_parse_and_map(node, 0); // includes mapping irq to virq

	dev_sth->rtnl_link_ops = &sth_eth_link_ops;
	err = register_netdevice(dev_sth);
	if (err < 0)
		goto cleanup;

	interfaces[index] = dev_sth;

	// one tasklet per interface
	tasklet_init(&(rx_tasklet[index]), sth_eth_receive, (unsigned long)dev_sth);

	pr_info("STH IGC network interface eth%d created with %u producer buffers and %u consumer buffers\n", index, NR_OF_BUFS, NR_OF_BUFS);

	if(!run_once) {
		// this should only be done once!
		run_once = 1; // true

		// setup pre allocated skbs
		for(i = 0; i < NR_OF_SKBS; i++) {
			list_of_skbs[i] = netdev_alloc_skb(dev_sth, BUF_LEN+2);
			if (unlikely(list_of_skbs[i] == NULL)) {
				pr_warning("could not pre allocate skbs\n");
				unregister_netdev(dev_sth);
				err = -ENOMEM;
				goto cleanup;
			}
			skb_head++;
		}

		// Setup worker thread, even if we have multiple interfaces, only one
		// thread will be created
		thread = kthread_run(worker_thread, dev_sth, "sth_eth worker_thread");
		if (thread == ERR_PTR(-ENOMEM)) {
			pr_warning("worker_thread thread creation failed\n");
		}
	}

	return 0;

cleanup:
	// we may need to undo some things
	while(skb_tail != skb_head)
		dev_kfree_skb(list_of_skbs[skb_tail++ & SKB_MASK]);
	if(thread)
		kthread_stop(thread);
	return err;
}

/* Match table for of_platform binding */
static const struct of_device_id sth_eth_of_match[] = {
		{ .compatible = "sth,eth", }, /* sth as in SICS Thin Hypervisor */
	{}
};

static int sth_eth_probe(struct platform_device *op)
{
	struct device_node *master_node;
	struct device_node *child;
	const struct of_device_id *match;
	struct resource res;
	int i, err = 0;

	pr_info("SICS Thin Hypervisor Ethernet driver\n");

	master_node = op->dev.of_node;
	/* sanity check: this device matches the compatible string */
	match = of_match_device(sth_eth_of_match, &op->dev);
	if (!match)
		return -EINVAL;

	// The following is basically an unwrapped of_io_request_and_map except we
	// want to allocate IGC as cachable memory
	err = of_address_to_resource(master_node, 0, &res);
	if(err) {
		pr_warning("of_address_to_resource failed\n");
		return err;
	}

	if (!request_mem_region(res.start, resource_size(&res), res.name)) {
		pr_warning("iomem region already in use: 0x%X size 0x%X\n", (uint) res.start, (uint) resource_size(&res));
		return -EBUSY;
	}

	if(resource_size(&res) == 0) {
		pr_warning("size of igc area found to be 0 - configuration mistake?\n");
		return 0; // no igc so we are done
	}

	// make it cacheable!
	virt_base = ioremap_cache(res.start, resource_size(&res));
	if (!virt_base) {
		pr_warning("size of igc area found to be 0 - configuration mistake?\n");
		release_mem_region(res.start, resource_size(&res));
		return -ENOMEM;
	}

	phys_base = res.start;
	total_size = resource_size(&res);

	if(ioread32(virt_base) != IGC_MAGIC) {
		pr_warning("Magic value did not match, expected 0x%X found 0x%X\n", IGC_MAGIC, ioread32(virt_base));
		err = -EINVAL;
		goto exit_free_iomem;
	}

	err = of_property_read_u32_index(master_node, "nr_of_channels", 0, &num_sth_eths);
	if(err) {
		pr_warning("of_property_read_u32_index failed\n");
		err = -EINVAL;
		goto exit_free_iomem;
	}

	if(num_sth_eths == 0) {
		pr_warning("nr of channels found to be 0 - configuration mistake?\n");
		err = 0; // no channels so we are done
		goto exit_free_iomem;
	}

	rtnl_lock();
	err = __rtnl_link_register(&sth_eth_link_ops);
	if (err < 0) {
		rtnl_unlock();
		goto exit_free_iomem;
	}

	/* Construct ethernet interface for each channel */;
	i = 0;
	for_each_child_of_node(master_node, child) {
		err = sth_eth_init_one(child, i);
		if (err) {
			__rtnl_link_unregister(&sth_eth_link_ops);
			rtnl_unlock();
			goto exit_free_iomem;
		}
		i++;
		if(i == MAX_NR_OF_IFS)
			break;  // for now we support a limited number of interfaces!
	}

	rtnl_unlock();

	return 0;

exit_free_iomem:
	iounmap(virt_base);
	release_mem_region(phys_base, total_size);
	return err;
}

int sth_eth_remove(struct platform_device *op)
{
	int i;
	for(i=0; i < num_sth_eths; i++)
		unregister_netdev(interfaces[i]);
	rtnl_link_unregister(&sth_eth_link_ops);
	// free all pre allocated skbs
	while(skb_tail != skb_head)
		dev_kfree_skb(list_of_skbs[skb_tail++ & SKB_MASK]);
	if(thread)
		kthread_stop(thread);
	iounmap(virt_base);
	release_mem_region(phys_base, total_size);
	return 0;
}

MODULE_DEVICE_TABLE(of, sth_eth_of_match);

static struct platform_driver sth_eth_platform_driver = {
	.probe = sth_eth_probe,
	.remove = sth_eth_remove,
	.driver = {
		.name = DRV_NAME,
		.of_match_table = sth_eth_of_match,
	},
};

module_platform_driver(sth_eth_platform_driver);

MODULE_DESCRIPTION("HASPOC Thin Hypervisor IGC Virtual Ethernet driver");
MODULE_AUTHOR("Erik Norell, Tutus AB");
MODULE_LICENSE("GPL v2");
MODULE_ALIAS_RTNL_LINK(DRV_NAME);
MODULE_VERSION(DRV_VERSION);
