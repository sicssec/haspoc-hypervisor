/* Copyright (c) 2016 Tutus AB.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

#ifndef STH_ETH_H
#define STH_ETH_H

#define DRV_NAME        "sth-eth"
#define DRV_VERSION     "1.1"

#define MAX_NR_OF_IFS   2 // for now we (barely) support no more than 2 IFs
#define IGC_MAGIC       0xAA640CCA
#define BUF_LEN         1522 /* bytes, reflecting MTU of 1500 bytes */
#define TX_THRESHOLD    16
#define NR_OF_SKBS      32 // must be a power of 2 because of AND-mask below !!
#define SKB_MASK        ((NR_OF_SKBS) - 1)
// !! 0 =< NR_OF_BUFS <= 256 !! Otherwise change to uint16_t in sth_eth_priv
#define NR_OF_BUFS      256 // must be a power of 2 because of AND-mask below
#define BUF_MASK        ((NR_OF_BUFS) - 1)

struct igc_hdr {
  uint32_t magic;
  uint32_t id;
  uint32_t version;
  uint32_t notify;
  uint32_t write_adr;
  uint32_t write_size;
  uint32_t read_adr;
  uint32_t read_size;
};

typedef struct _buffer_ {
  uint16_t  len; /* max len is 65535 bytes */
  uint8_t   data[BUF_LEN]; /* arbitrary data type */

} __attribute__((packed)) buffer_t;

// we need producer head/tail to atomically add full or free buffers to
// respective list in the igc area
typedef struct _multi_producer_head_ {
  uint8_t   p_head, p_tail;

} __attribute__((packed)) mp_head_t;

/*
 * c_ as in consumer and p_ as in producer
 */

struct igc_write_hdr {
  // the heads and tails refer to free_x_bufs and full_x_bufs, see further down

  // if we produce we write to head
  volatile mp_head_t p_full_head; // we push full production buffers to the other side
  volatile mp_head_t c_free_head; // we push empty consumer buffers to the other side
  // if we consume we write to tail
  volatile uint8_t c_full_tail; // we consume full consumer buffers from other side
  volatile uint8_t p_free_tail; // we consume free production buffers from other side

  // list of free consumer buffers and full producer buffers
  volatile uint8_t free_c_bufs[NR_OF_BUFS], full_p_bufs[NR_OF_BUFS];

  // the buffers we fill and send to other side
  volatile buffer_t p_bufs[NR_OF_BUFS];
};

struct igc_read_hdr {
  // the heads and tails refer to free_x_bufs and full_x_bufs, see further down

  // if we consume we read from head
  volatile const mp_head_t  c_full_head; // mirrors p_full_head
  volatile const mp_head_t  p_free_head; // mirrors c_free_head
  // if we produce we read from tail
  volatile const uint8_t  p_full_tail; // mirrors c_full_tail
  volatile const uint8_t  c_free_tail; // mirrors p_free_tail

  // list of full consumer buffers and free producer buffers
  volatile const uint8_t  free_p_bufs[NR_OF_BUFS], full_c_bufs[NR_OF_BUFS];

  // the buffers we read and get from the other side
  volatile const buffer_t c_bufs[NR_OF_BUFS];
};

struct sth_eth_priv {
  // size is defined at compile time, NR_OF_BUFS (using BUF_MASK for indexing)
  struct igc_hdr         *base_ptr;
  uint8_t                if_index;  // priv of ethX where X == if_index
  struct igc_write_hdr   *rw;  // read/write
  struct igc_read_hdr    *ro; // read only
  int                    *notify; // notify address
};

#endif /* STH_ETH_H */
