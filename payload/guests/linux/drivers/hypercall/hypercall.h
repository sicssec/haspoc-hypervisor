/* Copyright (c) 2016 Tutus AB.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

#ifndef STH_HCD_H
#define STH_HCD_H

// HCD as in HyperCall Driver
#define HCD_VERSION "1.0"

#define SINGLE_DEVICE 1
#define HCD_DEVICE_NAME "hypercall"
#define HCD_PERIODIC_DEBUG 0xC3000888
#define PERIOD 30

#define __asmeq(x, y)  ".ifnc " x "," y " .err ; .endif\n\t"

struct hcd_dev {
    int irq;
    struct cdev hcd_cdev;
};

static int noinline __call_hvc64(uint64_t x0, uint64_t x1, uint64_t x2, uint64_t x3){
    __asm volatile(
        __asmeq("%0", "x0")
        __asmeq("%1", "x1")
        __asmeq("%2", "x2")
        __asmeq("%3", "x3")
        "hvc    #0"
        : "+r" (x0)
        : "r" (x1), "r" (x2), "r" (x3)
    );

    return x0;
}

#endif /* STH_HCD_H */