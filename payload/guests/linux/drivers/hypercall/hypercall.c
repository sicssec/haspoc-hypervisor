/* Copyright (c) 2016 Tutus AB.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

#include <linux/module.h>
#include <linux/fs.h>
#include <linux/slab.h>
#include <linux/cdev.h>
#include <linux/device.h>

#include "hypercall.h"

static unsigned int hcd_major = 0;
static struct hcd_dev *hcd_device = NULL;
static struct class *hcd_class = NULL;

static ssize_t write_hcd(struct file *file, const char __user *buf,
                         size_t count, loff_t *ppos)
{
    long unsigned int val;

    /* Copy user buffer input to x0 interpreted as hexadecimal and trap to
     * hypervisor with HVC
     */
    sscanf(buf, "%lx", &val);
    printk(KERN_INFO "Making hypercall with x0: 0x%lx\n", val);
    __call_hvc64(val, 0, 0, 0);

    return count;
}

const struct file_operations hcd_fops = {
    .owner		= THIS_MODULE,
    .write		= write_hcd,
};

void hcd_remove(void)
{
    printk(KERN_INFO "Cleaning up hcd-module...\n");

    /* Get rid of character device */
    if (hcd_device) {
        device_destroy(hcd_class, MKDEV(hcd_major, 0));
        cdev_del(&hcd_device->hcd_cdev);
        kfree(hcd_device);
    }

    if (hcd_class)
        class_destroy(hcd_class);

    unregister_chrdev_region(MKDEV(hcd_major, 0), 1);
}

static int __init hcd_init(void)
{
    int err = 0;
    dev_t dev = 0, devno;
    struct device *device = NULL;

    pr_info("SICS Thin Hypervisor: driver for making hypercalls\n");

    /* Get a range of minor numbers (starting with 0) to work with */
    err = alloc_chrdev_region(&dev, 0, SINGLE_DEVICE, HCD_DEVICE_NAME);
    if (err < 0) {
        printk(KERN_WARNING "[target] alloc_chrdev_region() failed\n");
        return err;
    }
    hcd_major = MAJOR(dev);

    /* Create device class (before allocation of the array of devices) */
    hcd_class = class_create(THIS_MODULE, HCD_DEVICE_NAME);
    if (IS_ERR(hcd_class)) {
        err = PTR_ERR(hcd_class);
        goto fail;
    }

    /* Allocate the array of devices */
    hcd_device = (struct hcd_dev *)kzalloc(sizeof(struct hcd_dev), GFP_KERNEL);
    if (hcd_device == NULL) {
        err = -ENOMEM;
        goto fail;
    }

    /* Construct device */
    cdev_init(&hcd_device->hcd_cdev, &hcd_fops);
    hcd_device->hcd_cdev.owner = THIS_MODULE;

    devno = MKDEV(hcd_major, 0);
    err = cdev_add(&hcd_device->hcd_cdev, devno, 1);
    if (err) {
        printk(KERN_WARNING "[target] Error %d while trying to add %s%d",
               err, HCD_DEVICE_NAME, 0);
        goto fail;
    }

    device = device_create(hcd_class, NULL, /* no parent device */
                           devno, NULL, /* no additional data */
                           HCD_DEVICE_NAME "%d", 0);

    if (IS_ERR(device)) {
        err = PTR_ERR(device);
        printk(KERN_WARNING "[target] Error %d while trying to create %s%d",
               err, HCD_DEVICE_NAME, 0);
        cdev_del(&hcd_device->hcd_cdev);
        goto fail;
    }

    return 0; /* success */

fail:
    hcd_remove();
    return err;
}

module_init(hcd_init);
module_exit(hcd_remove);
MODULE_DESCRIPTION("Hypercall driver for the HASPOC Thin Hypervisor");
MODULE_AUTHOR("Erik Norell, Tutus AB");
MODULE_VERSION(HCD_VERSION);
MODULE_LICENSE("GPL v2");
