/*
 * Emergency Erase GPIO driver
 *
 * Creates a device, /dev/eme, and sets up a GPIO port (as specified in the
 * device tree) that enables communication of emergency erase requests between
 * user space, kernel space and the hypervisor. Reads and writes to the EME
 * device will return 0 unless the GPIO interrupt has been triggered. A
 * subsequent write to /dev/EME will trigger the emergency erase call to the
 * hypervisor.
 *
 * The emergency erase request may be denied by the hypervisor. If this happens
 * writes to the EME device will have no effect and all reads will return an
 * error code.
 */

#include <asm/uaccess.h>
#include <linux/fs.h>
#include <linux/gpio.h>
#include <linux/init.h>
#include <linux/interrupt.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/mutex.h>
#include <linux/of_gpio.h>
#include <linux/of_platform.h>
#include <linux/types.h>

#define DRIVER_NAME		"eme-gpio"
#define DEVICE_NAME		"eme"
#define CLASS_NAME		"eme"

#define ERR_PERMISSION_DENIED	0xFFFFFFFD

extern u32 __emergency_erase(void);

struct eme_gpio_config {
	struct class *class;
	struct device *device;
	int major;
	int gpio;
	int irq;
};

/* Reading from the EME device will return one of the following values */
enum eme_status {
	EME_IDLE,
	EME_REQUESTED,
	EME_FAILED,
};

/* Currently, only a single instance of the device is allowed. */
static DEFINE_MUTEX(dev_mutex);

static struct eme_gpio_config config;
static enum eme_status status = EME_IDLE;
static const struct of_device_id eme_gpio_of_match[] = {
	{ .compatible = "eme-gpio" },
	{ }
};
MODULE_DEVICE_TABLE(of, eme_gpio_of_match);

static int dev_open(struct inode *ip, struct file *fp)
{
	if (!mutex_trylock(&dev_mutex)) {
		pr_warning(DRIVER_NAME ": device in use\n");
		return -EBUSY;
	}
	return 0;
}

static int dev_release(struct inode *ip, struct file *fp)
{
	mutex_unlock(&dev_mutex);
	return 0;
}

static ssize_t
dev_read(struct file *filep, char *buffer, size_t len, loff_t *offset)
{
	u8 sts = (u8) status;
	if (copy_to_user(buffer, &sts, 1) != 0)
		return -EFAULT;
	return 1;
}

static ssize_t
dev_write(struct file *filep, const char *buffer, size_t len, loff_t *offset)
{
	u32 ret;
	u8 byte;
	if (status != EME_REQUESTED)
		return 0;
	if (copy_from_user(&byte, buffer, 1) != 0)
		return -EFAULT;
	pr_info(DRIVER_NAME ": user has completed emergency erase, notifying hypervisor");
	ret = __emergency_erase();
	if (ret == ERR_PERMISSION_DENIED) {
		/* This is probably very bad... */
		pr_alert(DRIVER_NAME ": the hypervisor denied the emergency erase");
		status = EME_FAILED;
	}
	return 1;
}

static struct file_operations fops = {
	.read = dev_read,
	.write = dev_write,
	.open = dev_open,
	.release = dev_release,
};

static irqreturn_t eme_gpio_interrupt_handler(int irq, void *dev_id)
{
	pr_info(DRIVER_NAME ": GPIO interrupt!\n");
	status = EME_REQUESTED;
	return IRQ_HANDLED;
}

static int eme_gpio_probe(struct platform_device *pdev)
{
	int result = 0;
	struct device_node *np = pdev->dev.of_node;

	pr_info("Emergency Erase GPIO driver\n");
	if (!np)
		return -EINVAL;

	config.gpio = of_get_named_gpio(np, "gpio", 0);
	pr_info(DRIVER_NAME ": using gpio %d\n", config.gpio);
	if (!gpio_is_valid(config.gpio))
		return -EINVAL;

	gpio_request(config.gpio, "sysfs");
	gpio_direction_input(config.gpio);
	gpio_export(config.gpio, false);
	config.irq = gpio_to_irq(config.gpio);
	pr_info(DRIVER_NAME ": using irq %d\n", config.irq);

	result = request_irq(config.irq,
			     eme_gpio_interrupt_handler,
			     IRQF_TRIGGER_RISING,
			     DRIVER_NAME, NULL);
	if (result) {
		pr_err(DRIVER_NAME ": failed to request irq %d\n",
		       config.irq);
		return result;
	}

	config.major = register_chrdev(0, DEVICE_NAME, &fops);
	if (config.major < 0) {
		pr_err(DRIVER_NAME ": register_chrdev failed\n");
		return config.major;
	}

	config.class = class_create(THIS_MODULE, CLASS_NAME);
	if (IS_ERR(config.class)) {
		pr_err(DRIVER_NAME ": class_create failed\n");
		unregister_chrdev(config.major, DEVICE_NAME);
		return PTR_ERR(config.class);
	}

	config.device = device_create(config.class, NULL,
				      MKDEV(config.major, 0), NULL,
				      DEVICE_NAME);
	if (IS_ERR(config.device)) {
		pr_err(DRIVER_NAME ": device_create failed\n");
		class_destroy(config.class);
		unregister_chrdev(config.major, DEVICE_NAME);
		return PTR_ERR(config.class);
	}

	mutex_init(&dev_mutex);

	return 0;
}

static int eme_gpio_remove(struct platform_device *pdev)
{
	mutex_destroy(&dev_mutex);

	device_destroy(config.class, MKDEV(config.major, 0));
	class_unregister(config.class);
	class_destroy(config.class);
	unregister_chrdev(config.major, DEVICE_NAME);

	free_irq(config.irq, NULL);
	gpio_unexport(config.gpio);
	gpio_free(config.gpio);

	return 0;
}

static struct platform_driver eme_gpio_driver = {
	.driver = {
		.name           = DRIVER_NAME,
		.owner          = THIS_MODULE,
		.of_match_table = of_match_ptr(eme_gpio_of_match),
	},
	.probe  = eme_gpio_probe,
	.remove = eme_gpio_remove,
};
module_platform_driver(eme_gpio_driver);

MODULE_DESCRIPTION("Emergency Erase GPIO driver");
MODULE_AUTHOR("Mattias Fransson, Sectra Communications AB");
MODULE_VERSION("0.1");
MODULE_LICENSE("GPL v2");
