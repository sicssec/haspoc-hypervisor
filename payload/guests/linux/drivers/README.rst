There are Linux drivers available to utilize
    Inter guest communication
    Calls to hypervisor
    Emergency erase function through GPIO

=== Inter Guest Communication (IGC) ===

found under igc/

There are two drivers available to utilize IGC, one for basic operation or
proof of concept (sth-char) and one for communication (sth-eth).

sth-char sets up a character device for each channel, /dev/igcX, which you can
write to (echo "message" > /dev/igc0) and read from (cat /dev/igc0). It is not
effective (ping-pong interrupt for each message) and not recommended for other
use than simple messaging or proof of concept.

sth-eth sets up IGC as an ethernet device (eth0) which copy the packets to the
other side. It can be used as a regular ethernet deivce (ifconfig, ping, etc)
and is for now the recommended way to use IGC. With it you can achieve a
bandwith of around 700 Mbit/s for TCP traffic.

The needed device tree is similar for both drivers, with only difference being
the compatible string (either "sth,igc" for char device or "sth,eth" for
ethernet device), eg:

igc-channels@0x200000 {
    compatible = "sth,eth";
    reg = <0x200000 0x182000>; /* <[addr] [total size for all channels]> */
    nr_of_channels = <2>;

    channel0 {
        ctrl  = <0x0 0x1000>; /* always exist as first page */
        write = <0x1000 0x60000>; /* must match config */
        read  = <0x61000 0x60000>; /* must match config */
        interrupts = <0 16 0>; /* <0 [intr number] 0> */
        interrupt-parent = <&gic>;
        other-end = "2nd guest"; /* max 20 chars */
    };

    channel1 {
        ctrl  = <0x0C1000 0x1000>;
        write = <0x0C2000 0x60000>;
        read  = <0x122000 0x60000>;
        interrupts = <0 17 0>;
        interrupt-parent = <&gic>;
        other-end = "3rd guest";
    };
};

Of course these figures must correspond to the igc node in the hypervisor
config. See payload/devicetree/src/hikey/{config_linux,guest{1,2,3}}.dts
for examples.

Note that the number of buffers in the eth driver is defined in the driver and
the numbers in the example and current config for linux allows for this number
of buffers. It should be ok to use fewer, otherwise the dts files need to be
adapted.

=== Hypercall driver ===

found under hypercall/

This driver sets up a device /dev/hypercall0. This can be used to trap to the
hypervisor. The driver expects a hexadecimal value as input to the device
using e.g. the echo command. This value is then placed in the x0 register
which is read by the hypervisor to determine the message. See the following
files for example use:
payload/guests/linux/filesystem/prebuilt/hypercall/usr/bin/{print_debug,emergency_erase}

The driver is inserted by insmod and is done automatically on boot for the
current Linux guests in
payload/guests/linux/filesystem/prebuilt/hypercall/etc/init.d/S03hypercall

=== Emergency erase driver ===

found under eme_gpio/

Creates a device, /dev/eme, and sets up a GPIO port (as specified in the
device tree) that enables communication of emergency erase requests between
user space, kernel space and the hypervisor. Reads and writes to the EME
device will return zero unless the GPIO interrupt has been triggered. After
an emergency erase has been requested a subsequent write to /dev/eme from
user space will trigger the emergency erase hypercall.

The emergency erase request may be denied by the hypervisor. If this happens
writes to the EME device will have no effect and all reads will return an
error code.

Example device tree configuration:

eme: eme_gpio {
    compatible = "eme-gpio";
    gpio = <&gpio2 0 0>;
};
