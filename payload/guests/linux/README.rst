This folder contain the logic to build Linux and the initial filesystem.

Note that the foreign repositories are stored in external/

For more information on available Linux drivers related to the hypervisor
please see drivers/README
