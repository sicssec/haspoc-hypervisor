
#include "defs.h"

    .text
    .global __emergency_erase_secure_state



/*
 * quick memory zeroization for 64byte blocks
 * x0 = start, x1 = end
 */
__ee_zero_memory:
    b 2f
1:
    stp xzr, xzr, [x0], #16
    stp xzr, xzr, [x0], #16
    stp xzr, xzr, [x0], #16
    stp xzr, xzr, [x0], #16
2:
    cmp x0, x1
    blt 1b
    ret

/*
 * make sure caches are clean
 *
 * NOTE: this code is incorrect, it
 */
__ee_empty_caches:
    /* get rid of the TLB while we are empty caches */
    tlbi ALLE1
    tlbi ALLE2
    tlbi ALLE3

    /* i-cache, the easy one */
    ic iallu

    /* d-cache, XXX: this is incorrect, cleans only the first way */
    dc cisw, xzr

    ret

__ee_empty_registers:

    /*
     * create an empty memory region first
     */
    ldr x0, =INTERNAL_RAM_START
    mov x1, x0
    stp xzr, xzr, [x1], #16
    stp xzr, xzr, [x1], #16
    stp xzr, xzr, [x1], #16
    stp xzr, xzr, [x1], #16


    /*
     * floating points & SIMD
     */
    ldp q0, q1, [x0]
    ldp q2, q3, [x0]
    ldp q4, q5, [x0]
    ldp q6, q7, [x0]
    ldp q8, q9, [x0]
    ldp q10, q11, [x0]
    ldp q12, q13, [x0]
    ldp q14, q15, [x0]
    ldp q16, q17, [x0]
    ldp q18, q19, [x0]
    ldp q20, q21, [x0]
    ldp q22, q23, [x0]
    ldp q24, q25, [x0]
    ldp q26, q27, [x0]
    ldp q28, q29, [x0]
    ldp q30, q31, [x0]

    /*
     * general purpose, excluding lr
     */

    ldp x2 , x3 , [x0]
    ldp x4 , x5 , [x0]
    ldp x6 , x7 , [x0]
    ldp x8 , x9 , [x0]
    ldp x10, x11, [x0]
    ldp x12, x13, [x0]
    ldp x14, x15, [x0]
    ldp x16, x17, [x0]
    ldp x18, x19, [x0]
    ldp x20, x21, [x0]
    ldp x22, x23, [x0]
    ldp x24, x25, [x0]
    ldp x26, x27, [x0]
    ldp x28, x29, [x0]
    mov x0, xzr
    mov x1, xzr
    ret

/*
 * secure state: wait here and do nothing
 */
__ee_secure_state:
    msr DAIFSet, #15

#ifdef ARCH_HAS_GICv2
    /* disable CPU interface */
    ldr x0, = GICC_BASE
    str xzr, [x0]

    /* disable distributor interface */
    ldr x0, = GICD_BASE
    str xzr, [x0]
#endif

1:
    wfi
    b 1b
    b 1b
    b 1b


/*
 * perform emergency erase and enter secure state
 */
__emergency_erase_secure_state:
    /*
     * ensure we are not interruptd
     */
    msr DAIFSet, #15

#ifdef ARCH_HAS_GICv2
    /* disable CPU interface */
    ldr x0, = GICC_BASE
    str xzr, [x0]
#endif

    /*
     * erase external RAM (there may be two), then erase internal RAM
     */
    ldr x0, =EXTERNAL_RAM_START
    ldr x1, =EXTERNAL_RAM_END
    bl __ee_zero_memory

#ifdef EXTERNAL_RAM2_START
    ldr x0, =EXTERNAL_RAM2_START
    ldr x1, =EXTERNAL_RAM2_END
    bl __ee_zero_memory
#endif

    ldr x0, =INTERNAL_RAM_START
    ldr x1, =INTERNAL_RAM_END
    bl __ee_zero_memory

    bl __ee_empty_caches
    bl __ee_empty_registers
    mov x30, #0 /* lets clear this one too, just to be complete */

    /* and enter secure state */
1:
    bl __ee_secure_state
    b 1b
