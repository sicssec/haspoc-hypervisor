#ifndef _DEFS_H_
#define _DEFS_H_

# include <base.h>
# include <arch_defs.h>
# include <armv8.h>

/* PSCI return codes */
#define PSCI_RET_NOT_SUPPORTED SMC_RET_UNKNOWN
#define PSCI_RET_INVALID_PARAMETERS ((uint32_t)-2)
#define PSCI_RET_DENIED ((uint32_t)-3)
#define PSCI_RET_ALREADY_ON ((uint32_t) -4)

/* PSCI commands */
#define PSCI_CMD_CPU_ON 0xC4000003
#define PSCI_CMD_CPU_OFF 0x84000002
#define PSCI_CMD_VERSION 0x84000000
#define PSCI_CMD_SYSTEM_OFF 0x84000008
#define PSCI_CMD_SYSTEM_RESET 0x84000009


#define UPCALL_EMERGENCY_ERASE 0xC3000666

/* static structure sizes */
#define PERCPU_BITS 5
#define PERCPU_SIZE 32
#define STACK_BITS 10
#define STACK_SIZE 1024

#ifdef __ASSEMBLER__

#else /* ! __ASSEMBLER__ */

enum cpustate {SLEEP, INIT, SPIN, DROP};

struct percpu_data {
    uint64_t adr;
    uint64_t arg0;
    uint64_t arg1;
    uint32_t arg2;
    uint32_t state;
};

extern void __system_off(); /* turn off system, returns -1 if failed */
extern void __system_reset(); /* reset system, returns -1 if failed */

extern int __die();
extern int __cpu_num();

extern struct percpu_data *__cpu_data_this();
extern struct percpu_data *__cpu_data_get(int index);
extern void __cpu_data_commit(int index);
extern void __cpu_data_wait();

extern void dprintf(const char *fmt, ...);

extern void el3_drop_to_el2();

extern void __emergency_erase_secure_state();

#endif /* ! __ASSEMBLER__ */


#endif /* _DEFS_H_ */
