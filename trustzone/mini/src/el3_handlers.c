

#include <minilib.h>
#include <gicv2.h>
#include "defs.h"


void el3_next_instruction(struct registers *regs)
{
    regs->pc += 4;
}


uint32_t psci_handle_cpu_on(struct registers *r)
{
    cpuid_t cpuid;
    volatile struct percpu_data *cpu;

    if(!mpidr_is_valid(r->x[1])) {
        return PSCI_RET_INVALID_PARAMETERS;
    }

    cpuid = mpidr_to_cpuid(r->x[1]);
    cpu = __cpu_data_get(cpuid);

    dprintf("CPU_ON %x %d -> state=%d\n", r->x[1], cpuid, cpu->state);

    if(cpu->state != SPIN) {
        return PSCI_RET_ALREADY_ON;
    }

    cpu->arg0 = r->x[2];
    cpu->arg1 = r->x[3];
    cpu->adr = (adr_t) el3_drop_to_el2;
    __cpu_data_commit(cpuid);

    return SMC_RET_SUCCESS;
}

void el3_handle_smc(struct registers *r)
{

    dprintf("SMC call %X:%x at %x\n", r->x[0], r->x[1], r->pc);


    switch( r->x[0]) {
    case PSCI_CMD_CPU_ON: /* CPU_ON */
        r->x[0] = psci_handle_cpu_on(r);
        break;

    case PSCI_CMD_SYSTEM_OFF:
        __system_off();
        /* if we get here we have failed */
        r->x[0] = PSCI_RET_NOT_SUPPORTED;
        break;

    case PSCI_CMD_SYSTEM_RESET:
        __system_reset();
        /* if we get here we have failed */
        r->x[0] = PSCI_RET_NOT_SUPPORTED;
        break;

    case UPCALL_EMERGENCY_ERASE:
        dprintf("Emergency erase monitor call...\n");
        __emergency_erase_secure_state();
        break;

    default:
        dprintf("Unknown SMC call: %X:%x\n", r->x[0], r->x[1]);
    }
}


void el3_irq_handler(struct registers *r, uint64_t psr, uint64_t elr)
{
    uint32_t iar;
    int i;

    iar = _gicc->iar;
    dprintf("EL3 IRQ IAR=%x, PSR=%x PC=%x\n", iar,  psr, r->pc);

    for(int i = 0; i < 7; i++) {
        if(_gicd->isactivern[i] | _gicd->ispendrn[i]) {
            dprintf("\tEL3 IRQ ENABLE_%d=%x PEND_%d=%x ACTIVE_%d=%x\n",
                   i, _gicd->isenablern[i],
                   i, _gicd->ispendrn[i],
                   i, _gicd->isactivern[i]
                   );
        }
    }

    // EOIR = IAR => ack it!
    _gicc->eoir = iar;
    for(;;);
}

void el3_fiq_handler(struct registers *r, uint64_t psr, uint64_t elr)
{
    uint32_t iar;
    int i;

    iar = _gicc->iar;
    switch(iar) {
    case 0x1d: /* SECURE TIMER */
        {
            static int old_mode = -1;
            int mode = psr & 5;

            MSR64("CNTPS_CTL_EL1", 3);

            if(old_mode != mode) {
                dprintf("EL3 sectimer: mode %d->%d PSR=%x PC=%X\n",
                       old_mode, mode, psr, r->pc);
                old_mode = mode;

                MSR64("CNTPS_TVAL_EL1", 0x100); /* timer value */

            }  else {
                MSR64("CNTPS_TVAL_EL1", MRS64("CNTFRQ_EL0") / 4); /* timer value */
            }
            MSR64("CNTPS_CTL_EL1", 1);
        }

        break;
    default:
        dprintf("EL3 FIQ IAR=%x, PSR=%x PC=%x\n", iar,  psr, r->pc);

        for(int i = 0; i < 7; i++) {
            if(_gicd->isactivern[i] | _gicd->ispendrn[i]) {
                dprintf("\tEL3 FIQ ENABLE_%d=%x PEND_%d=%x ACTIVE_%d=%x\n",
                       i, _gicd->isenablern[i],
                       i, _gicd->ispendrn[i],
                       i, _gicd->isactivern[i]
                       );
            }
        }
        __die();
    }


    // EOIR = IAR => ack it!
    _gicc->eoir = iar;
}


void el3_sync_handler(struct registers *r, uint64_t psr, uint64_t elr, union reg_esr esr)
{
    switch(esr.ec) {
    case ESR_EC_SMC64:
        el3_handle_smc(r);
        break;

    default:
        dprintf("Unknown sync exception ERR=%x at %x\n", esr.all, r->pc);
        dprintf("\tX0: %x, X1: %x, X2: %x, X3: %x\n", r->x[0], r->x[1], r->x[2], r->x[3]);
        for(;;) ; /* TEMP */
    }
}
