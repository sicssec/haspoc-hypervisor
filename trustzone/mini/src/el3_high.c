
#include <minilib.h>
#include "defs.h"
#include "gicv2.h"

/* forward refs */
void el3_setup_secondary();



int16_t secure_interrupts [] = {
    8, 9, 10, 11, 12, 13, 14, 15, /* SGI 8-15, same as ATF ?*/
    0x1D, /* Physical timer */
    56, /* TZ watchdog */
    -1 /* end of list */
};


void dprintf(const char *fmt, ...)
{
    va_list args;
    va_start(args, fmt);
    static spinlock_t lock = 0;

    spinlock_lock(&lock);
    
    if(*fmt == '_')
        fmt++;
    else
        printf("[C%d] ", __cpu_num() );
    
    vprintf(fmt, args);
    spinlock_unlock(&lock);
    va_end(args);
}


void el3_setup_gicv2_global()
{
    int lines, i;

    if( ((_gicd->iidr >> 16) & 15) >= 3) {  // GICv3?
        __die();
    }

    /* setup distributer */
    _gicd->ctlr &= ~3;
    lines = 1 + (_gicd->typer & 0x1f); /* GICD_TYPER.ITLinesNumber */


    /* for now, all is grp1 */
    for(i = 0; i < lines; i++)
        _gicd->igrouprn[i] = -1;

    /* for each secure SPI */
    for(i = 0; secure_interrupts[i] != -1; i++) {
        int id = secure_interrupts[i];
        if(gicv2_irqid_get_type(id) == GIC_IRQ_TYPE_SPI) {
            /* make it grp0, highest prio and send it to this CPU */
            dprintf("Making SPI %d secure...\n", id);
            gicv2_dist_set_group(_gicd, id, 0);
            gicv2_dist_set_priority(_gicd, id, 0);
            gicv2_dist_set_target(_gicd, id, 1); // CPU0
            gicv2_dist_set_enable(_gicd, id, 1);
        }
    }
    _gicd->ctlr |= 1;
}



void el3_setup_gicv2_local()
{
    int i;

    _gicc->pmr = 0xFF;
    _gicc->ctlr = 0x1E9;
    _gicd->igrouprn[0] = -1;

    /* for each secure SGI or PPI */
    for(i = 0; secure_interrupts[i] != -1; i++) {
        int id = secure_interrupts[i];
        if(gicv2_irqid_get_type(id) != GIC_IRQ_TYPE_SPI) {
            /* make it grp0, highest prio */
            dprintf("Making SGI/PPI %d secure...\n", id);
            gicv2_dist_set_group(_gicd, id, 0);
            gicv2_dist_set_priority(_gicd, id, 0);
            gicv2_dist_set_enable(_gicd, id, 1);
        }
    }
}


static void el3_setup_timer()
{
    int id;

    /* claim and setup the secure physical timer */
    MSR64("CNTPS_CTL_EL1", 0);
    MSR64("SCR_EL3", MRS64("SCR_EL3") &  ~(1UL << 11)); // EL3 access only
    MSR64("CNTPS_TVAL_EL1", 0x100); /* timer value */

    id = gicv2_get_irqid(GIC_IRQ_TYPE_PPI, IRQ_PPI_CNTPS);
    gicv2_dist_set_config(_gicd, id, GICV2_ICFG_1N | GICV2_ICFG_EDGE );
    gicv2_dist_set_group(_gicd, id, 0);
    gicv2_dist_set_priority(_gicd, id, 0);
    gicv2_dist_set_enable(_gicd, id, 1);
#if 0
    MSR64("CNTPS_CTL_EL1", 1);
#endif
}


void el3_drop_to_el2()
{
    volatile struct percpu_data *cpu;
    adr_t el2_entry;
    uint64_t el2_x0;

    cpu = __cpu_data_this();
    el2_entry = cpu->arg0;
    el2_x0 = cpu->arg1;


    MSR64("SCR_EL3", MRS64("SCR_EL3")
        | (1 << 0)   /* NS, needed to drop to el2 */
        | (1 << 2)   /* FIQ is taken in el3 */
        | (1 << 8)   /* EL1_ns can do monitor calls */
        | (1 << 10)  /* 64 bit */
    );

    MSR64("SPSR_EL3", 0x3c9); /* MODE EL2h */
    MSR64("ELR_EL3", el2_entry);    /* hypervisor entry */

    __asm__ volatile(
        "mov x0, %0\n"
        "isb\n"
        "eret"
        :: "r"(el2_x0)
        );
}

/* message secondary cores to starts their setup */
static void el3_setup_secondary_request()
{
    int i, whoami;
    volatile struct percpu_data *cpu;

    whoami = __cpu_num();
    for(i = 0; i < ARCH_CORES; i++) {
        cpu = __cpu_data_get(i);
        cpu->arg0 = 0;
        cpu->state = INIT;

        cpu->adr = (uint64_t) el3_setup_secondary;
        __cpu_data_commit(i);
    }
}



/* start waiting for PSCI command, wont return */
static void el3_psci_start_spin()
{
    int me;
    volatile struct percpu_data *cpu;

    me = __cpu_num();
    cpu = __cpu_data_get(me);
    cpu->state = SPIN;
    __cpu_data_commit(me);

    __cpu_data_wait();
}

void el3_setup_secondary()
{

    el3_setup_gicv2_local();
    el3_setup_timer();

    el3_psci_start_spin();
}

void el3_setup_primary()
{
    el3_setup_gicv2_global();
    el3_setup_gicv2_local();
    el3_setup_timer();

    el3_setup_secondary_request();


    /* prepare for first drop to EL2 */
    __cpu_data_this()->arg0 = HYP_ROM_START;
    __cpu_data_this()->arg1 = PAYLOAD_START;

    el3_drop_to_el2();

    /* should not return! */
}
