
EL3 (trustzone) boot code, this is code that executes *before* the hypervisor.
Hence it is not a part of the hypervisor *per se* but may be needed to start
the hypervisor binary.


The following types exist:

1. mini - minimal EL3 code for use with the simulator
2. ATF - ARM Trusted Firmware, i.e. the real deal
3. SB - Secure Boot, files are available elsewhere

You can use the TZ vaariable to select the one you want, e.g. ::


    make TZ=sb TARGET=hikey setup
    make TZ=sb TARGET=hikey test
