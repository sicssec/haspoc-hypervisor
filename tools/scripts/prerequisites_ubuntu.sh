#!/bin/bash

#
# install all required and optional tools on this system
#

set -e


sudo apt-get install -y git gitk \
     gcc gcc-aarch64-linux-gnu g++-aarch64-linux-gnu \
     device-tree-compiler bc build-essential \
     cflow cscope
