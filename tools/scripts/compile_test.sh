#!/bin/bash

#
# check if code compiles for all platform
#

TARGETS="fvp hikey s810"
GUESTS="linux mini"
TZ="sb atf"

for t in $TARGETS ; do
    for g in $GUESTS ; do
        for b in $TZ ; do
            make TARGET=$t GUEST=$g TZ=$b setup
            make TARGET=$t GUEST=$g TZ=$b guest
            make TARGET=$t GUEST=$g TZ=$b
        done
    done
done
