/* tool to read config and create the bundle payload */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <time.h>

#include "base.h"
#include "devicetree.h"
#include "bundle.h"

#define CHUNK_MAX 32
#define ALIGN_SIZE 64

#define hvend(x) litend(x)
#define dtend(x) bigend(x)

struct chunk {
    char *filename;
    char *id;
    size_t size, size_aligned;
    uint32_t time;
    uint32_t offset;
    uint32_t flags;
};

struct dt_context dtb_config;
struct chunk chunks[CHUNK_MAX];
int chunk_cnt = 0;
uint32_t file_total_size;

/* ----------------------------------------------- */
/* misc helpers */
void fatal(char *msg, char *data)
{
    fprintf(stderr, "FATAL ERROR: ");
    fprintf(stderr, msg, data);
    fprintf(stderr, "\n");
    fflush(stderr);
    exit(20);
}

/* ----------------------------------------------- */
/* IO helpers */


void io_get_stats(char *filename, size_t *size, uint32_t *time)
{
    struct stat st;

    if(stat(filename, &st) < 0)
        fatal("Could not get file size for '%s'", filename);

    if(size)
        *size = st.st_size;

    if(time)
        *time = st.st_mtim.tv_sec / 60;
}

void *io_read_file(char *filename, size_t *size)
{
    int o, n, m;
    FILE *fp;
    char *ret;

    io_get_stats(filename, size, 0);
    n = *size;
    if(!(ret = malloc(n)))
        fatal("Could not allocate file memory", 0);

    if(!(fp = fopen(filename, "rb")))
        fatal("Could not open file %s", filename);

    for(o = 0; n > 0; ) {
        if( (m = fread(ret + o, 1, n, fp)) <= 0)
            fatal("fread() failed", 0);
        n -= m;
        o += m;
    }
    fclose(fp);
    return ret;
}

void io_write_bin(FILE *fp, char *filename)
{
    int i, n;
    char buffer[1024 * 16];
    FILE *fp2;

    if(! (fp2 = fopen(filename, "rb")))
        fatal("could not open '%s' for reading\n", filename);

    for(;;) {
        n = fread(buffer, 1, sizeof(buffer), fp2);
        if(n < 1) break;

        if( fwrite(buffer, 1, n, fp) != n)
            fatal("Could not write '%s' to bundle", filename);
    }

    fclose(fp2);
}

void io_write_zeros(FILE *fp, int size)
{
    while(size-- > 0)
        fputc(0, fp);
}

/* ----------------------------------------------- */
/* chunks */

struct chunk *chunk_find(char *filename)
{
    int i;
    for(i = 0; i < chunk_cnt; i++)
        if(!strcmp(chunks[i].id, filename))
            return &chunks[i];
    return 0;
}

void chunk_add(char *filename, char *id)
{
    struct chunk *ch;

    if(chunk_find(filename)) /* already added ? */
        return;

    if(strlen(id) > BUNDLE_ID_LENGTH)
        fatal("chunk ID '%s' too long ", id);

    if(chunk_cnt >= CHUNK_MAX)
        fatal("Too manu chunks", 0);

    ch = & chunks[chunk_cnt++];
    ch->filename = filename;
    ch->id = id;
    io_get_stats(filename, & ch->size, & ch->time);
    ch->size_aligned = (ch->size + ALIGN_SIZE - 1) & ~(ALIGN_SIZE-1);
    ch->flags = 0;
}

void chunk_prepare()
{
    int i;
    int off = sizeof(struct bundle_hdr) +
        sizeof(struct bundle_data) * chunk_cnt;


    /* make sure size is aligned */
    off = (off + ALIGN_SIZE - 1) & ~(ALIGN_SIZE-1);

    for(i = 0; i < chunk_cnt; i++) {
        chunks[i].offset = off;
        off += chunks[i].size_aligned;
    }

    /* calc total file size */
    file_total_size = (off + ALIGN_SIZE - 1) & ~(ALIGN_SIZE-1);

    file_total_size += sizeof(struct bundle_ftr);
}


void chunk_write(char *filename)
{
    int i, s0, s1;
    FILE *fp;
    struct bundle_hdr hdr;
    struct bundle_ftr ftr;
    struct bundle_data data;

    printf("Creating bundle file '%s'...\n", filename);
    if(! (fp = fopen(filename, "wb")))
        fatal("Could not open bundle %s", filename);

    /* write header */
    hdr.magic = hvend(BUNDLE_MAGIC);
    hdr.size = hvend( file_total_size);
    hdr.count = hvend(chunk_cnt);
    hdr.flags = hvend(0);
    hdr.time = hvend( time(0) / 60);
    fwrite(&hdr, 1, sizeof(hdr), fp);

    /* write chunk data*/
    for(i = 0; i < chunk_cnt; i++) {
        memset( &data, 0, sizeof(data));
        data.size = hvend(chunks[i].size);
        data.offset = hvend(chunks[i].offset);
        data.flags = hvend(chunks[i].flags);
        data.time = hvend(chunks[i].time);
        strcpy(data.id, chunks[i].id);
        fwrite(&data, 1, sizeof(data), fp);
    }

    /* align data so far */
    s0 = sizeof(hdr) + sizeof(data) * chunk_cnt;
    s1 = (s0 + ALIGN_SIZE - 1) & ~(ALIGN_SIZE-1);
    io_write_zeros(fp, s1 - s0);

    /* write files */
    for(i = 0; i < chunk_cnt; i++) {
        printf("\tadding %08x bytes at %08x with %s\n",
               (uint32_t) chunks[i].size_aligned,
               (uint32_t) chunks[i].offset,
               chunks[i].id );

        io_write_bin(fp, chunks[i].filename);
        io_write_zeros(fp, chunks[i].size_aligned - chunks[i].size);
    }

    /* write footer */
    memset( &ftr, 0, sizeof(ftr));
    ftr.magic = hvend(hdr.time - hdr.magic);
    fwrite(&ftr, 1, sizeof(ftr), fp);

    printf("All done!\n");
    fclose(fp);
}

/* ----------------------------------------------- */
/* config */

void config_find_node(char *name, struct dt_block *parent,
                      struct dt_block *result)
{
    if(!dt_block_find(&dtb_config, parent, result, 1, name, 0))
        fatal("Could not find required node '%s'", name);
}

void config_find_prop(char *name, struct dt_block *parent,
                      struct dt_block *result)
{
    if(!dt_block_find(&dtb_config, parent, result, 0, name, 0))
        fatal("Could not find required property '%s'", name);
}
void config_parse_firmware(struct dt_block *fw)
{
    struct dt_block prop;
    config_find_prop("filename", fw, &prop);
    chunk_add(prop.data.str, prop.data.str);
}

void config_parse_guest(struct dt_block *guest)
{
    struct dt_block *fw, *tmp;
    struct dt_foreach fe;

    dt_foreach_init(&dtb_config, guest, &fe, 1);
    for(;;) {
        if( !( fw = dt_foreach_next(&fe)))
            break;
        if(strstr(fw->name, "firmware@") == fw->name)
            config_parse_firmware(fw);
    }
}
void config_parse()
{
    struct dt_block *guest, *tmp;
    struct dt_foreach fe;

    dt_foreach_init(&dtb_config, 0, &fe, 1);
    for(;;) {
        if( !( guest = dt_foreach_next(&fe)))
            break;
        if(strstr(guest->name, "guest@") == guest->name)
            config_parse_guest(guest);
    }
}

void config_load(char *filename)
{
    void *dtb_data;
    size_t dtb_size;
    struct dt_block tmp;

    dtb_data = io_read_file(filename, &dtb_size);
    if(!dt_init(&dtb_config, dtb_data, dtb_size)) {
        fatal("Could not parse config device tree '%s'", filename);
    }

    config_find_prop("id", 0, &tmp);
    printf("Using configuration file '%s'\n", tmp.name);

    /* add config itself to the list of chunks */
    chunk_add(filename, "config");
}

/* ----------------------------------------------- */

int main(int argc, char **argv)
{
    int i;

    if(argc < 3)
        fatal("Usage %s <config DTB> <output file>\n", argv[0]);

    config_load(argv[1]);
    config_parse();

    chunk_prepare();
    chunk_write(argv[2]);

    return 0;
}
