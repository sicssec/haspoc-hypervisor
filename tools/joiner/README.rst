
JOINER
------

Tool to join the hypervisor binary and the payload into a single file.
We use the target configuration to compute the positions.

[HV] + [PAYLOAD] =>  [HV | padding | PAYLOAD]

If the padding is too big, the resulting file will not be created


NOTE: this can easily be done with a shell script or similar but we
      used a more complex approach earlier which required more logic
      than what we have now.
