
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

/* this will get us the target-specific pointers */
#include "arch_defs.h"


/* if padding is bigger than this, dont do it */
#define MAX_PADDING (1024 * 1024 * 2)

/* some helpers */

void fatal(const char *msg, const char *data)
{
    fprintf(stderr, "FATAL ERROR: ");
    fprintf(stderr, msg, data);
    fprintf(stderr, "\n");
    fflush(stderr);
    exit(20);
}

void file_get_size(const char *filename, size_t *size)
{
    struct stat st;

    if(stat(filename, &st) < 0)
        fatal("Could not get file size for '%s'", filename);

    if(size)
        *size = st.st_size;
}

void file_write(FILE *in, FILE *out)
{
    #define BUFFER_SIZE 1024
    int n, m;
    char *tmp, buffer[BUFFER_SIZE];

    for(;;) {

        n = fread(&buffer, 1, BUFFER_SIZE, in);
        if(n < 0)
            fatal("Read error in file_write()", NULL);
        if(n == 0)
            return;

        tmp = buffer;
        while(n > 0) {
            m = fwrite(tmp, 1, n, out);
            if(m <= 0)
                fatal("Write error in file_write()", NULL);
            tmp += m;
            n -= m;
        }
    }
}

void file_join(const char *in1, const char *in2, const char *o3, size_t offset2)
{
    FILE *fp1, *fp2, *fp3;

    if(! (fp3 = fopen(o3, "wb")))
        fatal("Could not open '%s' for writing", o3);

    if(! (fp2 = fopen(in2, "rb")))
        fatal("Could not open '%s' for reading", in2);

    if(! (fp1 = fopen(in1, "rb")))
        fatal("Could not open '%s' for writing", in1);


    file_write(fp1, fp3);
    fclose(fp1);

    if(fseek(fp3, offset2, SEEK_SET) < 0)
        fatal("could not seek to end of padding in %s", o3);

    if(ftell(fp3)  != offset2)
        fatal("could not seek to end of padding in %s (checked)", o3);

    file_write(fp2, fp3);
    fclose(fp2);
    fclose(fp3);
}

/* main */
int main(int argc, char **argv)
{
    int i;
    size_t size_payload, size_hv;
    size_t size_padding, size_offset;

    const char *filename_hv = argv[1];
    const char *filename_payload = argv[2];
    const char *filename_out = argv[3];

    if(argc != 4)
        fatal("Usage %s <HV-BIN> <PAYLOAD-BIN> <output file>\n", argv[0]);

    /* always remove the old output */
    unlink(filename_out);

    file_get_size(filename_hv, &size_hv);
    file_get_size(filename_payload, &size_payload);

    /* sanity checks */
    if(size_hv > HYP_ROM_SIZE)
        fatal("hypervisor binary bigger than expected", 0);

    if(size_payload > PAYLOAD_SIZE)
        fatal("payload is bigger than expected", 0);


    size_offset = PAYLOAD_START - HYP_ROM_START;
    size_padding = size_offset -  size_hv;


    /* should we do it ? */
    if(size_padding < 0 || size_padding > MAX_PADDING) {
        fprintf(stderr,
                "\n"
                "*** padding too large or negative, will not create\n"
                "*** joined file %s"
                "\n\n",
                filename_out);
        return 0;
    }

    file_join(filename_hv, filename_payload, filename_out,
              PAYLOAD_START - HYP_ROM_START);


    printf("\n"
           "*** CREATED COMBINED HV+PAYLOAD IN %s\n"
           "*** PAYLOAD OFFSET 0x%08x (PHYSICAL 0x%08x)\n"
           "\n\n",
           filename_out, size_offset, PAYLOAD_START);

    return 0;
}
