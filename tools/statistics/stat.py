#!/usr/bin/python

import os,sys

if len(sys.argv) > 1:
	keyword = sys.argv[1]
else:
	print("This is a utility that searches logs for the keyword passed in argument")
	print("Example:")
	print("$ python stat.py 'Time at boot'")
	print("$ python stat.py 'int64 div'")
	sys.exit(1)

S_PATH = "../../external/haspoc-binaries/sth-binaries/hikey/"


# Find match in boot logs
# Boot logs cannot be named *1.log, *2.log or *3.log
print("Match found in boot logs:")
os.system("find "+ S_PATH +" -name \*.log -print | sort -k 1nr | xargs grep '"+ keyword + "' > match.txt")

with open ("match.txt", "r") as l:    # Read entire file into string
	file_list=l.readlines()

for match in file_list:
	if (match.find("performance.log") == -1) and (match.find("1.log") == -1) and (match.find("2.log") == -1) and (match.find("3.log") == -1):
		dirname = match[match.index("16"):match[match.index("16"):].rindex("/")+match.index("16")]
		matchline = match[match.index(":")+1:]
		print(dirname +" "+ matchline),


# Find match in guest logs
# Guest logs must be named *1.log, *2.log or *3.log
guest = 1
while guest < 4:
	print("Match found for Guest"+str(guest)+":")

	os.system("find "+ S_PATH +" -name \*"+str(guest)+".log -print | sort -k 1nr | xargs grep '"+ keyword + "' > match.txt")

	with open ("match.txt", "r") as l:    # Read entire file into string
		file_list=l.readlines()

	for match in file_list:
		dirname = match[match.index("16"):match[match.index("16"):].rindex("/")+match.index("16")]
		matchline = match[match.index(":")+1:]
		print(dirname +" "+ matchline),

	guest += 1

os.system("rm match.txt")

#find ../../ -name \*.log -print | sort -k 1nr | xargs grep 'Time at boot'

