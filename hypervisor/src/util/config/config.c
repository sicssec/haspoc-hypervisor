

#include "defs.h"
#include "debug.h"
#include "config.h"
#include "mman.h"

#include <bundle.h>

static struct dt_context config;
static adr_t payload_start;


uint64_t config_get_data(uint32_t **data, bool is64bit)
{
    uint64_t ret;
    ret = dtend(**data);
    (*data)++;
    if(is64bit) {
        ret = (ret << 32) | dtend(**data);
        (*data)++;
    }
    return ret;
}

/* try to find something in the configuration */
int config_try_get(struct dt_block *parent, struct dt_block *result,
    int asnode, char *name, int len)
{
    return dt_block_find(&config, parent, result, asnode, name, len);
}

bool config_get_array_try(struct dt_block *parent,  char *name,
                          int element_size, void **ptr, int *count)
{
    struct dt_block result;

    assert_lt("INTERNAL: element_size too small in array", 0, element_size);

    /* array of N * element_size elements, returns N */
    if(!config_try_get(parent, &result, 0, name, 0)) {
        *count = NULL;
        *ptr = NULL;
        return false;
    }

    assert_eq("prop array size error", result.data_len % element_size, 0);
    *count = result.data_len / element_size;
    *ptr = result.data.ptr;
    return true;
}

/*
 * get a property with the given name
 */
void config_get_prop(struct dt_block *parent, struct dt_block *result,
                     char *name, int data_len)
{
    if(!config_try_get(parent, result, 0, name, 0))
        fatal("Could not get property %s", name);

    if(data_len > 0 && result->data_len != data_len)
        fatal("Bad property length for %s", name);
}


adr_t config_get_prop_adr(struct dt_block *parent, char *name)
{
    uint32_t *data;
    int cnt;

    if(!config_get_array_try(parent, name, sizeof(uint32_t),
                             (void **)&data, &cnt))
        fatal("Could not get adr property %s", name);

    if(cnt != 1 && cnt != 2)
        fatal("Bad adr length for %s", name);

    return config_get_data(&data, cnt == 2);
}

uint32_t config_get_prop_int(struct dt_block *parent, char *name)
{
    struct dt_block prop;
    config_get_prop(parent, &prop, name, 4);
    return dtend(prop.data.num[0]);
}

char *config_get_prop_str(struct dt_block *parent, char *name)
{
    struct dt_block prop;
    config_get_prop(parent, &prop, name, 0);
    return prop.data.str;
}

void config_get_node(struct dt_block *parent, struct dt_block *result,
                            char *name)
{
    if(!config_try_get( parent, result, 1, name, 0))
        fatal("Could not get node %s", name);
}


void config_foreach_node(struct dt_block *parent, struct dt_foreach *result)
{
    dt_foreach_init(&config, parent, result, 1);
}

int config_count(struct dt_block *parent, char *prefix)
{
    struct dt_foreach fe;
    int cnt = 0;

    config_foreach_node(parent, &fe);
    while(dt_foreach_next_of(&fe, prefix))
        cnt++;

    return cnt;
}


struct bundle_data* config_find_bundle(const char *filename, adr_t *adr)
{
    struct bundle_data *ret = bundle_find((void *)payload_start, filename);

    if(ret)
        *adr = payload_start + ret->offset;
    return ret;
}


void config_init_early(adr_t pstart)
{
    struct bundle_data *bd;
    struct bundle_hdr *hdr;
    struct bundle_ftr *ftr;
    int i;
    uint32_t version;

#if (defined(TARGET_juno) || defined(TARGET_hikey) || defined(TARGET_s810) || defined(TARGET_tx1))
    pstart = PAYLOAD_START;
#endif

    printf("PAYLOAD at %X\n", pstart);

    /* some temporary sanity checks until we get this working on HW! */
    if(pstart != PAYLOAD_START)
        fatal("SANITY CHECK: your payload-start is wrong");

    payload_start = pstart;
    hdr = (void *)payload_start;

    /* payload sanity check: */
    if(hdr->magic != BUNDLE_MAGIC)
        fatal("PAYLOAD not found");

    for(i = 0; i < hdr->count; i++)
        if( hdr->data[i].offset + hdr->data[i].size > hdr->size)
            fatal("PAYLOAD data outside defined range");

    /* check footer too */
    ftr = (struct bundle_ftr *)(payload_start + hdr->size - sizeof(struct bundle_ftr));
    if(ftr->magic != hdr->time - hdr->magic)
        fatal("Payload damadged or of old format");

    /* extract config DTB from payload */
    if(!( bd = bundle_find((void *)payload_start, "config")))
       fatal("Could not load config");

    if(!dt_init(&config, (void *)(payload_start + bd->offset), bd->size))
        fatal("Could not load config device tree");


    /* check config version */
    version = config_get_prop_int(NULL, "version");
#if DEBUG > 1
    printf("Config version file: %d.%d, hypervisor: %d.%d\n",
           version >> 16, version & 0xFFFF,
           CONFIG_VERSION_MAJOR, CONFIG_VERSION_MINOR);
#endif

    if( (version >> 16) != CONFIG_VERSION_MAJOR ||
        (version & 0xFFFF) < CONFIG_VERSION_MINOR)
        fatal("Config version mismatch");

    /* save payload region for later checking */
    payload_region.start = pstart;
    payload_region.end = pstart + hdr->size;


#if DEBUG > 0
    printf("Bundle S=%x F=%x T=%x:\n",
           hdr->size, hdr->flags, hdr->time);

    for(i = 0; i < hdr->count; i++) {
        printf("\t%d: O=%x S=%x F=%x T=%x %s\n",
            i, hdr->data[i].offset, hdr->data[i].size,
            hdr->data[i].flags, hdr->data[i].time,
            hdr->data[i].id);
    }

    printf("Using configuration '%s' dated %x\n",
        config_get_prop_str(0, "id"), hdr->time);

#endif


}
