
#ifndef __CONFIG_H__
#define __CONFIG_H__

#ifndef __ASSEMBLER__
#include <devicetree.h>

#define CONFIG_VERSION_MAJOR 1
#define CONFIG_VERSION_MINOR 0

extern uint64_t config_get_data(uint32_t **data, bool is64bit);

/* this is is allowed to fail if the requested item is not found */
extern int config_try_get(struct dt_block *parent, struct dt_block *result,
    int asnode, char *name, int len);

/* get array with given sizes, *count is zero if it fails */
extern bool config_get_array_try(struct dt_block *parent,  char *name,
                                 int element_size, void **ptr, int *count);

/* these are mandatory reads (fatal() if not found */
extern void config_get_node(struct dt_block *parent, struct dt_block *result,
    char *name);

extern void config_get_prop(struct dt_block *parent, struct dt_block *result,
    char *name, int data_len); /* len ignored if <= 0 */

extern uint32_t config_get_prop_int(struct dt_block *parent, char *name);
extern adr_t config_get_prop_adr(struct dt_block *parent, char *name); /* 32/64b */
extern char *config_get_prop_str(struct dt_block *parent, char *name);

extern int config_count(struct dt_block *parent, char *prefix);

/* foreach helper */
extern void config_foreach_node(struct dt_block *parent,
    struct dt_foreach *result);


/* helper to load a bundled file */
extern struct bundle_data *config_find_bundle(const char *filename, adr_t *adr);


/* setup */
extern void config_init_early(uint64_t payload_start);

#endif /* ! __ASSEMBLER__H */

#endif /* ! __CONFIG_H__ */
