
#include "util.h"

void assert_record0_(char *msg, const char *op)
{
    dprintf("ASSERT FAILED: %s\n", op);
    fatal(msg);
}

void assert_record1_(char *msg, uint64_t v1, const char *op)
{
    dprintf("ASSERT FAILED: %s %x\n", op, v1);
    fatal(msg);
}
void assert_record2_(char *msg, uint64_t v1, uint64_t v2,
                    const char *op)
{
    dprintf("ASSERT FAILED: %s %x %x\n", op, v1, v2);
    fatal(msg);
}

void assert_record3_(char *msg, uint64_t v1, uint64_t v2,
                    uint64_t v3, const char *op)
{
    dprintf("ASSERT FAILED: %s %x %x %x\n", op, v1, v2, v3);
    fatal(msg);
}
