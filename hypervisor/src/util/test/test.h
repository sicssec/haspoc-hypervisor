#ifndef __TEST_H__
#define __TEST_H__

/**
 * @name test
 * @type module
 * @description minimal test and assertation framework
 */

#include "debug.h"

#ifdef __ASSEMBLER__

#else


/*
 * test functions
 */

/* XXX: these cast everyting to uint64_t means integer overflows are not detected */
#if 0
extern void assert(char *msg, int sanity);
extern void assert_range(char *msg, uint64_t v, uint64_t min, uint64_t max);
extern void assert_eq(char *msg, uint64_t v1, uint64_t v2);
extern void assert_neq(char *msg, uint64_t v1, uint64_t v2);
extern void assert_lt(char *msg, uint64_t v1, uint64_t v2);
extern void assert_null(char *msg, void *ptr);
extern void assert_not_null(char *msg, void *ptr);
#endif

/* test frontend */
#define assert(msg, v) \
    assert_record0(msg, v, "")

#define assert_range(msg, v, min, max) \
    assert_record3(msg, ((v) >= (min)) &&  ((v) <= (max)), v, min, max, "range")

#define assert_eq(msg, v1, v2) \
    assert_record2(msg, (v1) == (v2), v1, v2, "==")

#define assert_neq(msg, v1, v2) \
    assert_record2(msg, (v1) != (v2), v1, v2, "!=")

#define assert_lt(msg, v1, v2) \
    assert_record2(msg, (v1) < (v2), v1, v2, "<")

#define assert_le(msg, v1, v2) \
    assert_record2(msg, (v1) <== (v2), v1, v2, "<=")

#define assert_null(msg, v)                     \
    assert_record1(msg, (v) == NULL, v, "NULL")

#define assert_not_null(msg, v)                     \
    assert_record1(msg, (v) != NULL, v, "not NULL")


/* intermediate for optimizaing constant cases */
#define assert_record0(msg, r, op) \
    do { if(! (r)) assert_record0_(msg, op); } while(0)

#define assert_record1(msg, r, v1, op)                      \
    do { if(! (r)) assert_record1_(msg, (uint64_t)(v1), op); } while(0)

#define assert_record2(msg, r, v1, v2, op)                      \
    do { if(! (r)) assert_record2_( \
            msg, (uint64_t)(v1), (uint64_t)(v2), op); } while(0)

#define assert_record3(msg, r, v1, v2, v3, op) \
    do { if(! (r)) assert_record3_( \
            msg, (uint64_t)(v1), (uint64_t)(v2), (uint64_t)(v3), op); } while(0)


/* backend functions */
extern void assert_record0_(char *msg, const char *op);
extern void assert_record1_(char *msg, uint64_t v1, const char *op);
extern void assert_record2_(char *msg, uint64_t v1, uint64_t v2,
                            const char *op);
extern void assert_record3_(char *msg, uint64_t v1, uint64_t v2,
                            uint64_t v3, const char *op);

#endif /* __ASSEMBLER__ */

#endif /* __TEST_H__ */
