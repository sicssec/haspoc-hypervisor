
#include "defs.h"

#include <minilib.h>
#include <uart.h>

#include "gman.h"
#include "cman.h"
#include "vgic.h"

#if defined(ARCH_HAS_GICv2)
#include <gicv2.h>

static volatile struct gicv2_gicd * const gicd = (struct gicv2_gicd *) GICD_BASE;
static volatile struct gicv2_gicc * const gicc = (struct gicv2_gicc *) GICC_BASE;
static volatile struct gicv2_gicc * const gicv = (struct gicv2_gicc *) GICV_BASE;
static volatile struct gicv2_gich * const gich = (struct gicv2_gich *) GICH_BASE;

#endif

static spinlock_t dprint_lock = 0;

void dprintf(const char *fmt, ...)
{
    va_list args;
    va_start(args, fmt);

    spinlock_lock(&dprint_lock);

    if(*fmt == '_')
        fmt++;
    else
        printf("[C%d] ", mc_whoami());

    vprintf(fmt, args);
    spinlock_unlock(&dprint_lock);

    va_end(args);
}



void printf_putchar(int c)
{
#if defined(TARGET_hikey)
    uart_write(ARCH_UART3_BASE, c);
#else
    uart_write(ARCH_UART0_BASE, c);
#endif
}

void fatal(char *fmt, ...)
{
    va_list args;
    va_start(args, fmt);

    spinlock_lock(&dprint_lock);

    printf("[C%d] FATAL ERROR: ", mc_whoami());
    vprintf(fmt, args);
    printf("\n");
    spinlock_unlock(&dprint_lock);
    __die();
}


/* generic hexdump function */
void debug_dump_hex(char *msg, adr_t start, size_t size)
{
    int i, j;
    uint32_t *hex = (uint32_t *) (start & ~3);
    uint8_t *str = (uint8_t *) hex;

    if(msg)
        dprintf("%s:\n", msg);

    size = (size + 15) / 16;
    for(i = 0; i < size; i ++, hex += 4) {
        dprintf("%X: %x %x %x %x  ", hex, hex[0], hex[1], hex[2], hex[3]);
        for(j = 0; j < 4; j++, str += 4) {
#define CFILTER(c) ( (c) < 32 || (c) > 127 ? '.' : (c))
            dprintf("_ %c%c%c%c", CFILTER(str[0]), CFILTER(str[1]),
                    CFILTER(str[2]), CFILTER(str[3]));
        }
        dprintf("_\n");
    }
}


void debug_dump_registers(struct registers *regs)
{
    int i;

    for(i = 0; i < 10; i++)
        dprintf("X0%d=%X X%d=%X X%d=%X\n",
                i + 0, regs->x[i+0],
                i + 10, regs->x[i+10],
                i + 20, regs->x[i+20]);
    dprintf("X30=%X PC=%X CPSR=%x\n", regs->x[30], regs->pc, regs->cpsr);
}

void debug_dump_vgic(struct guest *g)
{
    int i;
    for(i = 0; i < VGIC_MAX_VIRQ; i++) {
        struct virq *virq = vgic_virq_get(g, i);
        if(!virq || virq->state == VIRQ_STATE_NONE)
            continue;


        if(virq->virt)
            dprintf("\tVIRQ %d: S=%d E=%d P=%d A=%d M=%d\n",
                    i, virq->state,
                    virq->gicd_enabled, virq->gicd_pending,
                    virq->gicd_active, virq->gicd_cpumask);
        else
            dprintf("\tVIRQ %d: P=%d S=%d E=%d(%d) P=%d(%d) A=%d(%d) M=%d(%d) Pr=%d(%d)/%d\n",
                    i, virq->pid, virq->state,
                    gicv2_dist_get_enable(gicd, virq->pid), virq->gicd_enabled,
                    gicv2_dist_get_pending(gicd, virq->pid), virq->gicd_pending,
                    gicv2_dist_get_active(gicd, virq->pid), virq->gicd_active,
                    gicv2_dist_get_target(gicd, virq->pid), virq->gicd_cpumask,
                    gicv2_dist_get_priority(gicd, virq->pid), virq->gicd_prio,
		    virq->config_prio_min);

              ;
    }
}
void debug_dump_guest(struct guest *g)
{
    int i, j;

    dprintf("Guest %d (%s) at %X on CPU %d\n", g->vmid, g->name, g, mc_whoami());
    dprintf("MAIR = %X, TCR = %x, SCTLR=%x\n",
            MRS64("MAIR_EL2"),
            MRS64("TCR_EL2"),
            MRS64("SCTLR_EL2")
            );

    dprintf("MPIDR_EL1=%X VMPIDR_EL2=%X\n",
            MRS64("MPIDR_EL1"),
            MRS64("VMPIDR_EL2"));

    debug_dump_vgic(g);
}

void dump_cache()
{
  uint32_t ccsidr, clidr, type;
  int i;

  printf("CACHES: CLIDR = %x, CTR = %x\n",
	 clidr = MRS64("CLIDR_EL1"),
	 MRS64("CTR_EL0"));

  for(i = 0; i < 7; i++) {
    uint32_t type = (clidr >> (i * 3)) & 7;
    if(!type) break;
    printf("\tL%d (type %d) ", i + 1, type);

    // data or unified cache:
    if(type == 2 || type == 3 || type == 4) {
      __asm__ volatile( "msr CSSELR_EL1, %0\n isb" ::"r"(i * 2));
      ccsidr = MRS64("CCSIDR_EL1");
      printf(" D/U CCSIDR=%x", ccsidr);
    }

    // instruction cache
    if(type == 1 || type == 3) {
      __asm__ volatile( "msr CSSELR_EL1, %0\n isb" ::"r"(i * 2 + 1));
      ccsidr = MRS64("CCSIDR_EL1");
      printf(" I CCSIDR=%x", ccsidr);
    }
    printf("\n");
  }
}

void debug_dump_gic()
{
    int i;
    struct pirq *pirq;
    struct pcpu *pcpu;

    pcpu = cman_pcpu_get_safe(); /* use safe version, may be called during init */

#if defined(ARCH_HAS_GICv2)
    for(i = 0; i < GIC_MAX_IRQ; i++) {
        int e = gicv2_dist_get_enable(_gicd, i);
        int p = gicv2_dist_get_pending(_gicd, i);
        int a = gicv2_dist_get_active(_gicd, i);

        pirq = gic_pirq_get(pcpu, i);

        if( (p | a | e) || (pirq && pirq->type != PIRQ_TYPE_NONE &&
                            pirq->type != PIRQ_TYPE_EL3)) {
            dprintf("PIRQ %d\tE=%d P=%d A=%d Tg=%x Cfg=%d Pr=%d",
                    i, e, p, a,
                    gicv2_dist_get_target(_gicd, i),
                    gicv2_dist_get_config(_gicd, i),
                    gicv2_dist_get_priority(_gicd, i)
                    );

            if(pirq) {
                dprintf("_ Typ=%d", pirq->type);
                if(pirq->type == PIRQ_TYPE_EL1) {
                    dprintf("_ Vid=%d G=%s", pirq->vid, pirq->owner->name);
                } else if(pirq->type == PIRQ_TYPE_EL2) {
                    dprintf("_ Cb=%X", pirq->callback);
                }
            }
            dprintf("_\n");
        }
    }

    dprintf("GICC CTLR=%x PMR=%x IIDR=%x max=%d/%d\n",
            gicc->ctlr, gicc->pmr, gicc->iidr,
            GIC_MAX_IRQ, gicv2_dist_get_max(_gicd));

    dprintf("GICD CTLR=%x BPR=%x RPR=%x HPPIR=%x\n",
            gicc->ctlr, gicc->bpr, gicc->rpr, gicc->hppir);

    dprintf("GICV CTLR=%x BPR=%x RPR=%x HPPIR=%x\n",
            gicv->ctlr, gicv->bpr, gicv->rpr, gicv->hppir);

    dprintf("GICH  HCR=%x  VTR=%x VMCR=%x\n",
            gich->hcr, gich->vtr, gich->vmcr);
    dprintf("GICH MISR=%x EISR=%x %x\n",
            gich->misr, gich->eisr[0], gich->eisr[1]);
    dprintf("GICH  APR=%x ELSR=%x %x\n",
            gich->apr, gich->elsr[0], gich->elsr[1]);
    dprintf("GICH  LR0=%x %x %x %x\n",
            gich->lr[0], gich->lr[1], gich->lr[2], gich->lr[3]);
    dprintf("GICH  LR4=%x %x %x %x\n",
            gich->lr[4], gich->lr[5], gich->lr[6], gich->lr[7]);
#endif
}

void perf_timestamp(char* label)
{
	printf("[C%d] ",mc_whoami());
	printf("Performance monitor; %s(ms): %d\n",label,MRS64("pmccntr_el0") / MRS64("cntfrq_el0"));
}

void perf_setup()
{
	// Ref: ARMv8 ARM D7.4
	MSR64("pmccfiltr_el0",0xFC000000); // Count cycles in all exceptionlevels
	//MSR64("pmcr_el0",5); // Reset ccntr : ARMv8-ARM D7.4.7: C[2]=1, E[0]=1
	perf_timestamp("Time at boot");
}

void perf_init()
{
	// Ref: ARMv8 ARM D7.4
	MSR64("pmccfiltr_el0",0xFC000000); // Count cycles in all exceptionlevels
	MSR64("pmcr_el0",5); // Reset ccntr
	MSR64("pmcntenset_el0",0x80000000); // Enable counting, time starts here
}

void debug_core_init()
{
	perf_init();
}

void debug_init_early()
{
    volatile uint32_t *base;

    /* 1. setup early uart: see target code */


    /* 2. print some boot debug messages */
    printf("Booting STH ARMv8\n");
    printf("$DATE='" __DATE__ " " __TIME__ "' "
           "$COMMIT='" STR(VERSION_STRING) "' "
           "$DEBUG=" STR(DEBUG) "'\n");
	perf_setup();
    printf("HYP RAM: %X-%X\n", HYP_RAM_START, HYP_RAM_END);
    printf("HYP ROM: %X-%X\n", HYP_ROM_START, HYP_ROM_END);

    printf("ID_AA64AFR  = (%X %X)\n",
           MRS64("ID_AA64AFR0_EL1"),
           MRS64("ID_AA64AFR1_EL1"));

    printf("ID_AA64DFR  = (%X %X)\n",
           MRS64("ID_AA64DFR0_EL1"),
           MRS64("ID_AA64DFR1_EL1"));

    printf("ID_AA64ISAR = (%X %X)\n",
           MRS64("ID_AA64ISAR0_EL1"),
           MRS64("ID_AA64ISAR1_EL1"));

    printf("ID_AA64MMFR = (%X %X)\n",
           MRS64("ID_AA64MMFR0_EL1"),
           MRS64("ID_AA64MMFR1_EL1"));

    printf("ID_AA64PFR  = (%X %X)\n",
           MRS64("ID_AA64PFR0_EL1"),
           MRS64("ID_AA64PFR1_EL1"));

    printf("ID_MMFR     = (%x %x %x %x)\n",
           MRS64("ID_MMFR0_EL1"),
           MRS64("ID_MMFR1_EL1"),
           MRS64("ID_MMFR2_EL1"),
           MRS64("ID_MMFR3_EL1"));

    printf("MAIR = %X, TCR = %x, SCTLR=%x\n",
           MRS64("MAIR_EL2"),
           MRS64("TCR_EL2"),
           MRS64("SCTLR_EL2")
           );
    printf("MIDR_EL1=%x MPIDR_EL1=%X VMPIDR_EL2=%X\n",
           MRS64("MIDR_EL1"),
           MRS64("MPIDR_EL1"),
           MRS64("VMPIDR_EL2"));

    dump_cache();

#if defined(ARCH_HAS_GICv2)
    printf("GICD=%x GICC=%x GICH=%x GICV=%x\n",
           GICD_BASE,
           GICC_BASE,
           GICH_BASE,
           GICV_BASE);
    printf("GICD CTLR=%x TYPER=%x IIDR=%x\n", gicd->ctlr, gicd->typer, gicd->iidr);
    printf("GICC CTLR=%x PMR=%x IIDR=%x\n", gicc->ctlr, gicc->pmr, gicc->iidr);
    printf("GICH HCR=%x VTR=%x VMCR=%x MISR=%x\n",
           gich->hcr, gich->vtr, gich->vmcr, gich->misr);
#endif

    printf("vGIC configuration: %d pIRQ -> %d vIRQ \n",
           GIC_MAX_IRQ, VGIC_MAX_VIRQ);
}
