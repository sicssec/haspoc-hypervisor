#ifndef _DEBUG_H_
#define _DEBUG_H_

#ifndef __ASSEMBLER__
struct guest;
struct vgic_driver;

#define LVL0 0
#define LVL1 1
#define LVL2 2
#define LVL3 3

#define debug_print(level, x...) \
            do { if ((level) <= DEBUG) { dprintf(x); } } while (0)

/*
 * dprintf() locks the terminal to avoid multi-core mash in printf()
 * Start with '_' to supress "[core=n] "message.
 */
extern void dprintf(const char *, ...);

/* bad things just happaned, stop execution! */
extern void fatal(char *msg, ...);

/* performance */
extern void perf_timestamp(char* label);

/* dump debug information */
extern void debug_dump_hex(char *msg, adr_t start, size_t size);
extern void debug_dump_guest(struct guest *);
extern void debug_dump_vgic(struct guest *);
extern void debug_dump_gic();
extern void debug_dump_registers(struct registers *regs);

extern void debug_core_init();
extern void debug_init_early();

#endif /* ! __ASSEMBLER__ */

#endif /* ! _DEBUG_H_ */
