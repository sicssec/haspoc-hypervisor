#ifndef __UTIL_H__
#define __UTIL_H__

/**
 * @name utilities
 * @type module
 * @description general utilities are gathered here
 */

#include "base.h"
#include "armv8.h"

#include "config.h"
#include "debug.h"
#include "test.h"

#ifdef __ASSEMBLER__

#else

/**
 * canary check
 */

#define CANARY_MAGIC 0xCAC0DAD0B0B01234ULL
struct canary {
    uint64_t volatile c0;
    uint64_t volatile c1;
};

static inline void canary_init(struct canary *c)
{
    c->c0 = (uint64_t) c;
    c->c1 = (c->c0 - 1) ^ CANARY_MAGIC;
}

static inline int canary_check(struct canary *c)
{
    return c->c0 == (uint64_t) c && c->c1 == ((c->c0 - 1) ^ CANARY_MAGIC);
}


/**
 * async circular queues
 */

struct async_queue {
    volatile uint32_t read;
    uint32_t mask;
    volatile uint32_t write;
    volatile spinlock_t lock;
    volatile uint32_t data[];
};

extern void async_queue_init(struct async_queue *aq, int size);
extern bool async_queue_read(struct async_queue *aq, int lockit, uint32_t *data);
extern bool async_queue_write(struct async_queue *aq, int lockit, uint32_t data);
extern bool async_queue_can_read(struct async_queue *aq);
extern bool async_queue_can_write(struct async_queue *aq);



/* returns index of the last bit that is ONE or -1 */
static int util_last_bit1(uint64_t val)
{
    uint64_t out;
    __asm__ volatile("clz %0, %1\n" : "=r"(out) : "r"(val));
    return 63 - out;
}

/* return index of first set bit or 64 if none found */
static int util_first_bit1(uint64_t val)
{
    uint64_t rev, out;
    __asm__ volatile("rbit %0, %1\n" : "=r"(rev) : "r"(val));
    __asm__ volatile("clz %0, %1\n" : "=r"(out) : "r"(rev));
    return out;
}



/* is n a power of two ? */
static inline int util_is_pot(uint64_t n)
{
    return (n != 0) && (n & (n-1)) == 0;
}


/* standard memory operations */

extern void __memcpy64(adr_t dst, adr_t src, size_t size64);
extern void __memset64(adr_t dst, uint64_t val, size_t size64);

#endif /* !__ASSEMBLER__ */


#endif /* __UTIL_H__ */
