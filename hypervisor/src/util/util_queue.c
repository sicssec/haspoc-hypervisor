
#include "defs.h"

#include "cman.h"
#include "cman_private.h"

// #include "mman.h"
// #include "debug.h"

void async_queue_init(struct async_queue *aq, int size)
{
    if(! util_is_pot(size))
        fatal("queue size is not power of 2");

    aq->read = aq->write = 0;
    aq->mask = size - 1;
    spinlock_init(&aq->lock, 0);
}

/**
 * @type function
 * @name async_queue_can_read
 * @description see if we can read from this queue. We dont lock it
 * since between can_read() and read() it wouldn't be atomic anyway
 */
bool async_queue_can_read(struct async_queue *aq)
{
    return aq->read != aq->write;
}

/**
 * @type function
 * @name async_queue_can_write
 * @description see if we can write to this queue. Not we dont lock it
 * since between can_read() and read() it wouldn't be atomic anyway
 */
bool async_queue_can_write(struct async_queue *aq)
{
    uint32_t write_next = (aq->write + 1) & aq->mask;
    return aq->read != write_next;
}

bool async_queue_read(struct async_queue *aq, int lockit, uint32_t *data)
{
    bool ret;

    if(lockit)
        spinlock_lock(&aq->lock);

    if(async_queue_can_read(aq)) {
        *data = aq->data[aq->read];
        aq->read = (aq->read + 1) & aq->mask;
        ret = true;
    } else {
        ret = false;
    }

    if(lockit)
        spinlock_unlock(&aq->lock);

    return ret;
}

bool async_queue_write(struct async_queue *aq, int lockit, uint32_t data)
{
    bool ret;

    if(lockit)
        spinlock_lock(&aq->lock);

    if(async_queue_can_write(aq)) {
        aq->data[aq->write] = data;
        aq->write = (aq->write + 1) & aq->mask;
        ret = true;
    } else {
        ret = false;
    }

    if(lockit)
        spinlock_unlock(&aq->lock);

    return ret;
}
