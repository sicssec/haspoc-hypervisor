#include "defs.h"
#include "cman.h"


    .text
    .section .boot, "ax"
    .align 3

    .global __boot_primary

    /*
     * hypervisor boot starts here. We assume that only primary CPU enters
     * at this point and that X0 points to payload. This may however not be
     * true on all platforms so we do some quick checks in the beginning,
     *
     * to be on the safe side, we save entry registers (first on stack and
     * later into pcpu->entry_hyp) but until we get there we will use X10-X11
     * as temporary registers
     */

__boot_primary:
    /* disable all interrupts ! */
    msr DAIFSet, #15

#if defined (NO_PSCI)
    /*
     * In certain platforms, PSCI is not supported and all CPU wakeup/entrys
     * will go through this entry address. A check is done to see if STH has
     * been initialized to skip init and current cpu context is saved and
     * continues to __boot_core with the correct pcpu
    */

#define STH_INIT_TOKEN 0x987abc781

    ldr x10, =STH_INIT_TOKEN
    ldr x11, =sth_initialized
    ldr x11, [x11]
    cmp x11, x10
    beq __boot_core

    /* set it for now, will repeat this after data setup */
    ldr x10, =STH_INIT_TOKEN
    ldr x11, =sth_initialized
    str x10, [x11]
#endif



    /* check this is the primary CPU */
    mrs x10, mpidr_el1
    and x10, x10, #0x03
    cbnz x10, __die

    /* set boot stack and save payload parameters */
    ldr x10, =__ebstack
    mov sp, x10

    /* now save our entry registers to stack. x10 will point to the regs */
    SAVE_REGISTERS 2
    mov x10, sp

    /*
     * EL2 boot sanity check:
     * performs a series of sanity checks to see if it matches the expected hw
     */

    /* check this is EL2 */
    mrs x2, currentel
    ubfx x2, x2, #2, #2
    cmp w2, #2
    bne __die

    /* check if this the correct hardware? */
    mrs x2, MIDR_EL1
    ldr x3, =ARCH_MIDR_VALUE
    eor x2, x2, x3
    bic x2, x2, #ARCH_MIDR_MASK
    cbnz x2, __die

    /* executing from the correct position? */
    adr x2, __shrom
    ldr x3, =HYP_ROM_START
    cmp x2, x3
    bne __die


    /*
     * early CPU initialization
     */

     /* SCTLR_EL2: EL2 system top level control
      *
      * Bits not shown are reseved (RES0/RES1)
      *--------------------------------------------------
      * 25 EE: Endianness for EL2 stage 1 and EL0/1 stage 2 translation table walks
      * 19 WXN: EL2 memory region write permission implies XN (Execute Never)
      * 12 I: EL2 instruction access cacheability control
      *  3 SA: EL2 load/store instruction stack alignment check enable
      *  2 C: EL2 normal memory data caching control
      *  1 A: EL2 instruction address argument alignment checking
      *  0 M: MMU enable for EL2 stage 1 address translation
      *--------------------------------------------------
      * stack align, I-cache, dont force W^X. little endian
      * [25]=0, [19]=1, [12]=1, [3]=1, [2]=1, [1]=0, [0]=0
      * Other bits are set according to RES0/RES1
      */

    ldr x2, =0x30CD187C
    msr sctlr_el2, x2

    /* Set exception vector */
    adr x2, el2_vectors
    msr vbar_el2, x2


    /*
     * we don't likes caches. wipe them all before we go any further
     */
    bl __cache_flush_dcache
    bl __cache_inv_icache


    /*
     * DATA and BSS setup
     */

    /* copy data from ROM to RAM */
    ldr x0, =__sdata_rom
    ldr x1, =__sdata
    ldr x2, =__edata
1:
    cmp x1, x2
    bge 2f
    ldp x3, x4, [x0], #16
    stp x3, x4, [x1], #16
    b 1b
2:

    /* zero BSS */
    ldr x0, =__sbss
    ldr x1, =__ebss
1:
    cmp x0, x1
    bge 2f
    stp xzr, xzr, [x0], #16
    b 1b
2:


#if defined(NO_PSCI)
    /* re-set init token */
    ldr x10, =STH_INIT_TOKEN
    ldr x11, =sth_initialized
    str x10, [x11]
#endif

    /* check if stack is balanced */
    ldr x0, =__ebstack
    sub x0, x0, #REGISTERS_SIZE
    cmp sp, x0
    bne __die

    /* EARLY PRIMARY INIT:
     * start with boot registers as paramaters */
    mov x20, x0
    bl boot_init_primary_early


    /* now we can use our pcpu and its new stack */
    mrs x0, mpidr_el1
    bl __mpidr_to_cpuid
    ldr x1, =pcpus
    ldr x0, [x1, x0, lsl #3]
    ldr x0, [x0, #0]

    /* push what we have on the old stack onto this one before we switch */
    ldr x1, =__ebstack
    mov x2, sp
1:
    ldr w3, [x1, #-4]!
    str w3, [x0, #-4]!
    cmp x1, x2
    bne 1b

    /* and now we can use the real stack */
    mov sp, x0

    /* LATE PRIMARY INIT */
    mov x20, x0
    bl boot_init_primary_late


    /* primary initialization is done and we can now start
     * core initialization, but first lets restore the registers
     * as they were during entry
     */

    RESTORE_REGISTERS 2
    b __boot_core



    .data
    .balign 8

#if defined (NO_PSCI)
sth_initialized:
    /* this will be set to STH_INIT_TOKEN when primary has been initialized
     * at boot we start with a dummy value that is not 0 */
    .dword 0x11223344
#endif
