/*
 * high-level EL2 functionaility (mostly setup code & interrupts)
 */

#include "defs.h"

#include "init.h"
#include "init_private.h"

#include "mman.h"
#include "util.h"
#include "gman.h"
#include "igc.h"

#include <minilib.h>

// TEMP fix for UART0 collision
#ifdef TARGET_hikey
void wait(void)
{
    int i, j;
	int wait_loop0 = 1000000;
    for(i = 0; i < 6; i++)
    {
        for(j = 0; j < wait_loop0; j++)
		{   // waste function, volatile makes sure it is not being optimized out by compiler
			int volatile t = 120 * j * i + j;
			t = t + 5;
		}
    }
}
#endif

static void boot_awake_used_cpus()
{
    struct pcpu *pcpu;
    struct vcpu *vcpu;
    struct guest *guest;
    msg_t msg;
    int me, i;

    me =  mc_whoami();

    /* if any of the sleeping CPUs are needed, bring them up */
    for(i = 0; i < ARCH_CORES; i++) {
        pcpu = pcpus[i];
        guest = pcpu->guest;
        vcpu = pcpu->vcpu;

        /* no need to wake a cpu without a guest */
        if(!guest )
            continue;

        /* sanity checks */
        assert_not_null("no vcpu assigned", pcpu->vcpu);


        spinlock_lock(&pcpu->lock);
        if(pcpu->state != PCPU_STATE_READY)
            fatal("CPU disabled or already running");

        /* for cpus started from here entry points are in the guest config */
        cman_vstate_set(vcpu, guest->entry.x0, guest->entry.x1, guest->entry.pc);

#ifndef NO_PSCI
        /* no need to wake ourselves */
        if(i != me){
            dprintf("Awakening sleeping CPU %d\n", i);
            msg.msg32[0] = PSCI_CMD_CPU_ON;
            msg.msg32[1] = pcpu->pid;

            if(!services_message("pm", &msg, true, true))
                fatal("unable to awak secondary cpu %d\n", pcpu->pid);
        }
#endif

        spinlock_unlock(&pcpu->lock);
    }
}


void boot_guest(struct pcpu *pcpu)
{
    int i;
    adr_t vadr, padr;
    struct guest *guest;
    struct vcpu *vcpu;
    struct vstate *vstate;
    struct registers gp;

    guest = pcpu->guest;
    vcpu = pcpu->vcpu;

    if(!guest || !vcpu)
        fatal("No guest to run on this CPU");

    debug_print(LVL2, "core boot is starting '%s'....\n", guest->name);
    vstate = &vcpu->vstate;

    /*
     * fill guest vstate with some sane initial values
     */
    vstate->sctlr_el1 = MRS64("SCTLR_EL1") & ~0x1005; /* turn off EL1 mmu and caches */


    cman_vstate_restore(guest, vcpu, &gp);

    /* check it, just to be safe */
    vadr = vcpu->vstate.gp.pc;
    padr = __mmu_translate_s12(vadr);
    if( padr & 1) {
        dprintf("ERROR: guest entry VADR=%x is invalid (%x)!\n",vadr, padr);
        fatal("cannot start guest");
    } else {
        padr = (padr & ~0xFFF) | (vadr & 0xFFF);
        uint32_t *mem = (uint32_t *)padr;
        debug_print(LVL2, "\tGuest entry VADR=%x PADR=%x Memory=%x %x\n",
               vadr, padr, mem[0], mem[1]);
    }

// TEMP fix for UART0 collision
#ifdef TARGET_hikey
	char *linux = "linux 1";
	// first condition, we are linux 1
	// second condition, this is core 0 - which for now is booting guest1
	if((strcmp(guest->name, linux) == 0) && (mc_whoami() == 0)) {
		dprintf("Delaying guest1 boot...\n");
		wait();
	}

#endif
// END TEMP fix


    /* some debug stuff before we start the guest */
    debug_print(LVL2, "Starting guest %s on CPU %d at %x\n", guest->name, mc_whoami(), vadr);
    debug_print(LVL2, "MIDR_EL1=%x MPIDR_EL1=%x HCR_EL2=%X\n",
            MRS64("midr_el1"), MRS64("mpidr_el1"), MRS64("HCR_EL2"));

    /* will not return from this. it will also change the EL2 stack */
    __boot_guest(pcpu->stack, &gp);
}


/*
 * core initialization, this is done on every core that comes up
 */

void boot_init_core(struct pcpu *pcpu)
{
    /* core boot sanity check */
    if(pcpu != cman_pcpu_get() || pcpu != cman_pcpu_get_safe())
        fatal("PCPU not correctly set, core stack is probably wrong");

    if(pcpu->state != PCPU_STATE_READY)
        fatal("CPU disabled or already running");

    pcpu->state = PCPU_STATE_RUNNING;

    debug_print(LVL2, "core boot: PCPU=%X STACK=%x\n", pcpu, pcpu->stack);


#if DEBUG > 1
    dprintf("Core entry registers:\n");
    debug_dump_registers(&pcpu->entry_hyp);
#endif


    /*
     * init
     */
    init_target_core(INIT_START, pcpu); /* target init before we start */

	debug_core_init();

    cman_init(false, pcpu);
    runtime_init(false, pcpu);

    init_target_core(INIT_HAS_INT, pcpu); /* target init before we start */
    init_target_core(INIT_END, pcpu); /* target init before we start */

    /*
     * start guest
     */
    boot_guest(pcpu);
}


/*
 * primary initialization, this is done on the primary cpu.
 * it is divided into two parts: one on temporary stack (early)
 * and one on the pcpu stack. the reason for this is that
 * we want to switch stack as soon as possible so cman_pcpu_get()
 * works
 */
void boot_init_primary_early(struct registers *regs)
{
    adr_t payload_start;

    payload_start = regs->x[0];

    /*
     * primary init
     */
    init_target_primary(INIT_START, regs);
    debug_init_early();
    config_init_early(payload_start);

    mman_init_early();
    cman_init_early();

}

void boot_init_primary_late(struct registers *regs)
{

    struct pcpu *pcpu = cman_pcpu_get();

    /* some sanity checks first */
    assert_eq("late init: pcpu is correct", cman_pcpu_get_safe(), pcpu);

    cman_init(true, pcpu);

    runtime_init(true, pcpu);
    init_target_primary(INIT_HAS_INT, regs);

    gman_init();
    init_target_primary(INIT_END, regs);

	perf_timestamp("Time at guest1 launch");

    /*
     * start other cores
     */
    boot_awake_used_cpus();
}
