#ifndef __INIT_PRIVATE_H__
#define __INIT_PRIVATE_H__

#include "init.h"

#ifdef __ASSEMBLER__

#else /* ! __ASSEMBLER__ */

struct registers;

/*
 * target specfic init.
 * you can override these by providing your own in target/$TARGET/
 */

enum target_init_state { INIT_START, INIT_HAS_INT, INIT_END };

/*
 * very early init called from assembler, for very early hw initialization.
 *
 * regs contains the primary entry registers
 *
 * IMPORTANT: At this point you have stack but not even .data and .bss are
 *            valid so you wont't be able to use any variables etc.
 */
extern void __init_target_early(struct registers *regs);

/*
 * init called during later stages of boot
 *
 * NOTE that pcpu is NULL if global is non-zero!
 */
extern void init_target_primary(int state, struct registers *);
extern void init_target_core(int state, struct pcpu *);



extern void __boot_guest_restore_context(uint64_t *el2_stack,
                                         struct registers* regs );
extern void __boot_guest(uint64_t *el2_stack, struct registers* entry);


#endif /* ! __ASSEMBLER__ */


#endif /* __INIT_PRIVATE_H__ */
