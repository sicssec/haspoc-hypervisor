#ifndef __INIT_H__
#define __INIT_H__


#include "defs.h"

#include "cman.h"
#include "armv8.h"

#ifdef __ASSEMBLER__

#else /* ! __ASSEMBLER__ */



/*
 * boot entry point, you won't be calling this
 */

extern void __boot_primary();
extern void __boot_core();

extern void __ebstack;

/*
 * boot init, called from assembler
 */
extern void boot_init_primary(struct registers *regs);
extern void boot_init_core(struct pcpu *pcpu);

/*
 * guest start
 */

extern void boot_guest(struct pcpu *pcpu);


#endif /* ! __ASSEMBLER__ */


#endif /* __INIT_H__ */
