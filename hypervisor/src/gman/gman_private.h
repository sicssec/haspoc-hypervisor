
#ifndef __GMAN_PRIVATE_H__
#define __GMAN_PRIVATE_H__

extern void gman_guest_add(struct guest *g);

extern void gman_drivers_setup(struct guest *g, struct dt_block *config);

extern void gman_mem_guest_init(struct guest *g, struct dt_block *data);
extern void gman_mem_init(int count);

extern void gman_igc_init();


/*
 * shared memory
 */
#define GMAN_SHMEM_FLAG_RW 0
#define GMAN_SHMEM_FLAG_RO 1

extern void gman_shmem_init();

#endif /* !__GMAN_PRIVATE_H__ */
