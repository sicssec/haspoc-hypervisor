
#include "defs.h"
#include "util.h"
#include "cman.h"
#include "mman.h"

#include "gman.h"
#include "gman_private.h"


/* guest memory regions */

struct guest_mem *gman_mem_find_overlaping(struct guest *g, adr_t adr,
                                           size_t size, bool phy)
{
    adr_t start, end;
    int i;
    for(i = 0; i < g->mem_cnt; i++) {
        start = phy ? g->mem[i].padr : g->mem[i].ipadr;
        end = start + g->mem[i].size;

        if(regions_overlap(start, end, adr, adr + size))
            return &g->mem[i];
    }
    return NULL;
}

struct guest_mem *gman_mem_find_containing(struct guest *g, adr_t adr,
                                           size_t size, bool phy)
{
    adr_t start, end;
    int i;
    for(i = 0; i < g->mem_cnt; i++) {
        start = phy ? g->mem[i].padr : g->mem[i].ipadr;
        end = start + g->mem[i].size;

        if(region_inside(start, end, adr, adr + size))
            return &g->mem[i];
    }
    return NULL;
}


bool gman_ipadr_to_padr(struct guest *g, adr_t ipadr, size_t size, adr_t *padr)
{
    struct guest_mem *mem = gman_mem_find_containing(g, ipadr, size, false);
    if(mem) {
        *padr = mem->padr - mem->ipadr + ipadr;
        return true;
    }
    return false;
}

bool gman_padr_to_ipadr(struct guest *g, adr_t padr, size_t size, adr_t *ipadr)
{
    struct guest_mem *mem = gman_mem_find_containing(g, padr, size, true);
    if(mem) {
        *ipadr = mem->ipadr - mem->padr + padr;
        return true;
    }
    return false;
}


/*
 * guest page table operations
 */

static struct pagetable * gman_mem_alloc_pagetable(struct guest *g)
{
    adr_t adr;

    if(!mman_memory_get(MEMORY_HV, (adr_t *) &adr, PAGE_SIZE, PAGE_SIZE))
        fatal("Could not allocate guest pagetable");

    return (struct pagetable *) adr;
}

static uint64_t *gman_get_l1_entry(struct guest *g, adr_t ipadr, bool alloc_l2)
{
    uint64_t l1, type;
    l1 = mman_get_pt_index(1, ipadr);
    type = g->l1->entry[l1] & ENTRY_TYPE_MASK;

    /* do we insert an L2 table here first? */
    if(alloc_l2 && (type == ENTRY_TYPE_INVALID1 || type == ENTRY_TYPE_INVALID2))
        g->l1->entry[l1] = ATTR_S2_TABLE | (adr_t) gman_mem_alloc_pagetable(g);

    return &g->l1->entry[l1];
}

static uint64_t *gman_get_l2_entry(struct guest *g, adr_t ipadr, bool alloc_l3)
{
    uint64_t l2, *ent1, *ent2, type;
    struct pagetable *l2_pt;

    ent1 = gman_get_l1_entry(g, ipadr, true);
    assert_eq("2MB entry already mapped in L1",
              ENTRY_TYPE_TABLE_L12,  *ent1 & ENTRY_TYPE_MASK);

    l2 = mman_get_pt_index(2, ipadr);
    l2_pt = (struct pagetable *) (*ent1 & NL_TABLE_ADDR_MASK);


    type = l2_pt->entry[l2] & ENTRY_TYPE_MASK;

    /* do we insert an L2 table here first? */
    if(alloc_l3 && (type == ENTRY_TYPE_INVALID1 || type == ENTRY_TYPE_INVALID2))
        l2_pt->entry[l2] = ATTR_S2_TABLE | (adr_t) gman_mem_alloc_pagetable(g);

    return  &l2_pt->entry[l2];
}


static uint64_t *gman_get_l3_entry(struct guest *g, adr_t ipadr)
{
    uint64_t l3, *ent2, type;
    struct pagetable *l3_pt;

    l3 = mman_get_pt_index(3, ipadr);

    /* see if we have a L2 table, if not create one */
    ent2 = gman_get_l2_entry(g, ipadr, true);
    l3_pt = (struct pagetable *) (*ent2 & NL_TABLE_ADDR_MASK);
    type =  (*ent2 & ENTRY_TYPE_MASK);

    assert_neq("L3 table already mapped as L2 block", type, ENTRY_TYPE_BLOCK_L12);

    return  &l3_pt->entry[l3];
}

static void gman_mem_map_4k_one(struct guest *g, adr_t ipadr, adr_t padr, uint64_t attr)
{
    uint64_t type, *ent3;

    ent3 = gman_get_l3_entry(g, ipadr);
    type = *ent3 & ENTRY_TYPE_MASK;

    if(type != ENTRY_TYPE_INVALID1 && type != ENTRY_TYPE_INVALID2)
        fatal("4K entry %x already mapped in L3\n", ipadr);

    *ent3 = padr | attr;
}


static void gman_mem_map_2m_one(struct guest *g, adr_t ipadr, adr_t padr,
                         uint64_t attr)
{
    uint64_t type, *ent2;
    ent2 = gman_get_l2_entry(g, ipadr, false);
    type = *ent2 & ENTRY_TYPE_MASK;

    assert_neq("2M entry already mapped", type, ENTRY_TYPE_BLOCK_L12);
    assert_neq("2M entry already mapped to 4K", type, ENTRY_TYPE_TABLE_L12);

    *ent2 = padr | attr;
}


void gman_mem_map_4k(struct guest *g, adr_t ipadr, adr_t padr, uint64_t attr,
    int pages)
{
    assert_eq("4K padr or ipadr must be 4K aligned", (ipadr | padr) & (PAGE_SIZE-1), 0);

    attr = (attr & ~(ENTRY_TYPE_MASK)) | ENTRY_TYPE_PAGE_L3;

    while(pages--) {
        gman_mem_map_4k_one(g, ipadr, padr, attr);
        ipadr += PAGE_SIZE;
        padr += PAGE_SIZE;
    }
}



void gman_mem_map_2m(struct guest *g, adr_t ipadr, adr_t padr,
                     uint64_t attr, int count)
{
    assert_eq("2M padr or ipadr must be 2M aligned", (ipadr | padr) & (2 * MB - 1), 0);

    attr = (attr & ~(ENTRY_TYPE_MASK)) | ENTRY_TYPE_BLOCK_L12;

    while(count--) {
        gman_mem_map_2m_one(g, ipadr, padr, attr);
        ipadr += 2 * MB;
        padr += 2 * MB;
    }
}

/*
 * map a number of bytes as 4K or 2M pages
 */
void gman_mem_map(struct guest *g, adr_t ipadr, adr_t padr,
                  uint64_t attr, size_t bytes)
{
    uint64_t attr4k, attr2m;
    assert_eq("memory to map & size must be 4K aligned",
              (bytes | ipadr | padr) & (PAGE_SIZE - 1), 0);

    attr2m = (attr & ~(ENTRY_TYPE_MASK)) | ENTRY_TYPE_BLOCK_L12;
    attr4k = (attr & ~(ENTRY_TYPE_MASK)) | ENTRY_TYPE_PAGE_L3;

    /* map any 4K pages before 2M blocks */
    while(bytes > 0 && (ipadr | padr) & (2 * MB - 1)) {
        gman_mem_map_4k_one(g, ipadr, padr, attr4k);
        ipadr += PAGE_SIZE;
        padr += PAGE_SIZE;
        bytes -= PAGE_SIZE;
    }

    /* map 2MB pages as long as we can */
    while(bytes >= 2 * MB ) {
        gman_mem_map_2m_one(g, ipadr, padr, attr2m);
        ipadr += 2 * MB;
        padr += 2 * MB;
        bytes -= 2 * MB;
    }

    /* map the remaining 4K pages at the end */
    while(bytes > 0 ) {
        gman_mem_map_4k_one(g, ipadr, padr, attr4k);
        ipadr += PAGE_SIZE;
        padr += PAGE_SIZE;
        bytes -= PAGE_SIZE;
    }
}


/*
 * guest MMU related operations
 */


/* setup guest RAM in MMU */
static void gman_mem_guest_ram_map(struct guest *g)
{
    uint64_t attr, l1, l2;
    adr_t padr, ipadr;
    size_t size;
    int i, k;

    /* Set RAM sharing depending on guest cluster usage */
    if(g->vcpu_count < 1) {
        attr = ATTR_SH_NS; /* one cpu: not shareable */
    } else if(g->cluster_count < 1) {
        attr = ATTR_SH_IS; /* multiple cpus, one cluster: Inner shareable */
    } else {
        attr = ATTR_SH_OS; /* multiple cpus, multiple clusters: Outer shareable */
    }

#if DEBUG > 1
    dprintf("\tmemory sharing (SH) flags: %X\n", attr);
#endif

    attr |= ATTR_S2_BP_MEM;
    for(k = 0; k < g->mem_cnt; k++) {
        padr = g->mem[k].padr;
        ipadr = g->mem[k].ipadr;
        size = g->mem[k].size;

#if DEBUG > 0
        dprintf("\tMAP ram_%d %x -> %X-%X \n", k, ipadr, padr, padr + size);
#endif
        gman_mem_map(g, ipadr, padr, attr, size);
    }
}

/* register one ram region */
static void gman_mem_guest_ram_add(struct guest *g, uint32_t bank,
                                   adr_t ipadr, size_t size, uint32_t flags)
{
    adr_t padr, align;
    int c = g->mem_cnt;

    /* space & sanity check */
    assert_lt("Too many memory regions", c, GUEST_MEM_REGIONS);
    assert_eq("Bad memory alignment", 0, (ipadr | size) &  (PAGE_SIZE - 1));

    /* get memory with the best alignment */
    align = ((ipadr | size) & (2 * MB - 1))  ? 4 * KB : 2 * MB;
    if(!mman_memory_get(bank, & padr, size, align))
            fatal("Could not allocate guest memory for %s", g->name);

    g->mem[c].ipadr = ipadr;
    g->mem[c].padr = padr;
    g->mem[c].size = size;
    g->mem[c].flags = flags;
    g->mem_cnt = c + 1;

    dprintf("\tRAM_%d bank=%x flags=%x ipadr=%X padr=%x size=%x\n",
            c, bank, flags, ipadr, padr, size);
}

/* parse ram config and allocate memory */
static void gman_mem_guest_ram_init(struct guest *g, struct dt_block *config)
{
    struct dt_block prop;
    adr_t ipadr;
    size_t size, size2;
    int i, k, bank;
    uint32_t flags, *mem;

    /* guest memory configuration */
    config_get_prop(config, &prop, "ram", 0);
    k = prop.data_len;
    mem = prop.data.num;

    assert_eq("Guest RAM format error", k % (4 * 6), 0);
    k /= 4 * 6;

    g->mem_cnt = 0;
    for(i = 0; i < k; i++) {
        bank = dtend(mem[0]);
        flags = dtend(mem[1]);
        ipadr = ((uint64_t) dtend(mem[2]) << 32) | dtend(mem[3]);
        size = ((uint64_t) dtend(mem[4]) << 32) | dtend(mem[5]);
        mem += 6;

        /* memory configuration sanity check */
        if(mman_bank_type(bank) != MEMORY_GUEST)
            fatal("guest RAM type is not a valid guest memory");

        assert_eq("guest memory start and size should be 4KB aligned",
                  (ipadr | size) & (PAGE_SIZE -1), 0);

        assert("memory regions overlap",
               !gman_overlaps_virt_region(g, ipadr, size));

        /* check that the region is either 4KB or 2MB */
        if( ((ipadr | size) & (2 * MB - 1)) && size >= 2 * MB)
            fatal("guest RAM regions should be divided into 4KB and 2MB parts: %x:+%x.",
                  ipadr, size);

        gman_mem_guest_ram_add(g, bank, ipadr, size, flags);
    }
}




void gman_mem_guest_init(struct guest *g, struct dt_block *config)
{
    gman_mem_guest_ram_init(g, config);
    gman_mem_guest_ram_map(g);
}




/*
 * allocate all guest structures and their L1 in one go
 */

void gman_mem_init(int count)
{
    int i;
    size_t gsize1, lsize1, size;
    adr_t gpadr, lpadr;
    struct guest *g;

    /* allocate guests and L1 */
    gsize1 = ALIGN_UP(sizeof(struct guest), 64);
    lsize1 = 2 * PAGE_SIZE;
    size = (gsize1 + lsize1) * count;

    if(!mman_memory_get(MEMORY_HV, (adr_t *) &lpadr, size, lsize1))
        fatal("Could not allocate guest memory");

    gpadr = lpadr + 2 * PAGE_SIZE * count;

    dprintf("Allocating %d guests at %x. size=%d + %d\n",
            count, lpadr, gsize1, lsize1);

    for(i = 0; i < count; i++) {
        g =  (struct guest *) gpadr;
        g->vmid = i + 1; /* starts at 1 */
        g->l1 = (struct pagetable *)lpadr;

        dprintf("\tguest %d at %x, L1 at %x\n", g->vmid, g, g->l1);
        gman_guest_add(g);

        gpadr += gsize1;
        lpadr += lsize1;
    }
}
