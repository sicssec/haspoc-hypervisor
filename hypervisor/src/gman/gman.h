
#ifndef __GMAN_H__
#define __GMAN_H__

/**
 * @name guest manager
 * @type module
 * @description the guest manager module handles the guests and their data
 */

#include "runtime.h"
#include "vgic.h"
#include "cman.h"
#include "target.h"
#include "util.h"

/* max number of guests we will ever have */
#define MAX_GUEST 6


/*
 * define some of the guest virtual regions
 */

#ifdef TARGET_s810
#define GUEST_MAP_BASE_IGC (4ULL * GB + 2ULL * MB)
#else
#define GUEST_MAP_BASE_IGC (3ULL * GB + 2ULL * MB)
#endif

/*
 * defines a single region of guest memory.
 * we guarantee this to be mapped to a continuous physical region
 */
struct guest_mem {
    adr_t padr, ipadr;
    size_t size;
    uint32_t flags;
};


/*
 * This structure should contain all private data we have for a guest
 */
struct guest {
    /*
     * memory configuration: memory regions, RAM
     */
#define GUEST_MEM_REGIONS 6
    uint32_t mem_cnt;
    struct guest_mem mem[GUEST_MEM_REGIONS];

    /*
     * page tables and related stuff:
     */
    struct pagetable *l1; /* root */

    /* index of the first available entry in l3_igc  */
    uint32_t l3_igc_pos;


    /* guest flags */
#define GUEST_FLAG_ALLOW_EE _BV(0)
#define GUEST_FLAG_ALLOW_POWER _BV(1)
#define GUEST_FLAG_SERVICE _BV(2)
    uint32_t flags;

    /* original configuration start, may be used by platform code */
    struct dt_block config;

    /* lock */
    spinlock_t lock;

    /* CPU mask for total and active virtual/physical CPUs */
    uint32_t cpumask_pcpu;
    uint32_t cpumask_vcpu;
    uint32_t cpumask_pcpu_running;
    uint32_t cpumask_vcpu_running;

    /* worst case, which & how many clusters are involved in? */
    uint8_t cluster_membership;
    uint8_t cluster_count;

    /* vcpus */
    uint32_t vcpu_count;
    struct vcpu vcpus[GUEST_VCPU_MAX];

    /* vgic driver and global part if the virtual interrupts */
    struct vgic_driver vgic_driver;

    /* entry data */
    struct {
        uint64_t pc;
        uint64_t x0;
        uint64_t x1;
    } entry;

    /* driver list */
#define GUEST_MDRIVERS_COUNT 6
    int mdrivers_cnt;
    struct mdriver mdrivers[GUEST_MDRIVERS_COUNT];

    /* arch stuff */
    struct arch_guest arch;

    /* identification */
    char *name;
    uint32_t vmid;
};



/* guests */

extern int gman_guest_count();
extern struct guest *gman_guest_get(int index);
extern struct guest *gman_guest_find(const char *name);

/* mdrivers */
extern void gman_mdriver_dump(struct guest *g);
extern struct mdriver *gman_mdriver_find(struct guest *g, adr_t adr);
extern struct mdriver *gman_mdriver_register(struct guest *g,
                                             struct dt_block *config,
                                             char *name, adr_t vstart,
                                             adr_t vend, adr_t padr);


/* memory */
extern struct guest_mem *gman_mem_find_overlaping(struct guest *g, adr_t adr, size_t size, bool phy);
extern struct guest_mem *gman_mem_find_containing(struct guest *g, adr_t adr, size_t size, bool phy);

static inline bool gman_owns_phy_region(struct guest *g, adr_t padr, size_t size)
{
    return gman_mem_find_containing(g, padr, size, true) != NULL;
}

static inline bool gman_owns_virt_region(struct guest *g, adr_t ipadr, size_t size)
{
    return gman_mem_find_containing(g, ipadr, size, false) != NULL;
}

static inline bool gman_overlaps_virt_region(struct guest *g, adr_t ipadr, size_t size)
{
    return gman_mem_find_overlaping(g, ipadr, size, false) != NULL;
}

extern bool gman_ipadr_to_padr(struct guest *g, adr_t ipadr, size_t size, adr_t *padr);
extern bool gman_padr_to_ipadr(struct guest *g, adr_t padr, size_t size, adr_t *ipadr);

extern void gman_igc_alloc(struct guest *g, int count, int *index);

extern void gman_mem_map_4k(struct guest *g, adr_t ipadr, adr_t padr,
                            uint64_t attr, int count);
extern void gman_mem_map_2m(struct guest *g, adr_t ipadr, adr_t padr,
                            uint64_t attr, int count);
extern void gman_mem_map(struct guest *g, adr_t ipadr, adr_t padr,
                         uint64_t attr, size_t bytes);

extern void gman_device_map(struct guest *g, adr_t ipadr, adr_t padr, int pages);


/* core operations */
extern struct pcpu *gman_guest_add_core(struct guest *g, int vid);

static inline struct vcpu *gman_vcpu_get(struct guest *g, int vid)
{
    return (vid < 0 || vid >= GUEST_VCPU_MAX) ? NULL : &g->vcpus[vid];
}

/* guest setup */
extern void gman_init();


#endif /* !__GMAN_H__ */
