
/*
 * guest IGC configuration
 */

#include "defs.h"
#include "util.h"

#include "gman.h"
#include "gman_private.h"

#include <minilib.h>
#include <devicetree.h>
#include <bundle.h>

#include "igc.h"

/* get free IGC pages from this guest */
void gman_igc_alloc(struct guest *g, int count, int *index)
{
    int ret = g->l3_igc_pos;
    assert_lt("out of IGC pages", ret + count, MMAN_PT_ENTRIES);

    g->l3_igc_pos += count;
    *index = ret;
}


static void gman_igc_setup(struct dt_block *channel)
{
    int i;
    struct igc_channel *chan;
    struct dt_block prop, nep;

    char *id;
    struct guest *guests[2];
    int pages[2];
    int irqs[2];

    id = config_get_prop_str(channel, "id");

    /* read endpoint information */
    for(i = 0; i < 2; i++) {
        config_get_node(channel, &nep, (i ? "endpoint2" : "endpoint1"));
        guests[i] = gman_guest_find(config_get_prop_str(&nep, "guest"));
        pages[i] = config_get_prop_int(&nep, "pages");
        irqs[i] = config_get_prop_int(&nep, "irq");
    }

    igc_register_channel(id, guests, pages, irqs);
}

/*
 * guest IGC initialization
 */
void gman_igc_init()
{
    struct dt_block igc, *tmp;
    struct dt_foreach fe;


    /* setup guest IGC channels */
    if(!config_try_get(NULL, &igc, 1, "igc", 0) ) {
        dprintf("No IGC information found!\n");
    } else {
#if DEBUG > 0
    dprintf("IGC configuration:\n");
#endif
        config_foreach_node(&igc, &fe);
        while( (tmp = dt_foreach_next_of(&fe, "channel@"))) {
            gman_igc_setup(tmp);
        }
    }
}
