
#include "defs.h"
#include "util.h"
#include "cman.h"
#include "mman.h"

#include "gman.h"
#include "gman_private.h"

/* guest structures */
static struct guest *guests[MAX_GUEST];
static int guest_cnt;


/* guest list */

void gman_guest_add(struct guest *g)
{
    assert_lt("Too many guests added", guest_cnt, MAX_GUEST);
    guests[guest_cnt++] = g;
}

struct guest *gman_guest_get(int index)
{
    return index >= 0 && index < guest_cnt ? guests[index] : 0;
}

int gman_guest_count()
{
    return guest_cnt;
}

struct guest *gman_guest_find(const char *name)
{
    int i;
    for(i = 0; i < guest_cnt; i++)
        if(!strcmp(guests[i]->name, name))
            return guests[i];
    return NULL;
}

static int gman_guest_vcpu_new(struct guest *g)
{
    int i;
    uint32_t mask;

    mask = g->cpumask_vcpu & ~ g->cpumask_vcpu_running;
    for(i = 0; i < GUEST_VCPU_MAX; i++) {
        if(mask & (1UL << i)) {
            return i;
        }
    }
    return -1;
}


struct pcpu *gman_guest_add_core(struct guest *g, int vid)
{
    struct pcpu *pcpu;
    struct vcpu *vcpu;

    spinlock_lock(& g->lock);
    pcpu = NULL;

    /* allocate new vcpu if none was given */
    if(vid < 0)
        vid = gman_guest_vcpu_new(g);

    vcpu = gman_vcpu_get(g, vid);
    if(vcpu) {
        pcpu = cman_pcpu_allocate(g, vcpu);
        if(pcpu) {
            cman_pcpu_guest_register(pcpu, g, vcpu);
        }
    }

    spinlock_unlock(& g->lock);
    return pcpu;
}
