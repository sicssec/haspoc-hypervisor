
#include "defs.h"
#include "util.h"
#include "cman.h"
#include "mman.h"

#include "gman.h"
#include "gman_private.h"

#include <minilib.h>
#include <devicetree.h>
#include <bundle.h>

#include "runtime.h"


void gman_mdriver_dump(struct guest *g)
{
    int i;
    struct mdriver *it;

    dprintf("\tmdrivers for guest are:\n");
    for(i = 0; i < g->mdrivers_cnt; i++) {
        it = &g->mdrivers[i];
        dprintf("\t   %s at %x vadr=%x-%x padr=%x\n",
                it->module->name, it,
                it->vstart, it->vend, it->padr);
    }
}

struct mdriver *gman_mdriver_find(struct guest *g, adr_t adr)
{
    int n = g->mdrivers_cnt;
    struct mdriver *it = &g->mdrivers[0];

    while(n--) {
        if(it->vstart <= adr && adr < it->vend)
            return it;
        it++;
    }

    return NULL;
}


struct mdriver *gman_mdriver_register(struct guest *g, struct dt_block *config, char *name,
                                      adr_t vstart, adr_t vend, adr_t padr)
{
    struct mdriver *drv, *template;

    /* TODO: verify this vadr raneg doesn't collied with anything else */

    template = mdrivers_find(name);
    assert_not_null("driver type not found", template);

    /* get a copy of this driver for this guest */
    assert_lt("Too many guest drivers", g->mdrivers_cnt, GUEST_MDRIVERS_COUNT);
    drv = & g->mdrivers[g->mdrivers_cnt++];
    *drv = *template;

    drv->vstart = vstart;
    drv->vend = vend;
    drv->padr = padr;

    /* let driver initialize itself */
    if(drv->ops_guest_init)
        drv->ops_guest_init(drv, g, config);

#if DEBUG > 3
    dprintf("Registered driver %s at %x-%x\n", name, vstart, vend);
#endif

    return drv;
}

void gman_drivers_setup(struct guest *g, struct dt_block *config)
{
    struct dt_foreach fe;
    struct dt_block *node;
    char *name;
    adr_t vstart, vend, padr;
    uint32_t *data;
    int datacnt;
    bool ret;


    g->mdrivers_cnt = 0;

    config_foreach_node(config, &fe);
    while( (node = dt_foreach_next_of(&fe, "driver@"))) {
        /* find the module we use as template of this driver */
        name = config_get_prop_str(node, "type");

        /* get addresses */
        ret =  config_get_array_try(node, "vadr", sizeof(uint32_t),
                                    (void **)&data, &datacnt);
        assert("Bad or missing driver vadr", ret && (datacnt == 2 || datacnt == 4));

        vstart = config_get_data(&data, datacnt == 4);
        vend = config_get_data(&data, datacnt == 4);
        assert_lt("driver vstart < vend", vstart, vend);

        /* might also have a padr */
        ret =  config_get_array_try(node, "padr", sizeof(uint32_t),
                                    (void **)&data, &datacnt);
        if(ret) {
            assert("Bad driver padr", datacnt == 1 || datacnt == 2);
            padr = config_get_data(&data, datacnt == 2);
        } else {
            padr = -1; /* invalid */
        }

        gman_mdriver_register(g, node, name, vstart, vend, padr);
    }
}
