
#include "defs.h"
#include "util.h"
#include "cman.h"
#include "mman.h"

#include "gman.h"
#include "gman_private.h"

#include <minilib.h>
#include <devicetree.h>
#include <bundle.h>

#include "runtime.h"
#include "igc.h"



/* setup */
static void gman_cpu_setup(struct guest *g, uint32_t cpumask)
{
    int i, count = 0;

    /* how many physical cpus can we use ? */
    for(i = 0; i < ARCH_CORES; i++)
        if(cpumask & (1UL << i))
            count ++;

    /* sanity checks */
    if(cpumask > (1UL << ARCH_CORES))
        fatal("guest cpu mask uses physical cores not available");

    if(count > GUEST_VCPU_MAX)
        fatal("guest cpu mask uses virtual cores not available");

    for(i = 0; i < GUEST_VCPU_MAX; i++) {
        vcpu_init( &g->vcpus[i], g, i);
    }

    g->cpumask_pcpu = cpumask;
    g->cpumask_vcpu = (1UL << count) - 1;
    g->cpumask_pcpu_running = 0;
    g->cpumask_vcpu_running = 0;
    g->vcpu_count= count;

    /* compute our cluster membership and count */
    g->cluster_membership = 0;
    for(i = 0; i < ARCH_CORES; i++) {
        if(cpumask & (1 << i))
            g->cluster_membership |= 1 << cman_pcpu_get_by_cpuid(i)->cluster_num;
    }

    g->cluster_count = 0;
    for(i = 0; i < ARCH_CLUSTER_COUNT; i++) {
        if( g->cluster_membership & (1 << i))
            g->cluster_count++;
    }

    dprintf("\tpcpu mask %x => vcpu %x/%d, cluster%x/%d\n",
            g->cpumask_pcpu,
            g->cpumask_vcpu, g->vcpu_count,
            g->cluster_membership, g->cluster_count);
}

void gman_device_map(struct guest *g, adr_t ipadr, adr_t padr, int pages)
{
    int i, index;
    uint64_t attr = ATTR_S2_PAGE_DEV;

#if DEBUG > 0
    dprintf("\tdevice %X -> %X (%d pages)\n", ipadr, padr, pages);
#endif

    /* sanity check: address alignment */
    assert_eq("device ipadr/padr page aligned", 0, (ipadr | padr) & (PAGE_SIZE-1));

    /* map as 4KB page */
    gman_mem_map_4k(g, ipadr, padr, ATTR_S2_PAGE_DEV, pages);
}

static void gman_devices_setup(struct guest *g, struct dt_block *data)
{
    int pages;
    adr_t ipadr, padr;
    struct dt_foreach fe;
    struct dt_block *node;

    config_foreach_node(data, &fe);
    while( (node = dt_foreach_next_of(&fe, "device@"))) {
        ipadr = config_get_prop_adr(node, "vadr");
        padr = config_get_prop_adr(node, "padr");
        pages = config_get_prop_int(node, "pages");

        gman_device_map(g, ipadr, padr, pages);
    }
}


static void gman_firmware_setup(struct guest *g, struct dt_block *data)
{
    struct dt_foreach fe;
    struct dt_block *node;
    struct bundle_data *bd;
    char *filename;
    adr_t oadr, ipadr, padr;
    bool ok;

    config_foreach_node(data, &fe);
    while((node = dt_foreach_next_of(&fe, "firmware@"))) {
        ipadr = config_get_prop_int(node, "adr");
        filename = config_get_prop_str(node, "filename");
        if(!( bd = config_find_bundle(filename, &oadr)))
            fatal("Could not find firmware '%s'", filename);

        if(!gman_owns_virt_region(g, ipadr, bd->size))
            fatal("Guest firmware outside its memory: %s", g->name);

        ok = gman_ipadr_to_padr(g, ipadr, bd->size, &padr);
        assert("Guest firmware outside its memory", ok);
        dprintf("\tfw %X: ipadr=%x:+%x %s\n", padr, ipadr, bd->size, filename);

        __memcpy64(padr, oadr, bd->size);
    }
}


static void gman_setup(struct guest *g, struct dt_block *data)
{
    struct dt_block prop, *tmp;
    uint64_t attr;
    adr_t padr, ipadr;
    size_t size;
    int i, k, type;
    uint32_t *mem;

    dprintf("GUEST %d at %x\n", g->vmid, g);

    spinlock_init(& g->lock, 0);

    /* base guest information */
    g->name = config_get_prop_str(data, "id"); /* cannot use "name" in devicetree */
    g->flags = config_get_prop_int(data, "flags");
    dprintf("\tname=%s flags=%x at %X\n", g->name, g->flags, g);

    /* save configuration data for later use */
    g->config = *data;

    /* CPU configuration */
    gman_cpu_setup(g, config_get_prop_int(data, "cpumask"));


    /* setup guest ram, pagetables, devices, gic and firmware */
    gman_mem_guest_init(g, data);
    gman_drivers_setup(g, data);
    gman_devices_setup(g, data);
    gman_firmware_setup(g, data);

    /* guest register entry */
    config_get_prop(data, &prop, "entry", 4 * 3);
    g->entry.pc = dtend(prop.data.num[0]);
    g->entry.x0 = dtend(prop.data.num[1]);
    g->entry.x1 = dtend(prop.data.num[2]);


    /* check if entry is within guest memory */
    if(!gman_owns_virt_region(g, g->entry.pc, 8))
        fatal("Guest entry is outside its memory space");
}


/*
 * guest initialization
 */
void gman_init()
{
    int i, count;
    struct dt_block igc, *tmp;
    struct dt_foreach fe;
    struct guest *g;


    /* how many guests will we have ? */
    count = config_count(NULL, "guest@");
    assert_range("Bad number of guests in config", count, 1, MAX_GUEST -1);

    gman_mem_init(count);


    /* read guest configuration and setup guests */
    config_foreach_node(0, &fe);
    for(i = 0; tmp = dt_foreach_next_of(&fe, "guest@"); i++) {
        g = gman_guest_get(i);
        gman_setup(g, tmp);
    }


    /* with all guests in place, setup guest IGC and shared memories */
    gman_igc_init();
    gman_shmem_init();


    /* allocate initial cores for guests */
    count = gman_guest_count();
    for(i = 0; i < count; i++) {
        g = gman_guest_get(i);

        /* Do not allocate cores to service flag guests*/
        if (g->flags & GUEST_FLAG_SERVICE) {
            dprintf("Service guest: %s will not have cores allocated\n",
                    g->name);
        } else {
            if (gman_guest_add_core(g, -1) == NULL)
                fatal("Unable to allocate guests first core");
        }
    }
}
