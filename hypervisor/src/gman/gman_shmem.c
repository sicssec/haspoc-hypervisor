
/*
 * Guest shared memory
 *
 */

#include "defs.h"
#include "util.h"
#include "cman.h"
#include "mman.h"

#include "gman.h"
#include "gman_private.h"

#include <minilib.h>
#include <devicetree.h>
#include <bundle.h>


static void gman_shmem_setup_guest(struct dt_block *config, struct guest *owner,
                                   adr_t padr, size_t size)
{
    uint64_t attr;
    uint32_t flags;
    adr_t vadr;
    char *gname;
    struct guest *g;

    gname = config_get_prop_str(config, "guest");
    vadr = config_get_prop_adr(config, "vadr");
    flags = config_get_prop_int(config, "flags");
    g = gman_guest_find(gname);
    assert_not_null("Unknown guest in shmem", g);

    if(g == owner) {
        /* double check flags */
        assert_eq("Bad shmem flag: memory owner must be R/W",
                  0, flags & 1);

#if DEBUG > 1
        dprintf("\tmemory %X is owned by %s\n",
                vadr, g->name);
#endif
    } else {
        /* compute mapping attributes */
        attr = (flags & 1) == GMAN_SHMEM_FLAG_RO ?
            ATTR_SHARED_READ : ATTR_SHARED_READ_WRITE;
        gman_mem_map(g, vadr, padr, attr, size);

#if DEBUG > 1
        dprintf("\tmemory %X (attr=%x) mapped into %s\n",
                vadr, attr, g->name);
#endif
    }
}

/*
 * the memory shared is supposed to be mapped into exactly one
 *
 */
static void gman_shmem_find_owner(struct dt_block *shmem, size_t size,
                                  adr_t *padr_, struct guest **owner_)
{
    struct dt_block *tmp;
    struct dt_foreach fe;

    struct guest *g, *owner;
    adr_t ipadr, padr, flags;
    char *gname;

    /* see what guest has this ipadr region mapped */
    owner = NULL;

    config_foreach_node(shmem, &fe);
    while( (tmp = dt_foreach_next_of(&fe, "guest@"))) {
        gname = config_get_prop_str(tmp, "guest");
        ipadr = config_get_prop_adr(tmp, "vadr");
        g = gman_guest_find(gname);
        assert_not_null("Unknown guest", g);

        /* if this ipadr range belongs to this guest, record it + its padr */
        if(gman_owns_virt_region(g, ipadr, size)) {
            assert_null("Shared memory should be inside exactly one guests RAM", owner);
            owner = g;
            gman_ipadr_to_padr(g, ipadr, size, &padr);
        }
    }

    assert_not_null("No guest owns the shmem region", owner);
    *padr_ = padr;
    *owner_ = owner;
}


static void gman_shmem_setup_block(struct dt_block *shmem)
{
    struct dt_block *tmp;
    struct dt_foreach fe;
    size_t size;
    adr_t padr;

    struct guest *owner;

    size = config_get_prop_int(shmem, "size");

    /* figure out who owns this memory and get its padr */
    gman_shmem_find_owner(shmem, size, &padr, &owner);


#if DEBUG > 0
    dprintf("[shmem] padr=%x size=%x owner=%s\n", padr, size, owner->name);
#endif

    /* map memory */
    config_foreach_node(shmem, &fe);
    while( (tmp = dt_foreach_next_of(&fe, "guest@"))) {
        gman_shmem_setup_guest(tmp, owner, padr, size);
    }
}


/*
 * guest shared memory initialization
 */
void gman_shmem_init()
{
    struct dt_block *shmem;
    struct dt_foreach fe;

    config_foreach_node(NULL, &fe);
    while( (shmem = dt_foreach_next_of(&fe, "shmem@"))) {
        gman_shmem_setup_block(shmem);
    }
}
