/*
 * FVP target init
 */

#include "defs.h"
#include "init.h"
#include "init_private.h"

#include "util.h"
#include "uart.h"

#define UART_CLK 7257600
#define UART_BAUDRATE 115200

void init_target_primary(int state, struct registers *regs)
{
    uint32_t clkdiv_16_16;

    if(state == INIT_START) {
        /* C code begins, set up UART so we can have printf() */
        clkdiv_16_16 = uart_calc_clkdiv(UART_CLK, UART_BAUDRATE);
        uart_init(ARCH_UART0_BASE, clkdiv_16_16);
        uart_init(ARCH_UART1_BASE, clkdiv_16_16);
        uart_init(ARCH_UART2_BASE, clkdiv_16_16);
    }

#if DEBUG > 3
    dprintf("FVP init state %d\n", state);
#endif
}


void init_target_core(int state, struct pcpu *pcpu)
{

#if DEBUG > 3
    dprintf("FVP core init state %d\n", state);
#endif
}
