/*
 * Juno target init
 */
#include "defs.h"

#include "init.h"
#include "init_private.h"
#include "gman.h"
#include "debug.h"

void hvc_s810_driver_init(void);
void smc_fwd_driver_init(void);

static int optee_started = 0;

static void boot_optee(struct pcpu *pcpu)
{
    /*Before we switch to Android guest, boot to OPTEE and initialize
     *OPTEE Will do a hypercall when its finished and continue boot Android
     */
    cman_vstate_store(pcpu->guest, &pcpu->guest->vcpus[0], &pcpu->entry_hyp);
    pcpu->guest = gman_guest_find("OPTEE");
    pcpu->guest->vcpus[pcpu->pid].vstate.gp.pc = pcpu->guest->entry.pc;
    pcpu->vcpu = &pcpu->guest->vcpus[pcpu->pid];
    boot_guest(pcpu);
}

static void setup_guest_on_all_secondary_cpus()
{
    int i;
    for(i = 1; i < ARCH_CORES; i++) {
        printf("Init target ARCH CORES: %d\n", i);
        /*For now, all CPU run same guest as CPU0*/
        gman_guest_add_core(pcpus[0]->guest, i);
    }
}

/*Configure s810 specific cpu behaviour */
static void setup_s810_cpu()
{
    /*Configure Virtualization control HCR_EL2*/
    union reg_hcr _hcr_el2;
    _hcr_el2.all = MRS64("hcr_el2");
    _hcr_el2.twe   = 0;     // Dont trap WFE
    _hcr_el2.twi   = 0;     // Dont trap WFI
    _hcr_el2.amo   = 0;     // External aborts are not taken to EL2
    _hcr_el2.imo   = 1;     // IRQs are taken to EL2
    MSR64("hcr_el2", _hcr_el2.all);

    // Timer configuration
    MSR64("CNTHCTL_EL2", 0x3);	// Do not trap EL1 access to timer registers
}

void init_target_primary(int state, struct registers *regs)
{

    switch(state) {
    case INIT_END:
        hvc_s810_driver_init();
        smc_fwd_driver_init();
        setup_guest_on_all_secondary_cpus();
        break;
    }

    debug_print(LVL3, "s810 init state %d\n", state);
}

void init_target_core(int state, struct pcpu *pcpu)
{

    switch(state) {
    case INIT_END:
        setup_s810_cpu();
        /*Added here because of assert in boot_init_core.
         *SMC is not trapped so we dont change the state when cpu sleeps */
        if (PCPU_STATE_RUNNING == pcpu->state)
            pcpu->state = PCPU_STATE_READY;
        /* set the guest register for this platform */
        if(pcpu->vcpu) {
            /* Restore all registers from entry_hyp with pc set to x0  */
            pcpu->entry_hyp.pc = pcpu->entry_hyp.x[0];
            cman_vstate_store(pcpu->guest, &pcpu->guest->vcpus[pcpu->pid],
                              &pcpu->entry_hyp);
        }
        if (!optee_started) {
            optee_started = 1;
            boot_optee(pcpu);
        }

        break;
    }
    debug_print(LVL2, "s810 core init state %d\n", state);
}
