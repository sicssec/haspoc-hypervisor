#ifndef __TARGET_H__
#define __TARGET_H__

#include "runtime.h"

#ifdef __ASSEMBLER__

#else /* __ASSEMBLER__ */


struct mdriver_hi6220_gpio {
#define HI6220_GPIO_COUNT 20
    spinlock_t lock;
    int count;
    struct {
        uint16_t irq;
        uint8_t claimed_pins;
        uint8_t gpiomis;
    } port[HI6220_GPIO_COUNT];
    uint32_t claimed_ports;
};


struct arch_guest {
    struct mdriver ao_ctrl;
    struct mdriver_hi6220_gpio gpio;
};

struct arch_vcpu {
};

struct arch_pcpu {
};

#endif /* __ASSEMBLER__ */

#endif /* __TARGET_H__ */
