/*
 * HiKey ao_ctrl dummy driver
 */

#include "defs.h"
#include "init.h"

#include "cman.h"
#include "gman.h"

#include "target.h"
#include "target_private.h"

#include "drivers.h"


static void hi6220_aoctrl_guest_init(struct mdriver *obj, struct guest *g,
                                     struct dt_block *config)
{
    obj->padr = HI6220_AOCTRL_BASE;
}

static bool mdriver_hi6220_aoctrl_read(struct mdriver *drv, union reg_esr esr,
                                       adr_t ipadr, uint64_t *val)
{
    uint64_t padr = drv->padr - drv->vstart + ipadr;
    return mdriver_memory_read(padr, val, -1, esr);

}

static bool mdriver_hi6220_aoctrl_write(struct mdriver *drv, union reg_esr esr,
                                        adr_t ipadr, uint64_t val)
{
    uint64_t padr = drv->padr - drv->vstart + ipadr;
    return mdriver_memory_write(padr, val, -1, esr);
}

static struct mdriver hi6220_aoctrl = {
    .mask_read = MASK_WORD32 | MASK_BYTE8,
    .mask_write = MASK_WORD32 | MASK_BYTE8,

    .ops_guest_init = hi6220_aoctrl_guest_init,
    .ops_guest_read = mdriver_hi6220_aoctrl_read,
    .ops_guest_write = mdriver_hi6220_aoctrl_write,
};

DRIVER_DECLARE("hi6220_aoctrl", _hi6220_aoctrl, &hi6220_aoctrl);
