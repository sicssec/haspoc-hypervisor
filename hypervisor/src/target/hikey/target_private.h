#ifndef __TARGET_PRIVATE_H__
#define __TARGET_PRIVATE_H__

#include "runtime.h"
#include "target.h"

#define HI6220_AOCTRL_BASE 0xf7800000

#ifdef __ASSEMBLER__

#else /* __ASSEMBLER__ */

extern void driver_hi6220_aoctrl_init_primary();

#endif /* __ASSEMBLER__ */

#endif /* __TARGET_PRIVATE_H__ */
