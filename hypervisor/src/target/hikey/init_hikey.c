/*
 * HiKey target init
 */

#include "defs.h"
#include "cman.h"
#include "gman.h"

#include "init.h"
#include "init_private.h"

#include "uart.h"

#include "target.h"
#include "target_private.h"


/* Pagetable setup specific to hikey */
extern struct pagetable el2_s1_l1;

/* System global (shared by all cores) EL2 stage 1 level 2 page table. */
struct pagetable el2_s1_l2;

static void hikey_setup_el2_mmu()
{
    /* Set up EL2 stage 1 target specific MMU device memory map.
     * Hikey device base is indexed from l1[3]. In l2, device range is
     * from index 416 to 459. See DEVICE_BASE and DEVICE_SIZE.
     */
    int l2_index;
    adr_t oa_l2;
    uint64_t mem_attrs;
    uint64_t mair_el2_dev;

    /* Config MAIR_EL2 also for the device memory.
     *--------------------------------------------------
     * n = 1
     * [7:4] = 0000 Device memory
     * [3:0] = 0000 Device-nGnRnE memory
     */
    mair_el2_dev = 0x00ULL;
    MSR64("MAIR_EL2", MRS64("MAIR_EL2") | mair_el2_dev);

    for (l2_index = 0; l2_index < MMAN_PT_ENTRIES; l2_index++) {
        oa_l2 = (adr_t)l2_index << (12 + 9); /* Identity translation block OA*/

        if (l2_index >= 416 && l2_index <= 459) {
            /* Device memory */
            mem_attrs = ATTR_EL23_S1_BP_DEV;
        }
        else {
            /* Normal memory */
            mem_attrs = ATTR_EL23_S1_BP_MEM;
        }

        mmu_pt_add_map(&el2_s1_l2, l2_index, oa_l2,
                       mem_attrs | ATTR_SH_OS | ENTRY_TYPE_BLOCK_L12, MEM_OA_L2_ADDR_MASK);
    }
    /* Map l2 page table */
    mmu_pt_attach_table(&el2_s1_l1, 3, &el2_s1_l2);
}


void init_target_primary(int state, struct registers *regs)
{
    uint32_t clkdiv;
    switch(state) {
    case INIT_START:

#define UART_CLK 19200000
#define UART_RATE 115200
        clkdiv = uart_calc_clkdiv(UART_CLK, UART_RATE);

        uart_init(ARCH_UART0_BASE, clkdiv);
        uart_init(ARCH_UART2_BASE, clkdiv);
        uart_init(ARCH_UART3_BASE, clkdiv);
        break;

    case INIT_HAS_INT:
        hikey_setup_el2_mmu();
        break;

    case INIT_END:
        /* Currently the SMMU breaks graphics in android */
#ifndef NO_SMMU
        smmu_setup();
#endif
        break;
    }

#if DEBUG > 3
    dprintf("HiKey init state %d\n", state);
#endif
}


void init_target_core(int state, struct pcpu *pcpu)
{

#if DEBUG > 3
    dprintf("HiKey core init state %d\n", state);
#endif
}
