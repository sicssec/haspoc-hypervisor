/*
 * HiKey shared gpio driver
 */

#include "defs.h"
#include "cman.h"
#include "gman.h"

#include "init.h"

#include "target.h"
#include "target_private.h"
#include "drivers.h"

/* PL061 GPIO registers.
 * note that this are 32-bit array indices, hence the /4
 */
#define GPIODAT_00 ( 0x000 / 4)
#define GPIODAT_FF ( 0x3FC / 4)
#define GPIODIR ( 0x400 / 4)
#define GPIOIS  ( 0x404 / 4)
#define GPIOIBE ( 0x408 / 4)
#define GPIOIEV ( 0x40C / 4)
#define GPIOIE  ( 0x410 / 4)
#define GPIORIS ( 0x414 / 4)
#define GPIOMIS ( 0x418 / 4)
#define GPIOIC  ( 0x41C / 4)
#define GPIOAFSEL ( 0x420 / 4)

#define GPIOPID_0 ( 0xFE0 / 4)
#define GPIOPID_4 ( 0xFEC / 4)
#define GPIOCID_0 ( 0xFF0 / 4)
#define GPIOCID_4 ( 0xFFC / 4)


/* pid for the first gpio. it expands linearly from here */
#define HI6220_IRQ_GPIO0 84

/*
 * contains information about a single port
 * and guests sharing it
 */
struct pl061_port {
    spinlock_t lock;
    uint16_t pid;
    uint8_t claimed_pins;

    /* the port itself */
    volatile uint32_t *base;

    /* connected guests */
    uint8_t guest_count;
    uint16_t guest_irq[MAX_GUEST];
    struct guest *guest[MAX_GUEST];
};

/*
 * this is the global part of gpio driver.
 * it contains things that are not tied to a specific guest
 */
static struct hi6220_gpio {
    uint32_t claimed_ports;
    struct pl061_port port[HI6220_GPIO_COUNT];
} gdrv;

/*
 * given a pid, what port does this belong to?
 */
static int hi6220_gpio_pid2portnum(int pid)
{
    pid -= HI6220_IRQ_GPIO0;
    return (pid < 0 || pid >= HI6220_GPIO_COUNT) ? -1 : pid;
}

static int hi6220_gpio_portnum2pid(int portnum)
{
    return portnum + HI6220_IRQ_GPIO0;
}

/*
 * mdriver
 */
static bool mdriver_hi6220_gpio_read(struct mdriver *drv, union reg_esr esr,
                                     adr_t ipadr, uint64_t *val_)
{
    const uint64_t page = (ipadr - drv->vstart) / PAGE_SIZE;
    uint32_t mask, val, index;
    volatile uint32_t *pbase;
    struct mdriver_hi6220_gpio *ldrv;
    struct pl061_port *gport;

    index = (ipadr - drv->vstart) & (PAGE_SIZE - 1);

    /* sanity check */
    if(index & 3)
        goto fail0;

    ldrv = (struct mdriver_hi6220_gpio *) drv->user2;
    if(page < 0 || page >= ldrv->count)
        goto fail0;

    gport = &gdrv.port[page];
    pbase = gport->base;
    mask = ldrv->port[page].claimed_pins;
    index /= 4;
    val = 0;

    /* more sanity checks */
    assert_not_null("pbase sanity", (void *)pbase);
    assert_not_null("gport sanity", (void *)gport);

    spinlock_lock(&gport->lock);

    /* NOTE: all access is now 32-bit regardless of what guest requested */
    switch(index) {
        /* bit-banded area */
    case GPIODAT_00  ... GPIODAT_FF:
        val = pbase[index & mask] & mask;
        break;

        /* read out copy of MIS */
    case GPIOMIS:
        val = ldrv->port[page].gpiomis;
        break;

        /* read with mask added */
    case GPIODIR:
    case GPIOIS:
    case GPIOIBE:
    case GPIOIEV:
    case GPIOIE:
    case GPIORIS:
    case GPIOIC:
    case GPIOAFSEL:
        val = pbase[index] & mask;
        break;

        /* read with no mask */
    case GPIOPID_0 ... GPIOPID_4:
    case GPIOCID_0 ... GPIOCID_4:
        val = pbase[index];
        break;

    default:
        goto fail1;
    }

succ:
    spinlock_unlock(&gport->lock);

    /* mask value to correct size */
    val &= (0x100 << (esr.sas * 8)) -1;
    *val_ = val;
    return true;


fail1:
    spinlock_lock(&gport->lock);
fail0:
    return false;
}

static bool mdriver_hi6220_gpio_write(struct mdriver *drv, union reg_esr esr,
                                      adr_t ipadr, uint64_t val)
{
    const uint64_t page = (ipadr - drv->vstart) / PAGE_SIZE;
    uint32_t mask, index;
    volatile uint32_t *pbase;
    struct mdriver_hi6220_gpio *ldrv;
    struct pl061_port *gport;

    index = (ipadr - drv->vstart) & (PAGE_SIZE - 1);

    /* sanity check */
    if(index & 3)
        goto fail0;

    ldrv = (struct mdriver_hi6220_gpio *) drv->user2;

    /* do the driver cover this gpio page? */
    if(page < 0 || page >= ldrv->count)
        goto fail0;

    gport = &gdrv.port[page];
    pbase = gport->base;
    mask = ldrv->port[page].claimed_pins;
    index /= 4;
    val &= (0x100 << (esr.sas * 8)) -1; /* mask value to correct size */

    /* sanity checks */
    assert_not_null("pbase sanity", (void *)pbase);
    assert_not_null("gport sanity", (void *)gport);

    spinlock_lock(& gport->lock);

    /* NOTE: all access is now 32-bit regardless of what guest requested */
    switch(index) {
        /* bit-banded area */
    case GPIODAT_00 ... GPIODAT_FF:
        pbase[index & mask] = val & mask; /* 2nd mask not really needed... */
        break;

        /* clear only our copy of the gpiomis */
    case GPIOIC :
        ldrv->port[page].gpiomis &= ~(val & mask);
        break;

        /* write-back with mask */
    case GPIODIR:
    case GPIOIS:
    case GPIOIBE:
    case GPIOIEV:
    case GPIOIE:
    case GPIOAFSEL:
        pbase[index] = (pbase[index] & ~mask) | (val & mask);
        break;

    default:
        goto fail1;
    }

succ:
    spinlock_unlock(& gport->lock);
    return true;

fail1:
    spinlock_unlock(& gport->lock);
fail0:
    return false;
}

/*
 * GPIO interrupts
 */

static void idriver_hi6220_gpio(struct pcpu *pcpu, struct registers *r, uint32_t iar)
{
    uint32_t i, j, pid, vid, status;
    int portnum;
    struct pl061_port *gport;
    struct mdriver_hi6220_gpio *ldrv;
    struct guest *g;
    volatile uint32_t *base;


    /* what pid was this and was it really an interrupt? */
    pid = iar & 1023;
    if(pid >= 1022)
        return;

    /* get the port conncted to this pid */
    portnum = hi6220_gpio_pid2portnum(pid);
    assert("portnum sanity", portnum != -1);

    gport = &gdrv.port[portnum];
    base = gport->base;

    spinlock_lock(& gport->lock);

    /* what interrupts has this port active and who receives them? */
    status = base[ GPIOMIS ];
    for(i = 0; i < 8; i++) {
        if(status & (1UL << i)) {

            /* interrupt for pin "i" was asserted, who had claimed that? */
            for(j = 0; j < gport->guest_count; j++) {
                g = gport->guest[j];
                ldrv = & g->arch.gpio;
                if(ldrv->port[portnum].claimed_pins & (1UL << i)) {

                    /* guest "g" was the owner, let him know about the irq */
                    vid = gport->guest_irq[j];
                    if(vgic_send_virq_to_guest(pcpu, g, vid)) {

                        /* let driver remeber this guest saw this pin interrupt  */
                        spinlock_lock(&ldrv->lock);
                        ldrv->port[portnum].gpiomis |= 1 << i;
                        spinlock_unlock(&ldrv->lock);
                    } else {
                        fatal("Internal: could not send virq to guest");
                    }
                }
            }
        }
    }

    /* clear the interrupt signals now */
    base[GPIOIC ] = status;

    spinlock_unlock(& gport->lock);

#if DEBUG > 3
    dprintf("[HI6220] idriver: pid=%d portnum=%d status=%x\n",
            pid, portnum, status);
#endif

}

/*
 * update interrupt configuration based on what the guests have configured
 */
void hi6220_gpio_update_pirq(struct pl061_port *port)
{
    int i, vid;
    struct virq *virq;
    struct guest *g;
    uint32_t enable, target, priority, config;

    enable = target = 0;
    priority = 0xFF;

    spinlock_lock(&port->lock);
    config = gicv2_dist_get_config(_gicd, virq->pid); /* start with last good config */

    for(i = 0; i < port->guest_count; i++) {
        g = port->guest[i];
        vid = port->guest_irq[i];
        virq = vgic_virq_get(g , vid );
        if(!virq || !virq->gicd_enabled)
            continue;

        enable = 1;
        target |= cpu_vmask_to_pmask(g, virq->gicd_cpumask);
        config = virq->gicd_cfg; /* XXX: this will only take the last guest into account */
        if(priority > virq->gicd_prio)
            priority = virq->gicd_prio;
    }

    /* update the pirq */
    /* XXX: here we should lock the gic too ? */
    gicv2_dist_set_target(_gicd, port->pid, target);
    gicv2_dist_set_enable(_gicd, port->pid, enable);
    gicv2_dist_set_priority(_gicd, port->pid, priority);
    gicv2_dist_set_config(_gicd, port->pid, config);
    spinlock_unlock(&port->lock);


#if DEBUG > 3
    dprintf("[HI6220] virq notifier pid=%d -> enable=%d, mask=%x cfg=%d prio=%d\n",
            port->pid, enable, target, config, priority);
#endif
}

/*
 * this function is called when OS changes a virtual interrupt that
 * we have connected to our physical interrupt.
 */
static void idriver_hi6220_gpio_notifier(struct guest *g, struct virq *virq,
                                         void *userdata,
                                         uint32_t reg, uint32_t val)
{
    switch(reg) {
    case 0x200 ... 0x27C: /* ISPENDR */
    case 0x280 ... 0x2FC: /* ICPENDR */
    case 0x300 ... 0x37C: /* ISACTIVER */
    case 0x380 ... 0x3FC: /* ICACTIVER */
        /* no need to update configuration for these */
        break;
    default:
        hi6220_gpio_update_pirq((struct pl061_port *) userdata);
    }
}





/*
 * driver initialization
 */

/* record that a guest is claiming parts of a port */
static void hi6220_gpio_guest_record_claim(struct guest *g, int port,
                                           int irq, uint32_t mask)
{
    int pid;
    struct virq *virq;
    struct pl061_port *gport;
    uint32_t old_port_mask, old_pin_mask;

    /* nothing is claimed */
    if(!mask)
        return;

    gport = &gdrv.port[port];

    /* see if it has been claimed by someone else! */
    assert_eq("Hi6220 GPIO ownership overlap", 0, gport->claimed_pins & mask);

    /* record port and pin claim */
    old_pin_mask = gport->claimed_pins;
    old_port_mask = gdrv.claimed_ports;
    gport->claimed_pins |= mask;
    gdrv.claimed_ports |= _BV(port);

    /* record the guest */
    gport->guest[gport->guest_count] = g;
    gport->guest_irq[gport->guest_count] = irq;
    gport->guest_count++;

    /* this port is used for the first time, lets active its interrupt */
    if(old_port_mask != gdrv.claimed_ports) {

        /* let gic know we are handling this irq */
        pid = hi6220_gpio_portnum2pid(port);
        gicv2_dist_set_enable(_gicd, pid, 0);
        gicv2_dist_set_priority(_gicd, pid, VIRQ_PRIO_DEFAULT_INIT);
        gic_pirq_callback_set(NULL, pid, idriver_hi6220_gpio);

#if DEBUG > 1
        dprintf("\t GPIO_%d, pirq %d pbase=%x:\n", port, pid, gport->base);
#endif
    }

    /* let vgic know we want to be notified if guest changes things */
    virq = vgic_virq_get(g, irq);

    assert_not_null("gpio virq invalid?", virq);
    assert("gpio virq not virtual?", virq->virt);
    assert_eq("gpio virq not delcared", virq->state, VIRQ_STATE_READY);
    vgic_virq_notifier_set(g, virq, idriver_hi6220_gpio_notifier, gport);

#if DEBUG > 1
    dprintf("\t\t%s\tvirq=%d notifier=%x\n",
            g->name, irq, idriver_hi6220_gpio_notifier);
#endif
}


static void hi6220_gpio_guest_init(struct mdriver *obj, struct guest *g,
                                   struct dt_block *config)
{
    int i, cnt, tmp;
    uint32_t *data;
    struct mdriver_hi6220_gpio *ldrv;
    struct pl061_port *gport;

    ldrv = &g->arch.gpio;
    ldrv->claimed_ports = 0;
    spinlock_init(&ldrv->lock, 0);

    obj->user2 = ldrv;

    /* PARSE irq: get the corresponding irq descriptors */
    if(!config_get_array_try(config, "irqs", sizeof(uint32_t), (void *)&data, &cnt))
        fatal("hi6220 driver has no irqs");

    assert_eq("hi6220 gpio vadr size doesn't match port count",
              cnt, (obj->vend - obj->vstart) / PAGE_SIZE);
    assert_range("hi6220 gpio list bad length", cnt, 0, HI6220_GPIO_COUNT-1);

    ldrv->count = cnt;
    for(i = 0; i < cnt; i++) {
        ldrv->port[i].irq = dtend(data[i]);
        assert_lt("gpio virq must be an SPI", 31, ldrv->port[i].irq);
    }

    /* PARSE mask: get the mask for the gpio pages we are considering */
    if(!config_get_array_try(config, "mask", sizeof(uint32_t),  (void *)&data, &tmp))
        fatal("hi6220 driver has no mask");

    assert_eq("hi6220 gpio mask size matches irq size", cnt, tmp);

    for(i = 0; i < cnt; i++) {
        gport = &gdrv.port[i];
        ldrv->port[i].claimed_pins = dtend(data[i]);
        if(ldrv->port[i].claimed_pins) {
            ldrv->claimed_ports |= _BV(i);
            hi6220_gpio_guest_record_claim(g, i, ldrv->port[i].irq,
                                           ldrv->port[i].claimed_pins);
        }
    }


#if DEBUG > 1
            dprintf("\tgi6220 gpio driver: ports=%x", ldrv->claimed_ports);
            for(i = 0; i < ldrv->count; i++)
                dprintf("_ %x:%d", ldrv->port[i].claimed_pins, ldrv->port[i].irq);
            dprintf("_\n");
#endif
}

static void hi6220_gpio_init(struct mdriver *m, bool global, struct pcpu *pcpu)
{
    int i;
    struct pl061_port *gport;

    const uint32_t HI6220_GPIO_BASES[] = {
        0xF8011000, 0xF8012000, 0xF8013000, 0xF8014000,
        0xF7020000, 0xF7021000, 0xF7022000, 0xF7023000,
        0xF7024000, 0xF7025000, 0xF7026000, 0xF7027000,
        0xF7028000, 0xF7029000, 0xF702A000, 0xF702B000,
        0xF702C000, 0xF702D000, 0xF702E000, 0xF702F000
    };

    if(global) {
        dprintf("[HI6220] install gpio driver...\n");

        /* initialize the gdrv structure */
        memset(& gdrv, 0, sizeof(gdrv));
        for(i = 0; i < HI6220_GPIO_COUNT; i++) {
            gport = &gdrv.port[i];
            gport->base = (uint32_t *) (adr_t) HI6220_GPIO_BASES[i];
            gport->pid = hi6220_gpio_portnum2pid(i);
            spinlock_init(&gport->lock, 0);
        }
    }
}

static struct mdriver hi6220_gpio = {
    .mask_read = MASK_WORD32 | MASK_BYTE8,
    .mask_write = MASK_WORD32 | MASK_BYTE8,

    .ops_init = hi6220_gpio_init,
    .ops_guest_init = hi6220_gpio_guest_init,
    .ops_guest_read = mdriver_hi6220_gpio_read,
    .ops_guest_write = mdriver_hi6220_gpio_write,
};

DRIVER_DECLARE("hi6220_gpio", _hi6220_gpio, &hi6220_gpio);
