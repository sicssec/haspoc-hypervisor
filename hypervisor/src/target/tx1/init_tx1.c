/*
 * TX1 target init
 */

#include "defs.h"
#include "init.h"
#include "init_private.h"


void init_target_primary(int state, struct registers *regs)
{
#if DEBUG > 3
    dprintf("TX1 init state %d\n", state);
#endif
}

void init_target_core(int state, struct pcpu *pcpu)
{
#if DEBUG > 3
    dprintf("TX core init state %d\n", state);
#endif
}
