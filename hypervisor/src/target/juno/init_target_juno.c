/*
 * Juno target init
 */

#include "defs.h"
#include "init.h"
#include "init_private.h"


#include "dma.h"
#include "smmu.h"
#include "uart.h"

#define UART_CLK 7257600
#define UART_CLKDIV_16_16 ((UART_CLK << 16) / (16 * 115200))


void init_target_primary(int state, struct registers *regs)
{

    switch(state) {
    case INIT_START:
        uart_init(ARCH_UART0_BASE, UART_CLKDIV_16_16);
        uart_init(ARCH_UART1_BASE, UART_CLKDIV_16_16);
        break;

    case INIT_END:
        smmu_setup();
        dma_transaction();
        break;
    }

#if DEBUG > 3
    dprintf("JUNO init state %d\n", state);
#endif
}

void init_target_core(int state, struct pcpu *pcpu)
{

#if DEBUG > 3
    dprintf("JUNO core init state %d\n", state);
#endif
}
