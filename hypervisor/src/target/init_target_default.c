/*
 * default target initialization.
 * Note that these functions are weak, if you define your own they will be overwritten
 */

#include "defs.h"
#include "init.h"
#include "init_private.h"


void __weak __init_target_early(struct registers *regs)
{
    /* empty */
}


void __weak init_target_primary(int state, struct registers *regs)
{
    /* empty */
}

void __weak init_target_core(int state, struct pcpu *pcpu)
{
    /* empty */
}
