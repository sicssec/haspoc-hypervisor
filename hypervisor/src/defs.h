/*
 * Definitions and such ...
 */

#ifndef _DEFS_H_
#define _DEFS_H_

#include <base.h>
#include <arch_defs.h>
#include <armv8.h>


/**
 * @name GUEST_VCPU_MAX
 * @type constant
 * @description maximum number of virtual cores (and therefore physical)
 * a guest can have.
 */
#if defined (TARGET_hikey) || defined (TARGET_s810)
  #define GUEST_VCPU_MAX 8
#else
  #define GUEST_VCPU_MAX 4
#endif

/* sanity checks */
#if ARCH_CORES > 8
  #error "You cannot have more than 8 cores in this architecture"
#endif

#if GUEST_VCPU_MAX > 8
   #error "Too many vcpus, this will not work with gic!"
#endif

#if GUEST_VCPU_MAX > ARCH_CORES
  #undef GUEST_VCPU_MAX ARCH_CORES
  #define GUEST_VCPU_MAX ARCH_CORES
#endif

#ifdef __ASSEMBLER__

#else /* __ASSEMBLER__ */


#include <minilib.h>

#include "util.h"
#include "cman.h"
#include "mman.h"
#include "debug.h"

#endif /* !__ASSEMBLER__ */

#endif /* _DEFS_H_ */
