
#ifndef __CMAN_H__
#define __CMAN_H__

/**
 * @name cpu manager
 * @type module
 * @description the CPU manager module
 */

#include "defs.h"
#include "armv8.h"
#include "target.h"


#include "psci.h"

#ifdef __ASSEMBLER__

#else /* __ASSEMBLER__ */

#include "gic.h"
#include "vgic.h"

#define UPCALL_DEBUG 0xC3000888
#define UPCALL_DIE 0xC3000999
#define UPCALL_NULL 0xC3000898

/* forward refs */
struct guest;
struct vcpu;
struct pcpu;

/*
 * vstate: state of a vcpu that can be saved and restored
 */

struct vstate {
    /* the general purpose registers are all here */
    struct registers gp;

    /* special registers */
    uint64_t sctlr_el1;
    uint64_t ttbr0_el1;
    uint64_t ttbr1_el1;
    uint64_t tcr_el1;
    uint64_t cpacr_el1;
    uint64_t sp_el1;
    uint64_t sp_el0;
    uint64_t mair_el1;
    uint64_t fpcr;
    uint64_t fpsr;
    uint64_t vbar_el1;
};


extern void cman_vstate_set(struct vcpu *vcpu, uint64_t x0, uint64_t x1,
                            uint64_t pc);
extern void cman_vstate_store(struct guest *guest, struct vcpu *vcpu,
                              struct registers *regs);
extern void cman_vstate_restore(struct guest *guest, struct vcpu *vcpu,
                                struct registers *regs);


/*
 * core interrupt flags
 */

static inline uint32_t cman_interrupts_disable()
{
    uint64_t ret;
    __asm__ volatile("mrs %0, daif\n"
                     "msr daifset, #15"
                           : "=r"(ret) :: "memory");
    return (uint32_t) ret;
}

static inline void cman_interrupts_restore(uint32_t mask)
{
    __asm__ volatile("msr daif, %0" :: "r"(mask) : "memory");
}

/*
 * cpu identification
 */

static inline cpuid_t mc_whoami()
{
    return mpidr_to_cpuid(MRS64("mpidr_el1"));
}

/*
 * Cache and MMU operations
 */

extern void __mmu_stage2_set(adr_t pt_root, int vmid, int enable);
extern void __mmu_stage2_tlb_inv(int vmid);
extern uint64_t __mmu_translate_el1_s1(uint64_t vadr);
extern uint64_t __mmu_translate_el2_s1(uint64_t vadr);
extern uint64_t __mmu_translate_s12(uint64_t vadr);

extern void __cache_inv_icache();
extern void __cache_flush_dcache();
extern void __cache_flush_dcache_range(void *start, size_t size);


/**
 * @name pcpu
 * @type struct
 * @description the pcpu structure contains data local to a single core
 */

struct pcpu {
    /* cpu stack */
    uint64_t *stack;
    uint64_t reserved;

    /* dont move these definitions, the must start at offset #16 */
    struct registers entry_hyp; /* hypervisor started with these registers */

    /* per core data for gic and vgic */
    struct gic_local gic_local;

    /* currently running guest */
    struct vcpu *vcpu;
    struct guest *guest;

    cpuid_t pid;     /* physical cpu ID */
    uint8_t cluster_num; /* what cluster do we belong to? */
    uint8_t cluster_core; /* what core in the cluster are we ? */

    spinlock_t lock; /* lock for changes form other cores */
    // struct cpu_entry entry;

    /* arch stuff */
    struct arch_pcpu arch;

#define PCPU_STATE_DISABLED 0
#define PCPU_STATE_FREE 1
#define PCPU_STATE_RESERVED 2
#define PCPU_STATE_READY 3
#define PCPU_STATE_RUNNING 4
    uint32_t state : 4;
};




/**
 * @name gic_pirq_get
 * @type function
 * @description returns the pirq of given number for the current core
 * @param0 the pcpu this concerns
 * @param1 index number of the physical IRQ
 * @return a pointer to the pirq or NULL if index is out of range
 * @error0 index if out of range. the function will then return NULL
 */
static inline struct pirq *gic_pirq_get(struct pcpu *pcpu, int index)
{
    if(index < 0 || index >= GIC_MAX_IRQ)
        return NULL;

    if(index < 32)
        return pcpu ? &pcpu->gic_local.pirqs[index] : NULL;
    else
        return &gic_global_.pirqs[index];
}

/**
 * @name COREDATA_SIZE
 * @type constant
 * @description this constants defines the area allocated for each core
 * this includes the pcpu structure and the stack
 */
#define COREDATA_SIZE (PAGE_SIZE * 2)

extern struct pcpu *pcpus[ARCH_CORES];

/**
 * @name cman_pcpu_get_by_cpuid
 * @type function
 * @description returns pcpu for the given index
 * @param0 index - cpuid between 0 and ARCH_CORES-1
 * @return pointer to pcpu structure with the given cpuid
 */

static inline struct pcpu *cman_pcpu_get_by_cpuid(cpuid_t index)
{
    return pcpus[index];
}


/**
 * @name cman_pcpu_get_safe
 * @type function
 * @description a slowe version of cman_pcpu_get that also works during global init
 */
static inline struct pcpu *cman_pcpu_get_safe()
{
    return cman_pcpu_get_by_cpuid( mc_whoami() );
}

/**
 * @name cman_pcpu_get
 * @type function
 * @description get poiner to pcpu on this core. Uses stack pointer
 * to quickly find the structure in memory.
 */
static inline struct pcpu *cman_pcpu_get()
{
    adr_t adr;
    adr = (adr_t) &adr;
    adr &=  ~(COREDATA_SIZE - 1);
    return (struct pcpu *) adr;
}

/**
 * @name vcpu
 * @type struct
 * @description represents a virtual cpu, which when active is connected
 * to a physical cpu /pcpu). a guest has a number of these
 */

struct vcpu {
    struct pcpu *pcpu;
    cpuid_t vid;

    /* saved context */
    struct vstate vstate;

    /* arch stuff */
    struct arch_vcpu arch;

};


extern void vcpu_init(struct vcpu *vcpu, struct guest *parent, cpuid_t vid);


static inline struct vcpu *cman_vcpu_get_by_cpuid(cpuid_t index)
{
    return cman_pcpu_get_by_cpuid(index)->vcpu;
}


static inline struct vcpu *cman_vcpu_get()
{
    return cman_pcpu_get()->vcpu;
}


/*
 * CPU
 */

extern struct pcpu *cman_pcpu_allocate(struct guest *g, struct vcpu *v);
extern void cman_pcpu_free(struct pcpu *p);
extern void cman_pcpu_guest_register(struct pcpu *pcpu, struct guest *guest, struct vcpu *vcpu);
extern void cman_pcpu_guest_unregister(struct pcpu *pcpu);

uint8_t cman_get_num_pcpus_in_cluster(uint8_t cluster);

extern uint8_t cpu_vmask_to_pmask(struct guest *g, uint8_t vmask);
extern uint8_t cpu_pmask_to_vmask(struct guest *g, uint8_t pmask);

extern uint64_t __cpu_smc(uint64_t x0, uint64_t x1, uint64_t x2,
    uint64_t x3, uint64_t vm_id);

extern uint64_t __cpu_smc_forward(struct registers *regs);

/* power management */
extern uint64_t pm_cpu_on(uint64_t target_mpidr, adr_t entry, uint64_t context);


/**
 * @name __die
 * @type function
 * @description stop execution on current core
 * @module cpu manager
 * @return none
 */
extern void __die();


/*
 * Reference for local boot entry.
 * FOR PSCI USE, NOT TO BE CALLED DIRECTLY!
 */
extern void __boot_core(struct pcpu *context);

/* setup */
extern void cman_init_early();
extern void cman_init(int global, struct pcpu *pcpu);

#endif /* !__ASSEMBLER__ */
#endif /* !__CMAN_H__ */
