#ifndef __CMAN_PRIVATE_H__
#define __CMAN_PRIVATE_H__

#include "defs.h"

#ifdef __ASSEMBLER__

#else

#include "gman.h"

/*
 * timer
 */
extern void cman_timer_init(int global, struct pcpu *pcpu);


/*
 * SMC & HVC call
 */
extern void cman_call_init(int global);
extern void smc_handle32(struct registers *regs, union reg_esr esr);
extern void smc_handle64(struct registers *regs, union reg_esr esr);
extern void hvc_handle32(struct registers *regs, union reg_esr esr);
extern void hvc_handle64(struct registers *regs, union reg_esr esr);

/*
 * helpers
 */
extern void el2_traps_enable(int irq, int fiq, int abt);
extern void el2_inject_redirect(struct registers *regs, int pos);
extern void el2_inject_irq(struct registers *regs, uint32_t iar);
extern void el2_inject_sync(struct registers *regs, union reg_esr esr, bool update_level);
extern void el2_next_instruction(struct registers *regs);

extern void el2_shared_setup_timers();
extern void el2_prim_setup_gic();
extern void el2_shared_setup_gic();
extern void hyp_timer_restart();
extern void hyp_timer_tick(struct registers *regs);

/*
 * coredata and pcpu
 */

struct coredata {
    /* page 1: pcpu and nothing else */
    union {
	struct pcpu pcpu;
	uint8_t page[PAGE_SIZE];
    };

    /* page 2: stack surrounded by canary */
    struct canary c0;
    uint64_t stack_memory[(PAGE_SIZE - 2 * sizeof(struct canary)) / sizeof(uint64_t)];
    uint64_t stack_top[0];
    struct canary c1;
};

/* same as cman_pcpu_get */
static inline struct coredata *cman_coredata_get()
{
    adr_t adr;
    adr = (adr_t) &adr;
    adr &=  ~(COREDATA_SIZE - 1);
    return (struct coredata *) adr;
}

extern spinlock_t cman_lock;

extern struct pagetable el2_s1_l1;

/*
 * restore entry for exceptions
 */
extern void __el2_restore_64_stack(); /* will not return */
extern void __el2_restore_64_ptr(struct registers *regs); /* will not return */


static inline uint64_t mmu_translate_el1_s1(adr_t adr, bool write)
{
    if(write)
        __asm__ volatile("at S1E1W, %0" :: "r"(adr));
    else
        __asm__ volatile("at S1E1R, %0" :: "r"(adr));

    return MRS64("PAR_EL1");
}

static inline uint64_t mmu_translate_el0_s1(adr_t adr, bool write)
{
    if(write)
        __asm__ volatile("at S1E0W, %0" :: "r"(adr));
    else
        __asm__ volatile("at S1E0R, %0" :: "r"(adr));

    return MRS64("PAR_EL1");

}


#endif /* !__ASSEMBLER__ */
#endif /* !__CMAN_PRIVATE_H__ */
