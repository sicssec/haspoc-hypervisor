/**
 * @name gic_pcpu_mq.c
 * @type file
 * @description per-core message queue
 */

#include "defs.h"
#include "util.h"
#include "cman.h"
#include "gic.h"
#include "gic_private.h"


/*
 * some helper functions
 */
static struct gic_pcpu_mq *mq_get(struct pcpu *pcpu)
{
    return & pcpu->gic_local.pcpu_mq;
}

static bool mq_read(struct gic_pcpu_mq *mq, union gic_pcpu_mq_data *data)
{
    return async_queue_read(& mq->q, 1, &data->all);
}

static bool mq_write(struct gic_pcpu_mq *mq, union gic_pcpu_mq_data data)
{
    return async_queue_write(& mq->q, 1, data.all);
}

/*
 * the pcpu message queue handler: process all messages in the queue
 */
static void gic_pcpu_mq_handler(struct pcpu *pcpu, struct registers *r, uint32_t iar)
{
    union gic_pcpu_mq_data msg;
    gic_pcpu_mq_callback handler;
    struct gic_pcpu_mq *mq;

    mq = mq_get(pcpu);

    while(mq_read(mq, &msg)) {
        if(msg.func < GIC_PCPU_MSG_COUNT) {
            mq->count[msg.func] ++;
            handler = mq->handlers[msg.func];
            if(handler) {
                handler(pcpu, msg);
                continue;
            }
        }

        dprintf("ERROR: unknown queue message: %x\n", msg.all);
    }
}

/*
 * write a message to the pcpu queue of a different cpu
 */
bool gic_pcpu_mq_write(struct pcpu *target, uint32_t func, uint32_t data)
{
    union gic_pcpu_mq_data msg;
    struct gic_pcpu_mq *mq;

    mq = mq_get(target);
    msg.func = func;
    msg.sender = mc_whoami();
    msg.data = data;

    if(func >= GIC_PCPU_MSG_COUNT || (data & ~0x00FFFFFF) != 0) {
        dprintf("BAD mq call: %x:%x\n", func, data);
        return false;
    }

    if(mq_write(mq, msg)) {
        gic_send_sgi(GIC_SGI_HYP_MESSAGE, 1UL << target->pid);
        return true;
    }

    return false;
}

/*
 * register a handler for a certain type of messages on this cpu
 */
void gic_pcpu_mq_register_handler(struct pcpu *pcpu, uint32_t func,
                                  gic_pcpu_mq_callback handler)
{
    struct gic_pcpu_mq *mq;
    mq = mq_get(pcpu);

    /* sanity checks */
    assert("pcpu mq message out of range", func < GIC_PCPU_MSG_COUNT);
    assert_null("mq handler allready set", mq->handlers[func]);


    mq->handlers[func] = handler;
}

/*
 * set up the pcpu message queue for each core
 */
void gic_pcpu_mq_init(int global, struct pcpu *pcpu)
{
    int i, id;
    struct gic_pcpu_mq *mq;

    if(global)
        return;

    /* sanity checks */
    if(GIC_PCPU_MSG_COUNT >= 16)
        fatal("Too many MQ mesages defined");

    if(sizeof(union gic_pcpu_mq_data) != sizeof(uint32_t))
        fatal("gic_pcpu_mq_data bad size");

    mq = mq_get(pcpu);
    memset(mq, 0, sizeof(*mq));
    async_queue_init(& mq->q, GIC_PCPU_QUEUE_SIZE);

    /* setup queue interrupt & callback */
    id = gicv2_get_irqid(GIC_IRQ_TYPE_SGI, GIC_SGI_HYP_MESSAGE);
    gicv2_dist_set_enable(_gicd, id, 1);
    gicv2_dist_set_priority(_gicd, id, 0x00);
    gic_pirq_callback_set(pcpu, id, gic_pcpu_mq_handler);

    /* read back to see if we have it */
    assert_eq("MQ SGI available", 1,
              gicv2_dist_get_enable(_gicd, id));
}
