#ifndef __GIC_PRIVATE_H__
#define __GIC_PRIVATE_H__

static inline int gic_gich_free_lr()
{
    uint64_t list;

    list = *(uint64_t *)(& _gich->elsr[0]);
    return util_last_bit1(list);
}

/*
 * the irq message quete
 */

extern int gic_irq_mq_process(struct gic_irq_mq *q);
extern void gic_irq_mq_init(int global, struct pcpu *pcpu);

/*
 * inter-core message queue
 */
extern void gic_pcpu_mq_init(int global, struct pcpu *pcpu);


#endif /* __GIC_PRIVATE_H__ */
