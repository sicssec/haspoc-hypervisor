
/*
 * interrupt queue
 */

#include "defs.h"

#include <gicv2.h>

#include "util.h"
#include "gic.h"
#include "gic_private.h"


static struct gic_irq_mq *gic_irq_mq_get(struct pcpu *pcpu)
{
    return & pcpu->gic_local.irq_mq;
}

static bool gic_irq_mq_can_read(struct gic_irq_mq *mq)
{
    return async_queue_can_read(& mq->q);
}

static bool gic_irq_mq_can_write(struct gic_irq_mq *mq)
{
    return async_queue_can_write(& mq->q);
}

bool gic_irq_mq_read(struct gic_irq_mq *mq, uint32_t *data)
{
    return async_queue_read(& mq->q, 0, data);
}

bool gic_irq_mq_write(struct gic_irq_mq *mq, uint32_t data)
{
    return async_queue_write(& mq->q, 0, data);
}


/*
 * process as many messages in the queue as possible
 */
int gic_irq_mq_process(struct gic_irq_mq *q)
{
    int lr, cnt = 0;
    uint32_t val;

    while(gic_irq_mq_can_read(q)) {
        lr = gic_gich_free_lr();
        if(lr < 0) {
            /* add LR-regs are used, enable maintenance interrupts */
            _gich->hcr |= 0x02;
            return cnt;
        }

        if(gic_irq_mq_read(q, &val)) {
            cnt ++;
            _gich->lr[lr] = val;
#if DEBUG > 3
		dprintf("Qread[%d]:%x, R:%d, W:%d\n",lr,val,q->q.read, q->q.write);
#endif
        } else {
            fatal("BUG"); /* should not happen */
        }
    }
    /* Processed everything, no need for maintenace interrupts anymore */
    _gich->hcr &= ~0x02;
    return cnt;
}

/*
 * initialize the per-core internal interrupt queue
 */
void gic_irq_mq_init(int global, struct pcpu *pcpu)
{
    struct gic_irq_mq *queue;

    if(!global) {
        queue = gic_irq_mq_get(pcpu);;
        async_queue_init(& queue->q, GIC_IRQ_QUEUE_SIZE);
    }
}
