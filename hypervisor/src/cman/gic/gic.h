
#ifndef __GIC_H__
#define __GIC_H__

#include "defs.h"
#include <gicv2.h>
#include "util.h"
#include "runtime.h"

#define GIC_MAX_IRQ 256

/* our default GIC priorities */
#define GIC_PRIO_ALL 0xFF
#define GIC_PRIO_SPI 0xB0
#define GIC_PRIO_PPI 0xB0
#define GIC_PRIO_SGI 0x90

/* hypervisor interrupts */
#define GIC_SGI_HYP_MESSAGE 7
#define GIC_SGI_HYP_EE 6
#define GIC_PPI_MAINTETANCE 9

/* forward types */
struct pcpu;
struct pirq;

/**
 * @name pirq
 * @type struct
 * @description pirq contains information about a physical interrupt.
 * This interrupt can either be global (SPI) or per core
 */
struct pirq {
    union {
        struct guest *owner;
        icallback callback;
    };
    uint32_t vid : 10;

#define PIRQ_TYPE_NONE 0
#define PIRQ_TYPE_EL1 1
#define PIRQ_TYPE_EL2 2
#define PIRQ_TYPE_EL3 3
    uint32_t type : 2;
};




/*
 * inter-core message queue
 */

enum gic_pcpu_mq_msg {
    GIC_PCPU_MSG_VIRQ = 0,
    GIC_PCPU_MSG_DEBUG,
    GIC_PCPU_MSG_COUNT /* last one */
};

union gic_pcpu_mq_data {
    uint32_t all;
    struct {
        uint32_t func : 4;
        uint32_t sender : 4;
        uint32_t data : 24;
    };
};

/**
 * @name mq_callback
 * @type typedef
 * @description message received handler
 */

typedef void (*gic_pcpu_mq_callback)(struct pcpu *pcpu,
                                     union gic_pcpu_mq_data data);

/**
 * @name gic_pcpu_mq
 * @type struct
 * @description per cpu message queue
 */

struct gic_pcpu_mq {
#define GIC_PCPU_QUEUE_SIZE 128
    struct async_queue q;
    uint32_t data[GIC_PCPU_QUEUE_SIZE];
    gic_pcpu_mq_callback handlers[GIC_PCPU_MSG_COUNT];
    uint64_t count[GIC_PCPU_MSG_COUNT];
};

extern void gic_pcpu_mq_register_handler(struct pcpu *pcpu, uint32_t func4,
                                         gic_pcpu_mq_callback handler);
extern bool gic_pcpu_mq_write(struct pcpu *target, uint32_t func4, uint32_t data24);



/*
 * internal irq async queue
 */

/**
 * @name gic_irq_qm
 * @type struct
 * @describtion internal GIC mainatance queue, a small queue for
 * interrupts waiting for a free LR
 */
struct gic_irq_mq {
#define GIC_IRQ_QUEUE_SIZE (1 << 5)
    struct async_queue q;
    uint32_t data[GIC_IRQ_QUEUE_SIZE];
};

extern bool gic_irq_mq_read(struct gic_irq_mq *mq, uint32_t *data);
extern bool gic_irq_mq_write(struct gic_irq_mq *mq, uint32_t data);


struct gic_local {
    struct pirq pirqs[32];
    struct gic_irq_mq irq_mq;
    struct gic_pcpu_mq pcpu_mq;
};

struct gic_global {
    /* 32 first are not used */
    struct pirq pirqs[GIC_MAX_IRQ];
};

extern struct gic_global gic_global_;



extern int gic_pirq_is_reserved(struct pcpu *pcpu, int pirq);
extern bool gic_pirq_callback_set(struct pcpu *pcpu, int pid, icallback c);

extern bool gic_pirq_virq_connect(struct pcpu *pcpu, int pid, int vid, struct guest *g);
extern bool gic_pirq_virq_disconnect(struct pcpu *pcpu, int pid);

extern bool gic_gich_write_lr(struct pcpu *pcpu, uint32_t lrdata);

extern void gic_send_sgi(int sgi, int target_list);

extern void gic_hypervisor_enable();

extern void gic_disable(int dist, int core, int hyper);

extern void gic_init(int global, struct pcpu *);
extern void gic_handler(struct registers *r, int is_hyp);

extern void gicd_spinlock_lock();
extern void gicd_spinlock_unlock();

#endif /* __GIC_H__ */
