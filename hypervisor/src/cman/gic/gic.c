
#include "defs.h"

#include <gicv2.h>

#include "util.h"
#include "gic.h"
#include "gic_private.h"

#include "vgic.h"


/* global pirqs that are not per CPU */
struct gic_global gic_global_;

/*
 * GICD is a shared resources and requires locking
 */
static spinlock_t gicd_lock;

void gicd_spinlock_lock()
{
    spinlock_lock(&gicd_lock);
}
void gicd_spinlock_unlock()
{
    spinlock_unlock(&gicd_lock);
}

void gic_disable(int dist, int core, int hyper)
{
    if(dist)
        _gicd->ctlr &= ~1;

    if(core)
        _gicc->ctlr &= ~1;

    if(hyper)
        _gich->hcr &= ~1;

}


/*
 * pirq
 */

/* returns 0 if this pirq cannot be used by EL1 */
int gic_pirq_is_reserved(struct pcpu *pcpu, int id)
{
    struct pirq *pirq = gic_pirq_get(pcpu, id);
    return (pirq == NULL || pirq->type == PIRQ_TYPE_EL3 ||
            pirq->type == PIRQ_TYPE_EL2);
}

/* GIC registred callbacks */

bool gic_pirq_callback_set(struct pcpu *pcpu, int id, icallback c)
{
    struct pirq *pirq = gic_pirq_get(pcpu, id);
    if(pirq && pirq->type != PIRQ_TYPE_EL3) {
        pirq->type = c ? PIRQ_TYPE_EL2 : PIRQ_TYPE_NONE;
        pirq->callback = c;
        pirq->vid = -1;
        return true;
    }
    return false;
}

bool gic_pirq_virq_disconnect(struct pcpu *pcpu, int id)
{
    struct pirq *pirq = gic_pirq_get(pcpu, id);

    if(pirq && pirq->type == PIRQ_TYPE_EL1) {
        pirq->type = PIRQ_TYPE_NONE;
        pirq->vid = -1;
        pirq->owner = NULL;
        return true;
    }
    return false;
}

bool gic_pirq_virq_connect(struct pcpu *pcpu, int id,
                           int vid, struct guest *g)
{
    struct pirq *pirq = gic_pirq_get(pcpu, id);
    if(pirq) {
        /* register to guest */
        if(g && pirq->type != PIRQ_TYPE_EL3 && pirq->type != PIRQ_TYPE_EL2) {
            pirq->type = PIRQ_TYPE_EL1;
            pirq->vid = vid;
            pirq->owner = g;
            return true;
        }
    }
    return false;
}

/* SGI */
void gic_send_sgi(int sgi, int target_list)
{
    union gicd_sgir_reg r;

    /* lets limit ourselves to available cores */
    target_list &= (1UL << ARCH_CORES) - 1;

    /* use target list and non-secure mode */
    r.id = sgi;
    r.targetlist = target_list;
    r.filter = 0;
    r.ns = 1;

    /* send it */
    _gicd->sgir = r.all;
}

/*
 * GICH
 */

/**
 * @name gic_hypervisor_enable
 * @type function
 * @description activate hypervisor mode on GIC: trap irq/fiq and enable GICH
 */
void gic_hypervisor_enable()
{
    union reg_hcr hcr_el2;

    /* trap FIQ & IRQ  */
    hcr_el2.all = MRS64("hcr_el2");
    hcr_el2.fmo = 1;
    hcr_el2.imo = 1;
    MSR64("hcr_el2", hcr_el2.all);

    /* enable GICH */
    _gich->hcr = 1;
}

bool gic_gich_write_lr(struct pcpu *pcpu, uint32_t lrdata)
{
    int lr;
    struct gic_irq_mq *q;

    lr = gic_gich_free_lr();
    if(lr != -1) {
        _gich->lr[lr] = lrdata;
    } else {
        q = & pcpu->gic_local.irq_mq;

        if(!gic_irq_mq_write(q, lrdata)) {
            /* could not add data to queue, it is full */
#if DEBUG > 3
            dprintf("GICH could not add LR %x to queue\n", lrdata);
#endif
            /* XXX: as a last resort, try processing the queue and insert again */
            gic_irq_mq_process(q);
            if(!gic_irq_mq_write(q, lrdata))
                return false;
        }
        gic_irq_mq_process(q);
    }
    return true;
}

/* GIC handlers */

static void gic_maintenance_handler(struct pcpu *pcpu, struct registers *regs, uint32_t iar)
{
    int processed;
    struct gic_irq_mq *mq;

    /* underflow maintenance interrupt */
    if(_gich->misr & 2) {
        mq = & pcpu->gic_local.irq_mq;

        processed = gic_irq_mq_process( mq);

        /* if we just made room in out queue, check if there are any saved we can fire */
        if(processed > 0)
            vgic_process_queued(pcpu);
    }
}


void gic_handler(struct registers *r, int is_hyp)
{
    uint32_t iar, id;
    struct pirq *pirq;
    struct pcpu *pcpu;
    icallback cb;

    pcpu = cman_pcpu_get();

    for(;;) {
        iar = _gicc->iar;
        id = iar & 1023;

        /* why so spurious? */
        if(id >= 1022)
            break;

        pirq = gic_pirq_get(pcpu, id);
        if(pirq) {
            switch(pirq->type) {
            case PIRQ_TYPE_EL2:
                pirq->callback(pcpu, r, iar);
                _gicc->eoir = iar;
                _gicc->dir = iar;
                break;
            case PIRQ_TYPE_EL1:
                if(!is_hyp) {
                    vgic_handler(pcpu, pirq, r, iar);
                }
                _gicc->eoir = iar;
                break;
            default:
                dprintf("Unexpected interrupt IAR=%x, pirq type=%d\n",
                        iar, pirq->type);
                _gicc->eoir = iar;
            }
        } else {
            dprintf("Unknown interrupt IAR=%x\n", iar);
            _gicc->eoir = iar;
        }
    }
}

/* GIC init */
void gic_init(int global, struct pcpu *pcpu)
{
    int i, id, lines, irqs;

    /*
     * GIC global
     */
    if(global) {
        /* initialize gicd global lock */
        spinlock_init(&gicd_lock, 0);

        /*
         * configure GICD on primary core:
         *
         * trun off GICD, set up and disable all SPI's, set prio to min
         */

        _gicd->ctlr = 0;
        lines = gicv2_dist_get_lines(_gicd);
        irqs  = lines * 32;
        for(i = 32; i < irqs; i++) {
            gicv2_dist_set_enable(_gicd, i, 0);
            gicv2_dist_set_config(_gicd, i, 0);
            gicv2_dist_set_priority(_gicd, i, GIC_PRIO_ALL);
            gicv2_dist_set_target(_gicd, i, 0x1);
        }
        _gicd->ctlr = 1;
        return;
    }


    /*
     * gic_local
     */

    /*
     * configure GICC:
     *
     * Disable GICC, set mask to none, set grouping to none
     *
     * Configure and disable all PPI,
     * configure and enable (no change) all SGI
     */
    _gicc->ctlr = 0;
    _gicc->pmr = GIC_PRIO_ALL;
    _gicc->bpr = 0x00; /* no grouping */

    /* SGI */
    for(i = 0; i < 16; i++) {
        gicv2_dist_set_enable(_gicd, i, 1);
        gicv2_dist_set_priority(_gicd, i, GIC_PRIO_SGI);
    }
    /* PPI */
    for(i = 16; i < 32; i++) {
        gicv2_dist_set_enable(_gicd, i, 0);
        gicv2_dist_set_priority(_gicd, i, GIC_PRIO_PPI);
    }

    /* setup maintenance queue callback */
    id = gicv2_get_irqid(GIC_IRQ_TYPE_PPI, GIC_PPI_MAINTETANCE);
    gicv2_dist_set_enable(_gicd, id, 1);
    gicv2_dist_set_priority(_gicd, id, 0x80);
    gic_pirq_callback_set(pcpu, id, gic_maintenance_handler);

    /*
     * various queues
     */
    gic_irq_mq_init(global, pcpu);
    gic_pcpu_mq_init(global, pcpu);

    /*
     * enable GICC
     */
    _gicc->ctlr = 0x201; /* enable and EOImode */

}
