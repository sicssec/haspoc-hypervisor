/*
 * interrupt code including some handlers and setup
 */


#include "cman.h"
#include "cman_private.h"


#include <gicv2.h>

#include "runtime.h"
#include "util.h"
#include "vgic.h"

/*
 * This is called when user performs a cache operation to
 * unavailable memory
 */
static bool cache_op_handler(struct mdriver *drv, struct registers *regs,
    adr_t ipadr, union reg_esr esr)
{
    /* if it has a driver, its fine */
    return true;
}

static void check_coredata_sanity()
{
    struct coredata *cd = cman_coredata_get();
    if(!canary_check(& cd->c0) || !canary_check(&cd->c1))
	fatal("FATAL: coredata corrupted");
}


/*
 * handle memory abort by calling the correct driver or handler or fail
 */
static bool dabort_mem(struct registers *regs, union reg_esr esr)
{
    adr_t ipadr, vadr, ipadr_cln;
    struct mdriver *d;
    struct pcpu *pcpu;
    struct guest *guest;
    uint64_t val;

    /* get vadr and ipadr, consider cache op a write */
    vadr  = MRS64("FAR_EL2");
    ipadr = mmu_translate_el1_s1(vadr, esr.cm | esr.wnr);
    if(ipadr & 1) {
        printf("[dabort] translation failed. vadr=%X ipadr=%x esr=%x\n",
               vadr, ipadr, esr.all);
        goto dabort_fail;
    }

#if DEBUG > 8
	dprintf("[EL2-sync] ESR=%x PC=%X vadr=%X->ipadr=%x\n", esr, regs->pc, vadr, ipadr);
#endif


    /* get real ipadr, find driver */
    pcpu = cman_pcpu_get();
    guest = pcpu->guest;
    if(!guest) {
        fatal("INTERNAL: nil guest?");
        goto dabort_fail;
    }

    ipadr_cln = (ipadr & 0x0000FFFFFFFFF000) | (vadr & 0xFFF);
    d = gman_mdriver_find(guest, ipadr_cln);
    if(!d) {
        dprintf("[dabort] no access to %X ESR=%x at PC=%X\n", ipadr_cln, esr, regs->pc);
        goto dabort_fail;
    }

    /* see if that access is sane for this driver */
    if(!mdrivers_check_access(d, ipadr_cln, esr)) {
        dprintf("Driver check access failed at %x...\n", ipadr);
        goto dabort_fail;
    }

    /* cache operations ? */
    if(esr.cm) {
        if(!cache_op_handler(d, regs, ipadr_cln, esr))
            goto dabort_fail;
        return true;
    }


    /* standard memory access ? */
    if(!esr.isv) {
        printf("[dabort] Abort without instruction syndrome. ESR=%x\n", esr);
        goto dabort_fail;
    }

    if(esr.wnr) {
        /* WRITE operation ? */
        val = esr.srt < 31 ? regs->x[esr.srt] : 0;
        if(d->ops_guest_write(d, esr, ipadr_cln, val)) {

#if DEBUG > 4
            dprintf("[%s-write] ipadr=%x size=%d val=%x\n", d->module->name, ipadr_cln, esr.sas, val);
#endif
            return true;
        } else {
#if DEBUG > 1
            dprintf("[%s-write] FAILED ipadr=%x size=%d val=%x\n", d->module->name, ipadr_cln, esr.sas, val);
#endif
            goto dabort_fail;
        }
    } else {
        /* READ operation ? */
        val = 0;
        if(d->ops_guest_read(d, esr, ipadr_cln, &regs->x[esr.srt])) {
#if DEBUG > 4
            dprintf("[%s-read] ipadr=%x size=%d val=%x\n", d->module->name, ipadr_cln, esr.sas, val);
#endif
            return true;
        } else {
#if DEBUG > 1
            dprintf("[%s-read] FAILED ipadr=%x size=%d\n", d->module->name, ipadr_cln, esr.sas);
#endif
            goto dabort_fail;
        }
    }


dabort_fail:
    /* set EL1 fault address in case we are injecting this back to EL1 */
    MSR64("FAR_EL1", vadr);

    return false;
}

static void el2_handle_sync_lower_dabort(struct registers *regs, uint64_t psr,
                                         uint64_t lr, union reg_esr esr)
{
    bool ret = false;
    switch(esr.dfsc) {
    case DFSC_TF_L0 ... DFSC_TF_L3:
        ret = dabort_mem(regs, esr);
        break;

    default:
        dprintf("Unsupported dabort DFSC=%x\n", esr.dfsc);
        ret = false;
    }


    if(ret) {
        /* success */
        el2_next_instruction(regs);
    } else {
        /* failure */
        el2_inject_sync(regs, esr, true);
    }
}

static void el2_handle_sync_lower_pabort(struct registers *regs, uint64_t psr,
                                         uint64_t lr, union reg_esr esr)
{
    adr_t vadr;

#if DEBUG > 8
	dprintf("[pabort] ESR=%x PC=%X\n", esr, regs->pc);
#endif

    /* out current assumption is that something bad happened and we can't
     * really do much about it. Let the guest handle it, or die
     */

    /* XXX: do we need to set any other registers before we do this? */
    vadr  = MRS64("FAR_EL2");
    MSR64("FAR_EL1", vadr);

    el2_inject_sync(regs, esr, true);
}

/* ------------------------------------------------------------------------- */

void el2_handle_irq(struct registers *regs, uint64_t psr)
{
    check_coredata_sanity(); /* check coredata before */
    gic_handler(regs, 0);
    check_coredata_sanity(); /* check coredata after */

}


void el2_handle_fiq(struct registers *regs, uint64_t psr)
{
    uint64_t esr = MRS64("esr_el2");
    printf("IRQ  ESR=%x PSR=%x at %x\n", esr, psr, regs->pc);
    fatal("INTERNAL: cannot handle FIQ");
}


void el2_handle_irq_hyp(struct registers *regs, uint64_t psr)
{
    gic_handler(regs, 1);
}


static void el2_handle_sync_el0(struct registers *regs, uint64_t psr,
                                uint64_t lr, union reg_esr esr)
{
    switch(esr.ec) {
#if 0
    case ESR_EC_DABORT_LOWER: /* data abort from lower el: */
        el2_handle_sync_lower_dabort(regs, psr, lr, esr);
        break;
#endif
    default:
        dprintf("[EL0 sync] injecting PSR=%x ESR=%x LR=%x\n", psr, esr.all, lr);
        el2_inject_sync(regs, esr, false);
    }
}

static void el2_handle_sync_el1(struct registers *regs, uint64_t psr,
                                uint64_t lr, union reg_esr esr)
{
    switch(esr.ec) {
    case ESR_EC_WFx:   /* WFE, WFI */
        /* XXX: can this result in side-channel leackage ? */
        if(esr.all & 1) {
            __asm__ volatile("wfe");
        } else {
            __asm__ volatile("wfi");
        }
        el2_next_instruction(regs);
        break;

    case ESR_EC_DABORT_LOWER: /* data abort from lower el: */
        el2_handle_sync_lower_dabort(regs, psr, lr, esr);
        break;

    case ESR_EC_PABORT_LOWER: /* fetch abort from lower el: */
        el2_handle_sync_lower_pabort(regs, psr, lr, esr);
        break;

    case ESR_EC_HVC64:
        hvc_handle64(regs, esr);
        break;
    case ESR_EC_HVC32:
        hvc_handle32(regs, esr);
        break;

    case ESR_EC_SMC64:
        smc_handle64(regs, esr);
        break;

    case ESR_EC_SMC32:
        smc_handle32(regs, esr);
        break;

    default:
        printf("Unknown sync exception ERR=%x at %x\n", esr.all, regs->pc);
        printf("\tX0: %x, X1: %x, X2: %x, X3: %x\n", regs->x[0], regs->x[1], regs->x[2], regs->x[3]);
        fatal("cannot continue", NULL);
    }

}

void el2_handle_sync(struct registers *regs, uint64_t psr,
                     uint64_t lr, union reg_esr esr)
{
    /* check coredata before */
    check_coredata_sanity();

    /* allow only 64-bit code */
    if(psr & 0x10) {
        fatal("SYNC from 32 bit code", NULL);
    }

    /* allow only 32-bit ISA */
    if(! esr.il) {
        fatal("SYNC from 16-bit instruction", NULL);
    }

    /* we handle things differently depending on where we came from */
    if( (psr & 0xE) == 4)
        el2_handle_sync_el1(regs, psr, lr, esr);
    else
        el2_handle_sync_el0(regs, psr, lr, esr);

    /* check coredata after */
    check_coredata_sanity();
}

void el2_handle_serror(struct registers *regs, uint64_t psr,
                       uint64_t lr, union reg_esr esr)
{
    printf("SERROR PC=%X, PSR=%x, ESR_EL2=%x, ESR_EL1=%x\n",
           regs->pc, psr, esr.all, MRS64("esr_el1"));

    printf("SERROR IFSR32_EL2=%x, FAR_EL2=%x, FAR_EL1=%x\n",
           MRS64("IFSR32_EL2"),
           MRS64("FAR_EL2"),
           MRS64("FAR_EL1")
           );
    fatal("INTERNAL: cannot handle serror");
}


/* --------------------------------------------------------------- */

void el2_handle_sync_hyp(struct registers *regs, uint64_t psr,
                         uint64_t lr, union reg_esr esr)
{
    uint64_t far = MRS64("FAR_EL2");

    printf("FATAL: Same level sync: ESR=%x FAR=%x PC=%x\n", esr.all, far, lr);
    fatal("INTERNAL: cannot handle EL2 sync");
}
