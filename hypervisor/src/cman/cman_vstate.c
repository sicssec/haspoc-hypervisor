
#include "defs.h"

#include "cman.h"
#include "cman_private.h"

#include "mman.h"
#include "gman.h"
#include "util.h"

void cman_vstate_set(struct vcpu *vcpu, uint64_t x0, uint64_t x1, uint64_t pc)
{
    vcpu->vstate.gp.x[0] = x0;
    vcpu->vstate.gp.x[1] = x1;
    vcpu->vstate.gp.pc = pc;
}


void cman_vstate_restore(struct guest *guest, struct vcpu *vcpu, struct registers *regs)
{
    struct vstate *vstate;

    vstate = & vcpu->vstate;

    /* sanity checks */
    assert_not_null("guest is not NULL", guest);
    assert_not_null("vcpu is not NULL", vcpu);
    assert_not_null("regs is not NULL", regs);
    /* TODO: better sanity checks... */

    /* copy GP registers to our regs */
    *regs = vstate->gp;

    /* system registers */
    MSR64("VMPIDR_EL2", cpuid_to_mpidr(vcpu->vid)); /* set correct CPU ID */
    MSR64("SCTLR_EL1",  vstate->sctlr_el1);
    MSR64("CPACR_EL1",  vstate->cpacr_el1);
    MSR64("TTBR0_EL1",  vstate->ttbr0_el1);
    MSR64("TTBR1_EL1",  vstate->ttbr1_el1);
    MSR64("MAIR_EL1",   vstate->mair_el1);
    MSR64("TCR_EL1",    vstate->tcr_el1);
    MSR64("SP_EL1",     vstate->sp_el1);
    MSR64("SP_EL0",     vstate->sp_el0);
    MSR64("FPCR",       vstate->fpcr);
    MSR64("FPSR",       vstate->fpsr);
    MSR64("VBAR_EL1",   vstate->vbar_el1);

    /* set page table, clean all TLBs and activate MMU */
    __mmu_stage2_set((adr_t) guest->l1, guest->vmid, 1);
    __mmu_stage2_tlb_inv(guest->vmid);
    /* XXX: Do not do this, as EL2 already uses the caches
    __cache_flush_dcache();
    __cache_inv_icache();
    */

    /* install guest interrupts  */
    if(!vgic_guest_gic_restore(vcpu->pcpu, guest))
        fatal("Could not restore vgic");
}


void cman_vstate_store(struct guest *guest, struct vcpu *vcpu, struct registers *regs)
{
    struct vstate *vstate;

    vstate = & vcpu->vstate;

    /* sanity checks */
    assert_not_null("guest is not NULL", guest);
    assert_not_null("vcpu is not NULL", vcpu);
    assert_not_null("regs is not NULL", regs);
    /* TODO: better sanity checks... */

    /* copy GP registers to our regs */
    memcpy((void *)&vcpu->vstate.gp, (void *)regs, sizeof(struct registers));

    /* save system registers, should probably add all regs for completness */
    vcpu->vid = mpidr_to_cpuid(MRS64("VMPIDR_EL2")); /* save correct CPU ID */
    vstate->sctlr_el1 = MRS64("SCTLR_EL1");
    vstate->cpacr_el1 = MRS64("CPACR_EL1");
    vstate->ttbr0_el1 = MRS64("TTBR0_EL1");
    vstate->ttbr1_el1 = MRS64("TTBR1_EL1");
    vstate->mair_el1  = MRS64("MAIR_EL1");
    vstate->tcr_el1   = MRS64("TCR_EL1");
    vstate->sp_el1    = MRS64("SP_EL1");
    vstate->sp_el0    = MRS64("SP_EL0");
    vstate->fpcr      = MRS64("FPCR");
    vstate->fpsr      = MRS64("FPSR");
    vstate->vbar_el1  = MRS64("VBAR_EL1");

    /* remove page table, clean all TLBs and disable MMU */
    __mmu_stage2_set((adr_t) -1, guest->vmid, 0);
    __mmu_stage2_tlb_inv(guest->vmid);
    /* XXX: Do not do this, as EL2 already uses the caches
    __cache_flush_dcache();
    __cache_inv_icache();
    */

    /* save guest interrupts */
    if(!vgic_guest_gic_store(vcpu->pcpu, guest))
        fatal("Could not store vgic");
}
