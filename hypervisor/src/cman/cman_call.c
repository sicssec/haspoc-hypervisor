
/*
 * Hyper and monitor calls
 */

#include "defs.h"
#include "util.h"
#include "cman.h"
#include "cman_private.h"
#include "debug.h"


void smc_handle64(struct registers *regs,  union reg_esr esr)
{
    uint32_t ret = SMC_RET_SUCCESS;
    struct pcpu *pcpu;

    pcpu = cman_pcpu_get();
    /*Careful, Printing here can make it crash on certain platforms (s810)*/
    debug_print(LVL3, "SMC64 trap: %x at %x\n", esr.all, regs->pc);
    debug_print(LVL3,"\tX0: %x, X1: %x, X2: %x, X3: %x\n", regs->x[0], regs->x[1],
        regs->x[2], regs->x[3]);

    switch(regs->x[0]) {
        /* debug stuff */
    case UPCALL_NULL:
        break;

    case UPCALL_DIE:
        fatal("Suicide requested. Goodbye cruel world!");
        break;

        /* all other handlers */
    default:
        ret = cdriver_process(NULL, pcpu, regs, 1);
    }

    regs->x[0] = ret;

    /* since this was a trap, ESR_EL1 is still pointing to the SMC */
    el2_next_instruction(regs);
}

void hvc_handle64(struct registers *regs,  union reg_esr esr)
{
    uint32_t ret = SMC_RET_SUCCESS;
    struct pcpu *pcpu;

    pcpu = cman_pcpu_get();

#if DEBUG > 0
    dprintf("HVC %x at %x\n", esr.all, regs->pc);
    dprintf("\tX0: %x, X1: %x, X2: %x, X3: %x\n", regs->x[0], regs->x[1],
        regs->x[2], regs->x[3]);
#endif

    switch(regs->x[0]) {

        /* DEBUG stuff */
    case UPCALL_NULL:
        break;

    case UPCALL_DIE:
        fatal("Suicide requested. Goodbye cruel world!");
        break;

    case UPCALL_DEBUG:
        debug_dump_guest(pcpu->guest);
        debug_dump_gic();
        debug_dump_registers(regs);
        break;


        /* all other handlers */
    default:
        ret = cdriver_process(NULL, pcpu, regs, 0);
    }

    regs->x[0] = ret;
}

void smc_handle32(struct registers *regs,  union reg_esr esr)
{
    uint32_t ret = SMC_RET_SUCCESS;
    struct pcpu *pcpu;

    pcpu = cman_pcpu_get();

    switch(regs->x[0]) {
        /* debug stuff */
    case UPCALL_NULL:
        break;

    case UPCALL_DIE:
        fatal("Suicide requested. Goodbye cruel world!");
        break;

        /* all other handlers */
    default:
        ret = cdriver_process(NULL, pcpu, regs, 1);
    }

    regs->x[0] = ret;

    /* since this was a trap, ESR_EL1 is still pointing to the SMC */
    el2_next_instruction(regs);
}

void hvc_handle32(struct registers *regs,  union reg_esr esr)
{
    fatal("We dont support 32-bit hyeprcalls");
}




void cman_call_init(int global)
{
    union reg_hcr hcr_el2;

    if(!global) {
        /* trap SMC calls */
        hcr_el2.all = MRS64("hcr_el2");
        hcr_el2.tsc = 1;
        MSR64("hcr_el2", hcr_el2.all );
    }
}
