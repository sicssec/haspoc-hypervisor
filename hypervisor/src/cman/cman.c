
#include "defs.h"

#include "cman.h"
#include "cman_private.h"

#include "mman.h"
#include "gman.h"
#include "util.h"


struct pcpu *pcpus[ARCH_CORES];
spinlock_t cman_lock;

/* System global (shared by all cores) EL2 stage 1 level 1 (top level) page table. */
struct pagetable el2_s1_l1;

/*
 * Trapping and flow injection
 */



void el2_inject_redirect(struct registers *regs, int pos)
{
    MSR64("SPSR_EL1", regs->cpsr);
    MSR64("ELR_EL1", regs->pc);
    regs->pc = MRS64("VBAR_EL1") + EXP_SIZE * pos;

    if( (regs->cpsr & 0x1E) == 0x4) {
        /* same EL, no IRQ/FIQ */
        regs->cpsr = 0x3C0 | (regs->cpsr & 31);
    } else {
        /* EL2 */
        regs->cpsr = 0x3C5;
    }
}


void el2_inject_irq(struct registers *regs, uint32_t iar)
{
    el2_inject_redirect(regs, EXP_SRC_SAME | EXP_TYPE_IRQ);
}

void el2_inject_sync(struct registers *regs, union reg_esr esr,
                     bool update_level)
{
    if(update_level) {
        /* translate error codes to what could have been in EL1 */
        if(esr.ec == ESR_EC_DABORT_LOWER) {
            esr.ec = ESR_EC_DABORT_SAME;
        } else if(esr.ec == ESR_EC_PABORT_LOWER) {
            esr.ec = ESR_EC_PABORT_SAME;
        }
    }

    /* set ESR and inject sync abort */
    MSR64("ESR_EL1", esr.all);
    el2_inject_redirect(regs, EXP_SRC_SAME | EXP_TYPE_SYNC);
}

void el2_next_instruction(struct registers *regs)
{
    regs->pc += 4;
}


/*
 * pcpu
 */

static struct pcpu *cman_pcpu_find(uint32_t state, uint32_t mask)
{
    int i;
    for(i = 0; i < ARCH_CORES; i++) {
        if(pcpus[i]->state == state && (mask & (1UL << i))) {
            if(! pcpus[i]->vcpu)
                return pcpus[i];
        }
    }

    return NULL;
}

/* -----------------------------------------------------------------------
 * functions to allocate and associate a pcpu with a guest.
 *
 * note that allocation and asscoation are seperate f1unction since
 * we might want to stop a running core and associate it with a new guest
 * (e.g. when calling a trusted/service guest)
 */



/**
 * @name cman_pcpu_allocate
 * @type function
 * @description allocate a new pcpu for this guest/vcpu pair
 * @return pcpu associated with guest/vcpu or NULL
 */
struct pcpu *cman_pcpu_allocate(struct guest *g, struct vcpu *v)
{
    struct pcpu *pcpu;

    spinlock_lock(&cman_lock);
    pcpu = cman_pcpu_find(PCPU_STATE_FREE, g->cpumask_pcpu & ~g->cpumask_pcpu_running);
    if(pcpu) {
        pcpu->state = PCPU_STATE_RESERVED;
    }
    spinlock_unlock(&cman_lock);
    return pcpu;
}

/**
 * @name cman_pcpu_free
 * @type function
 * @description free a previously allocated pcpu
 */
void cman_pcpu_free(struct pcpu *p)
{
    fatal("not implemented");
}



/**
 * @name cman_pcpu_guest_register
 * @type function
 * @description register the relation between a pcpu and a guest/vcpu pair
 */
void cman_pcpu_guest_register(struct pcpu *pcpu, struct guest *guest, struct vcpu *vcpu)
{
    /* some sanity checks */
    assert_eq("expected pcpu state",
              pcpu->state, PCPU_STATE_RESERVED);

    assert_eq("cpu_vmask_to_pmask() sanity", guest->cpumask_pcpu_running,
              cpu_vmask_to_pmask(guest, guest->cpumask_vcpu_running));

    assert_eq("cpu_pmask_to_vmask() sanity", guest->cpumask_vcpu_running,
              cpu_pmask_to_vmask(guest, guest->cpumask_pcpu_running));

    pcpu->state = PCPU_STATE_READY;
    pcpu->vcpu = vcpu;
    pcpu->guest = guest;
    vcpu->pcpu = pcpu;
    guest->cpumask_vcpu_running |= (1U << vcpu->vid);
    guest->cpumask_pcpu_running |= (1U << pcpu->pid);
}

/**
 * @name cman_pcpu_guest_unregister
 * @type function
 * @description removes the association between a pcpu and its guest/vcpu
 */
void cman_pcpu_guest_unregister(struct pcpu *pcpu)
{
    struct vcpu *vcpu;
    struct guest *g;


    assert("attempt to unregister pcpu from invalid state",
           pcpu->state != PCPU_STATE_RUNNING && pcpu->state != PCPU_STATE_READY);

    vcpu = pcpu->vcpu;
    g = pcpu->guest;

    assert_not_null("sanity check: has guest", g);
    assert_not_null("sanity check: has vcpu", vcpu);

    pcpu->guest->cpumask_vcpu_running &= ~(1U << vcpu->vid);
    pcpu->guest->cpumask_pcpu_running &= ~(1U << pcpu->pid);
    pcpu->vcpu->pcpu = NULL;
    pcpu->vcpu = NULL;
    pcpu->guest = NULL;
    pcpu->state = PCPU_STATE_RESERVED;
}


uint8_t cman_get_num_pcpus_in_cluster(uint8_t cluster)
{
    uint8_t num;

    switch (cluster)
    {
        case 0:
            num = ARCH_CLUSTER0_COUNT;
            break;
        case 1:
            num = ARCH_CLUSTER1_COUNT;
            break;
        case 2:
            num = ARCH_CLUSTER2_COUNT;
            break;
        default:
            fatal("Cluster number out of range");
    }

    return num;
}

/*
 * translation between virtual and physical CPU masks
 */

uint8_t cpu_vmask_to_pmask(struct guest *g, uint8_t vmask)
{
    int i;
    uint8_t ret = 0;

    vmask &= g->cpumask_vcpu_running;
    for(i = 0; i < GUEST_VCPU_MAX; i++, vmask >>= 1) {
        if(vmask & 1) {
            assert_neq("active vcpu has no pcpu", (uint64_t)g->vcpus[i].pcpu, NULL);
            ret |= 1UL << g->vcpus[i].pcpu->pid;
        }
    }
    return ret;
}

uint8_t cpu_pmask_to_vmask(struct guest *g, uint8_t pmask)
{
    int i;
    uint8_t ret = 0;

    pmask &= g->cpumask_pcpu_running;
    for(i = 0; i < ARCH_CORES; i++, pmask >>= 1) {
        if(pmask & 1) {
            assert_neq("active pcpu has nu vcpu", (uint64_t)pcpus[i]->vcpu, NULL);
            ret |= 1UL << pcpus[i]->vcpu->vid;
        }
    }
    return ret;
}
