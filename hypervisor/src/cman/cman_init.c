
#include "defs.h"
#include "init.h"

#include "cman.h"
#include "cman_private.h"

#include "mman.h"
#include "gman.h"
#include "util.h"

void vcpu_init(struct vcpu *vcpu, struct guest *parent, cpuid_t vid)
{
    memset( vcpu, 0, sizeof(struct vcpu));
    vcpu->vid = vid;
}


/*
 * cman local init.
 * this runs on each CPU as they boot and ensures that EL2 is configured
 * correctly for hosting EL1 guests
 *
 */

static void cman_init_local(struct pcpu* pcpu)
{
    uint64_t tmp, reg;
    union reg_hcr hcr_el2;

    /* setup stage 2 MMU and disable it */
    __mmu_stage2_set(-1, 0, 0);

    /* MAIR_EL2: Specifies EL2 stage 1 MMU memory attributes. Selected by page
     * table descriptor index.
     * See D3.4.3 in ARM DDI 0487A.i for hint documentation
     *
     * Attr<n>, n = 0..7. Attr<n> is bits [8n+7:8n]
     *--------------------------------------------------
     * [7:4] Device or normal memory selection, plus temporal and cache policy
     *       allocation hints
     * [3:0] Device/normal subsettings dependent on [7:4]
     *--------------------------------------------------
     * n = 0
     * [7:6] = 11 Normal memory, outer write-back non-transient
     * [5:4] = 11 Outer read/write cache allocation hint = allocate
     * [3:0] = 1111 Normal Memory, Inner Write-back non-transient.
     *              Inner read/write cache allocation hint = allocate
     */
    MSR64("MAIR_EL2", 0xFFULL);

    /* Get PARange, supported physical address range. Bits [3:0] */
    tmp = MRS64("ID_AA64MMFR0_EL1");

    /* TCR_EL2: Controls translation table walks required for the stage 1
     * translation of memory accesses from EL2, and holds cacheability and
     * shareability information for the accesses
     *
     * Bits not shown are reseved (RES0/RES1)
     *--------------------------------------------------
     * 20 TBI: Top Byte ignored, or used for address match in TTBR0_EL2 region
     * 18:16 PS: Physical Address Size
     * 15:14 TG0: Granule size for TTBR0_EL2
     * 13:12 SH0: Shareability attribute for TTBR0_EL2 memory
     * 11:10 ORGN0: Outer cacheability for TTBR0_EL2 memory
     * 9:8 IRGN0: Inner cacheability for TTBR0_EL2 memory
     * 5:0 T0SZ: The size offset of the memory region addressed by TTBR0_EL2.
                 The region size is 2^(64-T0SZ) bytes
     *--------------------------------------------------
     * [20]=0, [18:16]=ID_AA64MMFR0_EL1[2:0], [15:14]=00, [13:12]=10,
     * [11:10]=01, [9:8]=01, [5:0]=(28)10
     * Other bits are set according to RES0/RES1
     *
     * TG0 is 00 = 4kB granule size
     * SH0 is 00, non-shareable, as it is only for this core
     * T0SZ=28 gives size 2^36, which is = PARange
     */
    reg = 0x8080251C | ((tmp & 7) << 16);
    MSR64("tcr_el2", reg);

    /* VTCR_EL2: Controls the translation table walks required for the stage 2
     * translation of memory accesses fromNon-secure EL0 and EL1, and holds
     * cacheability and shareability information for the accesses
     *
     * Bits not shown are reseved (RES0/RES1)
     *--------------------------------------------------
     * 18:16 PS: Physical Address Size
     * 15:14 TG0: Granule size for TTBR0_EL2
     * 13:12 SH0: Shareability attribute for TTBR0_EL2 memory
     * 11:10 ORGN0: Outer cacheability for TTBR0_EL2 memory
     * 9:8 IRGN0: Inner cacheability for TTBR0_EL2 memory
     * 7:6 SL0: Translation table starting level
     * 5:0 T0SZ: The size offset of the memory region addressed by TTBR0_EL2.
                 The region size is 2^(64-T0SZ) bytes
     */

    /* [7:6]=01 */
    reg |= 0x40;
    MSR64("vtcr_el2", reg);

    /* Enable EL2 stage 1 MMU */
    MSR64("TTBR0_EL2", &el2_s1_l1); /* Set EL2 stage 1 translation table base address */
    MSR64("SCTLR_EL2", MRS64("SCTLR_EL2") & 0x01); /* Enable EL2 stage 1 MMU */
    /* Test address translation */
    {
        uint64_t vadr=0xF97FFFFF, padr = 0; /* hikey last device address */
        padr = __mmu_translate_el2_s1(vadr);
        padr = (padr & 0xFFFFFFFFFFE00000) | (vadr & 0x1FFFFF);
        assert_eq("EL2 stage 1 vmem translation failed", vadr, padr);
    }

    /* Populate CPU type register for virtualized non-secure EL1 reads of MIDR_EL1 */
    MSR64("vpidr_el2", MRS64("midr_el1"));

    /* CPU and trap configurations */
    /* Cortex-A53 hcr_el2[63:34] is RES0 and register reset value is 0x02 */
    hcr_el2.all = MRS64("hcr_el2");
    hcr_el2.twi = 1;    /* trap WFI */
    hcr_el2.twe = 1;    /* trap WFE */
    hcr_el2.rw = 1;     /* guest is 64-bit */
    /* hcr_el2.ptw is left at 0 */
    MSR64("hcr_el2", hcr_el2.all );

    /* SANITY: disable aarch32 access to coprocessors */
    MSR64("hstr_el2", 0);
}



static void cman_init_global(struct pcpu *pcpu)
{
    int i, len;
    struct dt_block prop;
    uint64_t mpidr;
    cpuid_t cpuid;
    uint32_t flags;

    /* get cman lock initialized as soon as possible */
    spinlock_init(&cman_lock, 0);

    /* get cpu topology from config */
    config_get_prop(NULL, &prop, "topology", 0);
    len = prop.data_len / (2 * sizeof(uint32_t));
    assert_eq("Toplogy & ARCH_CORES size mismatch", len, ARCH_CORES);


    /* topology sanity check & setup, also mark enabled cores */
    for(i = 0; i < ARCH_CORES; i++) {
        mpidr = dtend( prop.data.num[i * 2 + 1]);

        /* sanity check */
        assert_eq("topology check cpuid (1)", i, mpidr_to_cpuid(mpidr));
        assert_eq("topology check cpuid (2)", i, __mpidr_to_cpuid(mpidr));
        assert_eq("topology check mpidr (2)", mpidr, cpuid_to_mpidr(i));

        flags = dtend( prop.data.num[i * 2 + 0]);
        if(!(flags & 1))
            pcpus[i]->state = PCPU_STATE_FREE;

        pcpus[i]->cluster_num = mpidr_get_cluster(mpidr);
        pcpus[i]->cluster_core = mpidr_get_core(mpidr);
    }

    /* Initialize system global EL2 stage 1 page table. Identity translation.
     * Note that this translation treats the entire system memory map as normal
     * (not device) memory.
     * Device memory is mapped by the respective target init code, as the device
     * memory map differs between targets.
     */
    {
        int l1_index;
        adr_t oa_l1;

        for (l1_index = 0; l1_index < MMAN_PT_ENTRIES; l1_index++) {
            oa_l1 = (adr_t)l1_index << (12 + 9 + 9); /* Identity translation block OA*/
            mmu_pt_add_map(&el2_s1_l1, l1_index, oa_l1,
                ATTR_EL23_S1_BP_MEM | ATTR_SH_OS | ENTRY_TYPE_BLOCK_L12, MEM_OA_L1_ADDR_MASK);
        }
    }

    /* debug */
    dprintf("CPU map:\n");
    for(i = 0; i < ARCH_CORES; i++) {
        dprintf("\t%i -> PCPU=%x PID=%d MPIDR=%x (%d:%d) STATE=%d STACK=%x\n",
                i, pcpus[i], pcpus[i]->pid, cpuid_to_mpidr(pcpus[i]->pid),
                pcpus[i]->cluster_core, pcpus[i]->cluster_num,
                pcpus[i]->state, pcpus[i]->stack);
    }

    /* sanity checks */
    assert_neq("Primary pCPU is not disabled",
               cman_pcpu_get_safe()->state,  PCPU_STATE_DISABLED);
}


/**
 * @name cman_init_early
 * @type function
 * @module cpu manager
 * @descrption early initialization while primary is on temp stack
 * basically, get pcpus ready so we can finish the late initialization
 */
void cman_init_early()
{
    int i;
    size_t size;
    struct coredata *cds;

    /* pcpu and coredata memory  layout sanity check */
    if(sizeof(struct coredata) != COREDATA_SIZE)
        fatal("coredata struct is of incorrect size");

    /* allocate memory for all pcpu structures */
    size = ARCH_CORES * COREDATA_SIZE;
    if(!mman_memory_get(MEMORY_HV, (adr_t *) &cds,
                        size, COREDATA_SIZE))
        fatal("Could not allocate pcpu region");

    /* initialize pcpus */
    for(i = 0; i < ARCH_CORES; i++, cds++) {
        pcpus[i] = (struct pcpu *) & cds->pcpu;
        pcpus[i]->pid = i;
        pcpus[i]->state = PCPU_STATE_DISABLED;
        pcpus[i]->stack = &cds->stack_top[0];
        spinlock_init(& pcpus[i]->lock, 0);

        canary_init(&cds->c0);
        canary_init(&cds->c1);
    }


}

/**
 * @name cman_init_late
 * @type function
 * @module cpu manager
 * @param0 1 if this is global (primary core) initialization 0 otehrwise
 * @param1 the pcpu this concerns, NULL if global init
 * @descrption handles initialization of cpu manager on primary core
 */
void cman_init(int global, struct pcpu *pcpu)
{
    if(global)
        cman_init_global(pcpu);
    else
        cman_init_local(pcpu);

    /* initialize the cman components */
    gic_init(global, pcpu);
    cman_call_init(global);

    cman_timer_init(global, pcpu);
}
