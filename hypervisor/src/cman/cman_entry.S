
/*
 * exception handler vector
 */

#include "defs.h"

#include "cman.h"
#include "cman_private.h"


    .text
    .align 3
    .global el2_vectors
    .global __el2_restore_64_stack
    .global __el2_restore_64_ptr


   .macro DUMMY_HANDLER	adr
        .align 7
    \adr:
        mrs x0, currentel
        mrs x1, spsr_el2
        mrs x2, esr_el2
        lsr x3, x2, #26

        mrs x10, AFSR0_EL2
        mrs x11, AFSR1_EL2
        mrs x12, FAR_EL2

        /* see how it translates in stage 2 */
        at S12E1W, x12
        mrs x3, PAR_EL1

        b __die
            CHECK_DISTANCE \adr . 0x80
    .endm


    .align 11
el2_vectors:
    /* from EL2, use SP_EL0 */
    DUMMY_HANDLER el2_sync_sp0
    DUMMY_HANDLER el2_irq_sp0
    DUMMY_HANDLER el2_fiq_sp0
    DUMMY_HANDLER el2_error_sp0

    /* from EL2, use SP_EL2 */
    HANDLE_EXCEPTION 2 el2_handle_sync_hyp __el2_restore_64_stack 1
    HANDLE_EXCEPTION 2 el2_handle_irq_hyp __el2_restore_64_stack 0
    DUMMY_HANDLER el2_fiq
    DUMMY_HANDLER el2_error

    /* from lower EL in aarch64 */
    HANDLE_EXCEPTION 2 el2_handle_sync __el2_restore_64_stack 1
    HANDLE_EXCEPTION 2 el2_handle_irq __el2_restore_64_stack 0
    HANDLE_EXCEPTION 2 el2_handle_fiq __el2_restore_64_stack 0
    HANDLE_EXCEPTION 2 el2_handle_serror __el2_restore_64_stack 1


    /* got lower EL in aarch32 */
    HANDLE_EXCEPTION 2 el2_handle_sync __el2_restore_64_stack 1 32
    DUMMY_HANDLER el2_irq_32
    DUMMY_HANDLER el2_fiq_32
    DUMMY_HANDLER el2_error_32


/*
 * register registers from the given pointer
 */
__el2_restore_64_ptr:

    /* push the entry to stack so we can use __el2_restore_64_stack */
    add x1, x0, #REGISTERS_SIZE
1:
    ldp x3, x4, [x1, #-16]!
    stp x3, x4, [sp, #-16]!
    cmp x0, x1
    blt 1b
    /* falls through */

/*
 *  common code for restoring GP registers
 */
__el2_restore_64_stack:
    RESTORE_REGISTERS 2
    eret
