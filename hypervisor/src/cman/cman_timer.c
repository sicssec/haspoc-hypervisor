
/*
 * hypervisor timer
 */

#include "defs.h"

#include "cman.h"
#include "cman_private.h"

#include "util.h"
#include "vgic.h"

#include <minilib.h>
#include <gicv2.h>



/*
 * the CPU hyp timer
 */

static void cpu_timer_hyp_restart()
{
    MSR64("CNTHP_CVAL_EL2", 0x00000000);
#if defined(TARGET_fvp)
    MSR64("CNTHP_TVAL_EL2", 0x20000000);
#else     /* hardware is fast enough to use longer periods */
    MSR64("CNTHP_TVAL_EL2", 0x80000000);
#endif

    MSR64("CNTHP_CTL_EL2", 1);
}

static spinlock_t debug_lock = 0;

void cpu_timer_hyp_handler(struct pcpu *pcpu, struct registers *regs, uint32_t iar)
{
    cpu_timer_hyp_restart();

    spinlock_lock(&debug_lock);
    dprintf("EL2 tick, PC=%X PSR=%x\n", regs->pc, regs->cpsr);
    debug_dump_guest( pcpu->guest);
    debug_dump_registers( regs);
    debug_dump_gic();
    spinlock_unlock(&debug_lock);

}

static void cpu_timer_hyp_init(struct pcpu *pcpu)
{
    int id;

    id = gicv2_get_irqid(GIC_IRQ_TYPE_PPI, IRQ_PPI_CNTHP);
    MSR64("CNTHP_CTL_EL2", 0);
    gicv2_dist_set_enable(_gicd, id, 1);
    gicv2_dist_set_config(_gicd, id, GICV2_ICFG_1N | GICV2_ICFG_EDGE);
    gicv2_dist_set_priority(_gicd, id, 0x80);
    gic_pirq_callback_set(pcpu, id,  cpu_timer_hyp_handler);
    cpu_timer_hyp_restart();
}

/*
 * The guest timer
 */

static void cpu_timer_guest_init(struct pcpu *pcpu)
{
    /* just disable it */
    MSR64("CNTVOFF_EL2", 0); /* no timer offset */
    MSR64("CNTHCTL_EL2", 7); /* gives EL0/1 access to physical timers */
    MSR64("CNTP_CTL_EL0", 0); /* physical timer disabled for now */
}


/*
 * CPU timer init
 */
void cman_timer_init(int global, struct pcpu *pcpu)
{
    if(!global) {
        cpu_timer_guest_init(pcpu);
        cpu_timer_hyp_init(pcpu);
    }
}
