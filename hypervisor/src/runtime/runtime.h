#ifndef __RUNTIME_H__
#define __RUNTIME_H__

#include "modules.h"
#include "drivers.h"
#include "services.h"

#ifdef __ASSEMBLER__

#else /* __ASSEMBLER__ */

extern void runtime_init(bool global, struct pcpu *pcpu);

#endif /* __ASSEMBLER__ */

#endif /* __RUNTIME_H__ */
