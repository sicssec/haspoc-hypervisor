/*
 * Hyprvisor drivers are responsible for managing guest access vialoation
 * and interrups
 *
 * This file contains a very naive and simple implementation of memory
 * drivers, which will be replaced with something better in due time
 */

#include "defs.h"
#include "drivers.h"
#include "gman.h"



/*
 * helper functions to simplify accessing memory from a driver
 */

bool mdriver_memory_read(adr_t padr, uint64_t *val_,
                          uint32_t size_mask, union reg_esr esr)
{
    uint64_t val;
    uint32_t size;

    /* is this access size allowed? */
    size = 1UL << esr.sas;
    if(! (size & size_mask))
        return false;

    /* check alignment too */
    if(padr & (size-1))
        return false;

    /* cant read to xzr */
    if(esr.srt == 31)
        return false;

    switch(esr.sas) {
    case 0:
        val = *((uint8_t *) padr);
        break;
    case 1:
        val = *((uint16_t *) padr);
        break;
    case 2:
        val = *((uint32_t *) padr);
        break;
    case 3:
        val = *((uint64_t *) padr);
        break;
    default:
        val = 0;
    }

#if DEBUG > 3
    dprintf("[mdriver] memory read sas=%d padr=%x val=%X\n",esr.sas, padr, val);
#endif

    *val_ = val;
    return true;
}

bool mdriver_memory_write(adr_t padr, uint64_t val,
                           uint32_t size_mask, union reg_esr esr)
{
    uint32_t size;

    /* is this access size allowed ? */
    size = 1UL << esr.sas;
    if(!(size & size_mask))
        return false;

    /* check alignment too */
    if(padr & (size-1))
        return false;

    switch(esr.sas) {
    case 0:
        *((uint8_t *) padr) = val;
        break;
    case 1:
        *((uint16_t *) padr) = val;
        break;
    case 2:
        *((uint32_t *) padr) = val;
        break;
    case 3:
        *((uint64_t *) padr) = val;
        break;
    }

#if DEBUG > 3
    dprintf("[mdriver] memory write sas=%d padr=%x val=%X\n",esr.sas, padr, val);
#endif

    return true;
}



/*
 * access sanity check for a mdriver
 */
bool mdrivers_check_access(struct mdriver *drv, adr_t ipadr, union reg_esr esr)
{
    /* don't know what to decode */
    if(!esr.il || !esr.isv) {
        fatal("Failed because il isv");
        return false;
    }

    /* can't handle these */
    if(esr.s1ptw || esr.ea) {
        fatal("Failed s1ptv ea");
        return false;
    }


    /* cant read to xzr */
    if(!esr.wnr && esr.srt == 31)
        return false;

    /* check read/write width */
    if(!((esr.wnr ? drv->mask_read : drv->mask_write) &
         (1UL << esr.sas)))
        return false;

    /* check address alignment */
    if(! drv-> allow_unaligned && (ipadr & ((1UL << esr.sas)-1)) != 0)
        return false;

    /* we are okay */
    return true;
}

struct mdriver *mdrivers_find(const char *name)
{
    struct module *module = modules_find(name, MODULE_TYPE_DRIVER);
    return module ? (struct mdriver *) module->ptr : NULL;
}


void mdrivers_init(bool global, struct pcpu *pcpu)
{
    int i;
    struct module *m;
    struct mdriver *md;

    if(global) {
        modules_dump("Available drivers", MODULE_TYPE_DRIVER);
    }

    for(i = 0;;) {
        m = modules_itr(MODULE_TYPE_DRIVER, &i);
        if(!m) break;
        md = (struct mdriver *) m->ptr;

        if(global) {
            /* some sanity checks */
            if(!md->ops_guest_read || !md->ops_guest_write)
                fatal("definition of mdriver %s is incomplete", m->name);

            assert_neq("mdriver no read/write mask set", 0, md->mask_read | md->mask_write);

            /* misc updates before the first init */
            md->module = m;
        }

        if(md->ops_init)
            md->ops_init(md, global, pcpu);
    }
}
