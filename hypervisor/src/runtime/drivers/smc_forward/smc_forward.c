#include "defs.h"
#include "cman.h"

// If this driver is registered first it will forward ALL SMC's
uint32_t smc_fwd_ccallback(struct pcpu *pcpu, struct registers *regs, int smc)
{
    /*Just forward everything*/
    __cpu_smc_forward(regs);
    /* regs->x[0] has been set after smc and needs to be returned
     * as top function will overwrite regs->x[0] again with return value */
   return regs->x[0];
}

void smc_fwd_driver_init()
{
    cdriver_register(NULL, smc_fwd_ccallback);
}
