#ifndef _DRIVER_SIMPLE_H_
#define _DRIVER_SIMPLE_H_

/*
 * simple drivers are a set of simple & generic driver implementations
 */

extern void mdriver_pass_init(struct mdriver *d);
extern void mdriver_block_init(struct mdriver *d);
extern void mdriver_limit_init(struct mdriver *d,
                               uint8_t offset, uint8_t shift,
                               uint8_t wmask, uint8_t rmask);

#endif /* _DRIVER_SIMPLE_H_ */
