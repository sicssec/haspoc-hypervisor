/*
 * a set of simple mdrivers, mostly to be used as a examples
 */


#include <stdint.h>

#include "defs.h"
#include "drivers.h"

/*
 * a dummy mdriver that will block all access
 */


static bool mdriver_block_read(struct mdriver *drv, union reg_esr esr,
                             adr_t ipadr, uint64_t *val)
{
    return false;
}

static bool mdriver_block_write(struct mdriver *drv, union reg_esr esr,
                                adr_t ipadr, uint64_t val)
{
    return false;
}

static struct mdriver block = {
    .mask_read = MASK_BYTE8 | MASK_WORD16 | MASK_WORD32 | MASK_WORD64,
    .mask_write = MASK_BYTE8 | MASK_WORD16 | MASK_WORD32 | MASK_WORD64,

    .ops_guest_read = mdriver_block_read,
    .ops_guest_write = mdriver_block_write,
};

DRIVER_DECLARE("block", _block, &block);


/*
 * a dummy pass-mdriver that will allow all access
 */

static bool mdriver_pass_read(struct mdriver *drv, union reg_esr esr,
                              adr_t ipadr, uint64_t *val)
{
    uint64_t padr = drv->padr - drv->vstart + ipadr;
    return mdriver_memory_read(padr, val, -1, esr);
}

static bool mdriver_pass_write(struct mdriver *drv, union reg_esr esr,
                               adr_t ipadr, uint64_t val)
{
    uint64_t padr = drv->padr - drv->vstart + ipadr;
    return mdriver_memory_write(padr, val, -1, esr);
}

static struct mdriver pass = {
    .mask_read = MASK_BYTE8 | MASK_WORD16 | MASK_WORD32 | MASK_WORD64,
    .mask_write = MASK_BYTE8 | MASK_WORD16 | MASK_WORD32 | MASK_WORD64,
    .ops_guest_read = mdriver_pass_read,
    .ops_guest_write = mdriver_pass_write,
};

DRIVER_DECLARE("pass", _pass, &pass);



/*
 * the limit mdriver is a driver that gives the guest access to a subset of one page
 */

union mdriver_limit {
    uint64_t all;
    struct {
        uint64_t offset : 8;
        uint64_t wmask, rmask : 4;
        uint64_t cshift : 5;
    };
};

/* check if we can perform this access */
static int mdriver_limit_check(struct mdriver *drv, adr_t ipadr,
                               union reg_esr esr, int write, adr_t *padr)
{
    adr_t index;
    uint32_t size;
    union mdriver_limit l = { .all = drv->user1 };

    /* make sure ipadr is aligned to its size, or we can go outside our range */
    size = 1 << esr.sas;
    if(ipadr & (size-1))
        return 0;

    *padr = drv->padr - drv->vstart + ipadr + l.offset;
    index = (ipadr - drv->vstart) >> l.cshift;

    if(index & ~31)
        return 0;  /* outside our mask anyway */
    else
        return (1UL << index)  & (write ? l.wmask : l.rmask);
}

static bool mdriver_limit_read(struct mdriver *drv, union reg_esr esr,
                               adr_t ipadr, uint64_t *val)
{
    adr_t padr;
    union mdriver_limit l = { .all = drv->user1 };

    /* can access this region ? */
    if(!mdriver_limit_check(drv, ipadr, esr, 0, &padr))
        return false;

    return mdriver_memory_read(padr, val, l.rmask, esr);
}

static bool mdriver_limit_write(struct mdriver *drv, union reg_esr esr,
                                adr_t ipadr, uint64_t val)
{
    adr_t padr;
    union mdriver_limit l = { .all = drv->user1 };

    /* can access this region ? */
    if(!mdriver_limit_check(drv, ipadr, esr, 1, &padr))
        return false;

    return mdriver_memory_write(padr, val, l.wmask, esr);
}

static void mdriver_limit_guest_init(struct mdriver *obj, struct guest *g,
                                     struct dt_block *config)
{
    union mdriver_limit data;

    data.rmask = config_get_prop_int(config, "rmask");
    data.wmask = config_get_prop_int(config, "wmask");
    data.offset = config_get_prop_int(config, "offset");
    data.cshift = config_get_prop_int(config, "cshift");

    obj->user1 = data.all;
}


static struct mdriver limit = {
    .mask_read = MASK_BYTE8 | MASK_WORD16 | MASK_WORD32 | MASK_WORD64,
    .mask_write = MASK_BYTE8 | MASK_WORD16 | MASK_WORD32 | MASK_WORD64,

    .ops_guest_read = mdriver_limit_read,
    .ops_guest_write = mdriver_limit_write,
};

DRIVER_DECLARE("limit", _limit, &limit);
