#ifndef _DRIVER_H_
#define _DRIVER_H_

#ifdef __ASSEMBLER__

#else /* __ASSEMBLER__ */

/* forward reference */
struct registers;
struct guest;
struct pcpu;

/*
 * a mdriver is a handler for trapped memory acccess.
 * the access logic is defined by the ops callbacks
 */
struct mdriver {
    adr_t vstart, vend;
    adr_t padr;

    /* access sanity flags */
#define MASK_BYTE8  _BV(0)
#define MASK_WORD16 _BV(1)
#define MASK_WORD32 _BV(2)
#define MASK_WORD64 _BV(3)
#define MASK_ALL 15
    uint32_t mask_read : 4;
    uint32_t mask_write : 4;
    uint32_t allow_unaligned : 1;

    void (*ops_init)(struct mdriver *drv, bool global, struct pcpu *pcpu);
    void (*ops_guest_init)(struct mdriver *obj, struct guest *g,
                           struct dt_block *config);

    /* read/write ops */
    bool (*ops_guest_read)(struct mdriver *drv, union reg_esr esr,
                           adr_t ipadr, uint64_t *val);
    bool (*ops_guest_write)(struct mdriver *drv, union reg_esr esr,
                            adr_t ipadr, uint64_t val);

    struct module *module;

    /* optional parameters, usage depends on the driver */
    uint64_t user1;
    void *user2;
};

/*
 * the driver functions
 */

#define DRIVER_DECLARE(name_,id_, ptr_) \
    MODULE_DECLARE(name_,driver_##id_, ptr_, MODULE_TYPE_DRIVER, 0)

extern bool mdrivers_check_access(struct mdriver *driver,
                                  adr_t ipadr, union reg_esr esr);
extern struct mdriver *mdrivers_find(const char *name);
extern void mdrivers_init(bool global, struct pcpu *pcpu);


/* helpers */
extern bool mdriver_memory_read(adr_t padr, uint64_t *val_,
                                uint32_t size_mask, union reg_esr esr);
extern bool mdriver_memory_write(adr_t padr, uint64_t val,
                                 uint32_t size_mask, union reg_esr esr);


/*
 * idriver is a driver for interrupt signals
 */

/**
 * @name icallback
 * @type typedef
 * @description interrupt handler callback function
 */
typedef void (*icallback)(struct pcpu *, struct registers *r, uint32_t iar);



/*
 * cdriver is a driver for callupp
 */

/**
 * @name ccallback
 * @type typedef
 * @description down-call handler callback function
 */
typedef uint32_t (*ccallback)(struct pcpu *, struct registers *r, int op);

struct cdriver_list {
#define CDRIVER_LIST_SIZE 8
    int count;
    ccallback func[CDRIVER_LIST_SIZE];
};

extern uint32_t cdriver_process(struct cdriver_list *cl, struct pcpu *p,
                                struct registers *r, int op);
extern void cdriver_register(struct cdriver_list *cl, ccallback cb);
extern void cdriver_list_init(struct cdriver_list *cl);

/* include the predefined simple drivers */
#include "driver_simple.h"


#endif /* __ASSEMBLER__ */


#endif /* _DRIVER_H_ */
