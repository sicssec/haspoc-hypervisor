/*
 * cdriver's handle call-up such as SMC and HVC
 *
 */

#include "defs.h"
#include "cman.h"
#include "drivers.h"

/* set of global cdrivers than don't belong to a particular cpu, guests etc */
static struct cdriver_list global_cdrivers = { .count = 0 };

uint32_t cdriver_process(struct cdriver_list *cl, struct pcpu *pcpu,
                         struct registers *regs, int smc)
{
    uint32_t ret = SMC_RET_UNKNOWN;
    int i;

    if(!cl)
        cl = &global_cdrivers;

    for(i = 0; i < cl->count; i++) {
        ret = cl->func[i](pcpu, regs, smc);
        if(ret != SMC_RET_UNKNOWN)
            break;
    }

    return ret;
}

void cdriver_register(struct cdriver_list *cl, ccallback cb)
{
    if(!cl)
        cl = &global_cdrivers;

    if(cl->count >= CDRIVER_LIST_SIZE)
        fatal("cdriver list is full");

    cl->func[cl->count++] = cb;
}

void cdriver_list_init(struct cdriver_list *cl)
{
    cl->count = 0;
}
