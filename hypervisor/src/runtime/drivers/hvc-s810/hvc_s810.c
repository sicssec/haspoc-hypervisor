#include "defs.h"
#include "cman.h"
#include "gman.h"
#include "init.h"

/* HVC call function IDs (register x0) for the guest */

#define HVC_CALL_AVAILIBLE              0xC3FFFFFF
#define HVC_CALL_OPTEE_INIT             0xC3FF0001
#define TEEHVC_OPTEED_RETURN_ENTRY_DONE 0xBE000000

#define TEEHVC_RETURN_RPC_PREFIX        0xFFFF0000
#define TEEHVC_CALL_WITH_ARG            0x32000002
#define TEEHVC_RETURN_FROM_RPC          0x32000003
#define TEEHVC_OPTEED_RETURN_CALL_DONE  0xBE000005

#define TEEHVC_ST_FUNCID_GET_SHM_CONFIG 0xB2005700

#define OPTEE_SMC_OFFSET 4; /* due to smc logic */

static uint64_t optee_entry = 0;

static uint32_t hvc_s810_go_optee_call(
    struct pcpu *pcpu, struct registers *regs, int64_t entry_off)
{
    /* TODO: check that current guest is android */
    /* store android */

    dprintf("call\n");
    dprintf("\tX0: %x, X1: %x, X2: %x, X3: %x\n",
            regs->x[0], regs->x[1], regs->x[2], regs->x[3]);

    cman_vstate_store(
        pcpu->guest, &pcpu->guest->vcpus[pcpu->pid], regs);
    /* restore optee */
    pcpu->guest = gman_guest_find("OPTEE");
    pcpu->vcpu = &pcpu->guest->vcpus[0];
    pcpu->vcpu->vstate.gp.pc = optee_entry + entry_off;
    pcpu->vcpu->vstate.gp.x[0] = regs->x[0];
    pcpu->vcpu->vstate.gp.x[1] = regs->x[1];
    pcpu->vcpu->vstate.gp.x[2] = regs->x[2];
    pcpu->vcpu->vstate.gp.x[3] = regs->x[3];
    pcpu->vcpu->vstate.gp.x[4] = regs->x[4];
    pcpu->vcpu->vstate.gp.x[5] = regs->x[5];
    pcpu->vcpu->vstate.gp.x[6] = regs->x[6];
    pcpu->vcpu->vstate.gp.x[7] = regs->x[7];
    cman_vstate_restore(pcpu->guest, pcpu->vcpu, regs);

    /* call optee */
    return regs->x[0];
}

static uint32_t hvc_s810_go_optee_rpc(
    struct pcpu *pcpu, struct registers *regs, int64_t entry_off)
{
    /* TODO: check that current guest is android */
    /* store android */

    dprintf("return rpc\n");
    dprintf("\tX0: %x, X1: %x, X2: %x, X3: %x\n",
            regs->x[0], regs->x[1], regs->x[2], regs->x[3]);

    cman_vstate_store(
        pcpu->guest, &pcpu->guest->vcpus[pcpu->pid], regs);
    /* restore optee */
    pcpu->guest = gman_guest_find("OPTEE");
    pcpu->vcpu = &pcpu->guest->vcpus[0];
    pcpu->vcpu->vstate.gp.pc = optee_entry + entry_off;
    pcpu->vcpu->vstate.gp.x[0] = regs->x[0];
    pcpu->vcpu->vstate.gp.x[1] = regs->x[1];
    pcpu->vcpu->vstate.gp.x[2] = regs->x[2];
    pcpu->vcpu->vstate.gp.x[3] = regs->x[3];
    cman_vstate_restore(pcpu->guest, pcpu->vcpu, regs);

    /* call optee */
    return regs->x[0];
}

static uint32_t hvc_s810_go_android(
    struct pcpu *pcpu, struct registers *regs)
{
    dprintf("done\n");
    dprintf("\tX0: %x, X1: %x, X2: %x, X3: %x\n",
            regs->x[0], regs->x[1], regs->x[2], regs->x[3]);

    cman_vstate_store(pcpu->guest, &pcpu->guest->vcpus[0], regs);
    pcpu->guest = gman_guest_find("Android");
    pcpu->vcpu = &pcpu->guest->vcpus[0];

    if ((regs->x[1] & TEEHVC_RETURN_RPC_PREFIX) == TEEHVC_RETURN_RPC_PREFIX) {
        pcpu->vcpu->vstate.gp.x[0] = regs->x[1];
        pcpu->vcpu->vstate.gp.x[1] = regs->x[2];
        pcpu->vcpu->vstate.gp.x[2] = regs->x[3];
    } else {
        pcpu->vcpu->vstate.gp.x[0] = regs->x[1];
        pcpu->vcpu->vstate.gp.x[1] = regs->x[2];
        pcpu->vcpu->vstate.gp.x[2] = regs->x[3];
        pcpu->vcpu->vstate.gp.x[3] = regs->x[4];
    }

    cman_vstate_restore(pcpu->guest, &pcpu->guest->vcpus[pcpu->pid], regs);
    return regs->x[0];
}

/* HVC API for s810 platform. Used for demo purposes*/
uint32_t hvc_s810_ccallback(struct pcpu *pcpu, struct registers *regs, int smc)
{
    uint32_t ret = SMC_RET_UNKNOWN;
    int64_t entry_off = 0;

    if (smc)
        entry_off -= OPTEE_SMC_OFFSET;

    switch(regs->x[0]) {
    case HVC_CALL_AVAILIBLE:
        dprintf("The STH hypervisor says hello from EL2\n"); ret = 0;
        break;
    case TEEHVC_OPTEED_RETURN_ENTRY_DONE:
        dprintf("Returned from OPTEE initialization\n");
        optee_entry = regs->x[1];
        /* store optee on "cpu 0" */
        cman_vstate_store(pcpu->guest, &pcpu->guest->vcpus[0], regs);
        pcpu->guest = gman_guest_find("Android");
        pcpu->vcpu = &pcpu->guest->vcpus[pcpu->pid];
        boot_guest(pcpu);
        /* Will not return Cya!*/
        break;
    case TEEHVC_OPTEED_RETURN_CALL_DONE:
        /* TODO: check that current guest is OPTEE */
        /* TODO: Make sure only one process is executing in OPTEE */
        /* store OPTEE */
        ret = hvc_s810_go_android(pcpu, regs);
        break;
    case TEEHVC_ST_FUNCID_GET_SHM_CONFIG:
        entry_off += 4;
        ret = hvc_s810_go_optee_call(pcpu, regs, entry_off);
        break;
    case TEEHVC_CALL_WITH_ARG:
        ret = hvc_s810_go_optee_call(pcpu, regs, entry_off);
        break;
    case TEEHVC_RETURN_FROM_RPC:
        ret = hvc_s810_go_optee_rpc(pcpu, regs, entry_off);
        break;
    default:
        return SMC_RET_UNKNOWN;
    }

    return ret;
}

void hvc_s810_driver_init()
{
    cdriver_register(NULL, hvc_s810_ccallback);
}
