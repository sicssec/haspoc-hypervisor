
#include "defs.h"

#include "drivers.h"
#include "cman.h"
#include "gman.h"
#include "util.h"
#include "mman.h"
#include "igc.h"

#define IGC_MAGIC_VALUE 0xAA640CCA
#define IGC_REG_MAGIC   0
#define IGC_REG_ID      1
#define IGC_REG_DATA_VERSION 2
#define IGC_REG_NOTIFY  3
#define IGC_REG_WRITE_ADR   4
#define IGC_REG_WRITE_SIZE  5
#define IGC_REG_READ_ADR    6
#define IGC_REG_READ_SIZE   7


static bool mdriver_igc_read(struct mdriver *drv, union reg_esr esr,
                             adr_t ipadr, uint64_t *val_)
{
    const uint64_t index = ipadr - drv->vstart;
    uint32_t val = 0;
    struct igc_channel *chan;
    struct igc_endpoint *ep_me, *ep_other;

    ep_me = (struct igc_endpoint *) drv->user2;
    ep_other = igc_channel_endpoint_other(ep_me);
    chan = igc_channel_from_endpoint(ep_me);

    switch(index / 4) {
    case 0:
        val = IGC_MAGIC_VALUE;
        break;
    case IGC_REG_ID:
        val = 0; /* TODO */
        break;
    case IGC_REG_DATA_VERSION:
        val = ep_other->data_version;
        break;
    case IGC_REG_WRITE_ADR:
        val = ep_me->vadr_write;
        break;
    case IGC_REG_WRITE_SIZE:
        val = ep_me->size_write;
        break;
    case IGC_REG_READ_ADR:
        val = ep_me->vadr_read;
        break;
    case IGC_REG_READ_SIZE:
        val = ep_me->size_read;
        break;

    default:
        goto bad_access;
    }

    *val_ = val;
    return true;

bad_access:
    return false;
}

static bool mdriver_igc_write(struct mdriver *drv, union reg_esr esr,
                              adr_t ipadr, uint64_t val)
{
    const uint64_t index = ipadr - drv->vstart;
    struct igc_channel *chan;
    struct igc_endpoint *ep_me, *ep_other;
    struct pcpu *pcpu;


    ep_me = (struct igc_endpoint *) drv->user2;
    ep_other = igc_channel_endpoint_other(ep_me);
    chan = igc_channel_from_endpoint(ep_me);

    switch(index / 4) {
    case IGC_REG_NOTIFY:
        pcpu = cman_pcpu_get();
        if(!igc_channel_endpoint_notify(pcpu, ep_me))
            fatal("ERROR: could not notify IGC endpoint");
        break;

    default:
        goto bad_access;
    }


    return true;

bad_access:
    return false;
}



static void igc_guest_init(struct mdriver *obj, struct guest *g, struct dt_block *config)
{
    /* note that config is NULL */
}

static struct mdriver igc = {
    .mask_read = MASK_WORD32,
    .mask_write = MASK_WORD32,

    .ops_init = driver_igc_init,
    .ops_guest_init = igc_guest_init,
    .ops_guest_read = mdriver_igc_read,
    .ops_guest_write = mdriver_igc_write,
};

DRIVER_DECLARE("igc", _igc, &igc);
