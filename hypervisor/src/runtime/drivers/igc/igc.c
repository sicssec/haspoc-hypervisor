
#include "defs.h"

#include <minilib.h>
#include <devicetree.h>
#include <bundle.h>

#include "drivers.h"
#include "cman.h"
#include "gman.h"
#include "mman.h"
#include "util.h"

#include "gic.h"
#include "vgic.h"
#include "igc.h"

#define IGC_MAX_CHANNELS 4


static struct igc_root {
    struct igc_channel channels[IGC_MAX_CHANNELS];
    int igc_channel_count;
} igc_root;

/*
 * IGC operations
 */

static struct igc_channel *igc_channel_allocate()
{
    assert("No more IGC channels available",
           igc_root.igc_channel_count < IGC_MAX_CHANNELS);

    return & igc_root.channels[igc_root.igc_channel_count++];
}

struct igc_endpoint *igc_channel_find(struct guest *src, struct guest *dst)
{
    int i;
    struct igc_channel *c;

    for(i = 0; i < igc_root.igc_channel_count; i++) {
        c = & igc_root.channels[i];
        if(c->endpoint[0].guest == src && c->endpoint[1].guest == dst)
            return & c->endpoint[1];
        if(c->endpoint[0].guest == dst && c->endpoint[1].guest == src)
            return & c->endpoint[0];
    }
    return 0;
}

/*
 * connect the endpoint to the other end and setup
 * the ctrl page driver
 */
static void igc_channel_endpoint_connect(struct igc_endpoint *ep,
                                         struct igc_endpoint *other)
{
    int i, n, page;
    struct guest *g = ep->guest;
    struct mdriver *md;

    /* initialize remaining endpoint data */
    ep->data_version = 0;
    ep->size_read = other->size_write;

    /* get some pages in the IGC space to map this channel */
    gman_igc_alloc(g, (ep->size_write + ep->size_read) / PAGE_SIZE, &page);

    /* get vadr for all parts */
    ep->vadr_ctrl = GUEST_MAP_BASE_IGC + page * PAGE_SIZE;
    ep->vadr_write = ep->vadr_ctrl + PAGE_SIZE;
    ep->vadr_read = ep->vadr_write + ep->size_write;
    page++;

    /* add ctrl page and its driver */
    md = gman_mdriver_register(g, NULL, "igc", ep->vadr_ctrl,
                               ep->vadr_ctrl + PAGE_SIZE, 0);
    md->user2 = ep;

    /* map read/write regions */
    gman_mem_map_4k(g, ep->vadr_write, ep->padr_write,
                    ATTR_SHARED_READ_WRITE,ep->size_write / PAGE_SIZE);
    gman_mem_map_4k(g, ep->vadr_read, other->padr_write,
                    ATTR_SHARED_READ, ep->size_read / PAGE_SIZE);
}

/*
 * notify the other end
 */

bool igc_channel_endpoint_notify(struct pcpu *pcpu, struct igc_endpoint *me)
{
    struct igc_channel *chan;
    struct igc_endpoint *other;

    /* find the channel and the reciving end */
    chan = igc_channel_from_endpoint(me);
    other = igc_channel_endpoint_other(me);

    /* update the data version */
    me->data_version++;
    __asm__ volatile("dsb st");

    return vgic_send_virq_to_guest(pcpu, other->guest, other->irq);
}


/*
 * initialize the end point, but dont set up the memories yet
 */
static void igc_channel_endpoint_init(struct igc_endpoint *ep, int index,
                                      struct guest *g, size_t wsize, int irq)
{
    ep->index = index;
    ep->guest = g;
    ep->irq = irq;
    ep->size_write = wsize;
    ep->size_read = 0; /* unknown */
    ep->vadr_ctrl = ep->vadr_write = ep->vadr_read = -1; /* unknown */
}

/*
 * configuration
 */

void igc_register_channel(char *id, struct guest **guests, int *pages, int *irqs)
{
    int i, ret;
    size_t size;
    adr_t padr;
    struct igc_channel *chan;

    /* sanity checks */
    assert("IGC sanity: guest invalid", guests[0] != NULL && guests[1] != NULL);
    assert("IGC sanity: guests not same", guests[0] != guests[1]);

    /* setup igc_channel structure */
    chan = igc_channel_allocate();
    assert("IGC channel allocation failed", chan != NULL);

    chan->id = id;
    for(int i = 0; i < 2; i++) {
        igc_channel_endpoint_init(& chan->endpoint[i], i, guests[i],
                                  pages[i] * PAGE_SIZE, irqs[i]);
    }

    /* allocate memory to be used */
    size = (pages[0] + pages[1]) * PAGE_SIZE;
    ret = mman_memory_get(MEMORY_IGC, &padr, size, PAGE_SIZE);
    assert("IGC memory allocation", ret);

    /* TEMP: manually zero memory since mheap_alloc doesn't yet */
    __memset64(padr, 0, size);

    /* record the physical memory and get the second part */
    chan->endpoint[0].padr_write = padr;
    chan->endpoint[1].padr_write = padr + chan->endpoint[0].size_write;

    /* setup endpoints connections */
    for(i = 0; i < 2; i++) {
        igc_channel_endpoint_connect(&chan->endpoint[i], &chan->endpoint[i^1]);

#if DEBUG > 0
        if(i == 0)
            dprintf("  IGC channel %s @%X\n", chan->id, chan);
        dprintf("    EP%d: ADR=%x:%x:%x WSIZE=%x IRQ=%d %s\n",
                i + 1,
                chan->endpoint[i].vadr_ctrl,
                chan->endpoint[i].vadr_write,
                chan->endpoint[i].vadr_read,
                chan->endpoint[i].size_write,
                chan->endpoint[i].irq,
                chan->endpoint[i].guest->name);
#endif
    }
}

void driver_igc_init(struct mdriver *drv, bool global, struct pcpu *pcpu)
{
    int id;

    if(global) {
        /* initialuize the IGC component */
        igc_root.igc_channel_count = 0;
    }
}
