#ifndef __IGC_H__
#define __IGC_H__

#include "defs.h"
#include "drivers.h"

struct pcpu;
struct guest;
struct igc_channel;


struct igc_endpoint {
    struct guest *guest;

    adr_t padr_write;
    adr_t vadr_ctrl, vadr_write, vadr_read;
    size_t size_write, size_read;

    uint32_t data_version;
    uint16_t irq;
    uint32_t index : 1;
    uint32_t unused: 15;

};

struct igc_channel {
    char *id;
    struct igc_endpoint endpoint[2];
};

static inline struct igc_channel *igc_channel_from_endpoint(struct igc_endpoint *ep)
{
    return GET_CONTAINER(ep, struct igc_channel, endpoint[ep->index]);
}

static inline struct igc_endpoint *igc_channel_endpoint_other(struct igc_endpoint *ep)
{
    return ep + (ep->index ?  -1 : +1);
}

extern bool igc_channel_endpoint_notify(struct pcpu *pcpu, struct igc_endpoint *me);
extern struct igc_endpoint *igc_channel_find(struct guest *src, struct guest *dst);

extern void igc_register_channel(char *id, struct guest **guests, int *pages, int *irqs);
extern void driver_igc_channel_init(struct igc_endpoint *ep);
extern void driver_igc_init(struct mdriver *drv, bool global, struct pcpu *pcpu);

#endif /* __IGC_H__ */
