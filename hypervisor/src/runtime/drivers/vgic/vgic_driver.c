#include <stdint.h>
#include <gicv2.h>


#include "defs.h"
#include "util.h"

#include "drivers.h"
#include "cman.h"
#include "gman.h"
#include "util.h"

#include "vgic.h"
#include "vgic_private.h"


/*
 * vgic read operations
 */

/* read from bit-wide register, set/clear registers return the same value */
static uint32_t vgic_gicd_read1(struct guest *g, uint32_t reg, int bit)
{
    struct virq *virq;
    uint32_t vid;

    /* get the corresponding virq */
    vid = bit + (reg & 0x7F) * 8;
    virq = vgic_virq_get(g, vid);
    if(!virq || virq->state != VIRQ_STATE_ACTIVE)
        return 0;

    /* what type of register and what value is this ? */
    switch(reg >> 8) {
    case 0x100 >> 8: /* ISENABLER, ICENABLER */
        return virq->gicd_enabled;
    case 0x200 >> 8: /* ISPENDR, ICPENDR */
        if(!virq->virt)
            virq->gicd_pending = gicv2_dist_get_pending(_gicd, virq->pid);
        return virq->gicd_pending;
    case 0x300 >> 8: /* ISACTIVER, ICACTIVER */
        if(!virq->virt)
            virq->gicd_active = gicv2_dist_get_active(_gicd, virq->pid);
        return virq->gicd_active;
    default:
        return 0;
    }
}

/* read from 2-bit registers */
static uint32_t vgic_gicd_read2(struct guest *g, uint32_t reg, int bitpair)
{
    struct virq *virq;
    uint32_t vid;

    /* get the corresponding virq */
    vid = bitpair + (reg & 0xFF) * 4;
    virq = vgic_virq_get(g, vid);
    if(!virq || virq->state != VIRQ_STATE_ACTIVE)
        return 0;

    /* what type of register is this ? */
    switch(reg >> 9) {
    case 0xC00 >> 9: /* ICFGR */
        return virq->gicd_cfg;
    default:
        return 0;
    }
}

/* read from byte-wide registers */
static uint32_t vgic_gicd_read8(struct guest *g, uint32_t reg, int byte_)
{
    struct virq *virq;
    uint32_t vid;

    /* get the corresponding virq */
    vid = byte_ + (reg & 0x37F);
    virq = vgic_virq_get(g, vid);
    if(!virq || virq->state != VIRQ_STATE_ACTIVE)
        return 0;

    /* what type of register is this ? */
    switch(reg >> 10) {
    case 0x400 >> 10: /* IPRIORITYR */
        return virq->gicd_prio;
    case 0x800 >> 10:  /* ITARGETSR */
        return virq->gicd_cpumask;
    default:
        return 0;
    }
}

static bool mdriver_vgic_gicd_read(struct mdriver *drv, union reg_esr esr,
                                   adr_t ipadr, uint64_t *val_)
{
    const uint64_t index = ipadr - drv->vstart;
    const uint64_t padr = drv->padr - drv->vstart + ipadr;
    struct guest *g = cman_pcpu_get()->guest;
    uint32_t val = 0;
    int i;

    /* only PRIO and TARGET can do byte access */
    if(esr.sas == 0 && !((index >= 0x400 && index < 0x778) ||
                         (index >= 0x800 && index < 0xB78)))
        goto bad_access;

    switch(index) {
    case 0x000: /* CTLR */
        val = g->vgic_driver.enabled;
        break;

    case 0x004: /* TYPER */
        /* how many lines & cpus are we showing it ? */
        val = (VGIC_MAX_VIRQ / 32 -1) | ((GUEST_VCPU_MAX-1) << 5);
        break;
    case 0x008: /* IIDR */
        val = *(uint32_t *) padr;
        break;

    case 0x080 ... 0x0FC: /* IGROUPR */
        /* do nothing */
        break;

        /* bit access */
    case 0x100 ... 0x17C: /* ISENABLER */
    case 0x180 ... 0x1FC: /* ICENABLER */
    case 0x200 ... 0x27C: /* ISPENDR */
    case 0x280 ... 0x2FC: /* ICPENDR */
    case 0x300 ... 0x37C: /* ISACTIVER */
    case 0x380 ... 0x3FC: /* ICACTIVER */
        for(val = 0, i = 31; i >= 0; i--)
            val = (val << 1) | vgic_gicd_read1(g, index, i);
        break;

    case 0x400 ... 0x7FC: /* IPRIORITYR */
    case 0x800 ... 0xBFC: /* ITARGETSR (SPI) */
        for(val = 0, i = (1UL << esr.sas)-1; i >= 0; i--)
            val = (val << 8) | vgic_gicd_read8(g, index, i);
        break;

    case 0xC00 ... 0xCFC: /* ICFGR */
        for(val = 0, i = 15; i >= 0; i--)
            val = (val << 2) | vgic_gicd_read2(g, index, i);
        break;
    default:
        goto bad_access;
    }

good_access:
    *val_ = val;
    return true;

bad_access:
    return false;
}

void vgic_gicd_notify(struct guest *g, struct virq *virq,
                      uint32_t reg, uint32_t val)
{
    struct virq_notifier *vn;

    vn = vgic_virq_notifier_get(g, virq);
    if(vn)
        vn->callback(g, virq, vn->userdata, reg, val);
}

/*
 * vgic write operations
 */

/* write to a bit-wide register */
static void vgic_gicd_write1(struct guest *g, uint32_t reg, int bit)
{
    struct virq *virq;
    uint32_t val, type, vid;
    int hw;

    /* get the corresponding virq */
    vid = bit + (reg & 0x7F) * 8;
    virq = vgic_virq_get(g, vid);
    if(!virq || virq->state != VIRQ_STATE_ACTIVE)
        return;

    /* what type of register and what value is this ? */
    val = ((~reg) >> 7) & 1;
    type = (reg >> 8);
    hw = /* g->vgic_driver.enabled && */ !virq->virt;

    switch(type) {
    case 0x100 >> 8: /* ISENABLER, ICENABLER */
        if(virq->gicd_enabled != val) {
            virq->gicd_enabled = val;
            if(hw)
                gicv2_dist_set_enable(_gicd, virq->pid, val);

            /* we just enabled an interrupt, see if we had it pending in queue */
            if(val) {
                if(g->vgic_driver.enabled && virq->saved_pending)
                    vgic_virq_queued_restore(cman_pcpu_get(), virq);
            }
        }
        break;
    case 0x200 >> 8: /* ISPENDR, ICPENDR */
        if(virq->gicd_pending != val) {
            virq->gicd_pending = val;
            if(hw)
                gicv2_dist_set_pending(_gicd, virq->pid, val);
        }
        break;
    case 0x300 >> 8: /* ISACTIVER, ICACTIVER */
        if(virq->gicd_active != val) {
            virq->gicd_active = val;
            if(hw)
                gicv2_dist_set_active(_gicd, virq->pid, val);
        }
        break;
    }

    /* notify hypervisor about the change if needed */
    vgic_gicd_notify(g, virq, reg, val);
}

static void vgic_gicd_write8(struct guest *g, uint32_t reg,
                             int byte_, uint32_t val)
{
    struct virq *virq;
    uint32_t type, vid;
    int hw;

    /* get the corresponding virq */
    vid = byte_ + (reg & 0x37F);
    virq = vgic_virq_get(g, vid);
    if(!virq || virq->state != VIRQ_STATE_ACTIVE)
        return;

    /* what type of register is this ? */
    type = (reg >> 10);
    hw = /* g->vgic_driver.enabled && */ !virq->virt;

    if(type == (0x400 >> 10)) { /* IPRIORITYR */
	if(val < virq->config_prio_min)
	    val = virq->config_prio_min;

        if(virq->gicd_prio != val) {
            virq->gicd_prio = val;
            if(hw)
                gicv2_dist_set_priority(_gicd, virq->pid, val);
        }
    } else if(type == (0x800 >> 10)) { /* ITARGETSR */
        if(virq->gicd_cpumask != val) {
            virq->gicd_cpumask = val;
            if(hw) {
                val = cpu_vmask_to_pmask(g, val);
                gicv2_dist_set_target(_gicd, virq->pid, val);
            }
        }
    }

    /* notify hypervisor about the change if needed */
    vgic_gicd_notify(g, virq, reg, val);
}

static void vgic_gicd_write2(struct guest *g, uint32_t reg,
                             int bitpair, uint32_t val)
{
    struct virq *virq;
    uint32_t type, vid;
    int hw;

    /* get the corresponding virq */
    vid = bitpair + (reg & 0xFF) * 4;
    virq = vgic_virq_get(g, vid);
    if(!virq || virq->state != VIRQ_STATE_ACTIVE)
        return;

    /* what type of register is this ? */
    type = (reg >> 9);
    hw = /* g->vgic_driver.enabled && */ !virq->virt;

    if(type == (0xC00 >> 9)) { /* ICFGR */
        if(virq->gicd_cfg != val) {
            virq->gicd_cfg = val;
            if(hw)
                gicv2_dist_set_config(_gicd, virq->pid, val);
        }
    }

    /* notify hypervisor about the change if needed */
    vgic_gicd_notify(g, virq, reg, val);
}

static int vgic_gicd_write_sgir(struct pcpu *pcpu, struct guest *g, uint32_t val)
{
    union gicd_sgir_reg sgir;
    struct virq *virq;

    sgir.all = val;
    virq = vgic_virq_get(g, sgir.id);

    switch(sgir.filter) {
    case 0:
        /* forward to the ones specified */
        break;
    case 1:
        /* forward to all other cpus */
        sgir.targetlist = ~(1UL << pcpu->vcpu->vid);
        sgir.filter = 0;
        break;
    case 2:
        /* forward only to those who have requested those */
        fatal("SGIR.filter = 2 is NOT IMPLEMENTED");
        return 0;
    case 3:
        /* bad filter */
        return 0;
    }

    /* check if we are allowed to send this */
    if(virq->state != VIRQ_STATE_ACTIVE) {
        dprintf("SGIR error: guest %s does not have access to SGIR %d\n", g->name, sgir.id);
        return 0;
    }

    /* TODO: should we check if it is enabled ? */
    /* convert the cpu list from virtual to physical */
    sgir.targetlist = cpu_vmask_to_pmask(g, sgir.targetlist);

    /* and write it */
    _gicd->sgir = sgir.all;

#if DEBUG > 7
    dprintf("SGIR: filter=%d, list=%x, id=%d ALL=%x\n",
        sgir.filter, sgir.targetlist, sgir.id, sgir.all);
#endif

    /* all clear */
    return 1;
}

static bool mdriver_vgic_gicd_write(struct mdriver *drv, union reg_esr esr,
                                    adr_t ipadr, uint64_t val64)
{
    const uint64_t index = ipadr - drv->vstart;
    const uint64_t padr = drv->padr - drv->vstart + ipadr;
    const uint32_t val = val64;
    struct pcpu *pcpu;
    struct guest *g;
    uint32_t tmp;
    int i;

    gicd_spinlock_lock();

    /* only PRIO and TARGET can do byte access */
    if(esr.sas == 0 && !((index >= 0x400 && index < 0x778) ||
                         (index >= 0x800 && index < 0xB78)))
        goto bad_access;

    pcpu = cman_pcpu_get();
    g = pcpu->guest;
    if(!g)
        goto bad_access;


    switch(index) {
    case 0x000: /* CTLR */
        if(g->vgic_driver.enabled != val & 1) {
            g->vgic_driver.enabled = val & 1;

            if(g->vgic_driver.enabled) {
                /* at this point, we may want to fire any queued virqs */
                vgic_process_queued(pcpu);
#if DEBUG > 3
                debug_dump_guest(g);
                debug_dump_gic();
#endif
            }
        }
        break;

        /* read only */
    case 0x004 ... 0x008: /* TYPER, IIDR */
        goto bad_access;

        /* do nothing */
    case 0x080 ... 0x0FC: /* IGROUPR */
        break;

        /* bit access */
    case 0x100 ... 0x17C: /* ISENABLER */
    case 0x180 ... 0x1FC: /* ICENABLER */
    case 0x200 ... 0x27C: /* ISPENDR */
    case 0x280 ... 0x2FC: /* ICPENDR */
    case 0x300 ... 0x37C: /* ISACTIVER */
    case 0x380 ... 0x3FC: /* ICACTIVER */
        for(i = 0, tmp = val; i < 32 && tmp; i++, tmp >>= 1) {
            if(tmp & 1)
                vgic_gicd_write1(g, index, i);
        }
        break;

    case 0x400 ... 0x7FC: /* IPRIORITYR */
    case 0x820 ... 0xBFC: /* ITARGETSR (SPI) */
        for(i = 0, tmp = val; i < (1UL << esr.sas); i++, tmp >>= 8)
            vgic_gicd_write8(g, index, i, tmp & 0xFF);
        break;

    case 0x800 ... 0x81C:  /* ITARGETSR (SGI, PPI) is readonly */
        goto bad_access;

    case 0xC00 ... 0xCFC: /* ICFGR */
        for(i = 0, tmp = val; i < 32/2; i++, tmp >>= 2)
            vgic_gicd_write2(g, index, i, tmp & 3);
        break;

    case 0xF00: /* GICD_SGIR */
        if(!vgic_gicd_write_sgir(pcpu, g, val))
            goto bad_access;
        break;

    default:
        goto bad_access;
    }

good_access:
    gicd_spinlock_unlock();
    return true;

bad_access:
    gicd_spinlock_unlock();
    return false;
}

void vgic_send_sgi(struct guest *g, int id, uint32_t vtarget)
{
    uint32_t ptarget;

    /* limit ourselves to the cores this guest has */
    vtarget &= g->cpumask_vcpu_running;

    /* convert mask of vcore to pcore */
    ptarget = cpu_vmask_to_pmask(g, vtarget);

    /* send it */
    gic_send_sgi(id, ptarget);
}


/*
 * vgic loading and saving interrupts
 */


void vgic_virq_queued_save(struct virq *virq)
{
    if(virq->saved_pending) {
        /* this is already pending, bbut now with lower (higher) prio */
        if(virq->saved_prio > virq->gicd_prio)
            virq->saved_prio = virq->gicd_prio;
    } else {
        virq->saved_pending = 1;
        virq->saved_prio = virq->gicd_prio;
    }
}

bool vgic_virq_queued_restore(struct pcpu *pcpu, struct virq *virq)
{
    uint32_t v;
    if(virq->saved_pending) {
        if(virq->virt)
            v = 0x10000000 | ((virq->saved_prio & ~7 ) << 20) | virq->vid;
        else
            v = 0x90000000 | ((virq->saved_prio & ~7 ) << 20) | (virq->pid << 10) | virq->vid;
        if(gic_gich_write_lr(pcpu, v) < 0) {
            virq->saved_pending = 0;
            return true;
        }
    }
    return false;
}


/*
 * vgic interrupt generation operations
 */


/**
 * @name vgic_virq_emit
 * @type function
 * @description emit a virtual interrupt that may be virtual
 */

static void vgic_virq_emit(struct pcpu *pcpu, struct virq *virq)
{
    uint32_t v;

    if(virq->virt)
        v = 0x10000000 | ((virq->gicd_prio & ~7 ) << 20) | virq->vid;
    else
        v = 0x90000000 | ((virq->gicd_prio & ~7 ) << 20) | (virq->pid << 10) | virq->vid;

    if(gic_gich_write_lr(pcpu, v) < 0)
        vgic_virq_queued_save(virq);
}

bool vgic_send_virtual(struct pcpu *pcpu, struct guest *g, int vid)
{
    struct virq *virq;
    int prio;

    virq = vgic_virq_get(g, vid);
    if(virq && virq->state == VIRQ_STATE_ACTIVE && virq->virt) {
        if(virq->gicd_enabled && g->vgic_driver.enabled) {
            vgic_virq_emit(pcpu, virq);

#if DEBUG > 3
            dprintf("Sending virtual interrupt %d\n", vid);
#endif
            return true;
        } else {
#if DEBUG > 3
            dprintf("Vgic_send_virtual %d when virq is not enabled\n", virq->vid);
#endif
            vgic_virq_queued_save(virq);
            return true;
        }
    } else fatal("Bad vid for virtual virq");

    return false;
}

/*
 * vgic driver and handler
 */

/**
 * @name vgic_process_queued
 * @type function
 * @description this indicates that the vgic on this core should
 * process all queued interrupts
 */
void vgic_process_queued(struct pcpu *pcpu)
{
    struct guest *g;
    struct virq *virq;
    int i;

    g = pcpu->guest;

    /* no point processing anything of we are shut down */
    if(!g->vgic_driver.enabled)
        return;

    vgic_guest_lock(g);

    for(i = 0; i < VGIC_MAX_VIRQ; i++) {
        virq = vgic_virq_get(g, i);
        if(virq->gicd_enabled && virq->saved_pending) {
            if( vgic_virq_queued_restore(pcpu, virq) < 0)
                break;
        }
    }

    vgic_guest_unlock(g);
}

bool vgic_handler(struct pcpu *pcpu, struct pirq *pirq,
                  struct registers *regs, uint32_t iar)
{
    uint32_t id, lrdata;
    int prio;
    struct virq *virq;
    struct vcpu *vcpu;
    struct guest *g;

    id = iar & 1023;
    vcpu = pcpu->vcpu;
    g = pcpu->guest;

    vgic_guest_lock(g);

    /* sanity check: is this really handled by our guy? */
    if(pirq->owner == g) {
        virq = vgic_virq_get(pirq->owner, pirq->vid);
        if(virq) {
            if(virq->gicd_enabled && g->vgic_driver.enabled) {
                vgic_virq_emit(pcpu, virq);
            } else {
                vgic_virq_queued_save(virq);
            }
            goto succeeded;
        } else fatal("Received interrupt with no virq");
    } else fatal("TODO: interrupt doesn't belong to this guest");

failed:
    vgic_guest_unlock(g);
    return false;

succeeded:
    vgic_guest_unlock(g);
    return true;

}


static void vgic_guest_virq_init(struct virq *virq, int id, uint32_t cpumask)
{
    virq->state = VIRQ_STATE_NONE;
    virq->vid = id;
    virq->pid = 0;
    virq->virt = 1;
    virq->config_prio_min = VIRQ_PRIO_DEFAULT_MIN;
    virq->gicd_prio = VIRQ_PRIO_DEFAULT_INIT;
    virq->gicd_enabled = virq->gicd_pending = virq->gicd_active = 0;
    virq->gicd_cpumask = cpumask;
    virq->gicd_cfg = (gicv2_irqid_get_type(id) == GIC_IRQ_TYPE_SGI) ? 2 : 1;
}

static void vgic_guest_init(struct mdriver *obj, struct guest *g,
                            struct dt_block *config)
{
    int i, j;
    struct vgic_driver *drv;

    int flags, vid, pid, cnt;
    uint32_t *data;
    struct dt_block prop;

    /* prepare additional vgic data structure */
    drv = &g->vgic_driver;
    drv->active_cpumask = 0;
    drv->enabled = 0;

    /* initlaize the mdriver */
    assert_eq("vgic driver should be exactly two pages large",
              2 * PAGE_SIZE, obj->vend - obj->vstart);

    /* now change it so page 0 is the mdriver and page 1 is GICV mapping */
    obj->padr = GICD_BASE;
    obj->vend = obj->vstart + PAGE_SIZE;
    gman_device_map(g, obj->vend, GICV_BASE, 1);


    /* initialize virq:s, both global and local */
    for(i = 0; i < VGIC_MAX_VIRQ; i++) {
        if(gicv2_irqid_get_type(i) == GIC_IRQ_TYPE_SPI) {
            vgic_guest_virq_init(& drv->global.virqs[i], i, 1);
        } else {
            for(j = 0; j < GUEST_VCPU_MAX; j++) {
                vgic_guest_virq_init(& drv->local[j].virqs[i], i, 1UL << j);
            }
        }
    }

    /* get the irq map */
    if(!config_get_array_try(config, "irq-map", 3 * sizeof(uint32_t),
                             (void **)&data, &cnt))
        fatal("vgic driver missing or bad irq-mapo format");

    dprintf("\tirq map: [");
    for(i = 0; i < cnt; i++) {
        flags = dtend(*data++);
        vid = dtend(*data++);
        pid = dtend(*data++);
        vgic_virq_configure(g, flags, vid, pid);
        dprintf("_ %d->%d", vid, pid);
    }
    dprintf("_ ]\n");
}


static void gic_pcpu_mq_virq_handler(struct pcpu *pcpu, union gic_pcpu_mq_data data)
{
    uint32_t virq;
    struct guest *guest;

    virq = data.data & 1023;
    guest = pcpu->guest;

    /* sanity checks */
    if(!guest)
        fatal("Internal error in virq mq");

    /* route virq now */
    vgic_send_virtual(pcpu, guest, virq);
}


static void vgic_init(struct mdriver *drv, bool global, struct pcpu *pcpu)
{
    /* sanity checks */
    if(sizeof(struct virq) > sizeof(uint64_t))
        fatal("INTERNAL: virq is too large");

    if(!global) {
        gic_hypervisor_enable();

        gic_pcpu_mq_register_handler(pcpu, GIC_PCPU_MSG_VIRQ,
                                     gic_pcpu_mq_virq_handler);
    }
}



static struct mdriver vgic = {
    .mask_read = MASK_WORD32 | MASK_BYTE8,
    .mask_write = MASK_WORD32 | MASK_BYTE8,

    .ops_init = vgic_init,
    .ops_guest_init = vgic_guest_init,
    .ops_guest_read = mdriver_vgic_gicd_read,
    .ops_guest_write = mdriver_vgic_gicd_write,
};

DRIVER_DECLARE("vgic", _vgic, &vgic);
