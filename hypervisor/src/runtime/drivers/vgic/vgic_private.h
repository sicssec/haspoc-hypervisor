#ifndef __VGIC_PRIVATE_H__
#define __VGIC_PRIVATE_H__


#ifdef __ASSEMBLER__

#else /* __ASSEMBLER__ */

/* this represents the virq flags as found in the DTS */
union virq_config_flags {
    uint32_t all;
    struct {
	uint32_t virt : 1;
	uint32_t reserved : 23;
	uint32_t min_prio : 8;
    };
};

extern bool vgic_virq_pirq_disconnect(struct pcpu * pcpu, struct guest *g, int vid);
extern bool vgic_virq_pirq_connect(struct pcpu * pcpu, struct guest *g, int vid);

extern void vgic_virq_queued_save(struct virq *virq);
extern bool vgic_virq_queued_restore(struct pcpu *pcpu, struct virq *virq);


static inline void vgic_guest_lock(struct guest *g)
{
    spinlock_lock(& g->vgic_driver.global.lock);
}

static inline void vgic_guest_unlock(struct guest *g)
{
    spinlock_unlock(& g->vgic_driver.global.lock);
}


#endif /* !__ASSEMBLER__ */

#endif /* __VGIC_PRIVATE_H__ */
