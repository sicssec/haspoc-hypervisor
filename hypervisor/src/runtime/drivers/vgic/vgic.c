
/* physical interrupts: the real ones */

#include <stdint.h>
#include <gicv2.h>

#include "defs.h"
#include "cman.h"
#include "gman.h"
#include "util.h"

#include "vgic.h"
#include "vgic_private.h"



void vgic_dump(struct guest *g)
{
#if DEBUG > 3
    int i, j;
    struct virq *virq;

    dprintf("vgic dump for %s\n", g->name);
    dprintf("\tGLOBAL: %x-%x\n", &g->vgic_driver.global,
            sizeof(g->vgic_driver.global) + (adr_t)&g->vgic_driver.global);
    for(i = 0; i < VGIC_MAX_VIRQ; i++) {
        virq = & g->vgic_driver.global.virqs[i];
        if(virq->state)
            dprintf("\t\t %d %x state=%d vid=%d pid=%d\n",
                    i, virq, virq->state, virq->vid, virq->pid);
    }

    for(j = 0; j < GUEST_VCPU_MAX; j++) {
        dprintf("\tLOCAL %d: %x-%x\n", j,
                &g->vgic_driver.local[j], sizeof(g->vgic_driver.local[j]) + (adr_t) &g->vgic_driver.local[j]);
        for(i = 0; i < 32; i++) {
            virq = & g->vgic_driver.local[j].virqs[i];
            if(virq->state)
                dprintf("\t\t %d %x state=%d vid=%d pid=%d\n",
                        i, virq, virq->state, virq->vid, virq->pid);
        }
    }
#endif
}


/*
 * virq: virtual interrupts as seen by the guest
 */

struct virq *vgic_virq_get(struct guest *g, int index)
{
    struct pcpu *pcpu;
    cpuid_t vid;

    if(index < 0 || index >= VGIC_MAX_VIRQ)
        return NULL;

    if(index < 32) {
        pcpu = cman_pcpu_get();

        /* sanity check: this should not happen */
        if(pcpu->guest != g || pcpu->vcpu == NULL)
            return NULL;

        vid = pcpu->vcpu->vid;
        return & g->vgic_driver.local[vid].virqs[index];
    } else {
        return & g->vgic_driver.global.virqs[index];
    }
}

static void vgic_virq_configure_one(struct guest *g, struct virq *virq,
				    int pid, union virq_config_flags flags)
{

    if(virq->state != VIRQ_STATE_NONE)
        fatal("virq already configured");

    /* priority adjustment */
    if(flags.min_prio)
        virq->config_prio_min = flags.min_prio;;

    if(virq->gicd_prio < virq->config_prio_min)
        virq->gicd_prio = virq->config_prio_min;

    /* physical interrupt id? */
    if(flags.virt) {
        virq->virt = 1;
        virq->pid = -1;
    } else {
        virq->virt = 0;
        virq->pid = pid;
    }

    /* and mark it ready to serve */
    virq->state = VIRQ_STATE_READY;
}

void vgic_virq_configure(struct guest *g, int flags_, int vid, int pid)
{
    int i;
    struct virq *virq;
    union virq_config_flags flags = { .all = flags_ };

    flags.all = flags_;

    /* sanity checks */
    if(vid < 0 || vid >= VGIC_MAX_VIRQ)
        fatal("virq id out of range");

    if(flags.virt) {
        assert_eq("pure virtual virq must be SPI",
                  gicv2_irqid_get_type(vid), GIC_IRQ_TYPE_SPI);
    } else {
        if(pid < 0 || pid >= GIC_MAX_IRQ)
            fatal("pirq id out of range");
        assert_eq("VIRQ type must match that of its PIRQ",
                  gicv2_irqid_get_type(vid),
                  gicv2_irqid_get_type(pid));
    }

    /* if local, register it on all cores otherwise just globally */
    if(vid < 32) {
        for(i = 0; i < GUEST_VCPU_MAX; i++) {
            virq = & g->vgic_driver.local[i].virqs[vid];
            vgic_virq_configure_one(g, virq, pid, flags);
        }
    } else {
        virq = & g->vgic_driver.global.virqs[vid];
        virq->gicd_cpumask = g->cpumask_vcpu;
        vgic_virq_configure_one(g, virq, pid, flags);
    }
}


/*
 * register and return the index to this notifier.
 * can detect NULL callback and existing notifiers
 */
int vgic_virq_add_notifier(struct guest *g, virq_notifier_callback n,
                           void* userdata)
{
    int i;
    struct vgic_driver *vd;

    /* NULL callback meens no notifier */
    if(!n)
        return 0;

    /* already have it ? */
    vd = & g->vgic_driver;
    for(i = 0; i < vd->virq_notifier_count; i++) {
        if(vd->virq_notifier[i].callback == n &&
           vd->virq_notifier[i].userdata == userdata)
            return i + 1;
    }

    /* register a new one */
    assert("No room for more virq notifiers",
           vd->virq_notifier_count < VIRQ_NOTIFIER_COUNT);

    vd->virq_notifier[vd->virq_notifier_count].callback = n;
    vd->virq_notifier[vd->virq_notifier_count].userdata = userdata;
    vd->virq_notifier_count++;

    return vd->virq_notifier_count;
}

/*
 * attach a notifier to a virq
 */
void vgic_virq_notifier_set(struct guest *g, struct virq *v,
                            virq_notifier_callback n, void *userdata)
{
    v->notifier_id = vgic_virq_add_notifier(g, n, userdata);
}

struct virq_notifier *vgic_virq_notifier_get(struct guest *g, struct virq *virq)
{
    if(!virq || virq->notifier_id == 0)
        return NULL;
    return & g->vgic_driver.virq_notifier[virq->notifier_id - 1];
}




/*
 * restore a virq to a pirq
 */
bool vgic_virq_pirq_restore(struct pcpu * pcpu, struct guest *g, int vid)
{
    struct virq *virq;


    virq = vgic_virq_get(g, vid);
    if(!virq)
        return false;

    if(virq->state == VIRQ_STATE_READY) {
        virq->state = VIRQ_STATE_ACTIVE;

        if(!virq->virt) {
            if(gic_pirq_virq_connect(pcpu, virq->pid, vid, g) < 0)
                return false;

            /* turn of for a sec */
            gicv2_dist_set_enable(_gicd, virq->pid, 0);

            /* restore gicd registers */
            gicv2_dist_set_priority(_gicd, virq->pid, virq->gicd_prio);
            gicv2_dist_set_target(_gicd, virq->pid,
                                  cpu_vmask_to_pmask(g, virq->gicd_cpumask));
            gicv2_dist_set_config(_gicd, virq->pid, virq->gicd_cfg);
            gicv2_dist_set_active(_gicd, virq->pid, virq->gicd_active);
            gicv2_dist_set_pending(_gicd, virq->pid, virq->gicd_pending);
            gicv2_dist_set_enable(_gicd, virq->pid, virq->gicd_enabled);
        }
    }

    return true;
}

/*
 * store a virtual interrupt that may have been installed earlier
 *
 */
bool vgic_virq_pirq_store(struct pcpu * pcpu, struct guest *g, int vid)
{
    struct virq *virq;

    virq = vgic_virq_get(g, vid);
    if(!virq)
        return false;

    if(virq->state == VIRQ_STATE_ACTIVE) {
        virq->state = VIRQ_STATE_READY;

        if(!virq->virt) {
            /* unregister connection to pirq */
            if(gic_pirq_virq_disconnect(pcpu, virq->pid) < 0)
                return false;

            /* disable it now that we don't have it */
            gicv2_dist_set_enable(_gicd, virq->pid, 0);

            /* copy values that may have just been changed */
            virq->gicd_pending = gicv2_dist_get_pending(_gicd, virq->pid);
            virq->gicd_active = gicv2_dist_get_active(_gicd, virq->pid);
        }
    }
    return true;
}



/*
 * restore vgic state from the gic- and uninstall / disable irqs
 */
bool vgic_guest_gic_store(struct pcpu *pcpu, struct guest *g)
{
    int i, cnt;
    uint32_t mask = 1UL << pcpu->vcpu->vid;
    bool okay = true;

    /* if we are the last cpu, remove all virqs otehrwise just locals */
    cnt = (g->vgic_driver.active_cpumask == mask) ? VGIC_MAX_VIRQ : 32;

    for(i = 0; i < 32; i++)
        if(!vgic_virq_pirq_store(pcpu, g, i))
            okay = false;

    /* this vcpu was removed from vgic */
    g->vgic_driver.active_cpumask &= ~mask;

    return okay;
}

/*
 * store vgic state into the gic
 */
bool vgic_guest_gic_restore(struct pcpu *pcpu, struct guest *g)
{
    int i, cnt;
    bool okay = true;


    /* if we are the first cpu, connect all virqs otehrwise just locals */
    cnt = g->vgic_driver.active_cpumask ? 32 : VGIC_MAX_VIRQ;

    /* start with local interrupts */
    for(i = 0; i < cnt; i++)
        if(!vgic_virq_pirq_restore(pcpu, g, i))
            okay = false;

    /* record this vcpu having restored to vgic */
    g->vgic_driver.active_cpumask |= 1UL << pcpu->vcpu->vid;

    return okay;
}

/*
 * find the first vcpu that accepts this
 */
struct vcpu *vgic_virq_find_receiver(struct guest *g, int vid)
{
    struct virq *virq;
    uint64_t mask;
    int target;

    virq  = vgic_virq_get(g, vid);
    if(virq == NULL || virq->state != VIRQ_STATE_ACTIVE) {
        return NULL;
    }

    mask = virq->gicd_cpumask & g->cpumask_vcpu_running;
    target = util_first_bit1(mask);
    if(target >= 0 && target <GUEST_VCPU_MAX) {
        return gman_vcpu_get(g, target);
    }

    /* did not find any ? */
    return NULL;
}


bool vgic_send_virq_to_guest(struct pcpu *me, struct guest *g, int vid)
{
    struct vcpu *vcpu;

    vcpu = vgic_virq_find_receiver(g, vid);
    if(vcpu) {
        /* already on the corect core */
        if(vcpu->pcpu == me) {
            vgic_send_virtual(vcpu->pcpu, g, vid);
            return true;
        } else {
            assert_not_null("vcpu->pcpu valid", vcpu->pcpu);
            return gic_pcpu_mq_write(vcpu->pcpu, GIC_PCPU_MSG_VIRQ, vid);
        }
    } else {
        /* TODO */
        dprintf("INTERNAL ERROR: guest cannot accept vid=%d on any core\n", vid);
    }

    return false;
}
