#ifndef __VGIC_H__
#define __VGIC_H__

#include "defs.h"
#include "drivers.h"
#include "mman.h"
#include "gic.h"

/* HiKey has 172 IRQs */
#ifdef TARGET_hikey
  #define VGIC_MAX_VIRQ 172
#else
  #define VGIC_MAX_VIRQ 96
#endif

/* priority assumptions*/
#define VIRQ_PRIO_DEFAULT_MIN  0xA0
#define VIRQ_PRIO_DEFAULT_INIT 0xE0

/* forward types */
struct vcpu;
struct pcpu;
struct pirq;
struct virq;

/* change notifier callback */
#define VIRQ_NOTIFIER_COUNT 3

typedef void (*virq_notifier_callback)(struct guest *g, struct virq *virq,
                                       void *userdata, uint32_t reg, uint32_t val);
struct virq_notifier {
    virq_notifier_callback callback;
    void *userdata;
};




/**
 * @name virq
 * @type struct
 * @description represents a virtual interrupt that may or may not
 * correspond to a physical interrupt.
 * The virq maintains its own gicd registers, but if the interrupt
 * is not virtual some of these may be out of sync with the real GICD
 * (pending and active for example), some may also have values that
 * cannot exist on real GICD (e.g. you cannot set PPI cpumask on real hw)
 */
struct virq {
    uint32_t vid : 10;
    /* an virq is either virtual [virt=1] or has a valid pirq [pid] */
    uint32_t pid : 10;
    uint32_t virt : 1;

#define VIRQ_STATE_NONE 0
#define VIRQ_STATE_READY 1
#define VIRQ_STATE_ACTIVE 2
    uint32_t state : 2;

    /* copy of the GICD registers */
    uint32_t gicd_cpumask : 8;
    uint32_t gicd_prio : 8;
    uint32_t gicd_enabled : 1;
    uint32_t gicd_pending : 1;
    uint32_t gicd_active : 1;
    uint32_t gicd_cfg : 2;

    /* from guest configuration */
    uint32_t config_prio_min : 8;

    /* interrupts to be emitted once enabled */
    uint32_t saved_pending : 1;
    uint32_t saved_prio : 8;

    /* notifier callback, points to vgic_driver->virq_notifier. 0 is none */
    uint32_t notifier_id : 2; /* log2(1 + VIRQ_NOTIFIER_COUNT) */
};

/**
 * @name vgic_driver
 * @type struct
 * @description vgic driver including mdriver and global interrups
 * (local interrupts are in vgic_local)
 * all virtual cores. this includes virtual SPI and general flags etc
 */
struct vgic_driver {
    struct mdriver gicd_mdriver;

    /* flags */
    uint32_t volatile active_cpumask : 8;
    uint32_t volatile enabled : 1;
    uint64_t volatile reserved : (64 - 8 - 1);

    /* GLOBAL (shared between all cores) */
    struct vgic_virqs_global {
        /* first 32 are not used */
        struct virq virqs[VGIC_MAX_VIRQ];
        spinlock_t lock;
    } global;

    /* one per cpu */
    struct vgic_virqs_local {
        struct virq virqs[32];
    } local[GUEST_VCPU_MAX];

    int virq_notifier_count;

    struct virq_notifier virq_notifier[VIRQ_NOTIFIER_COUNT];
};



/**
 * @name vgic_virq_get
 * @type function
 * @description retrieve the virq structure with the given vid.
 * Depending on the vid, this can be a local (different for each
 * core) or global virq.
 */
extern struct virq *vgic_virq_get(struct guest *g, int vid);


/**
 * @name vgic_virq_configure
 * @type function
 * @description initial configuration of a virq based on
 * information from the guest configuration.
 * Note that this will configure all copies of the virq if it has
 * a copy for each core
 */
extern void vgic_virq_configure(struct guest *g, int dts_flags,
                                int vid, int pid);

/* virq change notifiers */
extern void vgic_virq_notifier_set(struct guest *g, struct virq *virq,
                                   virq_notifier_callback c, void *userdata);
extern struct virq_notifier *vgic_virq_notifier_get(struct guest *g,
                                                    struct virq *virq);

/**
 * @name vgic_send_virq_to_guest
 * @type function
 * @description send a virq to a guest on an unknown core
 */

extern bool vgic_send_virq_to_guest(struct pcpu *me, struct guest *g, int vid);

extern void vgic_send_sgi(struct guest *g, int id, uint32_t target_list);
extern bool vgic_send_virtual(struct pcpu *pcpu, struct guest *g, int vid);

extern bool vgic_guest_gic_restore(struct pcpu *pcpu, struct guest *g);
extern bool vgic_guest_gic_store(struct pcpu *pcpu, struct guest *g);

extern void vgic_process_queued(struct pcpu *pcpu);
extern bool vgic_handler(struct pcpu *pcpu, struct pirq *pirq,
                         struct registers *regs, uint32_t iar);


/**
 * @name vgic_virq_find_receiver
 * @type function
 * @description find the first vcpu that accepts this virq right now
 */
extern struct vcpu *vgic_virq_find_receiver(struct guest *g, int virq);


extern void vgic_dump(struct guest *g);

#endif /* ! __VGIC_H__ */
