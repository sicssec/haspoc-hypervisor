
#include "defs.h"
#include "cman.h"
#include "gman.h"

#include "psci.h"

/* power management */


/* PSCI return codes */
#define PSCI_RET_NOT_SUPPORTED SMC_RET_UNKNOWN
#define PSCI_RET_INVALID_PARAMETERS ((uint32_t)-2)
#define PSCI_RET_DENIED ((uint32_t)-3)
#define PSCI_RET_ALREADY_ON ((uint32_t) -4)

/*
 * PSCI
 */

uint64_t psci_smc(uint32_t op, uint64_t param0, uint64_t param1, uint64_t param2)
{
    uint64_t ret;
    ret = __cpu_smc(op, param0, param1, param2, 0);
    dprintf("psci request (%x %X %X %X) -> %x\n", op, param0, param1, param2, ret);
    return ret;
}


static uint64_t psci_func_system_reboot(struct pcpu *pcpu, uint32_t op)
{
    struct guest *g;
    uint64_t ret;

    ret = PSCI_RET_DENIED;
    g = pcpu->guest;

    /* sanity check */
    if(g) {
        if(g->flags & GUEST_FLAG_ALLOW_POWER) {
            ret = psci_smc(op, 0, 0, 0);
            dprintf("psci request %x failed with %x?\n", op, ret);
        } else {
            dprintf("psci request %x denied\n", op);
        }
    }
    return ret;
}

static uint32_t psci_func_version()
{
    return 2; /* PSCI 0.2 */

}

static uint32_t psci_func_cpuon(struct pcpu *pcpu_this,
                                uint64_t mpidr,
                                adr_t entry,
                                uint64_t context)
{
    cpuid_t vid, pid;
    struct guest *guest;
    struct pcpu *pcpu_that;
    uint32_t ret;
    msg_t msg;

    dprintf("CPUON: MPIDR=%x entry=%X contect=%X\n", mpidr, entry, context);

    ret = PSCI_RET_INVALID_PARAMETERS;

    if(mpidr_is_valid(mpidr)) {
        vid = mpidr_to_cpuid(mpidr);
        guest = pcpu_this->guest;
        if(vid < ARCH_CORES && !(guest->cpumask_vcpu_running & (1UL << vid))) {
            if( pc_is_valid(entry) && gman_owns_virt_region(
                guest, entry, sizeof(adr_t))) {
                if(guest->vcpus[vid].pcpu == NULL) {
                    pcpu_that = gman_guest_add_core(guest, vid);

                    /* all looks good, start the guest */
                    if(pcpu_that != NULL && pcpu_that->vcpu != NULL) {

                        /* store its requested entry state when it wakes up */
                        cman_vstate_set(pcpu_that->vcpu, context, 0, entry);


                        /* ask the driver to start that core in hyp mode */
                        msg.msg32[0] = PSCI_CMD_CPU_ON;
                        msg.msg32[1] = pcpu_that->pid;
                        if(services_message("pm", &msg, true, true)) {
                            /* it worked! */
                            ret = SMC_RET_SUCCESS;
                        } else {

                            /* TODO: free pcpu_that! */
                            dprintf("CPU_ON smc failed: %x", ret);
                        }
                    } else dprintf("CPU_ON could not allocate core");
                } else dprintf("CPU_ON already has a pcpu\n");
            } else dprintf("CPU_ON bad entry: %X\n", entry);
        } else dprintf("CPU_ON bad CPU vid: %d\n", vid);
    } else dprintf("CPU_ON bad mpidr: %x\n", mpidr);

    return ret;
}


/*
 * called from EL1
 */
static uint32_t psci_ccallback(struct pcpu *pcpu, struct registers *regs, int smc)
{
    switch(regs->x[0]) {
    case PSCI_CMD_VERSION:
        return psci_func_version();

    case PSCI_CMD_CPU_ON:
        return psci_func_cpuon(pcpu, regs->x[1], regs->x[2], regs->x[3]);
        break;

    case PSCI_CMD_SYSTEM_RESET:
    case PSCI_CMD_SYSTEM_OFF:
        return psci_func_system_reboot(pcpu, regs->x[1]);

    default:
        return SMC_RET_UNKNOWN;
    }
}

/*
 * called from EL2
 */
static bool psci_message(struct service *s, msg_t *msg)
{
    uint32_t ret;
    int pid;
    struct pcpu *pcpu;

    switch(msg->msg32[0]) {
    case PSCI_CMD_CPU_ON:
        pid = msg->msg32[1];
        if(pid < 0 || pid >= ARCH_CORES)
            return false;

        pcpu = cman_pcpu_get_by_cpuid(pid);
        assert_eq("pcpu state when woken up", PCPU_STATE_READY, pcpu->state);

        dprintf("PID: %d %d %x\n", pid, pcpu->pid, pcpu);
        ret = psci_smc( PSCI_CMD_CPU_ON,
                        cpuid_to_mpidr(pcpu->pid),
                        (uint64_t) __boot_core,
                        (uint64_t) pcpu);

        return ret == SMC_RET_SUCCESS;
    default:
        return false;
    }
}

static void psci_service_init(struct service *s, bool global, struct pcpu *pcpu)
{
    if(global) {
#if DEBUG > 3
        printf("Power management: PSCI init...\n");
#endif
        cdriver_register(NULL, psci_ccallback);
    }
}



static struct service psci = {
    .ops_init = psci_service_init,
    .ops_message = psci_message,
};

SERVICE_DECLARE("pm", _psci, &psci, 0);
