#ifndef __PSCI_H__
#define __PSCI_H__


/* PSCI commands */
#define PSCI_CMD_CPU_ON 0xC4000003
#define PSCI_CMD_CPU_OFF 0x84000002
#define PSCI_CMD_VERSION 0x84000000
#define PSCI_CMD_SYSTEM_OFF 0x84000008
#define PSCI_CMD_SYSTEM_RESET 0x84000009


#ifdef __ASSEMBLER__

#else /* __ASSEMBLER__ */


#endif /* __ASSEMBLER__ */

#endif /* __PSCI_H__ */
