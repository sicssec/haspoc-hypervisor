#ifndef __EE_H__
#define __EE_H__

#define UPCALL_EMERGENCY_ERASE 0xC3000666

#ifdef __ASSEMBLER__

#else /* __ASSEMBLER__ */


extern void __ee_secure_erase(adr_t start, adr_t end);
extern int __ee_verify_zero(adr_t start, adr_t end);
extern void __ee_erase_myself(int callmonitor);

#endif /* !__ASSEMBLER__ */
#endif /* __EE_H__ */
