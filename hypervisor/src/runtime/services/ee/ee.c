/*
 * Emergency erase:
 *
 * this file contains the logic to detect and handler the emergency erase
 * request in a multi-core virtualized enviorment. this turns out to be a
 * rather complex task since we must ensure we don't crash the hypervisor
 * on any cores while erasing its work memory.
 *
 * the actuall erase and verification functions are defined in ee_low.S
 * and can be changed to match your requirements.
 *
 * !!! IMPORTANT NOTE !!!
 *
 * This is just a sample. You must implement your own EE logic
 * according to your requirements.
 */

#include "defs.h"
#include "cman.h"
#include "gman.h"
#include "util.h"
#include "services.h"

#include "ee.h"

#define EE_STATE_NONE 0xABCD6688
#define EE_STATE_ONGOING 0x55AAFF22
static spinlock_t ee_lock;
static uint64_t ee_state = EE_STATE_NONE;


/*
 * clear all memories that are not MEMORY_HV or MEMORY_NONE
 */

static bool ee_clear_memory_others()
{
    int i, type;
    adr_t start, end;
    bool ret = true;

    for(i = 0; ; i++) {
        if(!mman_memory_get_region(i, &type, &start, &end))
            break;

        if(type == MEMORY_RESERVED || type == MEMORY_HV || type == MEMORY_DEVICE) {
            /* can not / should not erase these ... */
        } else {
            dprintf("Erasing memory %d,%d at %X-%X\n", i, type, start, end);

            /* clear it ! */
            __ee_secure_erase(start, end);

            /* verify it */
            if(__ee_verify_zero(start, end))
                ret = false;
        }
    }
    return ret;
}


bool ee_clear_local(struct pcpu *pcpu)
{
    dprintf("Performing emergency erase as secondary\n");

    /* disable interrupts and exceptions */
    cman_interrupts_disable();
    gic_disable(0, 1, 1);

    /* will not return */
    __ee_erase_myself(0);
    return true;
}

bool ee_clear_global(struct pcpu *pcpu)
{
    uint32_t n, targets;
    int i;
    bool ret;

    /* Check if this guest is allowed to do emergency erase */
    if(!pcpu || !pcpu->guest || !(pcpu->guest->flags & GUEST_FLAG_ALLOW_EE)) {
        dprintf("NOTE: Emergency Erase denied to guest\n");
        return false;
    }

    /*
     * if we end up here from multiple cores, we need a system to let
     * only one continue. At first this seams easy enough but remember
     * we are erasing memory at the same time so we cant really trust
     * any variable that holds zero
     *
     * our current way of fixing this is to have a non-zero magic
     * constant and a lock
     */
    spinlock_lock(&ee_lock);
    if(ee_state != EE_STATE_NONE) {
        spinlock_unlock(&ee_lock);
        return ee_clear_local(pcpu);
    } else {
        ee_state = EE_STATE_ONGOING;
        __cache_flush_dcache_range(&ee_state, sizeof(ee_state));
        spinlock_unlock(&ee_lock);
    }

    dprintf("Performing emergency erase as primary\n");

    /* disable local interrupts and exceptions */
    cman_interrupts_disable();

    /*
     * to avoid race conditions with CPU_ON, disable all
     * cores right away. Note that we don't lock it
     * to minimize risk of errors
     */
    for(i = 0; i < ARCH_CORES; i++) {
        pcpu = cman_pcpu_get_by_cpuid(i);
        pcpu->state = PCPU_STATE_DISABLED;
        __cache_flush_dcache_range(pcpu, sizeof(*pcpu));
    }

    /* notify others that EE is being performed */
    targets = -1 & ~(1UL << pcpu->pid);
    gic_send_sgi(GIC_SGI_HYP_EE, targets);

    /* disable GICC and GICH, just in case */
    gic_disable(0, 1, 1);


    /* delete igc & guest memory memory */
    ret = ee_clear_memory_others();

    /* TODO: at this point, erase may have failed but what do we do about it ?*/
    if(!ret) {
        dprintf("Memory erase failed. Continuing...\n");
    }

    /* flush caches */
    __cache_inv_icache();
    __cache_flush_dcache();


    /* will not return */
    __ee_erase_myself(1);
    return ret;
}

/*
 * EE  broadcast interrupts are received at this point.
 * If we are here then some other core is doing global EE
 */
static void ee_icallback(struct pcpu *pcpu, struct registers *r, uint32_t iar)
{
    dprintf("EE broadcast received...\n");
    ee_clear_local(pcpu);
}

/*
 * EE up-call request.
 * If we are here, a guest has just requested an emergency erase
 */
uint32_t ee_ccallback(struct pcpu *pcpu, struct registers *regs, int smc)
{
    if(regs->x[0] == UPCALL_EMERGENCY_ERASE) {
        if(ee_clear_global(pcpu) >= 0) {
            return SMC_RET_SUCCESS;
        }
    }

    return SMC_RET_UNKNOWN;
}


/*
 *
 */
static void ee_message(struct service *s, msg_t *msg)
{
    /* empty, handled via ccall and icall handlers */
}

/*
 * EE initialization
 * register callbacks for the interrupt and up-call drivers
 */

static void ee_service_init(struct service *s, bool global, struct pcpu *pcpu)
{
    int id;

    if(global) {
        dprintf("EE driver global init...\n");
        spinlock_init(&ee_lock, 0);
        cdriver_register(NULL, ee_ccallback);
    } else {
        dprintf("EE driver init on cpu %d...\n", pcpu->pid);

        /* setup broadcast callback */
        id = gicv2_get_irqid(GIC_IRQ_TYPE_SGI, GIC_SGI_HYP_EE);
        gicv2_dist_set_enable(_gicd, id, 1);
        gicv2_dist_set_priority(_gicd, id, 0x00); /* Note the priority! */
        gic_pirq_callback_set(pcpu, id, ee_icallback);
    }
}

static struct service ee = {
    .ops_init = ee_service_init,
    .ops_message = ee_message,
};

SERVICE_DECLARE("ee", _ee, &ee, 0);
