#ifndef __SERVICES_H__
#define __SERVICES_H__

#include "defs.h"
#include "util.h"
#include "modules.h"

#ifndef __ASSEMBLER__

#define SERVICE_FLAGS_TYPE_PRIMARY 0
#define SERVICE_FLAGS_TYPE_SHARED 1
#define SERVICE_FLAGS_TYPE_ARRAY 2
#define SERVICE_FLAGS_TYPE_MASK 3


struct regs;
struct guest;

typedef union msg {
    uint64_t msg64[2];
    uint32_t msg32[4];
    struct {
        uint32_t op : 8;
        uint32_t param0 : 24;
        uint32_t params[3];
    };
} msg_t;


/* service */
struct service {
    /* short cut for service->module-> dito */
    char *name;
    uint32_t flags;

    void (*ops_init)(struct service *s, bool global, struct pcpu *pcpu);
    bool (*ops_message)(struct service *s, msg_t *msg);

    struct module *module;
};

typedef void (* service_init)(struct service *s);


/* service module support */
#define SERVICE_DECLARE(name_,id_, ptr_, flags_)                        \
    MODULE_DECLARE(name_,service_##id_, ptr_, MODULE_TYPE_SERVICE, flags_)


extern struct service *services_find(const char *name);
extern bool services_message(const char *name, msg_t *msg,
                             bool fatal_if_not_found, bool fatal_if_failed);

extern void services_init(bool global, struct pcpu *pcpu);



#endif /* ! __ASSEMBLER__ */


#endif /* __SERVICES_H__ */
