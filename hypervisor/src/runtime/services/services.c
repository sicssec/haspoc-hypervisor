
#include "defs.h"
#include "util.h"

#include "cman.h"
#include "cman_private.h"

#include "services.h"

/*
 * service array:
 *
 * we can get the services directly from modules but
 * this might be easier to follow and may be a tiny bit faster too
 */
#define SERVICES_COUNT 12
static int services_cnt = 0;
static struct service *services[SERVICES_COUNT];


static void services_add(struct service *s)
{
    assert_lt("Too many services", services_cnt, SERVICES_COUNT);
    services[services_cnt++] = s;
}

struct service *services_find(const char *name)
{
    int i;
    for(i = 0; i < services_cnt; i++)
        if(!strcmp(name, services[i]->name))
            return services[i];

    return NULL;
}

bool services_message(const char *name, msg_t *msg,
                      bool fatal_if_not_found,
                      bool fatal_if_failed)
{
    struct service *s;
    bool ret;
    s = services_find(name);

    if(!s) {
        if(fatal_if_not_found)
            fatal("service not found");
        return false;
    }

    ret = s->ops_message ? s->ops_message(s, msg) : false;

    if(!ret && fatal_if_failed)
        fatal("service message failed");
    return ret;
}


void services_init(bool global, struct pcpu *pcpu)
{
    int i;

    struct module *m;
    struct service *s;

    struct dt_foreach fe;
    struct dt_block *node;

    if(global) {
        /* list all available services */
        modules_dump("Available services", MODULE_TYPE_SERVICE);

        /* create the services mentioned in the config */
        i = 0;
        while(true) {
            m = modules_itr(MODULE_TYPE_SERVICE, &i);
            if(!m)
                break;

            s = (struct service *) m->ptr;
            s->module = m;
            s->flags = m->flags;
            s->name = m->name;

            services_add(s);
        }
    }

    /* initialize used services */
    for(i = 0; i < services_cnt; i++) {
        if(services[i]->ops_init)
            services[i]->ops_init(services[i], global, pcpu);
    }
}
