
#include "defs.h"

#include "util.h"
#include "modules.h"

extern struct module __modules_start, __modules_end;

struct module *modules_find(const char *name, uint32_t type)
{
    struct module *c, *e;

    for(c = &__modules_start, e = &__modules_end; c < e; c++) {
        if(c->type == type && !strcmp(name, c->name))
            return c;
     }
     return NULL;
 }


struct module *modules_itr(uint32_t type, int *index)
{
    int i;
    struct module *c, *e;

    i = *index;
    for(c = &__modules_start + i, e = &__modules_end; c < e; c++) {
        i++;
        if(c->type == type) {
            *index = i;
            return c;
        }
     }
     return NULL;
 }

void modules_dump(const char *msg, uint32_t type)
{
    struct module *c, *e;

    dprintf("%s:\n", msg);
    for(c = &__modules_start, e = &__modules_end; c < e; c++) {
        if(c->type == type)
            dprintf("\t%x: %x %s\n", c, c->flags, c->name);
     }

}
