#ifndef __MODULES_H__
#define __MODULES_H__

#ifndef __ASSEMBLER__


/* modules */
struct module {

#define MODULE_TYPE_DRIVER 0
#define MODULE_TYPE_SERVICE 1
    uint32_t type;
    uint32_t flags;

    char *name;
    void *ptr;
};



#define M0_(a,b)  a##b
#define M1_(a) (M0_(moduledata_, a)
#define M2_ M1_(__COUNTER__)


#define MODULE_DECLARE(name_,id_, ptr_,type_, flags_)                   \
    static const struct module moduledata_##id_ __section(".modules") __used =   \
    { .name = name_, .ptr = (void *) ptr_, .type = type_, .flags = flags_ };


extern struct module *modules_find(const char *name, uint32_t type);
extern struct module *modules_itr(uint32_t type, int *index);

extern void modules_dump(const char *msg, uint32_t type);

#endif

#endif /* __MODULES_H__ */
