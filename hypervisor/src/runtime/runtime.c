
#include "defs.h"
#include "runtime.h"

void runtime_init(bool global, struct pcpu *pcpu)
{
    services_init(global, pcpu);
    mdrivers_init(global, pcpu);
}
