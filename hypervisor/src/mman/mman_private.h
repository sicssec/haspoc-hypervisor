#ifndef __MMAN_PRIVATE_H__
#define __MMAN_PRIVATE_H__

#ifdef __ASSEMBLER__

#else


/**
 * @name mman_bank_num
 * @type union
 * @description the type field specified in the config is actually
 *              a type+index pair with the
 * @module memory manager
 */
union mman_bank_num {
    uint32_t all;
    struct {
        uint32_t type : 4;

#define BANK_INDEX_ANY 0xFF /* used as wildcard when searching for memory */
        uint32_t index : 8;

        /* don't clean this memory upon allocation! */
        uint32_t dont_zeroize : 1;

    };
};


extern void mman_memory_init(uint32_t *cfg, int count);

#endif /* !_ ASSEMBLER__ */

#endif /* !__MMAN_PRIVATE_H__ */
