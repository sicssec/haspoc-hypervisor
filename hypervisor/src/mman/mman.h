#ifndef __MMAN_H__
#define __MMAN_H__

/**
 * @name memory manager
 * @type module
 * @description this is the memory manager module, responsible for
 * any memory related tasks
 */

#include "cman.h"

/*
 * the [1:0] type field in a table
 */
#define ENTRY_TYPE_MASK 3
#define ENTRY_TYPE_INVALID1 0
#define ENTRY_TYPE_INVALID2 2
#define ENTRY_TYPE_TABLE_L12 3
#define ENTRY_TYPE_PAGE_L3 3
#define ENTRY_TYPE_BLOCK_L12 1


/* Mask for next-level table address, 4KB granule. Masked-in bits are [47:12].
 * See ARM DDI 0487A.g, pg D4-1696
 */
#define NL_TABLE_ADDR_MASK 0x0000FFFFFFFFF000

/* Mask for memory output address, 4KB granule, level 1 descriptor.
 * Masked-in bits are [47:30]. See ARM DDI 0487A.g, pg D4-1696.
 */
#define MEM_OA_L1_ADDR_MASK 0x0000FFFFC0000000

/* Mask for memory output address, 4KB granule, level 2 descriptor.
 * Masked-in bits are [47:21]. See ARM DDI 0487A.g, pg D4-1696.
 */
#define MEM_OA_L2_ADDR_MASK 0x0000FFFFFFE00000

/* Mask for memory output address, 4KB granule, level 3 descriptor.
 * Masked-in bits are [47:12]. See ARM DDI 0487A.g, pg D4-1698.
 */
#define MEM_OA_L3_ADDR_MASK 0x0000FFFFFFFFF000


/*
 * Stage 2 attributes for memory block and page descriptors.
 * Bits not shown are ignored.
 * --------------------------------------------------
 * 54  XN, execute-never
 * 53   0
 * 52  contiguous
 * 11   0
 * 10  AF, access flag (generates trap if region accessed when 0)
 * 9:8 SH, shareability:
 *          00 Non-shareable
 *          01 UNPREDICTABLE
 *          10 Outer Shareable (coherency for typically all agents between clusters)
 *          11 Inner Shareable (coherency for typically all agents within a cluster)
 * 7:6 S2AP, stage 2 access permissions
 *          00 None
 *          01 Read-only
 *          10 Write-only
 *          11 Read/write
 * 5:2 MemAttr, memory attribute regions
 *          00_00 Device-nGnRnE (G=Gathering, R=Reordering, E=Early write)
 *          00_01 Device-nGnRE
 *          00_10 Device-nGRE
 *          00_11 Device-GRE
 *          01_xx Normal Outer Non-cacheable
 *          10_xx Normal Outer Write-Through Cacheable
 *          11_xx Normal Outer Write-Back Cacheable
 *       where xx is inner cachability flag and yy != 00:
 *          yy_00 UNPREDICTABLE
 *          yy_01 Inner Non-cacheable
 *          yy_10 Inner Write-Through Cacheable
 *          yy_11 Inner Write-Back Cacheable
 */

 /*
  * Stage 1 EL2, EL3 attributes for memory block and page descriptors.
  * Bits not shown are identical to the stage 2 attributes, or ignored.
  * --------------------------------------------------
  * 54  see stage 2
  * 52  see stage 2
  * 11  nG, RES0. TLB entry applicable to all ASID values?
  * 10  see stage 2
  * 9:8 see stage 2
  * 7:6 AP[2:1], AP[2] determines data access permissions. AP[1] is SBO.
  * 5   NS, RES1 at EL2
  * 4:2 AttrIndx[2:0]. See D4-1712 in ARM DDI 0487A.g
  */

/* Stage 2 table entry attributes. All bits are res0 or constant.
 * See ARM DDI 0487A.g, pg D4-1696
 */
#define ATTR_S2_TABLE 0x03ULL

#if 0
/* Descriptor type Block, for level 0-2 and Page for level 3*/
#define ATTR_BLOCK 0x01ULL
#define ATTR_PAGE 0x03ULL
#endif

/* Memory shareability (SH) bits */
#define ATTR_SH_NS 0x00ULL
#define ATTR_SH_OS 0x200ULL
#define ATTR_SH_IS 0x300ULL

/* Stage 2 normal memory (block or page) entry attributes.
 * [54]=0, [52]=0, [10]=1, [9:8]=XX, [7:6]=11, [5:2]=1111 [1:0]=YY
 *
 * Execute-never could be set if we could distinguish code/data mem usage
 * Since we treat all output addresses the same, we cannot utilize the contiguous optimization
 * XX and YY are set separately. Here they are all 0
 */
#define ATTR_S2_BP_MEM 0x4FCULL

/* EL2, EL3 stage 1 normal memory (block or page) entry attributes.
 * [54]=0, [52]=0, [10]=1, [9:8]=XX, [7:6]=01, [5]=1, [4:2]=000 [1:0]=YY
 *
 * Execute-never could be set if we could distinguish code/data/device mem usage
 * Since we treat all output addresses the same, we cannot utilize the contiguous optimization
 * [4:2] indexes 0 in MAIR_EL2,3
 * XX and YY are set separately. Here they are all 0
 */
#define ATTR_EL23_S1_BP_MEM 0x460ULL

/* EL2, EL3 stage 1 device memory (block or page) entry attributes.
 * [54]=1, [52]=0, [10]=1, [9:8]=XX, [7:6]=01, [5]=1, [4:2]=001 [1:0]=YY
 *
 * Execute-never is set for device memory
 * Since we treat all output addresses the same, we cannot utilize the contiguous optimization
 * [4:2] indexes 1 in MAIR_EL2,3
 * XX and YY are set separately. Here they are all 0
 */
#define ATTR_EL23_S1_BP_DEV 0x40000000000464ULL

/* Stage 2 device memory (page) entry attributes.
 * [54]=1, [52]=0, [10]=1, [9:8]=XX, [7:6]=11, [5:2]=0000 [1:0]=YY
 *
 * Since we treat all output addresses the same, we cannot utilize the contiguous optimization
 * Since core/cluster sharing is unknown at this level we must use outer shareability
 * XX and YY are set separately. Here they are all 0
 */
#define ATTR_S2_PAGE_DEV 0x400000000004C0ULL

/* Stage 2 normal memory (page) entry attributes for shared memory.
 * [54]=1, [52]=0, [10]=1, [9:8]=XX, [7:6]=AP, [5:2]=1111 [1:0]=YY
 *
 * Since we treat all output addresses the same, we cannot utilize the contiguous optimization
 * Since core/cluster sharing is unknown at this level we must use outer shareability
 * XX and YY are set separately. Here they are all 0
 */
#define ATTR_SHARED_WRITE 0x400000000004BCULL
#define ATTR_SHARED_READ 0x4000000000047CULL
#define ATTR_SHARED_READ_WRITE 0x400000000004FCULL

/*
 * MMU page tables
 */
#define MMAN_PT_SIZE ((adr_t)PAGE_SIZE)
#define MMAN_PT_ENTRIES ((size_t)512)

struct pagetable {
    uint64_t entry[MMAN_PT_ENTRIES];
};

static inline uint64_t mman_get_pt_index(int level, adr_t adr)
{
    return (adr >> (39 - level * 9)) & (MMAN_PT_ENTRIES - 1);
}

static inline adr_t mman_get_pt_mask(int level)
{
    return ((adr_t)(MMAN_PT_ENTRIES - 1)) << (39 - level * 9);
}

static inline uint64_t mmu_pt_get(struct pagetable *pt, int index)
{
    return pt->entry[index];
}
static inline void mmu_pt_attach_table(struct pagetable *pt, int index,
    struct pagetable *sub)
{
    pt->entry[index] = ATTR_S2_TABLE | ((uint64_t) sub & NL_TABLE_ADDR_MASK);
}

static inline void mmu_pt_add_map(struct pagetable *pt, int index,
    adr_t padr, uint64_t flags, uint64_t mask)
{
    pt->entry[index] = flags | ((uint64_t) padr & mask);
}

/*
 * memory region operations
 */

/* two memory regions overlap? */
static inline bool regions_overlap(adr_t s0, adr_t e0, adr_t s1, adr_t e1)
{
    return s1 < e0 && s0 < e1; /* overlapp exclusive! */
}

/* second memory region inside the first one?? */
static inline bool region_inside(adr_t s0, adr_t e0, adr_t s1, adr_t e1)
{
    return s0 <= s1 && e1 <= e0;
}


/**
 * @name mregion
 * @type struct
 * @module memory manager
 * @descrption defines a memory region with a start point and an end
 */
struct mregion {
    adr_t start;
    adr_t end;
};

static inline bool mregion_overlap(const struct mregion *mr, adr_t adr, adr_t end)
{
    return regions_overlap(mr->start, mr->end, adr, end);
}

static inline bool mregion_check_sanity(const struct mregion *mr, adr_t adr, int sas)
{
    const uint32_t bytes = 1UL << sas;
    return mregion_overlap(mr, adr, adr + bytes) &&
        !(adr & (bytes - 1)); /* access not aligned to write size */
}

/**
 * @name mman_memory_type
 * @type enum
 * @description types of available memories
 * @module memory manager
 */
enum mman_memory_type {
    MEMORY_RESERVED = 0,
    MEMORY_DEVICE,
    MEMORY_HV,
    MEMORY_IGC,
    MEMORY_GUEST,
    /* not a type, hold the number of available memories */
    MEMORY_COUNT
};

extern void mman_memory_dump();
extern bool mman_memory_get(uint32_t bank, adr_t *start, size_t size, int alignment);
extern bool mman_memory_get_region(int index, uint32_t *bank, adr_t *start, adr_t *end);
extern bool mman_memory_overlap(int mask, adr_t start, adr_t end, bool initial);
extern bool mman_memory_inside(int mask, adr_t start, adr_t end, bool initial);

extern enum mman_memory_type mman_bank_type(uint32_t bank);

/* payload regions is nice to have, to see if it doesnt overlap with heaps */
extern struct mregion payload_region;


extern void mman_init_early();


#endif /* !__MMAN_H__ */
