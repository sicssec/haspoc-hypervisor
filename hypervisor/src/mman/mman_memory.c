/*
 * hypervisor memory heaps
 */


#include "defs.h"
#include "util.h"
#include "cman.h"

#include "mman.h"
#include "mman_private.h"


#define MMAN_REGION_COUNT 16

struct mman_memory_region {
    struct mregion curr;
    struct mregion initial;
    union mman_bank_num bank;
};

struct mman_memory_heap {
    struct mman_memory_region regs[MMAN_REGION_COUNT];
    spinlock_t lock;
    uint32_t count;
};

static struct mman_memory_heap heap;

/*
 * helper functions for mman
 */

enum mman_memory_type mman_bank_type(uint32_t bank)
{
    union mman_bank_num bn = { .all = bank };
    return (enum mman_memory_type) bn.type;
}


/* a region type is valid */
static bool mman_bank_valid(uint32_t bank)
{
    union mman_bank_num bn = { .all = bank };
    return bn.type >= 0 && bn.type  < MEMORY_COUNT;
}

/* returns true if the regions matches the bank specification */
static bool mman_bank_match(struct mman_memory_region *reg, uint32_t bank)
{
    union mman_bank_num bn = { .all = bank };

    return (reg->bank.index == bn.index || bn.index == BANK_INDEX_ANY) &&
        (reg->bank.type == bn.type);
}

/**
 * @name mman_type_allocatable
 * @type function
 * @module mman
 * @description check if a region type can be allocated
 * @return true if type is a guest memory
 */
static inline bool mman_type_allocatable(int bank)
{
    union mman_bank_num bn = { .all = bank };

    return bn.type != MEMORY_RESERVED && bn.type != MEMORY_DEVICE;
}


/**
 * @name mman_memory_overlap
 * @type function
 * @module mman
 * @description check if a region overlaps with any part of the heap
 * @param1 mask for regions accepted
 * @param2 start of region
 * @param3 end of region
 * @param4 use initial  or current range
 * @return true if there is an overlap
 */
bool mman_memory_overlap(int mask, adr_t start, adr_t end, bool initial)
{
    int i;
    struct mman_memory_region *reg;
    adr_t s, e;

    for(i = 0; i < heap.count; i++) {
        reg = &heap.regs[i];
        if(mask & (1UL << reg->bank.type)) {
            s = initial ? reg->initial.start : reg->curr.start;
            e = initial ? reg->initial.end : reg->curr.end;
            if(regions_overlap(s, e, start, end))
                return true;
        }
    }
    return false;
}


/**
 * @name mman_memory_inside
 * @type function
 * @module mman
 * @description check if a region overlaps with any part of the heap
 * @param0 mask for regions accepted
 * @param1 start of region
 * @param2 end of region
 * @param3 use initial  or current range
 * @return true if there is an overlap
 */
bool mman_memory_inside(int mask, adr_t start, adr_t end, bool initial)
{
    int i;
    struct mman_memory_region *reg;
    adr_t s, e;

    for(i = 0; i < heap.count; i++) {
        reg = &heap.regs[i];
        if(mask & (1UL << reg->bank.type)) {
            s = initial ? reg->initial.start : reg->curr.start;
            e = initial ? reg->initial.end : reg->curr.end;
            if(region_inside(s, e, start, end))
                return true;
        }
    }
    return false;
}


bool mman_memory_get_region(int index, uint32_t *bank, adr_t *start, adr_t *end)
{

    if(index < 0 || index >= heap.count)
        return false;

    *bank = heap.regs[index].bank.all;
    *start = heap.regs[index].initial.start;
    *end = heap.regs[index].initial.end;

    return true;
}

bool mman_memory_get(uint32_t bank, adr_t *start, size_t size, int alignment)
{
    struct mman_memory_region *reg;
    int i;

    adr_t s1, s2, e1, e2, u1, u2;
    bool can1, can2, found;

    /* sanity check */
    if(size == 0 || !mman_bank_valid(bank))
        return false;

    /* are we allowed to allocate memory of this type*/
    if(!mman_type_allocatable(bank))
        return false;

    /* force alignment to be large enough */
    if(alignment < 16)
        alignment = 16;

    if(!util_is_pot(alignment)) {
        fatal("memory allocation not a power of two");
        return false;
    }


    spinlock_lock(&heap.lock);


    for(found = false, i = 0; !found && i < heap.count; i++) {
        reg = &heap.regs[i];
        if(!mman_bank_match(reg, bank))
            continue;

        /* can we allocate from the start of this? */
        s1 = ALIGN_UP(reg->curr.start, alignment);
        e1 = s1 + size;
        u1 = e1 - reg->curr.start;
        can1 = e1 <= reg->curr.end;

        /* can we allocate from the end of this? */
        s2 = ALIGN_DOWN(reg->curr.end - size, alignment);
        e2 = s2 + size;
        u2 = reg->curr.end - s2;
        can2 = s2 >= reg->curr.start;

        if(can1 && (!can2 || u1 <= u2)) {
            found = true;
            *start = s1;
            reg->curr.start = e1;
        } else if(can1) {
            found = true;
            *start = s2;
            reg->curr.end = s2;
        }
    }

    spinlock_unlock(&heap.lock);

    if(found) {
        /* verify that the memory is aligned as requested */
        assert_eq("Allocator alignment sanity check", 0, *start & (alignment - 1));

        /* verify the we really had this region and removed it from free list */
        assert("Allocator removal sanity check: had",
               mman_memory_overlap(-1, *start, *start + size, true));

        assert("Allocator removal sanity check: removed",
               ! mman_memory_overlap(-1, *start, *start + size, false));

        /* make sure memory is cleared before we hand it over to anyone */
        if(! reg->bank.dont_zeroize)
            __memset64(*start, 0, size);

#if DEBUG > 7
        dprintf("allocated bank=%x reg=%d size=%x align=%x PTR=%X\n",
                reg->bank.all, i, size, alignment, *start);
#endif
    }

    return found;
}

static void mman_memory_add(uint32_t bank, adr_t start, adr_t end)
{
    struct mman_memory_region *reg;
    union mman_bank_num bn;

    bn.all = bank;

    /* sanity checks */
    assert_lt("Configuration memory regions are too many",
             heap.count, MMAN_REGION_COUNT);

    assert("Unknown memory type in configuration", mman_bank_valid(bank));

    assert("Only guest memory can be dont_zero",
           !bn.dont_zeroize || bn.type == MEMORY_GUEST);

    assert("Configuration memory regions overlap",
           !mman_memory_overlap(-1, start, end, true));


    reg = &heap.regs[heap.count];
    reg->bank = bn;
    reg->curr.start = reg->initial.start = start;
    reg->curr.end = reg->initial.end = end;
    heap.count++;
}


void mman_memory_dump()
{
    int i;
    struct mman_memory_region *reg;

    dprintf("Available memory [#,type,idx A? G? Z? region current (initial) ]:\n");
    for(i = 0; i < heap.count; i++) {
        reg = &heap.regs[i];
        dprintf("%d,%d,%d  %c %c %c  %X-%X (%x-%x)\n",
                i, reg->bank.type, reg->bank.index,
                mman_type_allocatable(reg->bank.all) ? 'Y' : 'N',
                (reg->bank.type == MEMORY_GUEST) ? 'Y' : 'N',
                reg->bank.dont_zeroize ? 'N' : 'Y',
                reg->curr.start, reg->curr.end,
                reg->initial.start, reg->initial.end);
    }
}

static void mman_memory_heap_init()
{
    memset(&heap, 0, sizeof(heap));
    spinlock_init(& heap.lock, 0);
    heap.count = 0;
}

/**
 * @name mman_memory_init
 * @type function
 * @module memory manager
 * @description initialize the memory allocators
 * @param0 memory definition list from devicetree
 * @param1 number of entries in the list
 * @return nothing
 */
void mman_memory_init(uint32_t *cfg, int count)
{
    int i, len, type, mask;
    adr_t start, end;

    /* initialize the heap and and the defined regions to it */
    mman_memory_heap_init();

    for(int i = 0; i < count; i ++) {
        type = dtend(cfg[0]);
        start = (((uint64_t)dtend(cfg[1])) << 32) + dtend(cfg[2]);
        end = (((uint64_t)dtend(cfg[3])) << 32) + dtend(cfg[4]);
        cfg += 5;
        mman_memory_add(type, start, end);
    }

    /* sanity checks to see if anything is overlapping default regions */
    mask = ~(1 << MEMORY_RESERVED); /* anything but RESERVED */

    if(mman_memory_overlap(mask, HYP_RAM_START, HYP_RAM_END, true))
        fatal("memory overlap HYP_RAM_*");

    if(mman_memory_overlap(mask, HYP_ROM_START, HYP_ROM_END, true))
        fatal("memory overlap HYP_ROM_*");

    if(mman_memory_overlap(mask, payload_region.start, payload_region.end, true))
        fatal("memory overlap payload");

    /* dump the result for the user to see */
    mman_memory_dump();

}
