#include "defs.h"
#include "mman.h"
#include "mman_private.h"

#include "cman.h"
#include "util.h"

/**
 * @name payload_region
 * @type variable
 * @module memory manager
 * @description hold the memory region for the paylaod
 */
struct mregion payload_region;

/**
 * @name mman_init
 * @type function
 * @module memory manager
 * @description initialize the memory manager module
 */
void mman_init_early()
{
    struct dt_block mem;
    int i, len;
    uint32_t type, *data;
    adr_t start, end;

    /* get memory from the configuration */
    config_get_prop(NULL, &mem, "regions", 0);
    if( mem.data_len < 1 || (mem.data_len % (sizeof(uint32_t) * 5)) != 0)
        fatal("invalid config memory property");

    /* prepare memory allocators */
    mman_memory_init(& mem.data.num[0], mem.data_len / (5 * sizeof(uint32_t)));
}
