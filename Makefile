
# all is here to ensure it is the first make target, no matter what we include
all:
	# nothing

# build configuration is defined in config.
-include config

# if config is missing these will have a temporary config
TARGET ?= fvp
GUEST ?= linux
TZ ?= mini
CORES ?= 4
TEST ?= std

#
BASE = .
DIRS = tools hypervisor payload trustzone

##
include $(BASE)/common/make/filenames.inc
include $(BASE)/common/target/$(TARGET)/arch_build.mk


#####################################################################

all: $(BUILD) sanity tools
	set -e ; for d in $(DIRS); do make -C $$d; done
	ls -l $(NAME_HV)*
#


sim: all $(GUEST_BIN) $(GUEST_DTB)
compile: all

run: all
	make sim

show:
	make -C hypervisor show
#####################################################################

fip: all
	make -C trustzone/atf fip

test: all
	make -C tools/tester

cscope:
	make -C hypervisor cscope
	make -C payload cscope
	make -C trustzone cscope

setup: $(BUILD) sanity
	make -C external setup
	make -C tools setup
	make -C trustzone setup
	make -C payload setup

guest: sanity tools
	@echo Forcing guest build:
	make -C payload guest

.PHONY: tools	# this forces tools directory to be built
tools: sanity $(BUILD)
	make -C tools

#####################################################################

.PHONY: sanity
sanity: config build

help:
	@echo "Valid targets are:"
	@echo "	all, setup, guest, compile, fip"
	@echo "	run, sim, debug"
	@echo "	clean"
	@echo "Important variables are:"
	@echo "	GUEST  -> linux, mini, ..."
	@echo "	TARGET -> fvp, juno, ..."


config:
	echo "TARGET ?= fvp" > config
	echo "GUEST ?= linux" >> config
	echo 'ifeq ($$(TARGET),hikey)' >> config
	echo "    TZ ?= sb" >> config
	echo "else" >> config
	echo "    TZ ?= mini" >> config
	echo "endif" >> config
	echo "CORES ?= 4" >> config
	echo "TEST ?= std" >> config


$(BUILD):
	mkdir -p $(BUILD)

#

clean:
	set -e ; for d in $(DIRS); do make -C $$d $(OPT) clean; done
	rm -rf $(BUILD)
