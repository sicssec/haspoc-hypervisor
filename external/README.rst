
Files here are downloaded from elsewhere. Unfortunately we cannot use git
submodules for most repos in this folder, hence files are downloaded manually
via scripts.

NOTE 1:
To minimize network load, the files are first downloaded to ~/.gitcache/HASPOC ,
which is used as a local git cache for files in here.


Note 2:
Do not refer to these folders directly if you need to use them in your scripts.
Instead, include $(BASE)/external/external.inc and use the variables $(xxx_DIR)
where xxx is the component you want to access.
