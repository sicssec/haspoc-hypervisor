# -*- Makefile -*-

#
# This file contains version numbers or release and commit tags
# for external software.
#
# It has been moved out here to allow it to be included in other files
#

GITCACHE = $(HOME)/.gitcache/HASPOC

# Linux
LINUX_TAG = v4.4
LINUX_DIR = $(BASE)/external/linux_$(LINUX_TAG)
LINUX_GIT_CACHE = $(GITCACHE)/linux_$(LINUX_TAG)
LINUX_GIT = git://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git

# LK
LK_TAG = d22f805
LK_PATCHSET=$(BASE)/external/patches/lk_minimal_fvp
LK_DIR = $(BASE)/external/lk_patched_$(LK_TAG)
LK_GIT = https://github.com/littlekernel/lk.git

# OPTEE
OPTEE_DIR = $(BASE)/external/optee
OPTEE_TAG = medpro
OPTEE_GIT = https://bitbucket.org/martin_hovang/optee_os.git

# dev might want to use something else...
ifeq ($(TARGET),hikey)
    LINUX_TAG = v4.4.0-mainline-rebase-96boards
    LINUX_GIT = https://github.com/eriknore/linux.git
    LINUX_GIT_CACHE = $(GITCACHE)/linux_hikey_$(LINUX_TAG)
    LINUX_DIR = $(BASE)/external/linux_HIKEY_$(LINUX_TAG)
    LINUX_CONFIG = hikey_v4.4
else ifeq ($(TARGET),tx1)
    LINUX_TAG = master
    LINUX_GIT = git://git.kernel.org/pub/scm/linux/kernel/git/next/linux-next.git
    LINUX_GIT_CACHE = $(GITCACHE)/linux_tx1_next
    LINUX_DIR = $(BASE)/external/linux_TX1_next
    LINUX_CONFIG = tx1_v4.4
endif

ifeq ($(TARGET):$(GUEST),hikey:android)
    LINUX_TAG = aosp_4.1
    LINUX_GIT = https://github.com/eriknore/hikey_linaro_aosp_kernel
    LINUX_GIT_CACHE = $(GITCACHE)/linux_hikey_$(LINUX_TAG)
    LINUX_DIR = $(BASE)/external/linux_HIKEY_$(LINUX_TAG)
endif

ifeq ($(TARGET):$(GUEST),hikey:debian)
    LINUX_TAG = hikey
    LINUX_GIT = https://github.com/96boards/linux.git
    LINUX_GIT_CACHE = $(GITCACHE)/linux_hikey_debian
    LINUX_DIR = $(BASE)/external/linux_HIKEY_debian
    LINUX_PATCHSET = $(BASE)/external/patches/linux_debian
endif

ifeq ($(TARGET):$(GUEST),hikey:ubuntu_server)
    LINUX_TAG = v2.1-rc1
    LINUX_GIT = https://github.com/open-estuary/kernel.git
    LINUX_GIT_CACHE = $(GITCACHE)/linux_hikey_$(GUEST)
    LINUX_DIR = $(BASE)/external/linux_HIKEY_$(GUEST)
endif




# Buildroot
BUILDROOT_TAG = 2015.08
BUILDROOT_GIT = git://git.buildroot.net/buildroot
BUILDROOT_GIT_CACHE = $(GITCACHE)/buildroot_$(BUILDROOT_TAG)
BUILDROOT_DIR = $(BASE)/external/buildroot_$(BUILDROOT_TAG)

# ATF
# right now, hikey needs a special ATF repo
ifeq ($(TARGET),hikey)
    ATF_TAG = hikey
    ATF_GIT = https://github.com/96boards/arm-trusted-firmware
    ATF_GIT_CACHE = $(GITCACHE)/atf_hikey_$(ATF_TAG)
    ATF_DIR = $(BASE)/external/arm-trusted-firmware-HIKEY_$(ATF_TAG)
    ATF_PATCHSET = $(BASE)/external/patches/atf_hikey
else
    ATF_TAG = v1.1
    ATF_GIT = https://github.com/ARM-software/arm-trusted-firmware.git
    ATF_GIT_CACHE = $(GITCACHE)/atf_arm_$(ATF_TAG)
    ATF_DIR = $(BASE)/external/arm-trusted-firmware_$(ATF_TAG)
endif

# haspoc binaries
HASPOCBIN_DIR = $(BASE)/external/haspoc-binaries
HASPOCBIN_VERSION = 160623_1456

# haspoc tester
HASPOCTESTER_DIR = $(BASE)/external/haspoc-hikey-tester
